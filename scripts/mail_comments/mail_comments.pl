#!/usr/bin/perl
# Copyright (c) 2005, Rutgers, The State University of New Jersey
#    This file is part of ruQueue.
#
#    ruQueue is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    ruQueue is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with ruQueue; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

use strict;
use warnings;
use DBI;
use Mail::MboxParser;
use Mail::Sendmail;

require '/scripts/mail_comments/base64.pl';

# The following command should be run in mysql, replacing password
# with the password you would like the user to use.  Please examine
# the function "get_the_password" for more information.

# grant select, insert, update on ruqueue.* to 'perl-mail' identified by 'password';
print "Processing mail...  \n";
# -----------------------------------------------------------
# GLOBAL VARIABLES 

my $log = "/tmp/comment";
my $cache = "/tmp/cache";
my $contact = "ruqueue\@localhost";
my $server = "localhost"; # mysql server
my $database = "ruqueue"; # mysql database
my $db = mysql_connect(); # connects to the specified database
use vars qw($log $cache $db $contact $server $database);

# Mail::MboxParser OBJECTS/VARS

my $parseropts = {
        enable_cache    => 1,
        enable_grep     => 1,
        cache_file_name => $cache, 
    };

my $mb = Mail::MboxParser->new($log, 
			       decode     => 'ALL',
			       parseropts => $parseropts);

# -----------------------------------------------------------
# FUNCTIONS 

sub mysql_connect {
    my $pw = get_the_password();
    if (!$pw) {
        print "Error:  Couldn't read MySQL password\n";
	exit;
    }
    else {
	my $dbif = "DBI:mysql:$database:$server";
	my $user = "perl-mail";
	my $db = DBI->connect($dbif, $user, $pw);
      if (!$db) {
          fatal("Unable to connect to MySQL DB");
      }
      else { 
	  return $db;
      }
  }
}

# -----------------

sub get_the_password {
    my $pw = "password";
    #The option exists to retrieve the password from a file.  See below.
    #my $file = '/scripts/mail_comments/.password';
    #my $open = open(FILE, "<$file");
    #my $pw = join "", <FILE>;
    #close FILE;
    return $pw;
}

# -----------------

sub fatal {
    my ($msg) = @_;
    ## Error message which emails $contact and stops program 
    print "\nFatal Error:  \n$msg\n\n";

    $msg = "The following error occured:  \n\n$msg";
    my %mail = ( To      => $contact,
		 From    => $contact, 
		 Subject => 'ruQueue Mail Comment Error',
		 Message => $msg
		 );
    my $notify = sendmail(%mail);
    if ($notify) {
	print "$contact has been sent email about this. \n\n";
    }
    else {
	print "$contact has NOT been sent email about this (mail error).\n\n";
    }
    $db->disconnect();
    exit;
}

# -----------------

sub get_mail_staff {
    my ($from) = @_;
    ## returns username based on the email address
    ## if not found returns email address 
  
    my $q = "SELECT username FROM staff WHERE email like '%$from%'";
    my $query = $db->prepare($q);
    my $done = $query->execute;
    if (!$done) {
	return $from;
    }
    my $num_rows = $query->rows;
    if (($num_rows == 0) or ($num_rows > 1)) {
	return $from;
    }
    else {
	my @array = $query->fetchrow_array;
	my ($username) = @array;
	return $username;
    }
    $query->finish;
}

# -----------------

sub status_of_ticket {
    my ($ticket_id) = @_;
    # returns the status of ticket number $ticket_id
    my $q = "select current_status from ticket where id='$ticket_id'";
    my $query = $db->prepare($q);
    my $done = $query->execute;
    if (!$done) {
	return 0;
    }
    my $num_rows = $query->rows;
    if ($num_rows == 0) {
	print "\nUnable to get ticket status for ticket: $ticket_id\n";
	return 0;
    }
    else {
        my @array = $query->fetchrow_array;
        my ($status) = @array;
        return $status;
    }
    $query->finish;
}

# -----------------

sub can_comment {
    my ($ticket_id) = @_;
    ## return t or nil if $ticket_id can be commented on via email 

    my $q = "select mail_comments from queue, ticket where queue=q_name and ticket.id='$ticket_id'";
    my $query = $db->prepare($q);
    my $done = $query->execute;
    if (!$done) {
	print "\n\nMySQL Error:\n\n $q\n\n";
	reject($ticket_id, $contact, "Query Failed:\n\n$q", "internal");
	return 0;
    }
    my $num_rows = $query->rows;
    if ($num_rows == 0) {
	return 0;
    }
    else {
        my @array = $query->fetchrow_array;
        my ($mail_comments) = @array;
        return $mail_comments;
    }
    $query->finish;
}

# -----------------

sub mail_comment {
    my ($ticket_id, $staff, $body, $from) = @_;
    ## adds a comment to ticket if ticket will accept it 

    if (!can_comment($ticket_id)) {
	reject($ticket_id, $from, $body, "queue");
    }
    else {
	$body =~ s/\'/\\'/g;  # ' ## for emacs!
	$body =~ s/\"/\\"/g;  # " ## for emacs!
	$staff = get_mail_staff($staff);
	my $status = status_of_ticket($ticket_id);
        if (!$status) {
          print "\n\nMySQL Error:\n\n unable to get status\n\n";
          reject($ticket_id, $from, $body, "internal");
        }
        else {
  	  my $subject = "Comment via E-mail";
  	  my $q  = "INSERT INTO comment SET ";
	  $q .= "ticket_id='$ticket_id', status = '$status', ";
	  $q .= "staff='$staff', subject = '$subject', body='$body'";
	  my $query = $db->prepare($q);
	  my $done = $query->execute;
          if (!$done) {
            print "\n\nMySQL Error:\n\n $q\n\n";
            reject($ticket_id, $from, $body, "internal");
          }
	  my $insert_id = $db->{'mysql_insertid'};
	  $query->finish;

	  # update ticket table, for time and last insert id 
   	  my $q2  = "UPDATE ticket SET ";
 	  $q2 .= "last_comment_id='$insert_id', ";
 	  $q2 .= "date_created=date_created, ";
 	  $q2 .= "status_change_date=NOW() ";
	  $q2 .= "WHERE id='$ticket_id'";
 	  my $query2 = $db->prepare($q2);
 	  my $done2 = $query2->execute;
           if (!$done2) {
             print "\n\nMySQL Error:\n\n $q\n\n";
             reject($ticket_id, $from, $body, "internal");
           }
 	  $query2->finish;
        }
    }
}

sub get_ticket_id {
    my ($subj) = @_;
    ## Parses $subj to get ticket number, returns 0 for invalid subj
    if ($subj =~ /ticket:\s*[0-9]+/i) {
        my $found_subj = 0;
        my @words = split /\s+/, $subj;
        foreach my $word (@words) {
            if ($found_subj) {
                return $word;
            }
            if ($word =~ /ticket/i) {
                if ($word =~ /[0-9]$/) {
                    # if ticket num cancat'd onto tail           
                    $word =~ s/[^0-9]//g;
                    # pull out number                     
                    return $word;
                }
                else {
                    # want next word                                      
                    $found_subj = 1;
                }
            }
        }
    }
    else {
        return 0;
    }
}

sub get_email {
    my ($from) = @_;
    ## returns an email given items in "From: " mail header
    my $email = '^[A-Za-z0-9_-]+@[A-Za-z0-9_-]+.[A-Za-z0-9_-]+.*';
    my $html = '(<[^>]*>)';
    if ($from =~ /$email/i) {
	return $from;
    }
    else {
	# Cover the following case of a valid 'from:' header  
	# given  "Test User ... <test@host.fake.tld> ..." 
	# return "test@host.fake.tld".  
	my @words = split(/\s+/, $from);
	foreach my $word (@words) {
	    if ($word =~ /$html/) {
		$word =~ s/\<//;
		$word =~ s/\>//;
		if ($word =~ /$email/i) {
		    return $word;
		}
	    }
	}
    }
    return $from;  # use what's in header by default 
}

sub is_text {
    my ($msg) = @_;
    ## returns whether or not $msg is a standard txt message or 
    ## if the message contains multipart MIME components 

    my $mime = "(-)+(_)*=_NextPart"; 
    if ($msg =~ $mime) {
	return 0;
    }
    return 1;
}

sub reject {
    my ($ticket_id, $from, $body, $reason) = @_;
    ## returns t or nil if $from was emailed about rejection 
    ## due to HTML or multipart MIME encodings 

    my $msg = "";

    if ($reason =~ "html") {
	$msg = "We were not able to use your email to add a comment to ticket $ticket_id because of the way that your mail software is configured.  ruQueue only accepts text email.  Please resend your message after you have configured your mailer to send text email (not HTML or RTF).  The message that the system received is the following:  \n\n$body";
    }
    if ($reason =~ "queue") {
	$msg = "We were not able to use your email to add a comment to ticket $ticket_id because either the ticket does not exist or the queue that ticket $ticket_id is in does not allow for comments to be added via email.  If you need to be able to add comments to tickets in this queue please request that the queue administrator change the queue with the \"Configuration --> Queues --> Allow Mail Comments\" option.  The message that the system received is the following:  \n\n$body";
    }
    if ($reason =~ "subject") {
	$msg = "We were not able to use your email to add a comment to a ticket because the subject that you sent does not have the proper format.\n\nIf you want to add a comment to a ticket via email please make sure that your email's subject contains the word \"ticket\" followed immediately by a colon and finally the ticket number.  For example if you want to comment on ticket 37 your subject should look like \"ticket: 37\".  \n\nThe message that the system received is the following:  \n\n$body";
    }
    if ($reason =~ "internal") {
	$msg = "We were not able to use your email to add a comment to ticket $ticket_id because of an internal system error.  Please notify the administrator of this issue.  \n\nThe message that the system received is the following:  \n\n$body";
    }

    my %mail = (To      => $from,
		From    => $contact, 
		Subject => "ruQueue Mail Comment Error:  mail rejected",
		Message => $msg
		);
    my $notify = sendmail(%mail);
    if (!$notify) {
	print "Unable to send mail.  ";
	print "Mail Error:\n", $Mail::Sendmail::log;
	print "Log says:\n", $Mail::Sendmail::error;
	return 0;
    }
    else {
	return 1;
    }
}

sub fix_body {
    my ($msg) = @_;
    ## returns version of $msg that is standard txt message 
    ## with multipart MIME parts stripped
    ## Uses K&R style, Larry Wall would probably not approve     

    my $keep = "";
    my $mime = "(-)+(_)*=_NextPart";
    my $text = "Content-Type: text/plain;";
    my $b64 = "Content-Transfer-Encoding: base64";
    my $con  = "Content-";
    my $char = "charset=";
    my $in_text = 0;
    my $in_mime = 0;
    my $need64 = 0;
    my @lines = split(/\n/, $msg);

    foreach my $line (@lines) {
	if ($line =~ $mime) {
	    $in_mime = 1;
	}
	if ($in_mime) {
	    if ($line =~ $text) {
		$in_text = 1;
	    }
	    if ($line =~ $mime) {
		$in_text = 0;
	    }
	    if ($in_text) {
		if ($line =~ $b64) {  # Exchange via IE b64encodes 
		    $need64 = 1;
		}
		if (($line !~ $con) and ($line !~ $char)) {
		    $keep .= "$line\n";
		}
	    }
	}
    }
    if ($need64) {
	return &base64::b64decode($keep);
    }
    else {
	return $keep;
    }
}

sub errase_log {
    ## errases $log 
    my $cmd = "cat /dev/null > $log";  
    system($cmd);  ## uncomment when ready 
}

sub print_time {
    ##  Print Time for logging purposes 
    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) = localtime(time);
    $year += 1900;  
    $mon += 1;
    print "$mon/$mday/$year $hour:$min\n";
}

# -----------------------------------------------------------
# MAIN 

# Interate over mail in $log and get relevant info to create comment.  

for my $msg ($mb->get_messages) {
    my $ticket_id = get_ticket_id($msg->header->{subject});

    my $body = 
	"To:  " . $msg->header->{to} . "\n"  
	. "Cc:  " . $msg->header->{cc} . "\n"  
	# . "Subject:  " . $msg->header->{subject} . "\n"
        . "---\n" . 
	$msg->body;

    # If you see:
    #  Use of uninitialized value in concatenation (.) or string 
    #  at ./mail_comments.pl line 385
    # Don't panic.  This a superfluous perl warning. It should 
    # affect absolutely nothing.  

    if (!$ticket_id) {
	reject($ticket_id, $msg->header->{from}, $body, "subject");
    }
    else {
	my $rejected = 0;
	my $user = get_email($msg->header->{from});
	if (!is_text($body)) {
	    ## Uncomment one or the other depending on how we deal with HTML
	    $body = fix_body($body);
	    # $rejected=reject($ticket_id, $msg->header->{from},$body,"html");
	}
	if (!$rejected) {
	    mail_comment($ticket_id, $user, $body, $msg->header->{from});
	}
	## For debugging
	# print "\n", $ticket_id," ", $user, " \"", $body, "\" ";
    }
}
errase_log();
# print_time();

# -----------------------------------------------------------
$db->disconnect();
print "Done.  \n";
