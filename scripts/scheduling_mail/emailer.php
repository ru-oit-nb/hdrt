<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//
// Filename: Emailer.php
// Description: Sends email to notify users and consultants of appointments.
// Supprted Language(s):   PHP 4.0
//
    
/*
    These fields will be substituted:
    Ticket Number [ticket]
    User's Email [email]
    User's Name [user]
    Date/Time [datetime]
    Staff Email [staff]
    Staff First Name [first]
    Staff Last Name [last]
    Building Name [building]
    Room Number [room]
*/

require_once("../config.php"); // Contains the constants declared global below
//----------------------------------------------------------------------------
// SCHEDULING EMAILER CONFIGURATION
// <DO NOT CHANGE THESE VALUES HERE, USE config.php>
// Set this to the email address that emails sent automatically from
// ruQueue should list as their from/reply-to address.
//$FROM_EMAIL = "ruQueue@localhost";
//$REPLY_TO_EMAIL = "ruQueue@localhost";
//
// The system sends two emails (one to user, one to consultant) so it
// actually sends up to 40 emails every second with the above with a
// space of 2 seconds between each sending spree.
//$MAX_EMAIL_PER_SECOND = 20;
//$SECONDS_BETWEEN_MAIL = 2;
//
// Set the following to 1 to send mail, 0 to not send mail.
// This is useful for testing the script.
//$SEND_MAIL = 1;
//----------------------------------------------------------------------------
global $db, $MAX_EMAIL_PER_SECOND, $SECONDS_BETWEEN_MAIL, $SEND_MAIL, $FROM_EMAIL, $REPLY_TO_EMAIL;

$notify_hours = GetValueFromTable('option_value', 'sched_options', "where option_name='Send Reminder Emails (hours)'");
$datetime = date("Ymd").(date("h")+$notify_hours)."0000";
$where = $subject = $body = array();
    
$appointments = GetListFromTable('appointment_id', 'sched_mailer', '', ',');
$where[0] = "where status='Scheduled' and appointments.appointment_id not in ($appointments)";
$where[1] = "where datetime < $datetime and status='Scheduled' and reminder_sent!=1 and sched_mailer.appointment_id=appointments.appointment_id";
$where[2] = "where (status='Cancelled' or status='Not Yet Scheduled') and status!=last_status and sched_mailer.appointment_id=appointments.appointment_id";
$where[3] = "where status='Missed - User' and status!=last_status and sched_mailer.appointment_id=appointments.appointment_id";
$where[4] = "where status='Missed - Consultant' and status!=last_status and sched_mailer.appointment_id=appointments.appointment_id";
$where[5] = "where status='Completed' and status!=last_status and sched_mailer.appointment_id=appointments.appointment_id";
    
$subject[0] = "ruQueue: Appointment Scheduled";
$subject[1] = "ruQueue: Appointment Reminder";
$subject[2] = "ruQueue: Appointment Cancelled";
$subject[3] = "ruQueue: Appointment Missed - User";
$subject[4] = "ruQueue: Appointment Missed - Consultant";
$subject[5] = "ruQueue: Appointment Completed";
    
$body_type[0] = "Initial";
$body_type[1] = "Reminder";
$body_type[2] = "Cancelled";
$body_type[3] = "Missed(User)";
$body_type[4] = "Missed(Consultant";
$body_type[5] = "Completed";
    
for ($i = 0; $i < sizeof($where); $i++) {
  if ($i == 2) {
    $where_tail = " and ticket_id=ticket.id and user_id=user.id and staff.username=last_consultant and appointments.building_number=buildings.building_number";
  } else {
    $where_tail = " and ticket_id=ticket.id and user_id=user.id and staff.username=consultant_netid and appointments.building_number=buildings.building_number";
  }
  $query = "select distinct appointments.appointment_id appointment,ticket.id ticket,user.email email,user.name user,";
  $query .= "date_format(appointments.datetime,'%W, %M %e, %Y at %h:%i %p') datetime,staff.email staff,firstname first,lastname last,building_name building,room_number room, appointments.status status, appointments.consultant_netid netid";
  $query .= " from appointments,ticket,user,staff,sched_mailer,buildings ".$where[$i].$where_tail;
  $result = mysql_query($query, $db);
  SendMail($result, $subject[$i], $body_type[$i]);
}

mysql_close($db);
    
//-----------------------------------------------------------------------------
//
// Function: SendMail
// Description:  sends/displays mail based on a query and predefined options
//
// Parameters:
//    mysql_resource_id $result  results to gather email info from
//    string $subject            subject of emails
//    string $body_type          type of email body to send
//
// Return Values:
//    None
//
// Remarks:
//    None
//
//-----------------------------------------------------------------------------
function SendMail($result, $subject = "Test Subject", $body_type = "") {

  global $MAX_EMAIL_PER_SECOND;
  global $SECONDS_BETWEEN_MAIL;
  global $SEND_MAIL;
  global $FROM_EMAIL;
  global $REPLY_TO_EMAIL;
  $headers = "From: $FROM_EMAIL\r\nReply-To: $REPLY_TO_EMAIL\r\nX-Mailer: PHP/" . phpversion();
  $user_body_template = GetValueFromTable("option_value", "sched_options", "where option_name='User ".$body_type." Email'");
  $body_template = GetValueFromTable("option_value", "sched_options", "where option_name='Consultant ".$body_type." Email'");
       
  $counter = 0;
  while ($data = mysql_fetch_array($result)) {
    
    if (!($counter++ % $MAX_EMAIL_PER_SECOND)) {
      // sleep so as to not break policies for allowable mail over time
      sleep($SECONDS_BETWEEN_MAIL);
    }

    // create and send user email
    $to = $data['email'];
    $body = $user_body_template;
    $search = array("[ticket]", "[email]", "[user]", "[datetime]", "[staff]", "[first]", "[last]", "[building]", "[room]");
    $replace = array($data['ticket'], $data['email'], $data['user'], $data['datetime'], $data['staff'], $data['first'], $data['last'], $data['building'], $data['room']);
    $body = str_replace($search, $replace, $body);

    if ($SEND_MAIL) {
      mail($to,$subject,$body, $headers);
    }
    else {
      print "\n".$headers."\n";
      print 'To: '.$to."\n";
      print 'Subject: '.$subject."\n";
      print $body."\n";
    }
    
    // create and send staff email
    $to = $data['staff'];
    $body = $body_template;
    $search = array("[ticket]", "[email]", "[user]", "[datetime]", "[staff]", "[first]", "[last]", "[building]", "[room]");
    $replace = array($data['ticket'], $data['email'], $data['user'], $data['datetime'], $data['staff'], $data['first'], $data['last'], $data['building'], $data['room']);
    $body = str_replace($search, $replace, $body);

    if ($SEND_MAIL) {
      mail($to,$subject,$body, $headers);
    }
    else {
      print "\n".$headers."\n";
      print 'To: '.$to."\n";
      print 'Subject: '.$subject."\n";
      print $body."\n";
    }
    
    if ($body_type == "Initial") {
      $query = "insert into sched_mailer values (".$data['appointment'].",'".$data['status']."','".$data['netid']."',0)";
      ;
    } elseif ($body_type == "Reminder") {
      $query = "update sched_mailer set reminder_sent=1 where appointment_id=".$data['appointment'];
    } else {
      $query = "delete from sched_mailer where appointment_id=".$data['appointment'];
    }
    global $db;
    mysql_query($query, $db);
  }
       
}
    
//-----------------------------------------------------------------------------
//
// Function:  DisplayTable
// Description: displays a table for all values in $result
//
// Parameters:
//    mysql_resource_id  $result  results to display
//
// Return Values:
//    none
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DisplayTable($result) {
  if (mysql_num_rows($result) <= 0) {
    return;
  }
       
  $num_fields = mysql_num_fields($result);
  print "<p><table width=100% cellspacing=0 align=center border=0 cellspacing=1 cellpadding=3><tr>";
       
  for($i = 0; $i < $num_fields; $i++) {
    $field_name = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
    print "<th align=center>".$field_name;
    print "&nbsp;</th>";
  }
  echo "<th></th>";
  echo "</tr>\n";
       
  while ($row = mysql_fetch_array($result)) {
    print "<tr>";
          
    for($j = 0; $j < $num_fields; $j++) {
      print "<td align=center>";
      $field_value = $row[$j];
      if ($j == $loop_id) $field_value = "<b>".$field_value."</b>";
      print $field_value;
      print "&nbsp;</td>";
    }
          
    print "</tr>\n";
  }
       
  print "</table>";
}
    
//-----------------------------------------------------------------------------
//
// Function: GetValueFromTable
// Description:  Retrieves a specific value of a specific field in a table
//
// Parameters:
//    string $field_name  field to return information from
//    string $table       table to select from
//    string $where       option extensions to query
//
// Return Values:
//    Returns value for the field.
//
// Remarks:
//    None
//
//-----------------------------------------------------------------------------
function GetValueFromTable($field_name, $table, $where) {
  global $db;
  $result = mysql_query("select $field_name from $table $where limit 1");
  $field = mysql_fetch_object($result);
  return $field->$field_name;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetListFromTable
// Description:  Retrieves a list of values from a field in a table
//
// Parameters:
//    string $field_name  field to return information from
//    string $table       table to select from
//    string $where       option extensions to query
//    string $seperator   what to seperate values by
// Return Values:
//    Returns the list.
//
// Remarks:
//    None
//
//-----------------------------------------------------------------------------
function GetListFromTable($field_name, $table, $where, $seperator) {
  global $db;
  $result = mysql_query("select $field_name from $table $where");
  $field = mysql_fetch_object($result);
  $list = $field->$field_name;
  while ($field = mysql_fetch_object($result)) {
    ;
    $list .= $seperator.$field->$field_name;
  }
  return $list;
}
?>
