<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

// Filename:                scheduling_cacher.php
// Description:             caches the scheduling system for improved speed
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-02-23 16:36:31 jfulton> 

require_once('../config.php');

if (CachePotentialSchedule()) print "Potential Schedule Cached.\n";
else print "Potential Schedule Cache failed!\n";
if (CacheAppointments()) print "Appointments Cached.\n";
else print "Appointments Cache failed!\n";

// -------------------------------------------------------- 
// Function:                Class::CachePotentialSchedule

// Description:             caches potential schedules

// Type:                    public

// Parameters:
//    [bool] $fresh         clear table flag

// Return Values:           
//    bool                  true on success, false otherwise

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function CachePotentialSchedule($fresh = false) {
  global $db;
  if ($fresh) { //clear table
    $query = "DELETE FROM potential_schedule_cache";
    if (!mysql_query($query, $db)) return false;
  }
  $fields = implode(", ", array(
				'user_buildings.group_type group_type',
				'user_buildings.group_value group_value',
				'potential_schedule.day day',
				'potential_schedule.hours hours',
				'COUNT(*) num'
				));
  $tables = implode(", ", array(
				'user_buildings',
				'potential_schedule'
				));
  $where = implode(" AND ", array(
				  "user_buildings.netid = potential_schedule.netid",
				  "hours <> ''"
				  ));
  $group_by = implode(", ", array(
				  'user_buildings.group_type',
				  'user_buildings.group_value',
				  'potential_schedule.day',
				  'potential_schedule.hours'
				  ));
  $query = "SELECT $fields FROM $tables WHERE $where GROUP BY $group_by";
  if ($result = mysql_query($query, $db)) {
    while ($row = mysql_fetch_object($result)) {
      $hours = explode(",", $row->hours);
      foreach ($hours as $hour) { //break sets into seperate blocks
	$scheduled[$row->group_type][$row->group_value][$row->day][$hour] += $row->num;
      }
    }
    if (is_array($scheduled)) { //insert condensed form
      foreach ($scheduled as $group_type => $L1) {
	foreach ($L1 as $group_value => $L2) {
	  foreach ($L2 as $day => $L3) {
	    foreach ($L3 as $hour => $num) {
	      if ($num > 0) {
		$info = array(
			      'group_type' => "'$group_type'",
			      'group_value' => "'$group_value'",
			      'day' => "'$day'",
			      'hour' => "'$hour'",
			      'num' => $num,
			      'updated' => 'NOW()'
			      );
		$fields = implode(", ", array_keys($info));
		$values = implode(", ", $info);
		$query = "REPLACE DELAYED INTO potential_schedule_cache ($fields) VALUES ($values)";
		if (!mysql_query($query, $db)) return false;
	      }
	    }
	  }
	}
      }
    }
  }
  else return false;
  if (CleanupCache('potential_schedule_cache')) return true; //delete old entries if update success
  else return false;
}



// -------------------------------------------------------- 
// Function:                Class::CleanupCache

// Description:             delete outdated cache items

// Type:                    public

// Parameters:
//    none

// Return Values:           
//    bool                  true on success, false otherwise

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function CleanupCache($table) {
  global $db;
  $query = "DELETE LOW_PRIORITY FROM $table WHERE updated < DATE_SUB(NOW(), INTERVAL 30 MINUTE)";
  return mysql_query($query, $db);
}



// -------------------------------------------------------- 
// Function:                Class::CacheAppointments

// Description:             cache appointments table

// Type:                    public

// Parameters:
//    [bool] $fresh         clear table flag

// Return Values:           
//    bool                  true on success, false otherwise

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function CacheAppointments($fresh = false) {
  global $db;
  if ($fresh) { //clear table
    $query = "DELETE FROM appointments_cache";
    if (!mysql_query($query, $db)) return false;
  }
  $fields = implode(", ", array(
				'buildings.campus campus',
				'buildings.building_group building_group',
				'buildings.building_name building_name',
				'appointments.datetime datetime',
				'COUNT(*) num'
				));
  $tables = implode(", ", array(
				'buildings',
				'appointments'
				));
  $where = implode(" AND ", array(
				  "buildings.building_number = appointments.building_number",
				  "appointments.status <> 5",
				  "appointments.status <> 1"
				  ));
  $group_by = implode(", ", array(
				  'buildings.campus',
				  'buildings.building_group',
				  'buildings.building_name',
				  'appointments.datetime'
				  ));
  $query = "SELECT $fields FROM $tables WHERE $where GROUP BY $group_by";
  if ($result = mysql_query($query, $db)) {
    while ($row = mysql_fetch_object($result)) {
      $info = array(
		    'campus' => "'$row->campus'",
		    'building_group' => "'$row->building_group'",
		    'building_name' => "'$row->building_name'",
		    'datetime' => "'$row->datetime'",
		    'num' => $row->num,
		    'updated' => "NOW()"
		    );
      $fields = implode(", ", array_keys($info));
      $values = implode(", ", $info);
      $query = "REPLACE DELAYED INTO appointments_cache ($fields) VALUES ($values)";
      if (!mysql_query($query, $db)) return false;
    }
  }
  else return false;
  if (CleanupCache('appointments_cache')) return true; //delete old entries if update success
  else return false;
}


?>
