<?
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

// This file contains general configuration options for the php scripts.
// Please examine the scripts themselves if you need clarification.
// Don't forget to edit the mail_comments script separately.

// Set this variable to the location of the ruqueue_mysql.php file
// included with the distribution of ruQueue.
$mysql_file = "/var/www/localhost/htdocs/ruQueue/ruqueue_mysql.php";

//----------------------------------------------------------------------------
// NIGHTLY REPORT CONFIGURATION
// set to your email (so you know about bounces)
$FROM_ADDRESS = 'ruQueue@localhost';

// Managers who receive most emails.
$TOP_MANGER = 'test@localhost,test2@localhost';

// actually send email to $TOP_MANGER?
$MAIL_TOP = true;

// actually send email to supervisors?
$MAIL_SUPER = true;

// URL of ruQueue - used to generate links.
$SYSTEM_URL = 'https://ruQueue/';

// Name assigned to the system - most likely ruQueue
$SYSTEM_NAME = 'ruQueue';

// Set this variable to the location of the functions_date.php file
// included with the distribution of ruQueue.
$functions_date = "/var/www/localhost/htdocs/ruQueue/functions/functions-date.php";
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
// SCHEDULING EMAILER CONFIGURATION
// Set this to the email address that emails sent automatically from
// ruQueue should list as their from/reply-to address.
$FROM_EMAIL = "ruQueue@localhost";
$REPLY_TO_EMAIL = "ruQueue@localhost";

// The system sends two emails (one to user, one to consultant) so it
// actually sends up to 40 emails every second with the above with a
// space of 2 seconds between each sending spree.
$MAX_EMAIL_PER_SECOND = 20;
$SECONDS_BETWEEN_MAIL = 2;

// Set the following to 1 to send mail, 0 to not send mail.
// This is useful for testing the script.
$SEND_MAIL = 1;
//----------------------------------------------------------------------------

require_once($mysql_file);
?>
