<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

// Filename:                nightly_report.php
// Description:             Sends nightly email
//                          to queue supervisors
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-04-22 19:09:08 jfulton> 
// -------------------------------------------------------- 
/*
This script is run each night by cron.  It sends email to 
each queue supervisor.  The contents of the email is a 
list of overdue tickets for each queue.  A concatenation 
of each email is sent to the top manager.  
*/

require_once("../config.php");
//----------------------------------------------------------------------------
// NIGHTLY REPORT CONFIGURATION
// <DO NOT EDIT THIS INFORMATION, EDIT config.php>
// set to your email (so you know about bounces)
//$FROM_ADDRESS = 'ruQueue@localhost';
//
// Managers who receive most emails.
//$TOP_MANGER = 'test@localhost,test2@localhost';
//
// actually send email to $TOP_MANGER?
//$MAIL_TOP = true;
//
// actually send email to supervisors?
//$MAIL_SUPER = true;
//
// URL of ruQueue - used to generate links.
//$SYSTEM_URL = 'https://ruQueue/';
//
// Name assigned to the system - most likely ruQueue
//$SYSTEM_NAME = 'ruQueue';
//
// Set this variable to the location of the functions_date.php file
// included with the distribution of ruQueue.
//$functions_date = "/var/www/localhost/htdocs/ruQueue/functions/functions-date.php";
//----------------------------------------------------------------------------

// -------------------------------------------------------- 
// Make constants global
global $MAIL_TOP;  
global $MAIL_SUPER;  
global $TOP_MANGER;  
global $FROM_ADDRESS;  
global $SYSTEM_URL;
global $SYSTEM_NAME;

// -------------------------------------------------------- 
include $functions_date;
// -------------------------------------------------------- 

$tickets = getArrayOfOverdue();
$supers  = getArrayOfSupervisors();
$htmls = getArrayOfHtml();

$reports = array();  // associative array s.t. queues --> r_string
$fin_report = array();  // same as above but all HTML for upper management

foreach($tickets as $ticket => $obj) {
  $html = htmlEmail($obj->queue, $htmls);
  if (!isset($reports[$obj->queue])) {  
    // if this queue is not in the reports array add it
    $reports[$obj->queue] = newReport($obj->queue, $html);
    $fin_report[$obj->queue] = newReport($obj->queue, 1, 1);  // HTML
  }
  // add to the reports
  $reports[$obj->queue] = addToReport($obj, $reports[$obj->queue], $html);
  $fin_report[$obj->queue] = addToReport($obj, $fin_report[$obj->queue], 1);
}

$none = array();
$good = $bad = 0;

foreach($reports as $queue => $report) {
  // print "\n\n$report\n\n";  // debug
  $to = $supers[$queue];
  if (empty($to)) {
    $none[$bad++] = $queue;
  }
  else {
    ++$good;
    $html = htmlEmail($queue, $htmls);
    notifySupervisor($to, $report, $html);
  }
}
// final report for top manager
finalReport($good, $bad, $none, $fin_report);  

// --------------------------------------------------------
// This is an object that we'll use in getArrayOfOverdue() 
class Day {
  var $allowabledays;
  var $date;
}
// -------------------------------------------------------- 
// Function:                overdueTicketsQuery()

// Description:             returns SQL to extract overdue 
//                          tickets according to arguments

// Type:                    public

// Parameters:
//    [in] string $date        The date 
//    [in] int $allowabledays  The amount of days a ticket 
//                             can wait

// Return Values:           
//    string                The an SQL statement

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function overdueTicketsQuery($date, $allowabledays) 
{
  $sql  = "SELECT ticket.id as id, queue, status, ";
  $sql .= "ticket.date_created as date, ";
  $sql .= "DATE_FORMAT(ticket.date_created, '%Y-%m-%d') as pretty_date, ";
  $sql .= "DATE_FORMAT(comment.date_created, '%Y-%m-%d') as mod_date, ";
  $sql .= "owner, requester, subject ";
  
  $sql .= "FROM ticket, comment, queue ";
  
  $sql .= "WHERE status != 'Resolved' and ";

  $sql .= "queue.q_name = ticket.queue and ";
  $sql .= "ticket.last_comment_id = comment.id and ";
  $sql .= "LEFT(ticket.date_created,8) < '$date' and ";
  $sql .= "queue.allowabledays = '$allowabledays' ";

  $sql .= "GROUP by queue, id";

  return $sql;
}

// --------------------------------------------------------
// Function:                getArrayOfOverdue()

// Description:             returns an array $overdue such that
//                             $overdue[$ticket_number] = $ticket_obj
//                          every ticket is an overdue ticket

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    array                 An assoc array of ticket objects 
//                          where each ticket number maps to 
//                          a ticket object

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function getArrayOfOverdue() 
{
  $overdue = array(); // returned in the end

  $query  = "SELECT allowabledays FROM queue GROUP BY allowabledays";
  global $db;
  $result = mysql_query($query, $db);
  if (!$result) {
    print "MySQL Error:  \n\n$query\n\n";
  }
  else {
    $days = array();
    $i = 0;  
    while($obj = mysql_fetch_object($result)) {
      $day = new Day;
      $day->allowabledays = $obj->allowabledays;
      $date = NBDaysAgo($obj->allowabledays);  // from included lib
      $day->date = str_replace("-", "", $date);
      $days[$i++] = $day;
    }

    for ($i = 0; $i < count($days); $i++) {
      $day = $days[$i];
      $query = overdueTicketsQuery($day->date, $day->allowabledays);
      $result = mysql_query($query, $db); 
      if (!$result) {
	print "MySQL Error:  \n\n$query\n\n";
      }
      else {
	while ($obj = mysql_fetch_object($result)) {
	  $overdue[$obj->id] = $obj;
	}
      }
    }
  }
  return $overdue;
}

// --------------------------------------------------------
// Function:                getArrayOfHtml()

// Description:             returns an array $html of queues 
//                          whose supervisors who want html 
//                          email

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    array                 An array of queues

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function getArrayOfHtml() 
{
  $html = array();
  $q  = "SELECT q_name FROM q_supervisor, staff ";
  $q .= "WHERE staff.username = q_supervisor.username AND ";
  $q .= "staff.html_email='1'";
  global $db;
  $result = mysql_query($q, $db);
  if (!$result) {
    print "<p>MySQL Error:  <i>$q</i>\n";
  }
  else {
    $i = 0;
    while ($row = mysql_fetch_object($result)) {
      $html[$i++] = $row->q_name;
    }
  }
  return $html;
}

// --------------------------------------------------------
// Function:                getArrayOfSupervisors()

// Description:             returns an array $supers such 
//                          that $supers[$queue] = $email

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    array                 An assoc array of supervisors s.t.
//                          $supers[$queue] = $email

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function getArrayOfSupervisors() 
{
  $supers = array();
  global $db;
  $query  = "SELECT q_name, email FROM q_supervisor, staff ";
  $query .= "WHERE q_supervisor.username = staff.username";
  $result = mysql_query($query, $db);
  if (!$result) {
    print "MySQL Error:  \n\n$query\n\n";
  }
  else {
    while($obj = mysql_fetch_object($result)) {
      if (empty($supers["$obj->q_name"])) {
        $supers["$obj->q_name"] = $obj->email;
      }
      else {
	$old = $supers["$obj->q_name"];
	$supers["$obj->q_name"] = "$old, ".$obj->email;  // build list 
      }
    }
  }
  return $supers;
}

// -------------------------------------------------------- 
// Function:                showSupers() 

// Description:             Prints list of supervisors

// Type:                    public

// Parameters:
//    None                  
// Return Values:           
//    None                  
// Remarks:                 
//    Used to debug
// -------------------------------------------------------- 

function showSupers() 
{
  $supers  = getArrayOfSupervisors();
  foreach($supers as $queue => $email) {
    print "$queue ---> $email\n";
  }
}

// -------------------------------------------------------- 
// Function:                AddNSpaces()

// Description:             adds $n spaces to the end 
//                          of $str and returns $str

// Type:                    public

// Parameters:
//    [in] string $str      The string getting padded
//    [in] int $n           The amount of spaces

// Return Values:           
//    string                $str with $n spaces added 

// Remarks:                 
//    Could have just used string_pad()
// -------------------------------------------------------- 

function AddNSpaces($str, $n) 
{
  $spc = "";
  for ($i = 0; $i < $n; $i++) $spc .= " ";
  return $str . $spc;
}

// -------------------------------------------------------- 
// Function:                upToAt()

// Description:             given "asdf@zxcv.com" returns "asdf" 

// Type:                    public

// Parameters:
//    [in] string $url      An email address

// Return Values:           
//    string                The email address without the 
//                          domain or '@'

// Remarks:                 
//    Could be done with a regex, done with K&R string 
//    as an array of chars approach instead.  
// -------------------------------------------------------- 

function upToAt($string) 
{
  $end = "";
  $length = strlen($string);
  for ($i = 0; $i < $length; $i++) {
    $char = $string[$i];
    if ($char == "@") break;
    else $end .= $char;
  }
  return $end;
}

// -------------------------------------------------------- 
// Function:                addToReport()

// Description:             appends strings from ticket object 
//                          $obj onto $report with formatting

// Type:                    public

// Parameters:
//    [in] object $obj      A ticket object 
//    [in] string $report   The report text 
//    [in] boolean $html    True if report should be in HTML

// Return Values:           
//    string                The orginal report with items 
//                          of the object added 

// Remarks:                 
//    Does some ASCII art style spaceing for formatting
// -------------------------------------------------------- 

function addToReport($obj, $report, $html) 
{
  global $SYSTEM_URL;
  if ($html) {
   $ticket  = "<a href=\"" . $SYSTEM_URL;
   $ticket .= "ticket.php?id=" . $obj->id . "\">";
   $ticket .= $obj->id . "</a>";
  }
  else {
   $ticket = $obj->id;    
  }
  
  // edit vars
  $requestor = upToAt($obj->requester);
  $owner = upToAt($obj->owner);
  if (empty($requstor)) $requestor = "";
  if (empty($owner)) $owner = "";

  $ticket_length = 8;  // 6+2
  $modified_length = 12;  // 10+2
  $req_length = 10;  // 8 + 2
  $owner_length = 10;  // 8 + 2
  $status_length =  8;  // 8

  $ticket_space = $ticket_length - strlen($obj->id);  
  $modified_space = $modified_length - strlen($obj->mod_date);
  $req_space = $req_length - strlen($requestor);
  $owner_space = $owner_length - strlen($owner);
  $status_space =$status_length - strlen($obj->status);

  $ticket = AddNSpaces($ticket, $ticket_space);
  $modified = AddNSpaces($obj->mod_date, $modified_space);
  $requestor = AddNSpaces($requestor, $req_space);
  $owner = AddNSpaces($owner, $owner_space);
  $status = AddNSpaces($obj->status, $status_space);

  $report .= $ticket . $modified . $requestor . $owner . $status . "\n";

  return $report;
}

// -------------------------------------------------------- 
// Function:                newReport()

// Description:             Creates the start of the report
//                          to be concatenated onto

// Type:                    public

// Parameters:
//    [in] string $queue    The queue the report is for 
//    [in] boolean $html    Boolean flag for HTML email
//    (in) booelan $fin     Boolean flag for if this is 
//                          a final report 
//                          default:  false

// Return Values:           
//    string                The start of the report

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function newReport($queue, $html, $fin=0) 
{
  $str = "";
  if ($html and !$fin) {
    $str .= "<html><body>";
  }

  $str .= "\n\n\n";
  $str .= "The following tickets are reported as overdue in the ";
  $str .= "$queue queue:  \n\n";

  if ($html) {
    $str .= "<font face=\"courier, new\"><pre>\n";
  }
  
  $str .= "Ticket  "; // 6+2 
  $str .= "Modified    "; // 10+2
  $str .= "Req by    ";  // 8 + 2
  $str .= "Owner     ";  // 8 + 2
  $str .= "Status\n";  // 8
  for ($i = 0; $i < 48; $i++) $str .= "-";  
  $str .= "\n";
  return $str;
}

// -------------------------------------------------------- 
// Function:                notifySupervisor

// Description:             Sends email to supervisor

// Type:                    public

// Parameters:
//    [in] string $to      The email address of the supervisor
//    [in] string $report  The actual report, i.e. the message text
//    [in] boolean $html   A flag for HTML email if true

// Return Values:           
//    None                  

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function notifySupervisor($to, $report, $html) 
{
  global $SYSTEM_URL;
  global $SYSTEM_NAME;
  global $FROM_ADDRESS;  
  global $MAIL_SUPER;  
  if ($html) {
    $report .= "</pre></font>\n";
    $report .= "Click on any of the above ticket numbers for more ";
    $report .= "information.  \n\n";

    $report .= "If you don't want to get HTML email go to ";
    $report .= "<a href=\"" . $SYSTEM_URL . "user.php\">"; 
    $report .= $SYSTEM_URL . "user.php</a>  ";
    $report .= "and configure your " . $SYSTEM_NAME . " preferences.  ";
    $report .= "</body></html>\n";
  }
  else {
    $report .= "\n";
    $report .= "If you go to " . $SYSTEM_URL . " ";
    $report .= "you can enter any of the above ticket numbers ";
    $report .= "in the \"Go to ticket\" form for more information.  ";

    $report .= "\n\nIf you want to get HTML email (so that the above ";
    $report .= "ticket numbers are clickable) go to:  \n\n ";
    $report .= $SYSTEM_URL . "user.php  \n\n";
    $report .= "and configure your " . $SYSTEM_NAME  . " preferences.  ";
  }
  $report .= "\n\nThis report was sent " . date("l dS of F Y h:i:s A") . ".";
  $sub = "Overdue Tickets in " . $SYSTEM_NAME;
  $headers = "";
  if ($html) {
    $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
  }
  $headers .= "From:  " . $FROM_ADDRESS;  

  if ($MAIL_SUPER) {
    $sent = mail($to, $sub, $report, $headers);
    if (!$sent) {
      print "Unable to mail $to\n";
    }
  }
}

// -------------------------------------------------------- 
// Function:                htmlEmail()

// Description:             returns true if the supervisor 
//                          of $queue wants HTML email.  
//                          returns false otherwise

// Type:                    public

// Parameters:
//    [in] string $queue    The name of the queue 
//    [in] array $htmls     An associative array such 
//                          that $queue --> $supervisor 

// Return Values:           
//    boolean 

// Remarks:                 
//    $htmls (an assoc array is passed to save SQL calls) 
// -------------------------------------------------------- 

function htmlEmail($queue, $htmls) 
{
  return in_array($queue, $htmls);
}

// -------------------------------------------------------- 
// Function:                finalReport()

// Description:             sends overview email to top manager

// Type:                    public

// Parameters:
//    [in] int $good         The amount of emails sent
//    [in] int $bad          The amount of emails not sent
// 
//    [in] array $none       An array of queues without suervisors
// 
//    [in] array $fin_report An associative array of reports
//                           such that queues --> report string

// Return Values:           
//    None                  

// Remarks:                 
//    Sends HTML email.  Currently the top manager 
//    has no choice.  It could be re-written to make 
//    this be controlled by another constant.  
// -------------------------------------------------------- 

function finalReport($good, $bad, $none, $fin_report) 
{
  global $SYSTEM_NAME;
  global $TOP_MANGER;
  global $FROM_ADDRESS;
  global $MAIL_TOP;

  $none = array_unique($none);
  for ($i = 0; $i < $bad; $i++) {
    if (!empty($none[$i])) {
      $q_list .= "\n  " . $none[$i];
    }
  }
  $total = $good + $bad;

  $str = "<html><body><pre>\n";
  $str .= "There were $total queues with overdue tickets.  \n\n";
  $str .= "No supervisors were notifed about $bad of them ";
  $str .= "since there is no supervisor for those queues.  \n\n";
  $str .= "The following queues have no supervisor:  \n$q_list\n\n";
  $str .= "$good message(s) were sent to the other supervisors.  \n\n";
  $str .= "Here is the report for all of the queues:\n\n";
  $str .= "</pre>\n";

  foreach($fin_report as $queue => $report) {
    $str .= "<hr>$report</pre></font>\n";
  }
  $str .= "</body></html>";

  $sub = $SYSTEM_NAME . " Overview Report";
  $headers = "Content-type: text/html; charset=iso-8859-1\r\n";
  $headers .= "From: " . $FROM_ADDRESS;
  if ($MAIL_TOP) {
    if (!mail($TOP_MANGER, $sub, $str, $headers)) {
      print "Unable to email top manager (". $TOP_MANGER . ")\n";
    }
  }
}
?>
