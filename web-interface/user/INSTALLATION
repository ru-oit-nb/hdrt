~ Time-stamp:              <2008-01-15 16:16:50 fultonj> 
~ -------------------------------------------------------
HOW TO INSTALL THIS EXTENSION
~ -------------------------------------------------------
Overview:

This extension to ruQueue can be installed in a few relatively simple
steps:

* Install New Web Interface 
* Alter Database
* Set a Cookie to identify the user

The rest of this document will cover the above steps in more detail.

~ -------------------------------------------------------
* Install New Web Interface 

You must have ruQueue already installed in order to use this
extension.  First follow the instructions in the documentation:

 http://ruqueue.rutgers.edu/documentation.php

Take note of section 2.3.6 "Put the Web Interface in the Web Tree" and
pay attention to the path of your web tree.  We will reference the web
tree later.  

Unpack the extenstion:  

 tar xzf user.tar.gz 

You should have a directry called user.  Move this directry into the
web tree so that it is a subdirectory of the ruQueue web interface.
For example if you followed the documentation and installed your web
interface by doing:  

 mv web-interface /var/www/html/ruQueue

then you can install this extension by doing:  

 mv user /var/www/html/ruQueue/

So, if staff members go to:

 http://ruqueue.domain.tld/

to log tickets on behalf of users then users should go to:   

 http://ruqueue.domain.tld/user/

to use this extension to log their own tickets.  

~ -------------------------------------------------------
* Alter Database

If you skip this step then users will be able to place a ticket into
any queue.  The queue description field will appear as a pull down
menu labeled "Problem Category" and the ticket will appear in the
corresponding queue.  It will also be difficult to determine if a 
ticket was created from this extension or by a staff member.  

If you follow this step then only the queues which you specify will be
available for users to enter tickets.  If you only specify one queue
then the user will not even see the "Problem Category" field and
logging a ticket will be simpler.  

To alter the database familiarize yourself with "2.3.3 Install the
ruQueue database in MySQL" from the ruQueue documentation.  If you
installed your database with the following command:  

 mysql -u root -p ruqueue < ruqueue.sql

then you can alter your database for this feature with the following
command:  

 mysql -u root -p ruqueue < user_updates.sql

Note that the user_updates.sql file is located in the user directory
from the previous step.  No harm will come from leaving this file in
the user directory when you are done.  

Running the command above will do the following:

a) alter the queue table to add the user_input field
b) insert a staff member into the staff table

It's important that you understand the above so that you can change 
their values accordingly.  We'll now discuss them in more detail:

a) alter the queue table to add the user_input field 

This has the same effect as doing the following on the MySQL command
line:  

mysql> ALTER TABLE queue ADD user_input INT(1) NOT NULL default '0';
Query OK, 1 row affected (0.02 sec)
Records: 1  Duplicates: 0  Warnings: 0

mysql> 

It is important that you then select the queue which you want users to
be able to add tickets to.  If a queue's user_input field is greater
than zero, then tickets can be added to that queue via this
extension.  To enable this for the test queue use the following
command:  

mysql> UPDATE queue SET user_input=1 WHERE q_name='Test Queue';
Query OK, 1 row affected (0.00 sec)
Rows matched: 1  Changed: 1  Warnings: 0

mysql> 

Simply change the value of the q_name field above to enable this
feature for the desired queue.  

Here are some queries to check your work.  Before enabling this queue:

mysql> SELECT q_name, user_input FROM queue;
+------------+------------+
| q_name     | user_input |
+------------+------------+
| Test Queue |          0 | 
+------------+------------+
1 row in set (0.00 sec)

mysql> 

After enabling this queue:  

mysql> SELECT q_name, user_input FROM queue;
+------------+------------+
| q_name     | user_input |
+------------+------------+
| Test Queue |          1 | 
+------------+------------+
1 row in set (0.00 sec)

mysql> 

Simply change the value of user_input in the queue table with the
update query above to change which queues you want to take advantage
of this feature.  

b) insert a staff member into the staff table

Normally a staff member signs in to create a ticket on behalf of a
user.  This extension also does this on behalf of the user.  So when
it does this, you shoud have a way to answer the question of "who
created this ticket" from a staff point of view (note that we're not
asking "who was the ticket was created for").  Another way to ask this
question is to say "which tickets were created by this extension".
The solution to this problem will be to create a staff member and have
this extension create all of its tickets as this staff member.  Then
when you want to know who used this extension to create his/her ticket
you simply do a search based on this staff member.  

In our case this staff member will be called "user" and sourcing the 
user_updates.sql file has the same effect as running the following:

mysql> INSERT INTO staff VALUES ('user','ruQueue','External
Interface','user@localhost',now(),'','',0,0,'','','','','',
'','','','','','','',0,password(''));

Query OK, 1 row affected (0.00 sec)

mysql>

It is important that you set the email address of this staff member to
a real address.  Tickets are created by a requestor, which is an email
address and ruQueue's situation handlers might send email to this
address.  E.g. if you have a generic Help Desk address that is read my
multiple people in your organization who need to know about updates to
tickets, then you could use that address.  If it was called
help@domain.tld then you would run the following query:

UPDATE staff SET email='help@domain.tld' WHERE username='user';

You can then check your work by running a query like the following:  

mysql> select email from staff where username='user';
+----------------+
| email          |
+----------------+
| user@localhost | 
+----------------+
1 row in set (0.00 sec)

mysql> 

Note that the above should show the email address that you set, not
the localhost email address.  

~ -------------------------------------------------------
* Set a Cookie to identify the user

The first few lines of code in index.php (approximately line 30)
contain sample code which sets a cookie.  Use this example code for
your own Cookie setting functionality and then take this extension out
of testing mode so that it uses your cookie as described below.  

This extension assumes there is already a cookie set in the user's
browser which identifies the user.  E.g. if your network limits access
until a user authenticates via a web interface you might set a cookie
with that web interface so that ruQueue can use that cookie with this
extension.  If you use some other form of authentication that is fine,
you just need to be sure to set this cookie so that the ticket can be
connected to a user.  The information stored in this cookie is then
connected to the ruQueue user table.  

By default this extension is in testing mode and every ticket will be
made for the test user that is added by ruQueue's default
installation.  The ruQueue user table has the following fields (along
with example values) which are set by the test cookie:  

 'name' => "Test User",
 'email' => "test@localhost",
 'uid' => "test",
 'type' => "Faculty",
 'phone' => "(123) 555-1234",
 'fax' => "(123) 555-1234",
 'location' => "Main Campus",
 'address' => "123 Main St New Brunswick, NJ 08901",

The extension will expect your cookie to contain the fields above.
Not all of the fields above are required but you must have a unique
uid for each user since this is what ruQueue uses to look up the user.
The more of these fields you set with the cookie the more useful the
user table will be.  

If you don't have a uid you could use the email address field as it
should also be unique.  If you want this extension to use the email
address field instead of a uid modify the GetUserID function
accordingly.  Some example code for you to comment out is provided.  

To take this extension out of testing mode so that it no longer sets a
test cookie and instead uses your own cookie set the testing variable
to false.  I.e. change the line (approximately line 30) in index.php
from:

 $testing = 1;  

to:

 $testing = 0;  

the extension will then not set a cookie and it is up to you to take
care of setting it.  
