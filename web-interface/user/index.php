<?php
// Copyright (c) 2008, John Fulton (jfulton@member.fsf.org)
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
//    02110-1301  USA
// -------------------------------------------------------
// Filename:                index.php
// Description:             Users create their own tickets
// Supported Langauge(s):   PHP 5.2.x
// Time-stamp:              <2008-01-07 19:44:43 fultonj> 
// -------------------------------------------------------
// Connect to MySQL
require_once "../ruqueue_mysql.php";
// Library to HTML forms
require_once "forms.php";
// Library to send notifications based on queue configuration
require_once "../functions/functions-situation-handlers.php";
// Library to create form for special fields
require_once "../functions/functions-form-objects.php";
// -------------------------------------------------------
// SET VARIABLES 
$testing = 1;  
if ($testing) {  // , then set example cookie
  $cookie_values = array( 
	 'name' => "Test User",
	 'email' => "test@localhost",
	 'uid' => "test",
	 'type' => "Faculty",
	 'phone' => "(123) 555-1234",
	 'fax' => "(123) 555-1234",
	 'location' => "Main Campus",
	 'address' => "123 Main St New Brunswick, NJ 08901",
			  );
  foreach ($cookie_values as $key => $value) {
    setcookie($key, $value);     
  }
} 
// Otherwise set your own cookie elsewhere based on the above
// -------------------------------------------------------
// This array keeps track of where the user is in the program. 
// State will be partially passed in URL such that you can see 
// it by looking at $user_state[$_GET['state']] Note that state 
// can be set directly so the URL could get out of sync.  
$show_state = 0;  // announce state for debugging?
$user_state = array(
	       'Getting Ticket Data', 
	       'Correcting Ticket Data', 
	       'Getting Special Data', 
	       'Correcting Special Data', 
	       'Printing Ticket Receipt', 
		);
if (empty($_GET['state']))
  $_GET['state'] = 0;
// send them back to start if state is wrong
if ($_GET['state'] > (count($user_state) - 1))  {
  $url = $_SERVER['PHP_SELF'] . '?note=nice+try';
  header("Location: $url");
}
// -------------------------------------------------------
// MAIN:
// -------------------------------------------------------
$html = "";

if ($_GET['state'] == 0) {  
  // Idenfity the user (old or new)
  $user_id = GetUserID();  
  // Show that user a form to get to next state (1)
  $html .=  TicketForm($user_id, 1);
}
if ($_GET['state'] == 1) {  
  // Check if the user made a mistake
  $error_msg = GetTicketFormErrors();
  if (!empty($error_msg)) {
    // They did, so tell them how to fix it
    $html .=  $error_msg;
    // Re-display form, user is still in state 1
    $html .=  TicketForm($_POST['user_id'], 1);
  }
  else {  // User filled out form correctly
    // Save ticket without special fields
    $ticket = SaveTicketForm();
    if ($ticket)  // User moves to next state
      $_GET['state'] = 2;
    else
      die("Error:  unable to create ticket.");
  }
}
if ($_GET['state'] == 2) {  // Getting Special Data
  // Check if there is any special data to collect
  // Note that we have $ticket from state 1
  if (!SpecialFieldCount($_POST['queue'])) {
    // No special fields so jump to end 
    $_GET['state'] = 4; 
  }
  else {  
    // Collect the special fields and try to get next state
    $html .= SpecialFieldForm($_POST['user_id'], 3, 
			      $_POST['queue'], $ticket);
  }
}
if ($_GET['state'] == 3) {  // Correcting Special Data
  // Check special fields
  $error_msg = GetSpecialFieldErrors();
  if (!empty($error_msg)) {
    $html .= $error_msg;
    // keep them in this state
    $html .= SpecialFieldForm($_POST['user_id'], 3, 
			      $_POST['queue'], $_POST['ticket']);
  }
  else {
    $_GET['state'] = 4; 
  }
}
if ($_GET['state'] == 4) {  // Printing Ticket Receipt
  // get the value of ticket from either state
  if (empty($ticket)) 
    $ticket = mysql_real_escape_string($_POST['ticket']);
  // Save special fields
  if (SetSpecialFields($ticket)) {
    // Call situation handler to send notifications via scripts
    $sent = @SituationHandler('OnCreate', $ticket, 'user');
    // Show ticket
    $html .= GetReceipt($ticket, $sent);
  }
}
// -------------------------------------------------------
if ($show_state) // show user state for debugging
  print "<h2>" . $user_state[$_GET['state']] . "</h2>\n";
// -------------------------------------------------------
// Render Page
include "header.php";
print $html;
include "footer.php";
// -------------------------------------------------------
// FUNCTIONS

// -------------------------------------------------------
// Function:                GetReceipt

// Description:             Returns HTML with ticket message

// Parameters:
//    [in] int $ticket      The ID of the ticket
//    [boolean] $sent       Status of SituationHandler

// Return Values:           
//    boolean               True if special fields saved
//                          or false if errors

// Remarks:                 
//    None                  
// -------------------------------------------------------

function GetReceipt($ticket, $sent) {
  $html = "";
  $html .= "<div id='message'>\n";
  $html .= "<p />Your request has been submitted.  ";
  $html .= "<p />Your ticket number is: <strong>$ticket</strong>.  ";
  if ($sent) 
    $html .= "<p />Email has been sent to the relevant parties.  ";
  $html .= "<p />Someone will get back to you.  ";
  $html .= "</div>\n";
  return $html;
}


// -------------------------------------------------------
// Function:                SetSpecialFields

// Description:             Checks for errors

// Parameters:
//    [in] int $ticket      The ID of the ticket

// Return Values:           
//    boolean               True if special fields saved
//                          or false if errors

// Remarks:                 
//    None                  
// -------------------------------------------------------

function SetSpecialFields($ticket) {
  // check if they're coming in the wrong way and redirect 
  if (!is_array($_POST['q_specific_fields'])) {
    $url = $_SERVER['PHP_SELF'];
    header("Location: $url");
  }

  $sql = "INSERT INTO q_specific_ticket_value ";
  $sql .= "(ticket_id, field_name, value) VALUES ";
  foreach ($_POST['q_specific_fields'] as $var) {
    $sql .= "('$ticket', '" . mysql_real_escape_string($var) 
      . "', '" . mysql_real_escape_string($_POST[$var]) . "'), ";
  }
  $sql = substr($sql, 0, -2);  // trim last ", "
  $result = mysql_query($sql);
  if (!$result) { 
    // check if they reloaded...
    if (mysql_errno() == '1062') {  // mysql duplicate entry
      // send them back to the start
      $url = $_SERVER['PHP_SELF'];
      header("Location: $url");
    }
  }
  return $result;
}


// -------------------------------------------------------
// Function:                GetSpecialFieldErrors

// Description:             Checks for errors

// Parameters:
//    None                  

// Return Values:           
//    string                HTML with error message
//                          or null-string if no errors

// Remarks:                 
//    If the form is not fully filled out this function
//    will determine this from POST and describe the 
//    problem for the user.  
//
//    Nothing in $_POST['required_list'] should be empty
// -------------------------------------------------------

function GetSpecialFieldErrors() {
  $err = "";

  // Build hash mapping field_name to field_display_name 
  $name_map = array();
  $q_name = mysql_real_escape_string($_POST['queue']);
  $sql  = "SELECT field_name, field_display_name ";
  $sql .= "FROM q_specific_fields ";
  $sql .= "WHERE required='1' and q_name='$q_name'";
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_object($result)) 
    $name_map[$row->field_name] = $row->field_display_name;

  // check if the field that we need is there, else start over
  if (!is_array($_POST['required_list'])) {
    $url = $_SERVER['PHP_SELF'];
    header("Location: $url");
  }

  // See what's missing from the required list    
  foreach ($_POST['required_list'] as $var) {
    if (empty($_POST[$var])) {
      $err .= "  <li>Please provide the ";
      $err .= $name_map[$var];
      $err .= "</li>\n";
    }
  } 

  // Build error string, if applicable
  if (!empty($err)) {
    $msg = "\n<strong>Error</strong>\n";
    $msg .= "<p />We are unable to process your request:\n";
    $err = $msg . "<ul>\n" . $err . "</ul>\n";
    $err = '<div id="error">' . $err . '</div>';
  }
  return $err;
}

// -------------------------------------------------------
// Function:                SpecialFieldForm

// Description:             Form to get special fields

// Parameters:
//    [in] int $user_id     The ID of the user
//    [in] int $next_state  The ID of the next desired
//                          state of user input
//    [in] string $queue    The name of the the queue 
//                          the ticket goes into
//    [in] int $ticket      The ID of the ticket

// Return Values:           
//    string                HTML for form as a string

// Remarks:                 
//    Uses InputSpecialFields() from ruQueue's 
//    form-objects library to figure out and display 
//    the required fields.  
//    
//    The tables provided by the original form-objects 
//    Don't line up like the other form, so there is some 
//    HTML kludging.  
//
//    Save the ticket for later
// -------------------------------------------------------

function SpecialFieldForm($user_id, $next_state, $queue, $ticket) { 
  $url = $_SERVER['SCRIPT_NAME'] . "?state=" . urlencode($next_state);
  $html = "\n" . '<div id="form">' . "\n";
  $html .= StartForm($url, array("method" => "post"));
  $html .= HiddenField("user_id", $user_id);
  $html .= HiddenField("ticket", $ticket);
  $html .= HiddenField("queue", $queue);
  $html .= "<table cellpadding='5' cellspacing='5' border='0'>\n";
  $queue = mysql_real_escape_string($queue);
  $html .= InputSpecialFields($queue);  // main work
  $html .= EndFormTable();
  $html .= StartFormTable();
  for ($i = 0; $i < 40; $i++) 
    $space .= "&nbsp;";
  $html .= FormRow($space, SubmitField("", "Complete Request for Help"));
  $html .= EndFormTable();
  $html .= EndForm();
  $html .= "\n</div>\n";
  return $html;
}


// -------------------------------------------------------
// Function:                SpecialFieldCount

// Description:             Determines if a queue has 
//                          special fields

// Type:                    public

// Parameters:
//    [in] string $queue    The name of the queue

// Return Values:           
//    interger              The number of speical fields
//                          including 0, if none.  

// Remarks:                 
//    Queues can have special fields such that if a ticket 
//    is in them, the normal fields in the ticket and comment 
//    field don't contain all of the information about that 
//    ticket.  E.g. if you move a ticket to the Unix queue 
//    there could be a special field containg the output of 
//    the uname command.  This makes the fields associated 
//    with a queue flexible.  
// -------------------------------------------------------

function SpecialFieldCount($queue) {
  $queue = mysql_real_escape_string($queue);
  $sql = "SELECT COUNT(field_name) AS count ";
  $sql .= "FROM q_specific_fields WHERE q_name='$queue'";
  $result = mysql_query($sql) or die(mysql_error());
  $row = mysql_fetch_object($result);
  return $row->count;
}


// -------------------------------------------------------
// Function:                GetRequester

// Description:             Gets the requester of all tickets
//                          made by this inerface 

// Parameters:
//    None                  

// Return Values:           
//    string                email address

// Remarks:                 
//    Returns the email address of entry in the staff table 
//    with the username of "user".  During installation 
//    it should be set to a real email address.  Tickets are 
//    created by a requestor, which is an email address and 
//    ruQueue's situation handlers might send email to this
//    address.  
// -------------------------------------------------------

function GetRequester() {
  $sql = "SELECT email FROM staff WHERE username='user' LIMIT 1";
  $result = mysql_query($sql) or die(mysql_error());
  $row = mysql_fetch_object($result);
  return $row->email;
}

// -------------------------------------------------------
// Function:                SaveTicketForm

// Description:             Saves data in Ticket form

// Parameters:
//    None                  

// Return Values:           
//    int                   0 if data not saved OR the 
//                          ticket number as positive int

// Remarks:                 
//    Uses the POST array to insert relevant data into 
//    the TICKET, COMMENT and TICKETWATCHER tables.  
//    Insertion into these tables meets minimum requirement 
//    for creating a ticket.  However, if the queue has 
//    special fields that should be addressed elsewhere.  
// 
//    Avoids duplicate ticket entries by returning the 
//    result of searching for the ticket to be inserted.  
// -------------------------------------------------------

function SaveTicketForm() {
  $ticket_id = 0;

  // Extract and protect variables in $_POST
  foreach ($_POST as $key => $value) {
    ${$key} = mysql_real_escape_string($value);
  }
  $requester = GetRequester();  

  // Check if we already have an identical ticket to avoid duplicate
  $sql = "SELECT ticket.id FROM ticket, comment WHERE ";
  $sql .= "ticket.id=comment.ticket_id AND ";
  $sql .= "ticket.queue='$queue' AND ";
  $sql .= "ticket.user_id='$user_id' AND ";
  $sql .= "ticket.requester='$requester' AND ";
  $sql .= "comment.subject='$subject' AND ";
  $sql .= "comment.body='$body' ";
  $sql .= "LIMIT 1";
  $result = mysql_query($sql) or die(mysql_error());
  $row = mysql_fetch_object($result);
  if ($row->id > 0) {
    // we've already got this ticket
    $ticket_id = $row->id;
    print "<center><stong>Warning</strong>: ";
    print "we already seem to have ticket $ticket_id</center>\n<p />";
  }
  else {
    // Insert into the ticket table
    $sql = "INSERT INTO ticket SET ";
    $sql .= "queue='$queue', ";
    $sql .= "user_id='$user_id', ";
    $sql .= "requester='$requester'";
    $result = mysql_query($sql) or die(mysql_error());
    $ticket_id = mysql_insert_id();
    if ($ticket_id > 0) {
      // Insert into the comment table
      $sql = "INSERT INTO comment SET ";
      $sql .= "ticket_id='$ticket_id', ";
      $sql .= "subject='$subject', ";
      $sql .= "body='$body', ";
      $sql .= "ip_address='" . $_SERVER['REMOTE_ADDR'] . "', ";
      $sql .= "status='new', ";
      $sql .= "priority='0', ";
      $sql .= "staff='user'";  // assuming you use this staff member
      $result = mysql_query($sql) or die(mysql_error());
      $comment_id = mysql_insert_id();
      if ($comment_id > 0) {
	// Update the ticket table
	$sql = "UPDATE ticket SET ";
	$sql .= "first_comment_id='$comment_id', ";
	$sql .= "last_comment_id='$comment_id', ";
	$sql .= "date_created=date_created, ";
	$sql .= "status_change_date=status_change_date ";
	$sql .= "WHERE id='$ticket_id'";
	$result = mysql_query($sql) or die(mysql_error());
	// Insert into the watchers table
	if ($result) {
	  $sql = "INSERT INTO ticketwatcher SET ";
	  $sql .= "ticket_id='$ticket_id', ";
	  $sql .= "type='Requester', ";
	  $sql .= "email='$requester'"; 
	  $result = mysql_query($sql) or die(mysql_error());
	}
      }
    }
  }
  return $ticket_id;
}


// -------------------------------------------------------
// Function:                GetTicketFormErrors

// Description:             Checks for errors

// Parameters:
//    None                  

// Return Values:           
//    string                HTML with error message
//                          or null-string if no errors

// Remarks:                 
//    If the form is not fully filled out this function
//    will determine this from POST and describe the 
//    problem for the user.  
// -------------------------------------------------------

function GetTicketFormErrors() {
  $err = "";
  if (empty($_POST['subject'])) 
    $err .= "  <li>Please enter a subject</li>\n";
  if (empty($_POST['body']))
    $err .= "  <li>Please describe the issue</li>\n";
  if (!empty($err)) {
    $msg = "\n<strong>Error</strong>\n";
    $msg .= "<p />We are unable to process your request:\n";
    $err = $msg . "<ul>\n" . $err . "</ul>\n";
    $err = '<div id="error">' . $err . '</div>';
  }
  return $err;
}

// -------------------------------------------------------
// Function:                TicketForm

// Description:             Shows form to create ticket

// Parameters:
//    [in] int $user_id     The ID of the user
//    [in] int $next_state  The ID of the next desired
//                          state of user input

// Return Values:           
//    string                HTML for form as a string

// Remarks:                 
//    If only one queue is available then the user will not 
//    even see the "Problem Category" field and logging a 
//    ticket will be simpler.  
//    
//    The internal ID of the user and possibly the queue are 
//    supplied as hidden fields which will be used later to 
//    create the ticket in the database.    
// -------------------------------------------------------

function TicketForm($user_id, $next_state) {
  $url = $_SERVER['SCRIPT_NAME'] . "?state=" . urlencode($next_state);
  $html = "\n" . '<div id="form">' . "\n";
  $html .= StartForm($url, array("method" => "post"));
  $html .= HiddenField("user_id", $user_id);

  $html .= StartFormTable();
  $html .= FormRow("Subject", 
		   TextField("subject", 
			     stripslashes($_POST['subject']), 40));
  $html .= FormRow("Describe <br />the Issue", 
		   TextareaField("body", 
				 stripslashes($_POST['body']), 
				 40, 8, "hard"));
  $queues = GetQueues();
  switch(count($queues)) {
  case 0:
    die("Error: There are no available queues");
    break;
  case 1:
    $html .= HiddenField("queue", key($queues));
    break;
  default:
    $html .= FormRow("Problem Category", 
		     SelectField("queue", $queues, $_POST['queue']));
  }

  $html .= FormRow("", SubmitField("", "Request Help"));
  $html .= EndFormTable();
  $html .= EndForm();
  $html .= "\n</div>\n";
  return $html;
}

// -------------------------------------------------------
// Function:                GetQueues

// Description:             Gets a list of queues

// Parameters:
//    None                  

// Return Values:           
//    array                 Returns an assoc array of 
//                          queues mapping unique $q_name
//                          to $descrition, ordered by 
//                          $descrition.  

// Remarks:                 
//    Attempts to only return queues which can accept user 
//    input based on the queue table's user_input field.  
//    You must run user_updates.sql to alter the queue table 
//    if you wish to support this feature.  If the queue table 
//    lacks the user_input field, this function  will return 
//    all queues.  
// -------------------------------------------------------

function GetQueues() {
  $queues = array();
  $fields = GetFields('queue');
  $sql = "SELECT q_name, description FROM queue ";
  if (in_array('user_input', $fields))
    $sql .= "WHERE user_input > 0 ";
  $sql .= "ORDER BY description";
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_object($result)) 
    $queues[$row->q_name] = $row->description;
  return $queues;
}

// -------------------------------------------------------
// Function:                GetUserID

// Description:             Based on cookie, gets user.id
//                          from user table.  If user not 
//                          in user table, adds user and 
//                          returns the new id.  

// Parameters:
//    None                  

// Return Values:           
//    interger              The id from the user table

// Remarks:                 
//    Uses uid for lookup, this could be changed to email
// -------------------------------------------------------

function GetUserID() {
  // $email = $_COOKIE['email']; // uid alternative

  $uid = mysql_real_escape_string($_COOKIE['uid']);
  if (empty($uid)) 
    die("Error: unnable to determine user id via cookie");

  // lookup user in local database 
  $sql = "SELECT id FROM user WHERE uid='$uid'";
  $result = mysql_query($sql) or die(mysql_error());
  $row = mysql_fetch_object($result);
  $id = $row->id;
  if (!$id) {  // if user not found, then add
    $fields = GetFields('user');
    $sql = "INSERT INTO user SET ";
    foreach ($_COOKIE as $key => $value) {
      if (in_array($key, $fields))
	$sql .= $key . "='" . mysql_real_escape_string($value) . "', ";
    }
    $sql = substr($sql, 0, -2);  // trim last ", "
    $result = mysql_query($sql) or die(mysql_error());
    $id = mysql_insert_id();
  }
  return $id;
}

// -------------------------------------------------------
// Function:                GetFields

// Description:             Returns array of user table fields

// Parameters:
//    [in] string $table    Table to get fields for

// Return Values:           
//    array                 array of fields

// Remarks:                 
//    None                  
// -------------------------------------------------------

function GetFields($table) {
  $fields = array();
  $sql = "SHOW COLUMNS FROM $table";
  $result = mysql_query($sql) or die(mysql_error());
  while ($row = mysql_fetch_object($result)) 
    array_push($fields, $row->Field);
  return $fields;
}

// -------------------------------------------------------
mysql_close();
?>