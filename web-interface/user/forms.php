<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// -------------------------------------------------------
// Filename:                forms.php
// Description:             
//   This is a set of functions for controlling forms with 
//   PHP.  Most of it comes from Jay Greenspan and Brad Bulgar's 
//   book (MySQL/PHP Database Applications).  
//   It includes modifications for XHTML and imports other 
//   Greenspan/Bulgar dependencies.  
// 
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2004-06-24 17:55:07 jfulton> 
// -------------------------------------------------------- 
// 
// This library is a derivative work of forms.php from the 
// book "MySQL/PHP Database Applications" which has the 
// following in its header:
// 
//  ********************************************************
//  *** This script from MySQL/PHP Database Applications ***
//  ***         by Jay Greenspan and Brad Bulger         ***
//  ***                                                  ***
//  ***   You are free to resuse the material in this    ***
//  ***   script in any manner you see fit. There is     ***
//  ***   no need to ask for permission or provide       ***
//  ***   credit.                                        ***
//  ********************************************************
// 
// -------------------------------------------------------- 
// Function:                StartFormTable

// Description:             Used to line up a form nicely

// Type:                    public

// Parameters:
//    None

// Return Values:           
//    string                The start of an HTML table

// Remarks:                 
//    Uses HTML tables.  Could be updated to use CSS.  
// -------------------------------------------------------- 

function StartFormTable() 
{
  return "<table cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n";
}

// -------------------------------------------------------- 
// Function:                FormRow

// Description:             Shows body of table with actual 
//                          input paramters

// Type:                    public

// Parameters:
//    [in] string $label        label for input 
//    [in] string $form_element HTML input types

// Return Values:           
//    string                An HTML table row

// Remarks:                 
//    Uses HTML tables.  Could be updated to use CSS.  
// -------------------------------------------------------- 

function FormRow($label, $form_element) 
{
  $row  =  "  <tr>\n";
  $row .=  "    <td $align><strong>$label</strong></td>\n";
  $row .=  "    <td>&nbsp;</td>\n";
  $row .=  "    <td>\n$form_element\n    </td>\n";
  $row .=  "  </tr>\n";
  $row .=  "  <tr>\n";
  $row .=  "    <td colspan=\"3\">&nbsp;</td>\n";
  $row .=  "  </tr>\n";
  return $row;
}

// -------------------------------------------------------- 
// Function:                EndFormTable

// Description:             returns HTML to end the table

// Type:                    public

// Parameters:
//    None                  
// Return Values:           
//    string                The end of an HTML table 

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function EndFormTable() 
{
  return "</table>\n";
}

// -------------------------------------------------------- 
// Function:                DateForm

// Description:             returns form for nice date selection

// Type:                    public

// Parameters:
//    (in) string $var_name The variable name for this input type. 
//                          This name will be split into three parts for M-D-Y
//                          to be put back together later 
//                          (Default: "")

//    (in) array $input     An assoc array of default values
//                          This array would normally be $_GET 
//                          or $_POST to make the form represent 
//                          existing values 
//                          (Default: $_GET)

//    (in) int $start_year  The first year date for the pull-down
//                          (Default: "1950")

//    (in) int $end_year    The last year date for the pull-down
//                          which gets added to the current yearn
//                          e.g. if you want 5 years in advance 
//                          pass 5 as this argument
//                          (Default: "0")

// Return Values:           
//    string               The HTML of the form 

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function DateForm($var_name="", $input="", $start_year="1950", $end_year="0") 
{
  $months = array(
		  "0" => "Month", 
		  "1" => "January", 
		  "2" => "February", 
		  "3" => "March", 
		  "4" => "April", 
		  "5" => "May", 
		  "6" => "June", 
		  "7" => "July", 
		  "8" => "August", 
		  "9" => "September", 
		  "10"=> "October", 
		  "11" => "Novermber", 
		  "12" => "December"
		  );

  $days = array();
  $days[0] = "Day";
  for ($i = 1; $i <= 31; $i++) {
    $days[$i] = $i;
  }

  $years = array();
  $years[0] = "Year";
  $date = date("Y") + $end_year;
  for ($i = $start_year; $i <= $date; $i++) {
    $years[$i] = $i;
  }
  
  $date_partition = array(
		    $var_name . "_mo" => $months, 
		    $var_name . "_da" => $days, 
		    $var_name . "_yr" => $years
		    );

  if (empty($input)) $input = $_GET;

  if (!isset($input[$var_name . "_mo"]) && isset($input[$var_name])) {
    list($input[$var_name."_yr"], $input[$var_name."_mo"], $input[$var_name."_da"]) =
      explode('-', $input[$var_name]);
  }
  
  $str = "";
  foreach ($date_partition as $slice => $arr) {
    $str .= "<select name=\"$slice\">\n";
    foreach($arr as $key => $value) {
      if ($input["$slice"] == $key) {
	$str .= "  <option selected value=\"$key\">$value</option>\n";
      }
      else {
	$str .= "  <option value=\"$key\">$value</option>\n";
      }
    }
    $str .= "</select>\n";
  }
  return $str;
}

// -------------------------------------------------------- 
// Function:                GetStates

// Description:             Returns an assoc array maping US 
//                          state abberviations to state names

// Type:                    public

// Parameters:
//    None

// Return Values:           
//    array                 assoc array of states 

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function GetStates() {
  return array(
	       ''  =>'Other', 
	       'AL'=>'Alabama',
	       'AK'=>'Alaska',
	       'AZ'=>'Arizona',
	       'AR'=>'Arkansas',
	       'CA'=>'California',
	       'CO'=>'Colorado',
	       'CT'=>'Connecticut',
	       'DE'=>'Delaware',
	       'DC'=>'District of Columbia',
	       'FL'=>'Florida',
	       'GA'=>'Georgia',
	       'HI'=>'Hawaii',
	       'ID'=>'Idaho',
	       'IL'=>'Illinois',
	       'IN'=>'Indiana',
	       'IA'=>'Iowa',
	       'KS'=>'Kansas',
	       'KY'=>'Kentucky',
	       'LA'=>'Louisiana',
	       'ME'=>'Maine',
	       'MD'=>'Maryland',
	       'MA'=>'Massachusetts',
	       'MI'=>'Michigan',
	       'MN'=>'Minnesota',
	       'MS'=>'Mississippi',
	       'MO'=>'Missouri',
	       'MT'=>'Montana',
	       'NE'=>'Nebraska',
	       'NV'=>'Nevada',
	       'NH'=>'New Hampshire',
	       'NJ'=>'New Jersey',
	       'NM'=>'New Mexico',
	       'NY'=>'New York',
	       'NC'=>'North Carolina',
	       'ND'=>'North Dakota',
	       'OH'=>'Ohio',
	       'OK'=>'Oklahoma',
	       'OR'=>'Oregon',
	       'PA'=>'Pennsylvania',
	       'RI'=>'Rhode Island',
	       'SC'=>'South Carolina',
	       'SD'=>'South Dakota',
	       'TN'=>'Tennessee',
	       'TX'=>'Texas',
	       'UT'=>'Utah',
	       'VT'=>'Vermont',
	       'VA'=>'Virginia',
	       'WA'=>'Washington',
	       'WV'=>'West Virginia',
	       'WI'=>'Wisconsin',
	       'WY'=>'Wyoming'
	       );
}

// -------------------------------------------------------- 
// The rest of the functions are from Greenspan/Bulgar 
// -------------------------------------------------------- 
// Function:                GetAttList

// Description:             Converts assoc arrays into HTML

// Type:                    public

// Parameters:
//    [in] array $atts      An associative array of attributes 
//                          (Default: "")

//    [in] array $defaults  An associative array of default attributes
//                          (Default: "")

// Return Values:           
//    string                The generated HTML 

// Remarks:                 
//   This function will take an associative array and format as a string
//   that looks like 'name1="value1" name2="value2"', as is used by HTML tags.
//   Values for keys in the first argument will override values for the
//   same keys in the second argument. (For example, if $atts is (color=>red)
//   and $defaults is (color=>black, size=3), the resulting output will
//   be 'color="red" size="3"'.)
// -------------------------------------------------------- 
 
function GetAttList ($atts="",$defaults="")
{
  $localatts = array();
  $attlist = "";

  if (is_array($defaults)) { $localatts = $defaults; }
  if (is_array($atts)) { $localatts = array_merge($localatts, $atts); }

  while (list($name,$value) = each($localatts))
    {
      if ($value == "") { $attlist .= "$name "; }
      else { $attlist .= "$name=\"$value\" "; }
    }
  return $attlist;
}

// -------------------------------------------------------- 
// Function:                StartForm

// Description:             Starts an HTML form

// Type:                    public

// Parameters:
//   [in] string $action    The HTML action value
//   [in] array $attributes Assoc array of attributes for the form

// Return Values:           
//    string                The HTML to start a form

// Remarks:                 
//   This function returns an HTML <form> tag. If the first argument
//   is empty, the value of the global Apache variable SCRIPT_NAME
//   is used for the 'action' attribute of the <form> tag. Other
//   attributes for the form can be specified in the optional second
//   argument; the default method of the form is "post".

//   The behavior of this function on servers other than Apache is
//   not known. It's likely that it will work, as SCRIPT_NAME is
//   part of the CGI 1.1 specification.
// -------------------------------------------------------- 

function StartForm ($action="", $atts="")
{
	if (empty($action)) { $action = $_SERVER['SCRIPT_NAME']; }

	$attlist = GetAttList($atts,array("method"=>"post"));
	$output = <<<EOQ
<form action="$action" $attlist>\n
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                EndForm

// Description:             Ends an HTML form

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    string                HTML closing form tag

// Remarks:                 
//    This function returns an HTML </form> tag.
// -------------------------------------------------------- 

function EndForm ()
{
	$output = <<<EOQ
\n</form>
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                TextField

// Description:             Returns an HTML text input field

// Type:                    public

// Parameters:

//    (in) string $name     HTML name attribute
//                          (Default: "")

//    (in) string $value    HTML value attribute
//                          (Default: "")

//    (in) int $size        HTML size attribute
//                          (Default: "10")

//    (in) int $maxlen      HTML maxlength attribute
//                          (Default: "")

// Return Values:           
//    string                The HTML of a text field 

// Remarks:                 
//   This function returns an HTML text input field. The default size
//   of the field is 10. A value and maximum data length for the field
//   may be supplied.
// -------------------------------------------------------- 

function TextField ($name="", $value="", $size=10, $maxlen="")
{
	$maxatt = empty($maxlen) ? "" : "maxlength=\"$maxlen\"";
	$value = str_replace( '"', '&quot;', $value );
	$output = <<<EOQ
<input type="text" name="$name" value="$value" size="$size" $maxatt />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                TextareaField

// Description:             Creates HTML textarea

// Type:                    public

// Parameters:
//    (in) string $name     HTML textarea name attribute
//                          (Default: "")

//    (in) string $value    HTML textarea value attribute
//                          (Default: "")

//    (in) int $cols        HTML textarea columns attribute
//                          (Default: "50")

//    (in) int $rows        HTML textarea rows attribute
//                          (Default: "10")

//    (in) string $wrap     HTML textarea wrap attribute
//                          (Default: "soft")

// Return Values:           
//    string                The HTML of a text field 

// Remarks:                 
//   This function returns an HTML textarea field. The default size is
//   50 columns and 10 rows, and the default wrap mode is 'soft', which means 
//   no hard newline characters will be inserted after line breaks in what
//   the user types into the field. The alternative wrap mode is 'hard',
//   which means that hard newlines will be inserted.
// -------------------------------------------------------- 

function TextareaField ($name="", $value="", $cols=50, $rows=10, $wrap="soft")
{
	$output = <<<EOQ
<textarea name="$name" cols="$cols" rows="$rows" wrap="$wrap">$value</textarea>
EOQ;
	return $output;
}


// -------------------------------------------------------- 
// Function:                PasswordField

// Description:             Creates HTML password input field

// Type:                    public

// Parameters:
//    (in) string $name     HTML password input name attribute
//                          (Default: "")

//    (in) string $value    HTML password input value attribute
//                          (Default: "")

//    (in) int $size        HTML password input size attribute
//                          (Default: "10")

//    (in) int $maxlen      HTML password input maximum length attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for password field 

// Remarks:                 
//   This function returns an HTML password field. This is like a text field,
//   but the value of the field is obscured (only stars or bullets are visible
//   for each character).  The default size of the field is 10.  A starting
//   value and maximum data length may be supplied.
// -------------------------------------------------------- 

function PasswordField ($name="", $value="", $size=10, $maxlen="")
{
	$output = <<<EOQ
<input type="password" name="$name" value="$value" size="$size" maxlength="$maxlen" />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                HiddenField

// Description:             Creates HTML hidden input field

// Type:                    public

// Parameters:
//    (in) string $name     HTML hidden input name attribute
//                          (Default: "")

//    (in) string $value    HTML hidden input value attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for hidden field 

// Remarks:                 
//   This function returns an HTML hidden field. A value may be supplied.
// -------------------------------------------------------- 

// string hidden_field ([string name [, string value]])

function HiddenField ($name="", $value="")
{
	$output = <<<EOQ
<input type="hidden" name="$name" value="$value" />
EOQ;
	return $output . "\n";
}

// -------------------------------------------------------- 
// Function:                FileField

// Description:             Creates HTML file input field

// Type:                    public

// Parameters:
//    (in) string $name     HTML file input name attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for hidden field 

// Remarks:                 
//   This function returns an HTML file field. These are used to specify
//   files on the user's local hard drive, typically for uploading as
//   part of the form. (See http://www.zend.com/manual/features.file-upload.php
//   for more information about this subject.)
// -------------------------------------------------------- 

function FileField ($name="")
{
	$output = <<<EOQ
<input type="file" name="$name" />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                SubmitField

// Description:             Creates HTML file submit field

// Type:                    public

// Parameters:
//    (in) string $name     HTML submit input name attribute
//                          (Default: "")

//    (in) string $value    HTML submit input value attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for submit field 

// Remarks:                 
//   This function returns an HTML submit field. The value of the field
//   will be the string displayed by the button displayed by the user's
//   browser. The default value is "Submit".
// -------------------------------------------------------- 

function SubmitField ($name="", $value="")
{
	if (empty($value)) { $value = "Submit"; }

	$output = <<<EOQ
<input type="submit" name="$name" value="$value" />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                ImageField

// Description:             Creates HTML file image field

// Type:                    public

// Parameters:
//    (in) string $name     HTML image input name attribute
//                          (Default: "")

//    (in) string $src      The path to the image
//                          (Default: "")

//    (in) string $value    HTML image input value attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for image field 

// Remarks:                 
//   This function returns an HTML image field. An image field works
//   likes a submit field, except that the image specified by the URL
//   given in the second argument is displayed instead of a button.
// -------------------------------------------------------- 

function ImageField ($name="", $src="", $value="")
{
	if (empty($value)) { $value = $name; }

	$output = <<<EOQ
<input type="image" name="$name" value="$value" src="$src" />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                ResetField

// Description:             Creates HTML reset field

// Type:                    public

// Parameters:
//    (in) string $name     HTML reset input name attribute
//                          (Default: "reset")

//    (in) string $value    HTML reset input value attribute
//                          (Default: "Reset")

// Return Values:           
//    string                HTML for reset field 

// Remarks:                 
//   This function returns an HTML reset field. A reset field returns
//   the current form to its original state.
// -------------------------------------------------------- 

function ResetField ($name="reset", $value="Reset")
{
	$output = <<<EOQ
<input type="reset" name="$name" value="$value" />
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                CheckboxField

// Description:             Creates HTML checkbox field

// Type:                    public

// Parameters:
//    (in) string $name     HTML checkbox input name attribute
//                          (Default: "")

//    (in) string $value    HTML checkbox input value attribute
//                          (Default: "")

//    (in) string $label    HTML checkbox input label attribute
//                          (Default: "")

//    (in) string $match    HTML checkbox input match attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for checkbox field 

// Remarks:                 
//   This function returns an HTML checkbox field. The optional third argument
//   will be included immediately after the checkbox field, and the pair
//   is included inside a HTML <nobr> tag - meaning that they will be
//   displayed together on the same line.  If the value of the
//   second or third argument matches that of the fourth argument,
//   the checkbox will be 'checked' (i.e., flipped on).
// -------------------------------------------------------- 

function CheckboxField ($name="", $value="", $label="", $match="")
{
  if (is_array($match)) {
    foreach ($match as $key => $val) {
      if ($value == $val) {
	$checked = "checked";
	break;
      }
    }
  }
  else {
    $checked = ($value == $match || $label == $match) ? "checked" : "";
  }
  
  $output = <<<EOQ
<input type="checkbox" name="$name" value="$value" $checked /> $label
EOQ;
  return $output;
}

// -------------------------------------------------------- 
// Function:                RadioField

// Description:             Creates HTML radio field

// Type:                    public

// Parameters:
//    (in) string $name     HTML radio input name attribute
//                          (Default: "")

//    (in) string $value    HTML radio input value attribute
//                          (Default: "")

//    (in) string $label    HTML radio input label attribute
//                          (Default: "")

//    (in) string $match    HTML radio input match attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for radio field 

// Remarks:                 
//   This function returns an HTML radio button field. The optional third 
//   argument will be included immediately after the radio button, and the 
//   pair is included inside a HTML <nobr> tag - meaning that they will be
//   displayed together on the same line.  If the value of the
//   second or third argument matches that of the fourth argument,
//   the radio button will be 'checked' (i.e., flipped on).
// -------------------------------------------------------- 

function RadioField ($name="", $value="", $label="", $match="")
{
	$checked = ($value == $match || $label == $match) ? "checked" : "";
	$output = <<<EOQ
<input type="radio" name="$name" value="$value" $checked /> $label
EOQ;
	return $output;
}

// -------------------------------------------------------- 
// Function:                SelectField

// Description:             Creates HTML select field

// Type:                    public

// Parameters:
//    (in) string $name     HTML select input name attribute
//                          (Default: "")

//    (in) array $array     An array of default items that will be 
//                          checked  
//                          (Default: "")

//    (in) string $value    HTML select input default value attribute
//                          (Default: "")

// Return Values:           
//    string                HTML for select field 

// Remarks:                 
//   This function returns an HTML select field (a popup field).
//   If the optional second argument is an array, each key in the array
//   will be set to the value of an option of the select field, and
//   the corresponding value from the array will be the displayed string
//   for that option. If the key or the value from the array matches
//   the optional third argument, that option will be designated as the 
//   default value of the select field.
// -------------------------------------------------------- 

function SelectField ($name="", $array="", $value="")
{
	$output = <<<EOQ
<select name="$name">\n
EOQ;
	if (is_array($array))
	{
		while (list($avalue,$alabel) = each($array))
		{
			$selected = ($avalue == $value || $alabel == $value) ? 
				'selected="selected"' : ""
			;
			$output .= <<<EOQ
  <option value="$avalue" $selected>$alabel</option>\n
EOQ;
		}
	}
	$output .= <<<EOQ
</select>
EOQ;
	return $output;
}

?>