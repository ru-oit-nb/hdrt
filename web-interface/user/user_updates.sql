-- Filename:                user_updates.sql
-- Description:             Updates to DB for new feature
-- Supported Langauge(s):   MySQL 5.0
-- Time-stamp:              <2008-01-15 15:57:55 fultonj> 
-- -------------------------------------------------------

-- Alters the queue table to store whether or not a queue is 
-- enabled to support this feature.  

ALTER TABLE queue ADD user_input INT(1) NOT NULL default '0';

-- Inserts into the staff table a staff member to represent 
-- this form.  

INSERT INTO staff VALUES ('user','ruQueue','External Interface','user@localhost',now(),'','',0,0,'','','','','','','','','','','','',0,password(''));

