<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: lookup_netid.php
// Description: Looks up a user by netid.
//              Proceeds via LDAP, then the database.
// Supprted Language(s):   PHP 4.0
//

// Required Files
require_once("functions/functions-getters.php");
require_once("functions/functions-ldap.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-forms.php");
require_once("functions/ru_ldap_auth.php");
require_once("header.php");
$output = Head("Lookup the user by NetID", "$username");
$uid = $_POST["uid"];
if (empty($uid)) $uid = $_GET["uid"];

if (!empty($uid)) {
   $oSearch = new RutgersLDAPAuth;
   $oSearch->Connect();
   $ldap_query = "uid=$uid";
   $result_array = array();
   $oSearch->Search(&$result_array, $LDAP_ROOT_DN[$SERVER_ID],$ldap_query);
   $iid = strtoupper(LdapPrintAll($result_array, "rutgerseduiid"));

   if ($iid != "&NBSP;") {
     //$query = "Select id from user where iid='$iid' order by id limit 1";
     //$id = GetFieldFromQuery($query, "id");
     $query = "SELECT name, email, iid, uid, phone, fax, location, address, type, rcpid, ruservice, ruenable, rudisable FROM user WHERE iid='$iid' LIMIT 1";
     $result = mysql_query($query);
     if (mysql_num_rows($result) > 0) {
       $user_info = mysql_fetch_object($result);
       $result_array[0]["cn"][0] = $user_info->name;
       $result_array[0]["mail"][0] = $user_info->email;
       $result_array[0]["rutgerseduiid"][0] = $user_info->iid;
       $result_array[0]["uid"][0] = $user_info->uid;
       $result_array[0]["telephonenumber"][0] = $user_info->phone;
       $result_array[0]["facsimiletelephonenumber"][0] = $user_info->fax;
       $result_array[0]["postaladdress"][0] = $user_info->address;
       $result_array[0]["employeetype"][0] = $user_info->type;
       $result_array[0]["rcpid"][0] = $user_info->type;
       $result_array[0]["ruservice"][0] = $user_info->ruservice;
       $result_array[0]["rudisable"][0] = $user_info->rudisable;
       $result_array[0]["location"][0] = $user_info->location;
     }
      $output .= OpenColorTable("green", "Lookup User by NetID", "100%")
              .LdapPrintResultList($result_array, $user, $queue)
              .CloseColorTable();
   } else {
      $output .= OpenColorTable("green", "Lookup User by NetID", "100%")
              ."No User found with the NetID of "
              .FontBold($uid)
              .".<br> Click <a class=main href=\"lookup_name.php?user=$uid\">here</a>"
              ." to lookup by last name.<br><br>"
              ."Please fill in user's netid.<br></td></tr><tr>"
              .StartFormTable()
              .StartForm($action, array("method" => "get"))
              .FormRow("&nbsp;User's NetID:",TextField("uid", $uid))
              .FormRow("",SubmitField())
              .EndForm()
              .CloseColorTable();
   }

}
else {
      $output .= OpenColorTable("green", "Lookup User by NetID", "100%")
              ."Please fill in user's netid.<br></td></tr><tr>"
              .StartFormTable()
              .StartForm($action, array("method" => "get"))
              .FormRow("&nbsp;User's NetID:",TextField("uid"))
              .FormRow("",SubmitField())
              .EndForm()
              .CloseColorTable()."<br>";
}   
$output .= Foot();
print $output;
?>
