<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-form-objects.php");
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-ldap.php");
require_once("functions-ticket-static.php");
require_once("functions-widgets.php");
require_once("functions-index.php");
//
// Filename: functions-old-ticket.php
// Description: Contains functions for displaying and updating old tickets.
// Supprted Language(s):   PHP 4.0
//
global $self;
$self = $PHP_SELF;

//-----------------------------------------------------------------------------
//
// Function: ShowTicketUser
// Description:
//    Generates HTML to display info about the user.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ShowTicketUser($id) {
   global $db;
   $output = OpenColorTable("blue", "User", "100%");
   $query = "select queue, user_id from ticket where id='$id'";
   $result = mysql_query($query, $db);
   if (!$result) $output .= "Internal Error: <b>$query</b> -->no results";

   else {
      $row = mysql_fetch_array($result);
      $queue = $row["queue"];
      $user_id = $row["user_id"];
      if (empty($user_id)) $output .= "There is no user associated with this ticket.";

      else {
         $query = "select * from user where id='$user_id'";
         $result = mysql_query($query, $db);
         if (!$result) $output .= "Internal Error: <b>$query</b>-->no results";

         else {
            $row = mysql_fetch_array($result);
            $cn = $row["name"];
            $iid = $row["iid"];
            $email = $row["email"];
            $uid = $row["uid"];
            $type = $row["type"];
            $phone = $row["phone"];
            $fax = $row["fax"];
            $location = $row["location"];
            $addr = $row["address"];
            $rcpid = $row["rcpid"];
            $objclass = "";
            $ruservice = $row["ruservice"];
            $ruenable = $row["ruenable"];
            $rudisable = $row["rudisable"];
            $id = $id;
            $edit = "1";
            $queue = $queue;
            $output .= LdapShowForm($cn, $iid, $email, $uid, $type, $phone, $fax, 
                                    $location, $addr, $rcpid, $objclass, $ruservice,
                                    $ruenable, $rudisable, $user_id, $edit, $queue );
         }
      }
   }
   $output .= CloseColorTable();
   return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: OldTicketDisplay
// Description:
//    Generates HTML to display information about a ticket.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function OldTicketDisplay($id) {
   global $SCHEDULING;
   $output = "<table border=0 width=\"100%\"><tr valign=\"top\"><td width=\"50%\">"
           .OpenColorTable("red", "The Basics", "100%")
           .TicketStaticBasics($id)
           .CloseColorTable()."<p>".OpenColorTable("yellow", "People", "100%")
           .TicketStaticPeople($id)
           .CloseColorTable()
           .TicketStaticSpecialFields($id,$ticket_row->queue)
           ."<td>".OpenColorTable("blue", "Dates", "100%")
           .TicketStaticDates($id)
           .CloseColorTable()."<br>";
   if ($SCHEDULING) $output .= TicketStaticSchedule($id);
   $output .= OpenColorTable("green", "User", "100%")
           .TicketStaticUser($id)
           .CloseColorTable()."</td></tr></table>"
           .TicketHistory($id);
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: TicketHistory
// Description:
//    Generates HTML to display the comment history of a ticket.
// Parameters:
//    int $id	Ticket ID.
//
// Return Values:
//    HTML to display.
//
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function TicketHistory($id) {
   global $self, $db, $username;
   $output = "<table border=0 width=\"100%\"><tr valign=\"top\"><td width=\"100%\">"
             .OpenColorTable("blue", "History", "100%")
             ."<table width=\"100%\" border=0 cellpadding=3>";
   $decode_html = GetArrayFromTable("show_html_in_comments", "staffprefs where username='$username'");
   $color_arr = array("#FFFFFF", "#DCDCDC");
   $i = 0;
   $query = "select status, staff, subject, body, ";
   $query .= "date_format(date_created, '%a %M %e, %Y at %h:%i %p') ";
   $query .= "from comment where ticket_id='$id' order by date_created";
   $result = mysql_query($query, $db);
   while ($row = mysql_fetch_array($result)) {
      $status = $row["status"];
      $staff = $row["staff"];
      $subject = $row["subject"];
      $body = $row["body"];
      if ($decode_html[0]) {
	$subjectdisplay = htmlentities($subject);
        $contentdisplay = htmlentities($body);
      }
      else {
        $subjectdisplay = $subject;
        $contentdisplay = $body;
      }
      $date = $row["date_format(date_created, '%a %M %e, %Y at %h:%i %p')"];
      $color = $color_arr[$i++ % 2];
      $output .= "<tr><td bgcolor=\"$color\"><font size=-2>$date</font></td>"
              ."<td bgcolor=\"$color\" align=\"left\">"
              ."<b>$staff&nbsp;-&nbsp;$subjectdisplay</b>"
              ."</td><td bgcolor=\"$color\">Status: &nbsp;$status"
              ."</td><td bgcolor=\"$color\" align=\"right\">"
              ."<font size=-1>&nbsp;"
              ."[<a class=\"main\" href=\"$self?id=$id&menu=Reply\">Reply</a>]&nbsp;"
              ."[<a class=\"main\" href=\"$self?id=$id&menu=Comment\">Comment</a>]&nbsp;"
              ."</font></td></tr><tr><td bgcolor=\"$color\" colspan=4 valign=\"top\">"
              ."<pre>$contentdisplay</pre></td></tr>";
   }
   $output .= "</table>".CloseColorTable()."</td></tr></table>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketBasics
// Description:
//    Generates HTMl to display the basics of a ticket.
// Parameters:
//    int $id		Ticket ID.
//    string $string	Determines style of output.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketBasics($id, $string = "") {
   global $username, $allqueues, $username, $menu;
   $ticket_row = GetFirstRowFromTable("ticket where id = '$id'");
   $output = OpenColorTable("blue", "Modify ticket #$id", "100%");
   if ($string == "") $output .= "<form>";
   $output .= "<table>"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"basics\">"
	   ."<input type=\"hidden\" name=\"id\" value=\"$id\">";
   if (DoesUserHaveRight($username, "DeleteTicket", "", $id)) {
      $output .= OpenTable("Delete")."<input type=\"checkbox\" name=\"delete\">"
              .Font("(<i>Are you sure you want to do this?</i>)")
              .CloseTable();
   }
   $output .= OpenTable("Subject")
           ."<input type=\"text\" size=33 name=\"subject\" value=\""
           .htmlentities(GetOriginalSubjectOfTicket($ticket_row->id))."\">"
           .CloseTable();
   $status = GetCurrentStatusOfTicket($id);
   $output .= OpenTable("Status")
           ."<select name=\"status\"><option value=\"$status\">$status</option>"
           .CreateSelectOptions(GetStatus())
           ."</select>".CloseTable().OpenTable("Time Worked")
           ."<input type=\"text\" size=8 name=\"time_worked\" value=\""
           .GetTimeWorked($id)."\">".CloseTable().OpenTable("Time Left")
           ."<input type=\"text\" size=8 name=\"time_left\" value=\"$ticket_row->time_left\">"
           .CloseTable().OpenTable("Priority")
           ."<input type=\"text\" size=8 name=\"priority\" value=\""
           .GetCurrentPriorityOfTicket($id)."\">"
           .CloseTable().OpenTable("Final Priority")
           ."<input type=\"text\" size=8 name=\"final_priority\" value=\"$ticket_row->final_priority\">"
           .CloseTable().OpenTable("Queue")
           ."<select name=\"queue\"><option value =\"$ticket_row->queue\">"
           ."$ticket_row->queue</option>";

   if (!empty($allqueues)) {
      $output .= CreateSelectOptions(GetQueue())."</select>";
      $output .= "&nbsp;<a href=\"".$_SERVER['SCRIPT_NAME']."?menu=$menu&id=$id\" class=\"main\">Show Limited Queues</a>";
   }
   else {
      $output .= CreateSelectOptions(GetQueuesUserCanSee($username))."</select>";
      $output .= "&nbsp;<a href=\"".$_SERVER['SCRIPT_NAME']."?menu=$menu&allqueues=show&id=$id\" class=\"main\">Show All Queues</a>";
   }
   $output .= CloseTable()."</table>";

   if (empty($string)) {
      $output .= "<p align=\"right\">";

      if (DoesUserHaveRight($username, "ModifyTicket", $ticket_row->queue, $id)){
         $output .= "<input type=\"submit\" value=\"Save Changes\"></form>";
      }
   }
   $output .= CloseColorTable();
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: TicketSpecialFields
// Description:
//    Generates HTMl to display special fields.
// Parameters:
//    int $id	Ticket ID.
//    string $string	Determines style of output.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketSpecialFields($id, $string = "") {
   global $username;
   $ticket_row = GetFirstRowFromTable("ticket where id = '$id'");
   $output = OpenColorTable("blue", "Modify values of special (custom) fields for ticket #$id", "100%");
   if ($string == "") $output .= "<form>";
   $ticket_id = $id;
   $queue = $ticket_row->queue;
   $output .="<table><input type=\"hidden\" name=\"id\" value=\"$id\">"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Special_Fields\">"
           .InputSpecialFields($queue, $ticket_id)."</table>";
             
   if (empty($string)) {
      $output .= "<p align=\"right\">";

      if (DoesUserHaveRight($username, "ModifyTicket", $ticket_row->queue, $id)){
	$jscript = "onClick=\"enable()\"";
	$output .= "<input type=\"submit\" $jscript value=\"Save Changes\">";
      }
      $output .= "</form>";
   }
   $output .= CloseColorTable();
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: TicketDates
// Description:
//    Generates HTMl to display ticket dates.
// Parameters:
//    int $id	Ticket ID.
//    string $string	Determines style of output.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketDates($id, $string = "") {
   global $username;
   $ticket_row = GetFirstRowFromTable("ticket where id='$id'");
   $output = OpenColorTable("blue", "Modify dates for ticket #$id", "100%");
   if ($string == "") $output .= "<form>";
   $output .= "<table>".OpenTable("Format")."YYYY-MM-DD"
           .CloseTable().OpenTable("Starts")
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"dates\">"
           ."<input type=\"hidden\" name=\"id\" value=\"$id\">"
           ."<input type=\"text\" size=16 name=\"starts\" value=\"$ticket_row->starts\">"
           .CloseTable().OpenTable("Started")
           ."<input type=\"text\" size=16 name=\"started\" value=\"$ticket_row->started\">"
           .CloseTable().OpenTable("Last Contact")
           ."<input type=\"text\" size=16 name=\"last_contact\" value=\"$ticket_row->last_contact\">"
           .CloseTable().OpenTable("Due")
           ."<input type=\"text\" size=16 name=\"due\" value=\"$ticket_row->due\">"
           .CloseTable()."</table>";

   if (empty($string)) {
      $output .= "<p align=\"right\">";

      if (DoesUserHaveRight($username, "ModifyTicket", $ticket_row->queue, $id)){
         $output .= "<input type=\"submit\" value=\"Save Changes\">";
      }
      $output .= "</form >";
   }
   $output .= CloseColorTable();
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: TicketPeople
// Description:
//    Generates HTML to display ticket people.
// Parameters:
//    int $id	Ticket ID.
//    string $string	Determines style of output.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketPeople($id, $string = "") {
   global $username;
   $ticket = GetTicket($id);
   $output = OpenColorTable("blue", "Modify people related to ticket #$id", "100%");
   if ($string == "") $output .= "<form>";
   $output .= "<input type=\"hidden\" name=\"form_submitted\" value=\"people\">"
           ."<input type=\"hidden\" name=\"id\" value=\"$id\">"
           ."<table cellpadding=2 width=\"100%\"><tr><td><table><tr>"
           ."<td valign=\"top\"><h3>New watchers</h3>"
           .Spacer(2)."Add new watchers:<br><table><tr><td>Type</td><td>"
           ."Email</td><td></td><td>Username</td></tr><tr><td>"
           ."<select name=\"WatcherTypeEmail[0]\">"
           ."<option value=\"none\">-</option>"
           ."<option value=\"Requester\">Requester</option>"
           ."<option value=\"Cc\">Cc</option>"
           ."<option value=\"AdminCc\">AdminCc</option>"
           ."</select></td><td>"
           ."<input name=\"WatcherAddressEmail[0]\" size=15>"
           ."</td><td> or</td><td>" 
           ."<select NAME=\"WatcherUsername[0]\">"
           ."<option value=\"Nobody\"> </option>"
           .CreateSelectOptions(GetStaff())
           ."</select></tr><tr><td>"
           ."<select name=\"WatcherTypeEmail[1]\">"
           ."<option value=\"none\">-</option>"
           ."<option value=\"Requester\">Requester</option>"
           ."<option value=\"Cc\">Cc</option>"
           ."<option value=\"AdminCc\">AdminCc</option>"
           ."</select></td><td>"
           ."<input name=\"WatcherAddressEmail[1]\" size=15>"
           ."</td><td> or</td><td>" 
           ."<select name=\"WatcherUsername[1]\">"
           ."<option value=\"Nobody\"> </option>"
           .CreateSelectOptions(GetStaff())
           ."</select></tr><tr><td>"
           ."<select name=\"WatcherTypeEmail[2]\">"
           ."<option value=\"none\">-</option>"
           ."<option value=\"Requester\">Requester</option>"
           ."<option value=\"Cc\">Cc</option>"
           ."<option value=\"AdminCc\" >AdminCc</option>"
           ."</select></td><td>"
           ."<input name=\"WatcherAddressEmail[2]\" size=15>"
           ."</td><td> or</td><td>"
           ."<select name=\"WatcherUsername[2]\">"
           ."<option value=\"Nobody\"> </option>"
           .CreateSelectOptions(GetStaff())
           ."</select></tr></table></td><td valign=\"top\"><h3>Owner</h3>"
           ."Owner: <select name=\"Owner\">"
           ."<option value=\"$ticket->owner\">$ticket->owner</option>"
           .CreateSelectOptions(GetStaff())
           ."</select><h3>Current watchers</h3>"
           ."(Check box to delete)<br>Requesters:"
           .PrintCheckboxArray("DelTicketWatchersQueryArray1", GetTicketWatchers($id, "Requester"))
           ."Cc:"
           .PrintCheckboxArray("DelTicketWatchersQueryArray2", GetTicketWatchers($id, "Cc"))
           ."Administrative Cc:"
           .PrintCheckboxArray("DelTicketWatchersQueryArray3", GetTicketWatchers($id, "AdminCc"))
           ."</td></tr></table></td></tr></table>";

   if (empty($string)) {
      $output .= "<p align=\"right\">";

      if (DoesUserHaveRight($username, "ModifyTicket", $ticket_row->queue, $id)){
         $output .= "<input type=\"submit\" value=\"Save Changes\">";
      }
      $output .= "</form>";
   }
   $output .= CloseColorTable();
   return $output;
}
             
             
//-----------------------------------------------------------------------------
//
// Function: TicketJumbo
// Description:
//    Generates HTML to display all information about a ticket.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketJumbo($id) {
   global $username;

   $output = "<form><input type=\"hidden\" name=\"id\" value=\"$id\">"
             .TicketBasics($id, "no-button")
             .TicketDates($id, "no-button")
             .TicketPeople($id, "no-button")
             .OpenColorTable("blue", "Update Ticket #$id", "100%")
             ."<table border=0>"
             .OpenTable("Update Type")
             ."<select name=\"provision\">"
             ."<option value=\"OnComment\">Comments (Not sent to requesters)</option>"
             ."<option value=\"OnCorrespond\">Response to requesters</option>"
             ."</select>".CloseTable().OpenTable("subject")
             ."<input type=\"text\" size=30 name=\"jumbo_subject\" value=\"$jumbo_subject\">"
             .CloseTable().OpenTable("Additional Comments")
             ."<textarea cols=72 rows=15 wrap=hard name=body>$body</textarea>"
             .CloseTable()."</table>".CloseColorTable()
             ."<center><table border=0 width=\"33%\"><tr valign=\"top\">"
             ."<td width=\"50%\" align=\"right\">";

   if (DoesUserHaveRight($username, "ModifyTicket", "", $id)) {
      $output .= OpenColorTable("red", "Save Changes", "100%")
              ."<center><input type=\"hidden\" name=\"form_submitted\" value=\"jumbo\">"
              ."<input align=\"center\" type=\"submit\" name=\"create\" value=\"Save Changes\">"
              ."</form></center>".CloseColorTable();
   }
   $output .= "</td></tr></table></center>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: OldTicketMenu
// Description:
//    Generates HTMl to display the menu for tickets.
// Parameters:
//    array $main_menu	Main menu items.
//    array $sub_menu	Submenu items.
//    int $id		Ticket ID.
//    string $menu	Selected item.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OldTicketMenu($main_menu, $sub_menu, $id, $menu) {
   $output = '<table border="0" width="100%"><tr valign="top"><td width="65%">'
           .OpenColorTable("green", "Menu", "100%")
           .ThisMenu($main_menu, $id, $menu)
           ."<br>"
           .ThisMenu($sub_menu, $id, $menu)
           .CloseColorTable()
           ."</td><td width=\"35%\">"
           .OpenColorTable("red", "Tickets", "100%")
           .TicketDisplay()
           .CloseColorTable()
           ."</td></tr></table>";
    return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketDisplay
// Description:
//    Generates HTMl to display a form to search for a ticket.
// Parameters:
//    None.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketDisplay() {
   global $LOOKUP_TYPE;
   $output = '<table><form action="lookup_name.php" method="get">'
         .'<tr><td><input type=submit value="Lookup User (Name)">&nbsp;'
         .'<input type="text" size="7" name="user" value="">'
         .'</td></tr></form>';
   if ($LOOKUP_TYPE != "local") {
      $output .= '<form action="lookup_netid.php" method="get"><tr><td>'
         .'<input type="submit" value="Lookup User (NetID)">&nbsp;'
         .'<input type="text" size="7" name="uid" value="">'
         .'</td></tr></form>';
   }
   $output .= '<form action="ticket.php"><tr><td>'
         .'<input type=submit value="Go To Ticket">&nbsp;'
         .'<input size=7 name="id"></td></tr></form></table>';
   return $output;
}

    
//-----------------------------------------------------------------------------
//
// Function: ThisMenu
// Description:
//    Generates HTMl to display a list of links seperated by "  |  ".
// Parameters:
//    array $items	Items to display in menu
//    int $id   	Ticket ID.
//    string $menu	Selected item.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ThisMenu($items, $id, $menu) {
   global $self;
   $img = "&nbsp;<img border=\"0\" src=\"images/down-arrow.png\">";
   $url_close = "</a>";
   $spc = "&nbsp; | &nbsp;";
   $size = sizeof($items);
   $end = $size - 1;
       
   for ($i = 0; $i < $size; $i++) {
      $url_menu = str_replace(" ", "_", $items[$i]);
      if ($i == $end) $spc = "";
      $url_open = "<a class=\"main\"  href=\"$self?id=$id&menu=$url_menu\">";
      if ($items[$i] == $menu) $output .= Font("$url_open$items[$i]$url_close$img$spc");
      else $output .= Font("$url_open$items[$i]$url_close$spc");
   }
   return $output;
}

?>