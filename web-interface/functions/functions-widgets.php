<?php 
require_once("functions-getters.php");
//
// Filename: functions-widgets.php
// Description: Contains miscellaneous functions for displaying theme elements.
// Supprted Language(s):   PHP 4.0
//
global $self;
$self = $PHP_SELF;
$date = date(r);
    
//-----------------------------------------------------------------------------
//
// Function: Head
// Description:
//    Generates HTML to display the top of the front page.
// Parameters:
//    string $title	Title of the page.
//    string $login	Username logged in.
//    sting $rate	Refresh rate inseconds of page.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Head($title = "", $login = "", $rate = "") {
   if ($rate == "") $rate = GetValueFromTable("refresh", "staffprefs", "where username = \"$login\"");
   if (empty($title)) {
      $title = "login";
      $show_menu = 0;
      $login = "not logged in";
   } 

   else {
      $show_menu = 1;
      $login = "logged in as <b>$login</b>";
   }
   $output = Headers($title, $rate);
   $output .= TopBlue($title, $login, $show_menu);
   $output .= Reports();
   return $output;          
}
       
//-----------------------------------------------------------------------------
//
// Function: Headers
// Description:
//    Generates the headers for each page.
// Parameters:
//    string $title	Title of page.
//    string $rage	Refresh rate of page in seconds.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Headers($title = "", $rate = "") {
   global $date,$self;
   $output = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">';
   $output .= "\n<html>\n<head>";
   $session_expire = date("F d, Y H:i:s", mktime() + GetSessionExpireTime());
   if (!empty($rate)) {
      $output .= "\n<META HTTP-EQUIV=expires CONTENT=\"$date\">"
              ."\n<META HTTP-EQUIV=Refresh CONTENT=\"$rate\" URL=\"$self?rate=$rate\">";
   }
   if (!empty($_SESSION['username'])) {
     $output .= "\n<meta HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">"
	."\n"
	.'<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
	."\n"
	."<title>ruQueue:  $title</title>"
	."\n"
       .'<link rel=stylesheet type="text/css" href="style.css"></link>'
       ."\n";
     $output .= "<script type=\"text/javascript\" src=\"countdown/countdown.js\"></script>\n";
     $output .= "<script src=\"buildings.js\"></script>\n";
     $output .= "</head>\n\n";

     if (HaveCountdown()) {
       $countdown  = "Countdown('session_expire', '$session_expire', 'Your session will expire in ', ";
       $countdown .= "'Your session has expired.'); session_expire.start();";
     }
     $init = " jScriptInit(); "; // need some condition?  
     $javascript = $countdown . $init;
     $output .= "<body onLoad=\"$javascript\">\n\n";

   }
   else $output .= "<meta HTTP-EQUIV=\"PRAGMA\" CONTENT=\"NO-CACHE\">"
                ."<title>ruQueue:  $title</title>"
                .'<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
                .'<link rel=stylesheet type="text/css" href="style.css"></head>'
                .'<body text="#000000" bgcolor="#FFFFFF" alink="#CC0000" link="#003399" vlink="#CC00CC">';
   return $output;
}

// -------------------------------------------------------- 
// Function:                HaveCountdown

// Description:             Returns true if javascript counter 
//                          is available 

// Parameters:
//    None                  

// Return Values:           
//    interger              
//                          0 if countdown.js is not present
//                          1 if countdown.js is present

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function HaveCountdown() {
  return file_exists('countdown/countdown.js');
}

//-----------------------------------------------------------------------------
//
// Function: TopBlue
// Description:
//    Generates HTML to display the blue bar at the top of the page.
// Parameters:
//    string $title	Title of page.
//    string $login	User logged in.
//    boolean $menu	Display menu?
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TopBlue($title = "", $login = "", $menu = "") {
   global $SCHEDULING;
   $output = "";
   $output .= '<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#003399">'
           .'<tr><td width="20"><img src="images/tl.png" width="20" height="20"></td>'
           .'<td align="right" colspan=2><font face="Verdana, Ariel, Helvetica" color="#ffffff">'
           .$login.'</font></td><td align="right" width="20">'
           .'<img src="images/tr.png" width="20" height="20"></td>'
           .'</tr><td>&nbsp;</td><td><font face="Ariel, Helvetica" size="+2"color=#ffffff>'
           .'<b>ruQueue:  '.$title.'</b></font>'
           .'</td><td align="right" valign="top"><font face="Verdana, Ariel, Helvetica" color="#ffffff">'
           .'<span id="session_expire" /></font></td><td>&nbsp;</td></tr>'
           .'<tr><td width="20"><img src="images/bl.png" width="20" height="20"></td>'
           .'<td colspan=2><font face="Arial, Helvetica, sans-serif" size="2" color=#ffffff>&nbsp;';
   if ($menu) {
      $output .= '<a class="menu" href="index.php">Home</a> | '
              .'<a class="menu" href="search.php">Search</a> | ';
      if ($SCHEDULING) {
        $output .=  '<a class="menu" href="schedules.php">Schedules</a> | ';
      }
      $logout = $_SERVER['PHP_SELF'] . "?logout=1";
      $output .= '<a class="menu" href="reports.php">Reports</a> | '
              .'<a class="menu" href="admin.php">Configuration</a> | '
              .'<a class="menu" href="user.php">Preferences</a> | '
              .'<a class="menu" href="about.php">About</a> | '
              ."<a class=\"menu\" href=\"$logout\">Logout</a> ";
   } 
   $output .= '</font></td><td align="right" width="20">'
           .'<img src="images/br.png" width="20" height="20"></td>'
           .'</tr></table>'
           .'<center><table border="0" cellspacing="1" cellpadding="1" width="97%">'
           .'<tr><td><font face="Arial, Helvetica" size="2"><br>';
   return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: Foot
// Description:
//    Generates HTML to display bottom of the page.
// Parameters:
//    None.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Foot() {
  global $db;
  mysql_close($db);
  $output = "<p></font></td></tr></table></center>"
           .'<table border=0 width=100% cellspacing=0 bgcolor="#003399">'
           .'<tr valign="top"><td valign=center align=left>'
           .'<font size=-1 face="Verdana, Ariel, Helvetica" color="#ffffff">'
           .'&nbsp;'
           ."</td>\n</tr>\n</table>\n</body>\n</html>\n";
  return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: ErrorReport
// Description:
//    Generates HTML to display the report.
// Parameters:
//    string $msg	Message to display.
//    string $width	Width of table.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ErrorReport($msg = "", $width = "") {
   global $past_print;
   $output = "";
   if ($past_print) {
      $output .= "<center><br>\n";
      if ($width) $output .= OpenColorTable("red", "Error", "$width");
      else $output .= OpenColorTable("red", "Error", "50%");
      $output .= Font($msg);
      $output .= CloseColorTable();
      $output .= "</center>\n<p>\n";
      return $output;
   } 
   else {
      global $err_report;
      $err_report .= $errmsg;
   }
}
       
//-----------------------------------------------------------------------------
//
// Function: GoodReport
// Description:
//    Generates HTML to display the report.
// Parameters:
//    string $msg	Message to display.
//    string $width	Width of table.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GoodReport($msg = "", $width = "") {
  global $past_print;
  $output = "";
  if ($past_print) {
    $output .= "<center><br>\n";
    if ($width) $output .= OpenColorTable("green", "Success", "$width");
    else $output .= OpenColorTable("green", "Success", "50%");
    $output .= Font($msg);
    $output .= CloseColorTable();
    $output .=  "</center>\n<p>\n";
    return $output;
  }  
  else {
    global $good_report;
    $good_report .= $msg;
  }
}
       
//-----------------------------------------------------------------------------
//
// Function: NoticeReport
// Description:
//    Generates HTML to display the report.
// Parameters:
//    string $msg	Message to display.
//    string $width	Width of table.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function NoticeReport($msg = "", $width = "") {
   global $past_print;
   if ($past_print) {
      $output = "<center>\n";
      if ($width) $output .= OpenColorTable("green", "Notice", "$width");
      else $output .= OpenColorTable("green", "Notice", "50%");
      $output .= Font($msg);
      $output .= CloseColorTable();
      $output .= "</center>\n<p>\n";
      return $output;
   }

   else{
      global $notice_report;
      $notice_report .= $msg;
   }
}
       
//-----------------------------------------------------------------------------
//
// Function: Reports
// Description:
//    Generates HTMl to display any globally stored reports.
// Parameters:
//    None.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Reports() {
   global $good_report;
   global $err_report;
   global $notice_report;
   global $past_print;
   $past_print = 1;
   $ouput = "";
   if ($good_report) $output .= GoodReport($good_report);
   if ($err_report) $output .= ErrorReport($err_report);
   if ($notice_report) $output .= NoticeReport($notice_report);
   return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: HexColor
// Description:
//    Returns the hex code for a color.
// Parameters:
//    string $color	Color.
// Return Values:
//    Hex color code.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function HexColor($color = "") {
   if ($color == "blue") return "#003399";
   if ($color == "red") return "#CC0000";
   if ($color == "yellow") return "#FFCC33";
   if ($color == "purple") return "#9900CC";
   if ($color == "green") return "#97CF97";
   if ($color == "orange") return "EE7800";
   return "#003399";
}
       
//-----------------------------------------------------------------------------
//
// Function: OpenColorTable
// Description:
//    Generates HTML to open a table in color.
// Parameters:
//    string $color	Color of table border.
//    string $title	Title of table.
//    string $width	Width of table.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OpenColorTable($color = "", $title = "", $width = "") {
   if (($color == "yellow") or ($color == "green")) $font_color = "black";
   else $font_color = "white";
   $color = HexColor($color);
   if (empty($width))  $width = "1%";
   $output = "<table bgcolor=\"$color\" cellspacing=0 border=0 cellpadding=0 width=\"$width\">\n"
            ."<tr><td rowspan=2><img src=\"images/clearpix.png\" width=1 height=1 alt=\"\">\n"
            ."</td><td valign=middle align=left bgcolor=\"$color\">&nbsp;\n"
            ."<font size=-1 color=\"$font_color\"><b>$title</b></font>\n"
            ."</td><td align=\"right\" valign=\"middle\" bgcolor=\"$color\">\n"
            ."<font color=\"#ffffff\" size=-1></font>&nbsp;</td><td rowspan=2>\n"
            ."<img src=\"images/clearpix.png\" width=1 height=1 alt=\"\"></td></tr>\n"
            ."<tr><td colspan=2 bgcolor=\"#ffffff\" valign=\"top\" align=\"left\" width=\"100%\">\n"
            ."<table cellpadding=2 width=100%><tr><td>\n";
   return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: CloseColorTable
// Description:
//    Generates HTML to close the color table.
// Parameters:
//    None.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CloseColorTable() {
   return '</td></tr></table></td></tr><tr><td colspan=4>'
     .'<img src="images/clearpix.png" height=1 width=1>'
     .'</td></tr></table>';
}

//-----------------------------------------------------------------------------
//
// Function: Spacer
// Description:
//    Repeats "<br>" $lim times.
// Parameters:
//    int $lim	Amount of times to repeat.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Spacer($lim) {
   $output = "";
   for($i = 0; $i < $lim; $i++) $output .= "<br>";
   return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: Font
// Description:
//    Puts a string into a font tag.
// Parameters:
//    string $string	String to place in font tag.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Font($string="") {
   return "<font face=\"arial, helvetica\" size=2>$string</font>";
}

//-----------------------------------------------------------------------------
//
// Function: FontBold
// Description:
//    Puts a string into bold and font tags.
// Parameters:
//    string $string	String to place in tags.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function FontBold($string="") {
   return "<font face=\"arial, helvetica\" size=2><b>$string</b></font>";
}

//-----------------------------------------------------------------------------
//
// Function: ButtonLink
// Description:
//    Generates HTML to display a form button that links to a location.
// Parameters:
//    string $button_text	Text to display on the button.
//    string $location		Location to direct browser to.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ButtonLink($button_text, $location){
   return "<form action=\"$location\" method=\"get\">"
         ."<input type=submit value=\"$button_text\">"
         ."</form>";
}

//-----------------------------------------------------------------------------
//
// Function: Table
// Description:
//    Creates a table row.
// Parameters:
//    string $thing1	First data cell.
//    string $thing2	Second data cell.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Table($thing1="", $thing2="") {
   return "<tr><td>".FontBold($thing1)."</td><td>\n"
         .Font($thing2)."</td></tr>\n";
}

//-----------------------------------------------------------------------------
//
// Function: OpenTable
// Description:
//    Creates a table row.
// Parameters:
//    string $thing1	Data to put in cell.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OpenTable($thing1="") {
   return "<tr><td>".FontBold($thing1)."</td><td>\n";
}

//-----------------------------------------------------------------------------
//
// Function: CloseTable
// Description:
//    Closes a table row.
// Parameters:
//    None.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CloseTable() {
   return "</td></tr>\n";
}

//-----------------------------------------------------------------------------
//
// Function: TableRow
// Description:
//    Display array data in a table row.
// Parameters:
//    array $row_array	Array to display.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TableRow($row_array){
   $output =  "<tr>";

   foreach($row_array as $data_item){
      $output .= "<td>";
      if (!empty($data_item)) $output .= Font($data_item);
      $output .= "</td>";
   }
   $output .= "</tr>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: TableRowBold
// Description:
//    Display array data in a table row, bolded.
// Parameters:
//    array $row_array	Array to display.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TableRowBold($row_array){
   $output =  "<tr>";

   foreach($row_array as $data_item){
      $output .= "<td>";
      if (!empty($data_item)) $output .= FontBold($data_item);
      $output .= "</td>";
   }
   $output .= "</tr>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: RelatedTicketsToUser
// Description:
//    Displays a list of tickets related to a user.
// Parameters:
//    int $user_id	User ID.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function RelatedTicketsToUser($user_id) {
   global $db;
   $top = array("ID", "Subject", "Status", "Queue");
   $output = TableRowBold($top);
   $query = "select ticket.id,status, queue from ticket,comment ";
   $query .= "where ticket.last_comment_id=comment.id ";
   $query .= "and user_id=$user_id order by ticket.id DESC";
   $result = mysql_query($query, $db);
   if (!$result) return $output;
   while ($table_row = mysql_fetch_object($result)) {
      $id = $table_row->id;
      $queue = $table_row->queue;
      $status = $table_row->status;
      $url = "<a class=\"main\" href=\"ticket.php?id=$id\">";
      $subject = htmlentities(GetOriginalSubjectOfTicket($id));
      $row = array($url.$id."</a>", $url.$subject."</a>", $status, $queue);
      $output .= TableRow($row);
   }
   return $output;
}
?>