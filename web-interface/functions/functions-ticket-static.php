<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-form-objects.php");
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");
require_once("functions-ticket.php");
//
// Filename: functions-ticket-static.php
// Description: Contains functions for displaying ticket information.
// Supprted Language(s):   PHP 4.0
//
//-----------------------------------------------------------------------------
//
// Function: TicketStaticBasics
// Description:
//    Generates HTML to display ticket basics.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticBasics($id) {
   global $db;
   $query = "select queue from ticket where id='$id'";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $worked = GetTimeWorked($id);
   $queue = $row["queue"];
   $queue_link = "<a class=\"main\" href = \"./search.php?QueueOp==&valueOfQueue"
                ."=$queue&Action=Show+Results&Reset=0\">$queue</a>\n";
   $query = "select status, priority ";
   $query .= "from comment where ticket_id='$id' ";
   $query .= "order by date_created desc";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $status = $row["status"];
   $priority = $row["priority"];
   return "<table width=\"100%\" border=0 cellpadding=3 cellspacing=1>"
          ."<tr><th align=\"center\">".Font("Id")
          ."</th><th align=\"center\">".Font("Status")
          ."</th><th align=\"center\">".Font("Worked")
          ."</th><th align=\"center\">".Font("Priority")
          ."</th><th align=\"center\">".Font("Queue")
          ."</th></tr><tr><th align=\"center\">".Font("$id")
          ."</th><th align=\"center\">".Font("$status")
          ."</th><th align=\"center\">".Font("$worked")
          ."</th><th align=\"center\">".Font("$priority")
          ."</th><th align=\"center\">".Font("$queue_link")
          ."</th></tr></table>";
}

    
//-----------------------------------------------------------------------------
//
// Function: TicketStaticDates
// Description:
//    Generates HTML to display ticket dates.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticDates($id) {
  $sql = "
SELECT
  date_format(date_created, '%a %M %e, %Y at %h:%i %p') as date_created,
  date_format(status_change_date, '%a %M %e, %Y at %h:%i %p')
  as status_change_date,
  starts,
  due,
  started,
  last_contact
FROM
  ticket
WHERE
  id='$id'
";
  $result = mysql_query($sql) or die(mysql_error());
  $row = mysql_fetch_object($result);
  $fields = array(
		  "date_created",
		  "starts",
		  "started",
		  "last_contact",
		  "due",
		  "status_change_date",
		  );
  $str = "<table>\n";
  foreach ($fields as $field) {
    $name = ucwords(str_replace('_', ' ', $field));
    if ($field == 'status_change_date') $name = "Updated";
    $str .= OpenTable($name);
    if ($row->{$field} != '0000-00-00') {
      $str .= Font($row->{$field});
    }
    $str .= CloseTable();
  }
  $str .= "</table>\n";
  return $str;
}

//-----------------------------------------------------------------------------
//
// Function: TicketStaticDatesOld
// Description:
//    Generates HTML to display ticket dates.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    Delete me!, if the new one above is working, i.e. after testing.  
//-----------------------------------------------------------------------------
function TicketStaticDatesOld($id) {
   global $db;
   $query = "select date_format(date_created, '%a %M %e, %Y at %h:%i %p') ";
   $query .= "as date_created from comment ";
   $query .= "where ticket_id='$id' order by date_created desc";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $updated = $row["date_created"];
   $query = "select date_format(date_created, '%a %M %e, %Y at %h:%i %p'), ";
   $query .= "date_format(starts, '%a %M %e, %Y') as starts, ";
   $query .= "date_format(due, '%a %M %e, %Y') as due, ";
   $query .= "date_format(started, '%a %M %e, %Y') as started, ";
   $query .= "date_format(last_contact, '%a %M %e, %Y') as last_contact ";
   $query .= "from ticket where id='$id'";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $created = $row["date_format(date_created, '%a %M %e, %Y at %h:%i %p')"];
   $starts = $row["starts"];
   $started = $row["started"];
   $last_contact = $row["last_contact"];
   $due = $row["due"];
   return "<table>"
          .OpenTable("Created").Font($created).CloseTable()
          .OpenTable("Starts").Font($starts).CloseTable()
          .OpenTable("Started").Font($started).CloseTable()
          .OpenTable("Last Contact").Font($last_contact).CloseTable()
          .OpenTable("Due").Font($due).CloseTable()
          .OpenTable("Updated").Font($updated).CloseTable()
          ."</table>";
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketStaticUser
// Description:
//    Generates HTML to display ticket user info.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticUser($id) {
   global $db;

   $sql  = "SELECT user_id, name, email, phone FROM user, ticket ";
   $sql .= "WHERE ticket.user_id=user.id AND ticket.id='$id'";
   $result = mysql_query($sql) or die(mysql_error());
   $row = mysql_fetch_object($result);

   $mail_link  = 'mailto:' . $row->email;
   $mail_link .= '?subject=Ticket: ' . $id;
   $mail_link .= '&cc=' .  GetCommentEmail();

   $userlink = "<a class=\"main\" href=\"ticket.php?id=$id&menu=User\">";
   $emaillink = "<a class=\"main\" href=\"". $mail_link ."\">";
   return "<table width=\"100%\" border=0 cellpadding=3 cellspacing=1>"
          ."<tr><th align=\"center\">".Font("ID")
          ."</th><th align=\"center\">".Font("Name")
          ."</th><th align=\"center\">".Font("Email")
          ."</th></tr><tr><th align=\"center\">".Font("$row->user_id")
          ."</th><th align=\"center\">$userlink".Font("$row->name")
          ."</a></th><th align=\"center\">$emaillink".Font("$row->email")
          ."</a></th></tr><tr>"
          ."<th align=\"center\">".Font("Phone")
          ."</th><td align=\"center\">"
          .Font(str_replace("+", "<br>", trim($row->phone)))
          ."</td></tr></table>";
}

//-----------------------------------------------------------------------------
//
// Function: TicketStaticPeople
// Description:
//    Generates HTML to display ticket people info.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticPeople($id) {
   global $db;
   $query = "select owner, requester, cc, admin_cc ";
   $query .= "from ticket where id='$id';";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $owner = $row["owner"];
   return OpenTable("Owner").Font($owner)
         .CloseTable().OpenTable("Requesters")
         .PrintArray(GetTicketWatchers($id, "Requester"))
         .CloseTable().OpenTable("Cc")
         .PrintArray(GetTicketWatchers($id, "Cc"))
         .CloseTable().OpenTable("AdminCc")
         .PrintArray(GetTicketWatchers($id, "AdminCc"))
         .CloseTable();
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketStaticSpecialFields
// Description:
//    Generates HTML to display ticket special fields.
// Parameters:
//    int $id		Ticket ID.
//    string $queue	Ticket queue.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticSpecialFields($id, $queue) {
   $values = GetQueueSpecificValuesOfTicketSpecialFields($id);
   return VerticalArrayTable($values, "purple", "Special Fields", "100%");
}
    
//-----------------------------------------------------------------------------
//
// Function: VerticalArrayTable
// Description:
//    Generates HTML to display a table from a two dimensional array.
// Parameters:
//    array $values	Titles and values to display.
//    string $color	Color of table.
//    string $title	Title of table.
//    string $size	Size of table.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function VerticalArrayTable($values, $color, $title, $size) {
   if ($values[0] == "") return 0;
   $output = "<p>".OpenColorTable($color, $title, $size);
   $title_array = $values[0];
   $value_array = $values[1];
   if (sizeof($title_array) <sizeof($value_array))  $size = $sizeof($title_array);
   else $size = sizeof($value_array);

   for ($i = 0; $i < $size; $i++) {
      $output .= OpenTable($title_array[$i]).$value_array[$i].CloseTable();
   }
   $output .= CloseColorTable();
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketStaticSchedule
// Description:
//    Generates HTMl to display ticket scheduling information.
// Parameters:
//    id $id	Ticket ID.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketStaticSchedule($id) {
   global $db;
   require_once("scheduling/functions/functions-display.php");
   if (GetSchedulingPermissionsForTicket($id)) {
      $query = "select appointment_id, status, consultant_netid consultant, ";
      $query .= "date_format(datetime,'%a %M %e, %Y at %h:%i %p') date from ";
      $query .= "appointments where ticket_id=$id order by datetime desc";
      $result = mysql_query($query, $db);
      $output = OpenColorTable("orange", "Appointments", "100%");
      if (mysql_num_rows($result) > 0) $output .= DisplayAppointments($result);
      else {
         $output .= "No Appointments Have Been Created.<br>Click "
                 ."<a class=\"main\" href=\"new_appointment.php?ticket_id=$id\">"
                 ."here</a> to create one.";
      }
      $output .= CloseColorTable()."<br>";
      return $output;       
   }
}
?>