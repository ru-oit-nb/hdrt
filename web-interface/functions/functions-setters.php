<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-ticket.php");
//
// Filename: functions-setters.php
// Description: Contains functions for setting information in the database.
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: AddComment
// Description:
//    Adds a comment to a ticket.
// Parameters:
//    int $ticket_id	Ticket to attache comment to.
//    string $status	New status of ticket.
//    string $priority	Priority.
//    string $staff	Commenter.
//    string $subject	Comment subject.
//    string $attach	Not used.
//    string $body	Body of comment.
//    int $time_worked	Time worked.
// Return Values:
//    Returns the id of the new comment.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function AddComment($ticket_id, $status, $priority, $staff, $subject = "", $attach = "", $body = "", $time_worked="") {
   global $db;
   if (!DoesTicketExist($ticket_id)) return "Ticket does not exist";
   $query = "INSERT INTO comment SET ";
   $query .= "ticket_id='$ticket_id', status = '$status', priority = '$priority', ";
   $query .= "staff='$staff', time_worked = '$time_worked', subject = '$subject', ";
   $query .= "body='$body', ip_address='" . $_SERVER['REMOTE_ADDR'] . "'";
   $result = mysql_query($query, $db);
   if (!($result)) return "Error inserting comment into DB";
   $comment_id = mysql_insert_id();
   UpdateTicketStatus($ticket_id, $comment_id);
   return $comment_id;
}

//-----------------------------------------------------------------------------
//
// Function: UpdateTimeWorked
// Description:
//    Updates the time worked in a ticket.
// Parameters:
//    int $ticket_id	Ticket ID.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTimeWorked($ticket_id) {
   global $db;
   $query = "select sum(time_worked) as addup from comment where ticket_id='$ticket_id'";
   $result = mysql_query($query, $db);
   $time = mysql_fetch_object($result);
   $addup = $time->addup;
   UpdateTicket($ticket_id, "time_worked = '$addup'");
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateComment
// Description:
//    Updates a comment.
// Parameters:
//    int $id		Comment ID.
//    string $set	What to update.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateComment($id, $set) {
   global $db;
   $set .= ", date_created=date_created";
   $query = "update comment set $set where id = '$id'";
   $result = mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: DeleteUser
// Description:
//    Deletes a user.
// Parameters:
//    int $id	User ID.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DeleteUser($id) {
   global $db;
   $query = "delete from user where id=$id";
   mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateTicket
// Description:
//    Updates a ticket.
// Parameters:
//    int $id		Ticket ID.
//    string $set	What to update.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTicket($id, $set) {
   global $db;
   $set .= ", date_created=date_created";
   $query = "update ticket set $set where id = '$id'";
   $result = mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateTicketOwner
// Description:
//    Updates the owner of a ticket.
// Parameters:
//    int $id		Ticket ID.
//    string $owner	New owner.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTicketOwner($id, $owner) {
   UpdateTicket($id, "owner='$owner'");
}
    
//-----------------------------------------------------------------------------
//
// Function: DoesTicketExist
// Description:
//    Checks the existence of a ticket.
// Parameters:
//    int $ticket_id	Ticket ID.
// Return Values:
//    <=0 if not exist.  >0 if exists.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DoesTicketExist($ticket_id) {
   global $db;
   $query = "Select id from ticket where id='$ticket_id' limit 1";
   $result = mysql_query($query, $db);
   return mysql_num_rows($result);
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateTicketBasics
// Description:
//    Updates the basics of a ticket.
// Parameters:
//    int $id			Ticket ID.
//    int $time_worked		Time worked.
//    int $time_left		Time left.
//    int $final_priority	Final priority.
//    string $queue		Queue.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTicketBasics($id, $time_worked, $time_left, $final_priority, $queue) {
   global $db;
   $query = "update ticket ";
   $query .= "set time_worked='$time_worked', time_left='$time_left', ";
   $query .= "final_priority='$final_priority', queue = '$queue', ";
   $query .= "date_created = date_created where id='$id'";
   $result = mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateTicketDates
// Description:
//    Updates the ticket dates.
// Parameters:
//    int $id			Ticket ID.
//    date $starts		Start date.
//    date $started		Started date.
//    date $last_contact	Date of last contact.
//    date $due			Due date.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTicketDates($id, $starts, $started, $last_contact, $due) {
   $set = "starts= '$starts', last_contact = '$last_contact', due ='$due', started ='$started'";
   UpdateTicket($id, $set);
}
    
//-----------------------------------------------------------------------------
//
// Function: SetTicketWatchers
// Description:
//    Sets ticket watchers for a ticket.
// Parameters:
//    int $ticket_id		Ticket ID.
//    array $super_array	Array of all three types.
//    string $requester		Ticket Requeseter
//    array $cc			Ticket CC's
//    array $admin_cc		Ticket Admin CC's
// Return Values:
//    Error on error.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SetTicketWatchers($ticket_id, $super_array = "", $requester = "", $cc = "", $admin_cc = "") {
   if ($super_array != "") {
      $requester = $super_array[0];
      $cc = $super_array[1];
      $admincc = $super_array[2];
   }
       
   if (!(TicketExists($ticket_id))) return "Ticket doesn't exist<br>";
   global $db;

   if ($requester) {
      $query = "Insert into ticketwatcher set ticket_id='$ticket_id', type='Requester', email='$requester'";
      if (!mysql_query($query, $db)) return "Requester not set";
   }

   if ($cc != "") {

      foreach($cc as $email) {

         if ($email != "") {
            $query = "Insert into ticketwatcher set email='$email', ";
            $query .= "ticket_id='$ticket_id', type='Cc'";
            mysql_query($query, $db);
            $email = "";
         }
      }
   }

   if ($admincc != "") {

      foreach($admincc as $email) {

         if ($email != "") {
            $query = "Insert into ticketwatcher set email='$email', ";
            $query .= "ticket_id='$ticket_id', type='AdminCc'";
            mysql_query($query, $db);
            $email = "";
         }
      }
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: UpdateLocalPassword
// Description:
//    Generates html to allow change of password.
// Parameters:
//    string $user      User Name
//    string $password  New Password.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Applies only to local accounts..
//-----------------------------------------------------------------------------
function UpdateLocalPassword($username = "", $password = "") {
   global $db;
   $query = "update staff set password=password('$password') where username='$username'";
   $result = mysql_query($query, $db);
   if (!$result) return "Database Error!";
   return "Password updated successfully!";
}
?>