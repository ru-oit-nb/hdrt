<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-admin-users.php");
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");
//
// Filename: functions-admin.php
// Description: Administration Functions
// Supprted Language(s):   PHP 4.0
//

require_once("header.php");
global $self;
$self = $PHP_SELF;

//-----------------------------------------------------------------------------
//
// Function: MainMenu
// Description:
//    Creates the Main/Leftmost menu for the administration page.
// Parameters:
//    string $item	determines placement of arrow image.
// Return Values:
//    Returns HTML to display the menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function MainMenu($item = "") {
   global $self;
   $img = "<img src=\"images/arrow.png\">";
   $arrow1 = $arrow2 = $arrow3 = $arrow4 = $arrow5 = "";
   if ($item == "Groups") $arrow1 = $img;
   if ($item == "Queues") $arrow3 = $img;
   if ($item == "Global") $arrow4 = $img;
   if ($item == "Users") $arrow5 = $img;
   return Font("<a class=\"main\" href=\"$self?item=Groups\">Groups</a>$arrow1")
          ."<p>".Font("<a class=\"main\" href=\"$self?item=Queues\">Queues</a>$arrow3")
          ."<p>".Font("<a class=\"main\" href=\"$self?item=Global\">Global</a>$arrow4")
          ."<p>".Font("<a class=\"main\" href=\"$self?item=Users\">Users</a>$arrow5");
}

//-----------------------------------------------------------------------------
//
// Function: GroupsMenu
// Description:
//    Generates the center menu when the item "Groups" is selected.
// Parameters:
//    string $hl	Determines where the arrow image is placed.
//    string $item	Used to create links.
// Return Values:
//    Returns HTML to output the menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GroupsMenu($hl = "", $item = "") {
   global $self;
   $arr1 = GetPseudoGroups();
   $arr2 = GetGroups();
   $output = FontBold("Pseudogroups:")."<ul>"
           .CreateLinksFromArray($arr1, "item=$item&pseudo", "<br>", $hl)."</ul>";
   if ($hl == "first") $hl = "";
   $output .= FontBold("Groups:")."<ul>";
   if ($hl == "Create_a_new_group") $img = "<img border=\"0\"src=\"images/arrow.png\">";
   else $img = "";
   $output .= Font("<a class=\"main\" href=\"$self?item=Groups&group=Create_a_new_group\">Create a new group</a>$img<br>")
           .CreateLinksFromArray($arr2, "item=$item&group", "<br>", $hl)."</ul>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GroupsSubMenu
// Description:
//    Creates the third (rightmost) menu for groups administration.
// Parameters:
//    string $grp	Group Name
//    string $item	Used to create links for menu.
//    string $hl	Determines placement of the arrow image.
// Return Values:
//    Returns HTML to output the menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GroupsSubMenu($grp = "", $item = "", $hl = "") {
   global $self;
   $image = "<img src=\"images/down-arrow.png\">";
   $output = "";
   $special = 0;
   if ($hl == "first") $special = 1;
   $grp_orig = str_replace("_", " ", $grp);
   if (in_array($grp_orig, GetGroups())) {
      $set = "group";
      $arr = GetGroupPropertiesArray();
   }
   else {
      $set = "pseudo";
      $arr = GetPseudogroupPropertiesArray();
   }
   if ($set == "group") $var_name_hck = "item=$item&group=$grp&sub_group";
   elseif ($set == "pseudo") $var_name_hck = "item=$item&pseudo=$grp&sub_group";

   for ($i = 0; $i < sizeof($arr); $i++) {
      $value = str_replace(" ", "_", $arr[$i]);  
      if (($value == $hl) or $special) {
         $output .= "<a class=\"main\" href=\"$self?$var_name_hck=$value\">$arr[$i]</a>$image<br>";
         $special = 0;
      }
      else $output .= "<a class=\"main\" href=\"$self?$var_name_hck=$value\">$arr[$i]</a><br>";
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GeneralMenu
// Description:
//    Creates a list of links from an array, in a line - menu.
// Parameters:
//    string $hl	Determines placement of the arrow image.
//    array $arr	Items to place in menu.
//    string $hierarchy	Variable names to be included in link.
// Return Values:
//    Returns HTML to display generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GeneralMenu($hl = "", $arr = "", $hierarchy = "") {
   return CreateLinksFromArray($arr, $hierarchy, "<br>", $hl);
}
    
//-----------------------------------------------------------------------------
//
// Function: GeneralSubMenu
// Description:
//    Creates a list of links from an array, in a line - menu.
// Parameters:
//    string $item	Not used.
//    string $hl	Determines placement of the arrow image.
//    array $arr	Items to place in menu.
//    string $hierarchy	Variable names to be included in link.
// Return Values:
//    Returns HTML to display generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GeneralSubMenu($item = "", $hl = "", $arr = "", $hierarchy = "") {
  //echo "in generalsubmenu...";
	//echo "item=$item, hl=$hl, ".print_r($arr)."hierarchy=$hierarchy";
  global $self;
  $image = "<img src=\"images/down-arrow.png\">";
  $output = "";
  $special = 0;
  if ($hl == "first") $special = 1;

  for ($i = 0; $i < sizeof($arr); $i++) {
    $value = str_replace(" ", "_", $arr[$i]);
    if (($value == $hl) or $special) {
      $output .= "<a class=\"main\" href=\"$self?$hierarchy=$value\">$arr[$i]</a>$image<br>";
      $special = 0;
    } else $output .= "<a class=\"main\" href=\"$self?$hierarchy=$value\">$arr[$i]</a><br>";
  }
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: QueuesMenu
// Description:
//    Generates the list of queues for the admin page.
// Parameters:
//    string $hl	Determines where to place the arrow image.
//    string $item	Used to generate link hierarchy.
// Return Values:
//    Returns HTML to display the generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function QueuesMenu($hl = "", $item = "") {
   global $self;
   $arr = GetAllQueues();
   $hierarchy = "item=$item&queue";
   if ($hl == "Create_a_new_queue") $img = "<img border=\"0\"src=\"images/arrow.png\">";
   else $img = "";
   $output = Font("<a class=\"main\" href=\"$self?item=Queues&queue=Create_a_new_queue\">Create a new queue</a>$img<br>")
           .GeneralMenu($hl, $arr, $hierarchy);
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: QueuesSubMenu
// Description:
//    Creates the Queue Operations menu.
// Parameters:
//    string $sub_menu	Queue Name and other hierarchy.
//    string $item	Used to generate the hierarchy.
//    string $hl	Determines placement of the arrow image.
// Return Values:
//    Returns the HTML to display the generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function QueuesSubMenu($sub_menu = "", $item = "", $hl = "") {
   global $self;
   $hierarchy = "item=$item&queue=$sub_menu&sub_group";
       
   if ($sub_menu == "Create_a_new_queue") {
      $image = "<img src=\"images/down-arrow.png\">";
      $output = "<a class=\"main\" href=\"$self?$hierarchy=Basics\">Basics</a>$image<br>";
   }
   else {
      $arr = GetQueuePropertiesArray();
      $output = GeneralSubMenu($item, $hl, $arr, $hierarchy);
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GlobalMenu
// Description:
//    Generates the center "Global" menu.
// Parameters:
//    string $hl	Determines placement of the arrow image.
//    string $item	Used to generate the hierarchy.
// Return Values:
//    Returns the HTML to display the generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GlobalMenu($hl = "", $item = "") {
   $arr = GetGlobalPropertiesArray();
   $hierarchy = "item=$item&global";
   return GeneralSubMenu($item, $hl, $arr, $hierarchy);
}

//-----------------------------------------------------------------------------
//
// Function: UsersMenu
// Description:
//    Generates the center "Users" menu.
// Parameters:
//    string $hl	Determines placement of the arrow image.
//    string $item	Used to generate the hierarchy.
// Return Values:
//    Returns the HTML to display the generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function UsersMenu($hl = "", $item = "") {
  $arr = array("Find_Users", "Bulk_Management");
  $hierarchy = "item=$item&subitem";
  //return GeneralSubMenu($item, $hl, $arr, $hierarchy);
  return GeneralMenu($hl, $arr, $hierarchy);
}
    
//-----------------------------------------------------------------------------
//
// Function: UsersSubMenu
// Description:
//    Generates a menu list of all staff in the system.
// Parameters:
//    string $subitem	Used to generate the hierarchy.
//    string $item	Used to generate the hierarchy.
// Return Values:
//    Returns the HTML to display the generated menu.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function UsersSubMenu($hl="", $subitem = "", $item = "") {
	$bulk_menu = array("Add_New_Users", "User_Removal");
  if($subitem == "") $subitem = "Find_Users";
	switch ($subitem){

		case 'Bulk_Management':
   		$hierarchy = "item=$item&subitem=$subitem&bulk";
   		return GeneralSubMenu($item, $hl, $bulk_menu, $hierarchy);
			break;

		case 'Find_Users':
   		$hierarchy = "item=$item&subitem=$subitem&user";
			return GetStaffForm($hierarchy);
			break;
	} 
}


?>
