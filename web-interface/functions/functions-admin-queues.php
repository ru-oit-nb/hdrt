<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
// Filename: functions-admin-queues.php
// Description: Contains functions for ticket queue administration.
// Supprted Language(s):   PHP 4.0, 5.0

// Required Files
require_once("scheduling/functions/functions-admin-queues.php");
require_once("functions-admin-users.php");
require_once("functions-form-objects.php");
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");

global $db, $SCHEDULING, $self;
$self = $PHP_SELF;

if (!empty($_POST)) $input = $_POST;
else $input = $_GET;
$output = "";    

// Switch to decide how to handle input.
switch ($input['form_submitted']){
   case "Edit_Global_Options":
   $result = mysql_query("select * from system_variables", $db);
   while ($option = mysql_fetch_array($result)) {
      if ($input[$option['var']] !== NULL) {
         $query = "update system_variables set val='";
         $query .= $input[$option['var']]."' where var='".$option['var']."'";
         mysql_query($query, $db);
      }
   }

   break;
   case "Edit_Global_Scheduling_Options":
   $result = mysql_query("select * from sched_options", $db);

   while ($option = mysql_fetch_array($result)) {
      if ($input[str_replace(" ", "_", $option['option_name'])] !== NULL) {
         $query = "update sched_options set option_value='";
         $query .= $input[str_replace(" ", "_", $option['option_name'])]."' where option_name='".$option['option_name']."'";
         mysql_query($query, $db);
      }
   }
   break;

   case "Edit_Scheduling_Permissions":
   $q_name = str_replace("_", " ", $queue);
   $query = "UPDATE queue SET ";
      $query .= "allow_scheduling='$allow_scheduling' ";
      $query .= "WHERE q_name='$q_name'";
      $result = mysql_query($query, $db);
   break;
    
   case "Edit_Global_Scheduling_Permissions":
   if (isset($input['checked'])) {
      $rule_ids = GetArrayFromTable("rule_id", "sched_rules", "order by schedgroup");

      foreach($input['checked'] as $del) {
         DeleteSchedulingRule($rule_ids[$del]);
      }
   }
       
   if (isset($input['allowed'])) {
      foreach($input['allowed'] as $del) {
         $rule_id = GetValueFromTable('rule_id', 'sched_rules', 'where schedgroup="'.$del.'" and rule="Allow"');
         DeleteSchedulingRule($rule_id);
      }
   }
       
   if (isset($input['addschedgroups'])) {
      foreach($input['addschedgroups'] as $add) {
         AddSchedulingRule("", $add, "Allow");
      }
   }

   if ($input['privgroup'] != "-" && $input['schedgroup'] != "-" && $input['privgroup'] != NULL && $input['schedgroup'] != NULL) {
      AddSchedulingRule($input['privgroup'], $input['schedgroup'], $input['rule']);
   }
   if ($input['privgroup1'] != "-" && $input['schedgroup1'] != "-" && $input['privgroup1'] != NULL && $input['schedgroup1'] != NULL) {
      AddSchedulingRule($input['privgroup1'], $input['schedgroup1'], $input['rule']);
   }
   break;

   case "Edit Basics":
   $sub_group = "Basics";
   if ($input['requests_due'] > 28) $input['requests_due'] = 28; // max
   if ($input['requests_due'] < 0) $input['requests_due'] = 0; // min
   if ($input['queue_name'] == NULL) $input['queue_name'] = " ";
   if ($input['queue_descrip'] == NULL) $input['queue_descrip'] = " ";
   if ($input['correspond'] == NULL) $input['correspond'] = " " ;
   if ($input['comment'] == NULL) $input['comment'] = " " ;
   if ($input['priority'] == NULL) $input['priority'] = " " ;
   if ($input['priority_to'] == NULL) $input['priority_to'] = " " ;
   if ($input['enabled'] == NULLE) $input['enabled'] = 0;    
   if ($input['q_group_new'] != "") {
      $input['q_group'] = $input['q_group_new'];
   }
   $queue_name = str_replace("_", " ", $input['queue_name']);
   $insert_query = "Insert into queue set q_name='$queue_name'";
   $update = "Update queue SET description='".$input['queue_descrip']."', ";
   $update .= "correspondence_address='".$input['correspond']."', ";
   $update .= "comment_address='".$input['comment']."', ";
   $update .= "priority_starts_at='".$input['priority']."', ";
   $update .= "allowabledays='".$input['requests_due']."', ";
   $update .= "overtime_moves_to='".$input['priority_to']."', ";
   $update .= "color='".$input['color']."', ";
   $update .= "enabled='".$input['enabled']."', ";
   $update .= "require_resolve='".$input['require_resolve']."', ";
   $update .= "q_group='".$input['q_group']."'";
   $where = " WHERE q_name='$queue_name'";
   $query_var = $update.$where;
       
   if ($queue_name != "") {
      mysql_query($insert_query, $db);
      if (!mysql_query($query_var, $db)) {
         $output .= "error with query $query_var";
      }
   }
   break;

   case "Edit Greeting":
   $sub_group = "Greetings";
   if ($input['queue_name'] == NULL) $input['queue_name'] = " ";
   if ($input['greeting'] == NULL) $input['greeting'] = "" ;
   if ($input['replyto'] == NULL) $input['replyto'] = "help@nbcs.domain.tld";
   if ($input['greeting_subject'] == NULL) $input['greeting_subject'] = "Thank you for using the helpdesk.";
   if ($input['resolution'] == NULL) $input['resolution'] = "" ;
   if ($input['res_replyto'] == NULL) $input['res_replyto'] = "help@nbcs.domain.tld";
   if ($input['res_subject'] == NULL) $input['res_subject'] = "Thank you for using the helpdesk.";
   $queue_name = str_replace("_", " ", $input['queue_name']);
   $update = "update queue SET ";
   $update .= "greeting='".$input['greeting']."', ";
   $update .= "replyto ='".$input['replyto']."', ";
   $update .= "greeting_subject='".$input['greeting_subject']."', ";
   $update .= "resolution='".$input['resolution']."', ";
   $update .= "res_replyto ='".$input['res_replyto']."', ";
   $update .= "res_subject='".$input['res_subject']."' ";
   $where = " WHERE q_name='$queue_name'";
   $query_var = $update.$where;

   if (!mysql_query($query_var, $db)) {
      $output .= "error with query $query_var";
   }
   break; 
    
   case "Edit Watchers":
   $sub_group = "Watchers";
   global $arr_people;
   $queue = str_replace("_", " ", $input['queue']);
   $selectEW = "SELECT username FROM watcher WHERE q_name='$queue'";
   $resultEW = mysql_query($selectEW, $db);
   $number_of_current_watchers = 0;
   while ($rowEW = mysql_fetch_object($resultEW)) {
      $arrEW[$number_of_current_watchers] = $rowEW->username;
      $number_of_current_watchers++;
   }
   $delete_from = "DELETE FROM watcher";
   $EWwhere = " WHERE q_name='$queue'";
   $watcher_list = $input['watcher_list'];
   if (sizeof($watcher_list) > 0 ) {
      $EWwhere2 .= " AND";
      $i = 0;

      while (($i+1) <= $number_of_current_watchers ) {
         if ($watcher_list[$i] != NULL)
         $EWwhere2 .= " username = '$watcher_list[$i]' OR";
         $i++;
      }
      $EWwhere2 .= " username = 'This is a dummy string'";
      $delete_watcher = $delete_from.$EWwhere.$EWwhere2;
   }
   $mysql_delete_query = mysql_query($delete_watcher, $db);
   $UserOp = $input['UserOp'];
   $UserField = $input['UserField'];
   $UserString = $input['UserString'];
   if ($UserOp == "LIKE")
   $table = "staff WHERE $UserField $UserOp '%".$UserString."%' ";
   elseif($UserOp == "NOT LIKE")
   $table = "staff WHERE $UserField $UserOp '%$UserString%'";
   else
      $table = "staff WHERE $UserField $UserOp '$UserString'";
   if (($UserField == "username") && ($UserOp == "LIKE") && ($UserString != NULL))
   $arr_people = GetArrayFromTable("username", $table);
   $found_people = $input['found_people'];
   if ($found_people != NULL)

   foreach($found_people as $person) {

      $insert = "INSERT INTO watcher (q_name,username)";
      $insert .= " VALUES ('$queue','$person')";
      $result_insert = mysql_query($insert, $db);
   }
   break;

   case "Edit Supervisors":    
   $sub_group = "Supervisors";
   $queue = str_replace("_", " ", $input['queue']);
   $selectES = "SELECT username FROM q_supervisor WHERE q_name='$queue'";
   $resultES = mysql_query($selectES, $db);
   $number_of_current_supervisors = 0;
   while ($rowES = mysql_fetch_object($resultES)) {
      $arrES[$number_of_current_supervisors] = $rowES->username;
      $number_of_current_supervisors++;
   }
   $delete_from = "DELETE FROM q_supervisor";
   $ESwhere = " WHERE q_name='$queue'";
   $supervisor_list = $input['supervisor_list'];
   if (sizeof($supervisor_list) > 0 ) {
      $ESwhere2 .= " AND";
      $i = 0;

      while (($i+1) <= $number_of_current_supervisors ) {
         if ($supervisor_list[$i] != NULL)
         $ESwhere2 .= " username = '$supervisor_list[$i]' OR";
         $i++;
      }
      $ESwhere2 .= " username = 'This is a dummy string'";
      $delete_supervisor = $delete_from.$ESwhere.$ESwhere2;
   }
   $mysql_delete_query = mysql_query($delete_supervisor, $db);

   $UserOp = $input['UserOp'];
   $UserField = $input['UserField'];
   $UserString = $input['UserString'];

   if ($UserOp == "LIKE")
   $table = "staff WHERE $UserField $UserOp '%".$UserString."%' ";
   elseif($UserOp == "NOT LIKE")
   $table = "staff WHERE $UserField $UserOp '%$UserString%'";
   else $table = "staff WHERE $UserField $UserOp '$UserString'";
   if (($UserField == "username") && ($UserOp == "LIKE") && ($UserString != NULL))
   $arr_people = GetArrayFromTable("username", $table);
   $found_people = $input['found_people'];
   if ($found_people != NULL)
   foreach($found_people as $person) {
      $insert = "INSERT INTO q_supervisor (q_name,username)";
      $insert .= " VALUES ('$queue','$person')";
      $result_insert = mysql_query($insert, $db);
   }
   break;
    
   case "Edit Field":
   $sub_group = "Special_Fields";
   $queue = str_replace("_", " " , $input['queue']);
   $delete_value_list = $input['delete_value_list'];
   if ($delete_value_list) {

      foreach($delete_value_list as $deleteme) {
         $query = "Delete from q_specific_menu where field_name = '$field_edit' ";
         $query .= "and q_name='$queue' and item_value='$deleteme'";
         mysql_query($query, $db);
      }
   }

   $new_value = $input['new_value'];
   if ($new_value) {
      $query = "Insert into q_specific_menu set field_name = '$field_edit', ";
      $query .= "q_name='$queue', item_value='$new_value'";
      mysql_query($query, $db);
   }

   $old_field_display_name = $input['old_field_display_name'];
   if ($old_field_display_name) {
      $query = "Update q_specific_fields set field_display_name= ";
      $query .= "'$old_field_display_name', date_create=date_create ";
      $query .= "where field_name='$field_edit' and q_name='$queue'";
      mysql_query($query, $db);
   }

   $old_field_type = $input['old_field_type'];
   if ($old_field_type) {
      $query = "Update q_specific_fields set field_type= '$old_field_type', ";
      $query .= "date_create=date_create where field_name='$field_edit' ";
      $query .= "and q_name='$queue'";
      mysql_query($query, $db);
   }

   $new_required = $input['new_required'];
   if (!($new_required)) $new_required = 0;
   $query = "Update q_specific_fields set required= '$new_required', ";
   $query .= "date_create=date_create  where field_name='$field_edit' ";
   $query .= "and q_name='$queue'";
   mysql_query($query, $db);
   break;

   case "Edit Modules":
   $module_names = GetArrayFromTable("module_name", "modules");
   $i = 0;

   foreach ($modules as $enable) {
      SetModuleEnable($module_names[$i], $enable);
      $i++;
   }
   break;

   case "Special Fields":
   $sub_group = "Special_Fields";
   $queue = str_replace("_", " ", $input['queue']);
   $delete_from = "DELETE FROM q_specific_fields";
   $where = " WHERE q_name='$queue'";

   $delete_list = $input['delete_list'];
   if (sizeof($delete_list) > 0 ) {
      $where2 .= " AND (";
      $i = 0;
      for($i = 0; $i < sizeof($delete_list); $i++) {
         if ($delete_list[$i] != NULL)
         $where2 .= " field_name = '$delete_list[$i]' OR";
      }
      $where2 .= " field_name = 'This is a dummy string to cap off the or')";
      $delete_query = $delete_from.$where.$where2;
   }
   $mysql_delete_query = mysql_query($delete_query, $db);

   $add_list = $input['add_list'];
   if ($add_list) {

      foreach($add_list as $add) {
         $f_row = GetFirstRowFromTable("q_specific_fields where date_create='$add'");
         $f_queue = $f_row->q_name;
         $f_field_name = $f_row->field_name;
         $f_field_display_name = $f_row->field_display_name;
         $f_field_type = $f_row->field_type;
         $f_required = $f_row->required;
         $insert = "Replace q_specific_fields set field_name='$f_field_name', ";
         $insert .= "field_display_name='$f_field_display_name', ";
         $insert .= "field_type='$f_field_type', q_name='$queue', ";
         $insert .= "required='$f_required'";
         $result_insert = mysql_query($insert, $db);
             
         if ($f_field_type != "text") {
            $field = "item_value";
            $table = "q_specific_menu";
            $where = "field_name='$f_field_name' and q_name='$f_queue'";
            $item_value_array = GetArrayFromTable($field, $table, $where);

            if ($item_value_array) {

               foreach($item_value_array as $item_value) {
                  $query = "Insert into q_specific_menu ";
                  $query .= "set field_name='$f_field_name', ";
                  $query .= "q_name='$queue', item_value='$item_value'";
                  mysql_query($query, $db);
               }
            }
         }
      }
   }

   $new_field_name = $input['new_field_name'];
   $new_field_display_name = $input['new_field_display_name'];
   $new_field_type = $input['new_field_type'];
   $new_required = $input['new_required'];
   if ($new_field_name) {
      $new_field_name = FixVarName($new_field_name);
      if (!($new_field_display_name)) $new_field_display_name = $new_field_name;
      if (!($new_required)) $new_required = 0;
      $query = "Insert into q_specific_fields set ";
      $query .= "field_name='$new_field_name', ";
      $query .= "field_display_name='$new_field_display_name', ";
      $query .= "field_type='$new_field_type', q_name='$queue', ";
      $query .= "required='$new_required'";
      mysql_query($query, $db);
   }
   break;
    
   case "Edit Scripts":
   $sub_group = "Scripts";
   $queue = str_replace("_", " ", $input['queue']);
   $item = $input['item'];
   $egs = $input['egs'];
   $eqs = $input['eqs'];
   if ($item == "Global") {
      $queue = "global";
      if ($egs != NULL) {
         foreach($egs as $egs2) {
            $delete = "DELETE FROM script WHERE queue='global' AND id=$egs2";
            $result = mysql_query($delete, $db);
         }
      }
   }
   else {
      if ($eqs != NULL) {
         foreach($eqs as $eqs2) {
            $delete = "DELETE FROM script WHERE queue='$queue' AND id=$eqs2";
            $result = mysql_query($delete, $db);
         }
      }
   }

   $NewScriptProvision = $input['NewScriptProvision'];
   $NewScriptAction = $input['NewScriptAction'];
   $NewScriptTemplate = $input['NewScriptTemplate'];
   if (($NewScriptProvision != "-") && ($NewScriptAction != "-") && ($NewScriptTemplate != "-")) {
      $insert = "INSERT INTO script (queue,provision,action,template) VALUES";
      $insert .= " ('$queue','$NewScriptProvision','$NewScriptAction','$NewScriptTemplate')";
      $insert_script = mysql_query($insert, $db);
   }

   if (($NewScriptProvision != "-") && ($NewScriptAction != "-") && ($NewScriptTemplate == "-")) {
      $insert = "INSERT INTO script (queue,provision,action)";
      $insert .= "VALUES ('$queue','$NewScriptProvision','$NewScriptAction')";
      $insert_script = mysql_query($insert, $db);
   }

   if (($NewScriptProvision == "-") && ($NewScriptAction == "-") && ($NewScriptTemplate != "-")) {
      $insert = "INSERT INTO script (queue,template)";
      $insert .= "VALUES ('$queue','$NewScriptTemplate')";
      $insert_script = mysql_query($insert, $db);
   }
   break;

   case "Edit Group Rights":
   $sub_group = "Group_Rights";
   $queue = str_replace("_", " ", $input['queue']);
   $rights_to_revoke = $_POST['rights_to_revoke'];
   if ($rights_to_revoke == NULL) $rights_to_revoke = $_GET['rights_to_revoke'];
   $AddRightsFor = $_POST['AddRightsFor'];
   if ($AddRightsFor == NULL) $AddRightsFor = $_GET['AddRightsFor'];
   if ($_POST['item'] == "Global" || $_GET['item'] == "Global") $queue = "global";
   if ($AddRightsFor != NULL) {
      foreach($AddRightsFor as $group_and_rights) {
	$group_rights_array = preg_split('/---/', $group_and_rights);
	$group = $group_rights_array[0];
	$rights = $group_rights_array[1];
	$insert_group_rights = "INSERT INTO groupright (group_name,group_right,queue)";
	$insert_group_rights .= " VALUES ('$group','$rights','$queue')";
	$inserted = mysql_query($insert_group_rights, $db);
      }
   }

   if ($rights_to_revoke != NULL) {
      foreach($rights_to_revoke as $revoked_rights) {
	$revoked_array_one = preg_split('/:::/', $revoked_rights);
	$queue = $revoked_array_one[0];
	$revoked_array_two = preg_split('/---/', $revoked_array_one[1]);
	$group = $revoked_array_two[0];
	$rights = $revoked_array_two[1];
	$queue = str_replace("_", " ", $queue);
	$revoke = "DELETE FROM groupright WHERE queue = '$queue'";
	$revoke .= " AND group_name = '$group' AND group_right = '$rights'";
	$revoked = mysql_query($revoke, $db);
      }
   }
   break;

   case "Edit User Rights";
   $sub_group = "User_Rights";
   $queue = str_replace("_", " ", $input['queue']);
   $rights_to_revoke = $_POST['rights_to_revoke'];
   if ($rights_to_revoke == NULL) $rights_to_revoke = $_GET['rights_to_revoke'];
   $AddRightsFor = $_POST['AddRightsFor'];
   if ($AddRightsFor == NULL) $AddRightsFor = $_GET['AddRightsFor'];
   if ($_POST['item'] == "Global" || $_GET['item'] == "Global") $queue = "global";

   if ($AddRightsFor != NULL) {
      foreach($AddRightsFor as $user_and_rights) {
	 $rights_array = preg_split('/---/', $user_and_rights);
         $user = $rights_array[0];
         $rights = $rights_array[1];
         $insert_user_rights = "INSERT INTO staffright (username,staff_right,queue)";
         $insert_user_rights .= " VALUES ('$user','$rights','$queue')";
         $inserted = mysql_query($insert_user_rights, $db);
      }
   }

   if ($rights_to_revoke != NULL) {
      foreach($rights_to_revoke as $revoked_rights) {
         $rights_array = preg_split('/(---)|(:::)/', $revoked_rights);
         $queue = $rights_array[0];
         $user = $rights_array[1];
         $rights = $rights_array[2];         
         $queue = str_replace("_", " ", $queue);         
         $revoke = "DELETE FROM staffright WHERE queue = '$queue'";
         $revoke .= " AND username = '$user' AND staff_right = '$rights'";
         $revoked = mysql_query($revoke, $db);
      }
   }
   break;

   case "Edit Allow Mail Comments":
   $mail_comments = $input['mail_comments'];
   $q_name = str_replace("_", " ", $input['queue']);
   $query = "UPDATE queue SET ";
   $query .= "mail_comments='$mail_comments' ";
   $query .= "WHERE q_name='$q_name'";
   $result = mysql_query($query, $db);

   if (!$result) {
      $output .= "<p>MySQL Error:  $query<p>Unable to update mail comments\n";
   }
   break;    
}    

//-----------------------------------------------------------------------------
//
// Function: BasicQueueForm
// Description:
//    Generates the output for the BasicQueueForm
// Parameters:
//    string $item	Selected Item
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none
//-----------------------------------------------------------------------------
function BasicQueueForm($item = "", $queue = "") {
   global $username;
   $output = "";

   if ($queue != "Create_a_new_queue") {
      $queue_name = $queue;
      $queue = str_replace("_", " ", $queue);
      $select = "SELECT * FROM queue WHERE q_name='$queue'";
      global $db;
      $result = mysql_query($select, $db);
      $row = mysql_fetch_object($result);
   }
   $output .= "<form><input type=hidden name=\"form_submitted\" value=\"Edit Basics\">"
           ."<input type=hidden name=\"item\" value=\"Queues\">"
           ."<input type=hidden name=\"queue\" value=\"$queue\">"
           ."<table cellpading=\"2\" width=\"100%\">";

   if ($queue != "Create_a_new_queue") {
      $output .= OpenTable("Name:")
              ."<input type=\"hidden\" name=\"queue_name\" value=\"$queue_name\" size=32>"
              .$queue_name.CloseTable();
   } 

   else {
      $output .= OpenTable("Name:")
              ."<input name=\"queue_name\" value=\"$row->q_name\" size=32>"
              .CloseTable();
   }
   $output .= OpenTable("Description:")
           ."<input name=\"queue_descrip\" value=\"$row->description\" size=60>"
           .CloseTable().OpenTable("Correspondence Address:")
           ."<input name=\"correspond\" value=\"$row->correspondence_address\" size=22>"
           .CloseTable().OpenTable("Comment Address:")
           ."<input name=\"comment\" value=\"$row->comment_address\" size=22>"
           .CloseTable().OpenTable("Priority starts at:")
           ."<input name=\"priority\" value=\"$row->priority_starts_at\" size=22>"
           .CloseTable().OpenTable("Requests should be due in:")
           ."<input name=\"requests_due\" value=\"$row->allowabledays\" size=3>"
           .Font("days (max of 28)")
           .CloseTable().OpenTable("Over time priority moves to:")
           ."<input name=\"priority_to\" value=\"$row->overtime_moves_to\" size=22>"
           .CloseTable().OpenTable("Color (6 character Hex):")
           ."<input name=\"color\" value=\"$row->color\" size=10>";
/*           .CloseTable().OpenTable("Queue Grouping:");
   $current_group=$row->q_group;
   ${$current_group}=" selected";
   $field_name="q_group";
   $query="select q_group from queue group by q_group";
   $groups=GetArrayFromQuery($query, $field_name);
   $size=sizeof($groups);
   $output .= "Select group<select name=\"q_group\">";

   for($i = 0; $i < $size; $i++) {
      $output .= "<option value=\"".$groups[$i]."\" ".${$groups[$i]}.">"
              .$groups[$i]."</option>";
   }
   $output .= "</select>, Or create new group<img border=0 src=\"images/arrow.png\">"
           ."<input type=\"text\" name=\"q_group_new\" value=\"\" size=10>"
           .CloseTable();
*/
   if ($row->require_resolve) $chk = "checked";
   else $chk = "";
   $output .= OpenTable("Require comment on resolve:")
           ."<input type=\"checkbox\" name=\"require_resolve\" value=1 $chk>"
           .CloseTable();
   if ($row->enabled != 0) $chk = "checked";
   else $chk = "";
   $output .= OpenTable("Enabled:")
           ."<input type=\"checkbox\" name=\"enabled\" value=1 $chk>"
           .Font("(Unchecking this box disables this queue)")
           .CloseTable()."</table>";
   if (DoesUserHaveRight($username, "AdminQueue", $queue)){
     $output .= "<p><div align=\"right\">"
       ."<input type=\"submit\" value=\"Submit\"></div>";
   }
   $output .= "</form>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: GreetingQueueForm
// Description:
//    Allows configuration of the greeting messages sent to users.
// Parameters:
//    string $item	not used
//    string $queue	Queue Name
// Return Values:
//    Returns the HTML to output.
// Remarks:
//    none
//-----------------------------------------------------------------------------
function GreetingQueueForm($item = "", $queue = "") {
   global $db,$username,$self;
   $output = "";
   $queue_name = $queue;
   $queue = str_replace("_", " ", $queue);
   $select = "SELECT * FROM queue WHERE q_name='$queue'";
   $result = mysql_query($select, $db);
   $row = mysql_fetch_object($result);
   $output = "<form action=$self method=post>"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Greeting\">"
           ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
           ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
           ."<input type=\"hidden\" name=\"queue_name\" value=\"$queue\">"
           ."<table cellpading=2 width=\"100%\">"
           .OpenTable("Commands For Automatic Insert:")
           .Font("user_full_name, ticket_id, requester_email, ticket_subject, allowabledays, survey_url.<br>")
           .CloseTable().OpenTable("Greeting")."This is sent to the user when the ticket is created."
           .CloseTable().OpenTable("From Address:")
           ."<input name=\"replyto\" value=\"$row->replyto\" size=60>"
           .CloseTable().OpenTable("Greeting Subject:")
           ."<input name=\"greeting_subject\" value=\"$row->greeting_subject\" size=60>"
           .CloseTable().OpenTable("Greeting Body:");
   $greeting_exists = str_replace(" ", "", $row->greeting);

   if ($greeting_exists) {
      $greeting=$row->greeting;
   }

   else{
      $greeting = "";
   }

   $output .= "<textarea name=\"greeting\" cols=60 rows=10>$greeting</textarea>";

   if ($greeting == ""){
      $empty_note = "<b>There is no greeting currently set.</b>";
   }
   $output .= CloseTable().OpenTable("<BR>")
           .Font("<br>Note: <font color=\"red\">$empty_note</font> If greeting body is left blank, no email will be sent to the user.<br>")
           .CloseTable().OpenTable("Resolution")
           ."This is sent to the user when the ticket is resolved."
           .CloseTable().OpenTable("From Address:")
           ."<input name=\"res_replyto\" value=\"$row->res_replyto\" size=60>"
           .CloseTable().OpenTable("Resolution Subject:")
           ."<input name=\"res_subject\" value=\"$row->res_subject\" size=60>"
           .CloseTable().OpenTable("Resolution Body:");
	
   $res_exists = str_replace(" ", "", $row->resolution);

   if ($res_exists){
      $resolution=$row->resolution;
   }

   else{
      $default_greeting = "";
      $greeting=$default_greeting;
   }
   $output .= "<textarea name=\"resolution\" cols=60 rows=10>$resolution</textarea>";

   if ($resolution == "") {
      $empty_note = " <b>There is no resolution currently set. </b>";
   }
   $output .= CloseTable().OpenTable("")
              .Font("<br>Note: <font color=\"red\">$empty_note</font> If resolution body is left blank, no email will be sent to the user.")
              .CloseTable()."</table>";
   if (DoesUserHaveRight($username, "AdminQueue", $queue)){
      $output .= "<p><div align=\"right\">"
              ."<input type=\"submit\" value=\"Submit\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: WatchersQueueForm
// Description:
//    Produces HTML to allow new watchers to be added to a queue and current
//    watchers deleted.
// Parameters:
//    string $item	Not Used
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Also references global values $username and $arr_people.
//
//-----------------------------------------------------------------------------
function WatchersQueueForm($item = "", $queue = "") {
   global $username,$arr_people;
   $output = "<form><input type=\"hidden\" name=\"form_submitted\" value=\"Edit Watchers\">"  
	   ."<table border=0><tr><td width=\"50%\" valign=\"top\">"
           ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
           ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
           ."<input type=\"hidden\" name=\"sub_group\" value=\"Watchers\">"
           .FontBold("Current Watchers").Font("<br>(<i>Check box to delete</i>)");
   $hierarchy = "item=Queues&queue";
   $queue = str_replace("_", " ", $queue);
   $array = GetWatchers($queue);
   $output .= "<ol>".PrintCheckboxArray("watcher_list", $array)
	   ."</ol></td><td valign=\"top\">"
           .FontBold("New Watchers")."<p>"
           .FindUsersHtml()."<br>"
           .Font("Add new watchers:")."<p>"
           .PrintCheckboxArray("found_people", $arr_people)
           ."</td></tr></table>";
   if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
        $output .= "<p align=\"right\"><div align=\"right\">"
                .FontBold("If you've updated anything above, be sure to")
                ."<input type=\"submit\" value=\"Save Changes\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: SupervisorsQueueForm
// Description:
//    Produces HTML to allow supervisors to be added and deleted from queue.
// Parameters:
//    string $item	Not Used
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Also references global values $username and $arr_people
//-----------------------------------------------------------------------------
function SupervisorsQueueForm($item = "", $queue = "") {
   global $username,$arr_people;
   $output = "<form>"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Supervisors\">"
           ."<table border=0><tr><td width=\"50%\" valign=\"top\">"
           ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
           ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
           ."<input type=\"hidden\" name=\"sub_group\" value=\"Supervisors\">"
           .FontBold("Current Superviors").Font("<br>(<i>Check box to delete</i>)");
   $hierarchy = "item=Queues&queue";
   $queue = str_replace("_", " ", $queue);
   $array = GetSupervisors($queue);
   $output .= "<ol>"
           .PrintCheckboxArray("supervisor_list", $array)
           ."</ol></td><td valign=\"top\">"
           .FontBold("New Supervisors")
           ."<p>".FindUsersHtml()."<br>"
           .Font("Add new supervisors:")
           ."<p>".PrintCheckboxArray("found_people", $arr_people)
           ."</td></tr></table>";

   if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
      $output .= "<p align=\"right\"><div align=\"right\">"
              .FontBold("If you've updated anything above, be sure to")
              ."<input type=\"submit\" value=\"Save Changes\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: SpecialFieldsQueueForm
// Description:
//    Produces HTML to allow editing of Special Fields for a specific queue.
// Parameters:
//    string $item	Not Used
//    string $queue	Queue Name
//    boolean $f_e	Not Used
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Global values $username and $field_edit used.  Should be changed to use
//    $f_e.
//-----------------------------------------------------------------------------
function SpecialFieldsQueueForm($item = "", $queue = "", $f_e = "") {
   global $username,$field_edit;
   $output = "";
   if ($field_edit) {
     $output .= SpecialFieldsQueueSubformEditField();
   }
   $output .= "<form>"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Special Fields\">"
           ."<table border=0><tr><td width=\"50%\" valign=\"top\">"
           ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
           ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
           ."<input type=\"hidden\" name=\"sub_group\" value=\"Special_Fields\">"
           .FontBold("Current Special Fields")
           .Font("<br>(<i>Check box to delete. Click field name to edit that field.</i>)");
   $hierarchy = "item=Queues&queue";
   $queue = str_replace("_", " ", $queue);
   $result = GetSpecialFields($queue);
   $output .= "<ol>"
           .PrintSpecialFieldsCheckboxArray("delete_list", $result)
           ."</ol></td><td valign=\"top\">"
           .FontBold("Add Fields")."<p>";
   $result = GetSpecialFields("", $queue);
   $output .= PrintSpecialFieldsCheckboxArray("add_list", $result, "1");

   if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
      $output .= FontBold("If you've updated anything, be sure to ")
              ."<input type=\"submit\" value=\"Save Changes\">";
   }
   $output .= "<br></td></tr><tr><td><br>"
           .FontBold("Or Create A New Field:")
           ."<hr><p>";
   $field_types = array("text", "menu", "radio", "checkbox");
   $field_types_display = array("Text Box", "Menu Drop Down", "Radio Button Group");
   $output .= SetNameSetInput("new_field", "Field Variable Name", "", "new_field_name")
           .SetNameSetInput("new_field", "Field Display Name", "", "new_field_display_name")
           .SetNameSetInput("new_field", "Field Type", $field_types, "new_field_type", "menu", "", "", "radio", $field_types_display)
           .SetNameSetInput("new_field", "Required", "1", "new_required", "checkbox")
           ."</td></tr></table>";

   if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
      $output .= "<p align=\"center\"><div align=\"center\">"
              .FontBold("")
              ."<input type=\"submit\" value=\"Create New Field\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: SpecialFieldsQueueSubformEditField
// Description:
//    Produces HTML to edit special fields for a queue.
// Parameters:
//    none.
// Return Values:
//    Returns HTML to output the form.
// Remarks:
//    Uses global values of $username, $field_edit, and $queue instead of
//    parameteres.
//-----------------------------------------------------------------------------
function SpecialFieldsQueueSubformEditField() {
   global $username,$field_edit;
   $queue = $_GET['queue'];
   $output = "<form>"
             ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Field\">"  
             ."<table border=0><tr><td with=\"100%\" valign=\"top\">test</td></tr>"
             ."<tr><td width=\"50%\" valign=\"top\">"
             ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
             ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
             ."<input type=\"hidden\" name=\"sub_group\" value=\"Special_Fields\">"
             ."<input type=\"hidden\" name=\"field_edit\" value=\"$field_edit\"><br>"
             .FontBold("Delete values from the field \"$field_edit\"")
             .Font("<br>(<i>Check box to delete.</i>)");
   $hierarchy = "item=Queues&queue";
   $queue = str_replace("_", " ", $queue);
   $result = GetArrayFromTable("item_value", "q_specific_menu", "where q_name='$queue' and field_name='$field_edit'");
   $output .= "<ol>".PrintCheckboxArray("delete_value_list", $result)
           ."</ol></td><td valign=\"top\"><p>"
           .SetNameSetInput("new_value_set", "Create New Value", "", "new_value")
           ."<br></td></tr><tr><td><br>"
           .FontBold("Edit Field Information For &quot;$field_edit"."&quot;.")
           ."<hr><p>";
   $table = "q_specific_fields where field_name='$field_edit' and q_name = '$queue'";
   $field_row = GetFirstRowFromTable($table);
   $field_types = array("text", "menu", "radio", "checkbox");
   $field_types_display = array("Text Box", "Menu Drop Down", "Radio Button Group");
   $output .= SetNameSetInput("new_field", "Field Display Name", $field_row->field_display_name, "old_field_display_name")
           .SetNameSetInput("new_field", "Field Type", $field_types, "old_field_type", "menu", "", "", $field_row->field_type, $field_types_display);
   if ($field_row->required) $checked = "checked";
   $output .= SetNameSetInput("new_field", "Required", "1", "new_required", "checkbox", "", $checked)
           ."</td></tr></table>";

   if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
      $output .= "<p align=\"center\"><div align=\"center\">"
              ."<input type=\"submit\" value=\"Save Changes For Field\"><hr></div>";
   }
   $output .= "</form>";
   return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: ModulesGlobalForm
// Description:
//    Dynamic function to allow modules to be Enabled or Disabled.
// Parameters:
//    none.
// Return Values:
//    Returns HTML to produce a list of modules with radio buttons.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function ModulesGlobalForm() {
   global $db,$username;
   $result = mysql_query("select * from modules order by module_name")
   or die ("No Modules Installed.");
   $output = "<form>"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Modules\">"
           ."<input type=\"hidden\" name=\"item\" value=\"Global\">"
           ."<input type=\"hidden\" name=\"global\" value=\"Modules\">"
           ."<table border=0><tr><td>".FontBold("Global Modules:")
           ."</td></tr>";
   $i = 0;

   while ($module = mysql_fetch_array($result)) {
      $output .= "<tr><td>".FontBold($module[0]);
      if ($module[1]) {
         $output .= '<input type="radio" name="modules['.$i.']" value="1" checked>Enabled'
                 .'<input type="radio" name="modules['.$i.']" value="0">Disabled'
                 .'</td></tr>';
      }
          
      else {
         $output .= '<input type="radio" name="modules['.$i.']" value="1">Enabled'
                 .'<input type="radio" name="modules['.$i.']" value="0" checked>Disabled'
                 .'</td></tr>';
         }
         $i++;
   }
   $output .= "</table>";

   if (DoesUserHaveRight($username, "AdminGlobal")) {
      $output .= "<p><div align=\"right\"><input type=\"submit\" value=\"Submit\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: ScriptsQueueForm
// Description:
//    Produces HTML to allow editing of Global Options.
// Parameters:
//    none.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function OptionsGlobalForm(){
   global $db, $username;
   $query = "select * from system_variables where display_order>=0 order by display_order";
   $result = mysql_query($query, $db);

   $output .= "<form method=\"post\">\n"
           ."<input type=\"hidden\" name=\"item\" value=\"Global\">\n"
           ."<input type=\"hidden\" name=\"sub_group\" value=\"Global_Options\">\n"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit_Global_Options\">\n"
           ."<table border=0>\n";

   // Display Options
   if ($result) {
      while ($option = mysql_fetch_array($result)) {
         $display_name = ucwords(str_replace("_", " ", $option['var']));
         if ($display_name == "Authentication Type") {
            $display_option = RadioField($option['var'], "local", "Local Authentication", $option['val'])."<br>"
                             .RadioField($option['var'], "extensible", "Extensible Authentication", $option['val']);
         }
         elseif ($display_name == "User Lookup Type") {
            $display_option = RadioField($option['var'], "local", "Local Lookup", $option['val'])."<br>"
                             .RadioField($option['var'], "extensible", "Extensible Lookup", $option['val']);
         }
         elseif ($display_name == "Session Expire") {
            $times = array(300 => "Expire in 5 minutes.",
                           900 => "Expire in 15 minutes.",
                           1800 => "Expire in 30 minutes.",
                           3600 => "Expire in 1 hour.",
                           7200 => "Expire in 2 hours.",
                           14400 => "Expire in 4 hours.",
                           28800 => "Expire in 8 hours.",
                           57600 => "Expire in 12 hours.");
            $display_option = SelectField($option['var'], $times, $option['val']);
         }
	 elseif ($display_name == "Help Email Address") {
	   $display_option = TextField($option['var'], $option['val'], 30);
         }
         elseif ($display_name == "Comment Email Address") {
	   $display_option = TextField($option['var'], $option['val'], 30);
         }
         else $display_option = TextField($option['var'], $option['val']);
         $output .= FormRow($display_name, $display_option);
      }
   }
   else $output .= FontBold("No Options Found.");
   if (DoesUserHaveRight($username, "AdminGlobal")) {
      $output .= FormRow ("", SubmitField());
   }
   $output .= EndFormTable();
   $output .= EndForm();
   return $output;
}    

//-----------------------------------------------------------------------------
//
// Function: ScriptsQueueForm
// Description:
//    Produces HTML to allow editing of Scripts for a Queue.
// Parameters:
//    string $item	Allows choice of specific Queue or Global editing.
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function ScriptsQueueForm($item = "", $queue = "") {
  global $username;
  $priv_name = "AdminGlobal";
  $output = "";
  if ($item == "Queues") {
    $priv_name = "AdminQueue";
    $g_scripts = GetGlobalScripts();
    $q_scripts = GetQueueScripts($queue);
    $output .= "<form>"
      ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Scripts\">"
      ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
      ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
      ."<input type=\"hidden\" name=\"sub_group\" value=\"Scripts\">"
      .FontBold("Global Scripts:")
      .Font("(check box to delete script)<br>")
      .PrintScriptsCheckbox("global", "egs", $g_scripts)
      ."<p>".FontBold("Queue Scripts:")
      .Font("(check box to delete script)<br>")
      .PrintScriptsCheckbox($queue, "eqs", $q_scripts)
      ."<p>".FontBold("Add a script to this queue:")."<p>";
  }
  elseif ($item == "Global") {
    $g_scripts = GetGlobalScripts();
    $output .= "<form>"
      ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Scripts\">"
      ."<input type=\"hidden\" name=\"item\" value=\"Global\">"
      ."<input type=\"hidden\" name=\"global\" value=\"$queue\">"
      .FontBold("Global Scripts:").Font("(check box to delete script)<br>")
      .PrintScriptsCheckbox("global", "egs", $g_scripts)."<p>"
      .FontBold("Add a script which will apply to all queues:")."<form>";
  }

  $output .= "<table border=\"0\">"
    .OpenTable("Provision:")
    .PrintSelectArray("NewScriptProvision", GetProvisionArray(), "", "-")
    .CloseTable()."<p>".OpenTable("Action:")
    .PrintSelectArray("NewScriptAction", GetActionArray(), "", "-")
    .CloseTable()."<p>".OpenTable("Template:")
    .PrintSelectArray("NewScriptTemplate", GetTemplateArray(), "", "-")
    .CloseTable()."</table>";

  if (!empty($priv_name)) {
    if (DoesUserHaveRight($username, $priv_name, $queue)) {
      $output .= "<p><div align=\"right\">"
	."<input type=\"submit\" value=\"Submit\"></div>";
    }
  }
  
  $output .= "</form>";
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: PrintScriptsCheckbox
// Description:
//    Produces HTML for printing a checkbox list for scripts in a queue.
// Parameters:
//    string $queue_name	Not Used
//    string $name		Sets the name of the rows in the form.
//    mysqlobject $result	List of scripts to output.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function PrintScriptsCheckbox($queue_name, $name, $result) {
   $output = "<ul>";

   if (!($result)) {
      $output .=  "<li><i>none</i></li>";
   }
   $name = $name."[]";

   while ($row = mysql_fetch_object($result)) {
      $show = "PROVISION=$row->provision, ACTION=$row->action, TEMPLATE=$row->template<br>";
      $id = $row->id;
      $output .= "<blockquote><input type=\"checkbox\" name=\"$name\" value=\"$id\">$show</blockquote>";
   }
   $output .= "</ul>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetGlobalScripts
// Description:
//    Returns a mysql result object conatining all global scripts.
// Parameters:
//    none.
// Return Values:
//    Returns a mysql result object conatining all global scripts.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function GetGlobalScripts() {
   global $db;
   $queue = "global";
   $query = "Select * from script where queue='$queue'";
   return mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: GetQueueScripts
// Description:
//    Returns a mysql result object containing scripts for a specific queue.
// Parameters:
//    string $queue	Queue Name
// Return Values:
//    Returns a mysql result object containing scripts for a specific queue.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function GetQueueScripts($queue = "") {
   global $db;

   if ($queue == "") {
      global $queue;
   }
   $queue = str_replace("_", " ", $queue);
   $query = "Select * from script where queue='$queue'";
   return mysql_query($query, $db);
}
   
//-----------------------------------------------------------------------------
//
// Function: GroupRightQueueForm
// Description:
//    Produces HTML to allow editing of Group Rights for either a specific 
//    queue or globally.
// Parameters:
//    string $item	Determines Queues or Global
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output the form.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function GroupRightsQueueForm($item = "", $queue = "") {
   $output = "";

   if ($item == "Queues") {
      $output .= "<form>"
              ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
              ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
              ."<input type=\"hidden\" name=\"sub_group\" value=\"Group_Rights\">"
              ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Group Rights\">"
              .FontBold("Pseudogroups:")
              .RightTable(GetPseudoGroups(), $queue, "groupright")
              ."<p>".FontBold("Groups:")
              .RightTable(GetGroups(), $queue, "groupright");
   }

   elseif ($item == "Global") {
      $output .= "<form>"
              ."<input type=\"hidden\" name=\"item\" value=\"Global\">"
              ."<input type=\"hidden\" name=\"global\" value=\"Group_Rights\">"
              ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Group Rights\">"
              .FontBold("Pseudogroups:")
              .RightTable(GetPseudoGroups(), "global", "groupright")
              ."<p>".FontBold("Groups:")
	      .RightTable(GetGroups(), "global", "groupright");
   }
   $output .= "<p></form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: UserRightsQueueForm
// Description:
//    Produces HTML to allow editing of User Rights for either a specific queue 
//    or globally.
// Parameters:
//    string $item	Determines Queues or Global
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to output the form.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function UserRightsQueueForm($item = "", $queue = "") {
   $output = "";

   if ($item == "Queues") {
      $output .= "<form>"
              ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
              ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
              ."<input type=\"hidden\" name=\"sub_group\" value=\"User_Rights\">"
              ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit User Rights\">"
              .RightTable(GetStaff(), $queue, "staffright");
      if (DoesUserHaveRight($username,"AdminQueue",$queue)) {
         $output .= "<p><div align=\"right\">"
                 ."<input type=\"submit\" value=\"Apply Changes\"></div>";
      }
   }

   elseif ($item == "Global") {
      $output .= "<form>"
              ."<input type=\"hidden\" name=\"item\" value=\"Global\">"
              ."<input type=\"hidden\" name=\"global\" value=\"User_Rights\">"
              ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit User Rights\">"
              .RightTable(GetStaff(), "global", "staffright");
      if (DoesUserHaveRight($username,"AdminGlobal")) {
         $output .= "<p><div align=\"right\">"
                 ."<input type=\"submit\" value=\"Apply Changes\"></div>";
      }
   }

   $output .= "</form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: MailCommentsQueueForm
// Description:
//    Produces HTML to allow or disallow mail comments for a queue.
// Parameters:
//    string $item	Not Used.
//    string $queue	Queue Name
// Return Values:
//    Returns HTML to generate form.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function MailCommentsQueueForm($item = "", $queue = "") {
   $email = "comment@ruQueue.domain.tld";
   $subj = "\"Ticket: <u>number</u>\"";
   $special = "$email with the subject $subj";
   $ch1 = $ch2 = "";
   $q_name = str_replace("_", " ", $queue);
   $query = "select mail_comments from queue where q_name='$q_name'";
   global $db, $username;
   $result = mysql_query($query, $db);

   if ($result) {
      $obj = mysql_fetch_object($result);

      if ($obj->mail_comments) {
         $ch1 = "checked";
      }
      else {
         $ch2 = "checked";
      }
   }

   $output = "<form>"
           ."<input type=\"hidden\" name=\"item\" value=\"Queues\">"
           ."<input type=\"hidden\" name=\"queue\" value=\"$queue\">"
           ."<input type=\"hidden\" name=\"sub_group\" value=\"Allow_Mail_Comments\">"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"Edit Allow Mail Comments\">"
           ."<input type=\"radio\" name=\"mail_comments\" value=1 $ch1>"
           ."Yes, allow email sent to <i>$special</i> to add comments to this queue." 
           ."<p><input type=\"radio\" name=\"mail_comments\" value = 0 $ch2>"
           ."No, do NOT allow email sent to <i>$special</i> to add comments to this queue.";

   if (DoesUserHaveRight($username,"AdminQueue", $queue)) {
      $output .= "<p><div align=\"right\">"
              ."<input type=\"submit\" value=\"Apply Changes\"></div>";
   }
   $output .= "</form>";
   return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: RightTable
// Description:
//    Generates HTML to display the rightmost column in the admin hierarchy.
// Parameters:
//    array $array		Items to output in menu.
//    string $queue		Global or Queue Name.
//    string $group_or_user	Rights to edit.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function RightTable($array, $queue, $group_or_user) {
   global $username;
   $value_rights = GetRights();
   $show_rights = $value_rights;
   $output = "";

   for ($i = 0; $i < sizeof($array); $i++) {
      $output .= '<table border = "0" width="100%"><form method=post>';

      if ($queue == "global") {
         $output .= '<input type ="hidden" name="item" value = "Global">';

         if ($group_or_user == "groupright") {
            $output .= '<input type ="hidden" name="global" value = "Group_Rights">'
                    .'<input type="hidden" name="form_submitted" value="Edit Group Rights">';
         } 

         else {
            $output .= '<input type ="hidden" name="global" value = "User_Rights">'
                    .'<input type="hidden" name="form_submitted" value="Edit User Rights">';
         }
      } 

      else {
         $output .= '<input type = "hidden" name = "item" value = "Queues" >'
                 .'<input type ="hidden" name="queue" value = "'.$queue.'">';

         if ($group_or_user == "groupright") {
            $output .= '<input type ="hidden" name="sub_group" value = "Group_Rights">'
                    .'<input type="hidden" name="form_submitted" value="Edit Group Rights">';
         } 

         else {
             $output .= '<input type ="hidden" name="sub_group" value = "User_Rights">'
                     .'<input type="hidden" name="form_submitted" value="Edit User Rights">';
         }
      }
          
      $output .= '<p><tr><td align = "right">'.FontBold($array[$i])
              ."</td><td width='30%'>".Font("New Rights")
              ."</td><td width='30%'>".Font("Current Rights")
              ."</td><td width='20%'>";
      if (DoesUserHaveRight($username, "ModifyACL", $queue)){
         $output .= "<input type=\"submit\" size=\"2\" value=\"Modify $array[$i]\">";
      }
      $output .= "</td></tr><tr><td>&nbsp;</td><td>"
              .'<select multiple name = "AddRightsFor[]" size = "5">'
              .CreateSelectOptions($value_rights, "open", "", "", $array[$i])
              .'</select></td><td valign = "top">'
              .Font("<i>(Check box to revoke right)</i>");
      $rights_grp_or_user = GetRightsOf($array[$i], $queue, $group_or_user);
      $output .= "<ol>";
      for($j = 0; $j <sizeof($rights_grp_or_user); $j++) {
         $output .= Font("<li><input type=checkbox name=\"rights_to_revoke[]\" value=\"$queue:::$array[$i]---$rights_grp_or_user[$j]\">$rights_grp_or_user[$j]");
      }
      $output .= "</ol></td></tr></form>";
   }
   $output .= "</table>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: SetModuleEnable
// Description:
//    Enables or disables a module
// Parameters:
//    string $module_name	Module Name.
//    boolean $use_module	0 - don't use module 1 - use module
// Return Values:
//    none
// Remarks:
//    none
//-----------------------------------------------------------------------------
function SetModuleEnable($module_name, $use_module) {
   global $db;
   $query = "update modules set use_module=$use_module where module_name='$module_name'";
   $result = mysql_query($query, $db);
}
?>