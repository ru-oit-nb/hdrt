<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


//
// Filename: functions-date.php
// Description: Contains functions for date manipulation - used mostly in scripts.
// Supprted Language(s):   PHP 4.0
//
    
//-----------------------------------------------------------------------------
//
// Function: NBDaysAgo
// Description:
//    Counts business days backwards from the provided date.
// Parameters:
//    int $b_days	Number of days to count back (max 28)
//    date $date	Date to count from, defaults to current date.
// Return Values:
//    Returns a date in "YYYY-MM-DD" format, which will be the date from 
//    $b_days business days before $date
// Remarks:
//    If no arguments are passed for $date, then it will count back from the 
//    current date.
//-----------------------------------------------------------------------------
function NBDaysAgo($b_days = "", $date = "") {
       
   // check arguments
   if ($b_days == 0)
      return date("Y-m-d");
   if (empty($b_days)) {
      print "NBDaysAgo() needs 1st arguent";
      spacer(2);
      return 0;
   } elseif ($b_days > 28) {
      print "NBDaysAgo() can't count more than 28 b_days";
      spacer(2);
      return 0;
   }
       
   if (!empty($date)) {
      if (!date_good($date)) {
         $today = date("Y-m-d");
         $err_msg = "$date is not a valid date.  ";
         $err_msg .= "Using $today.  <br>";
         $date = $today;
         print $err_msg;
      }
      $d = substr($date, -2);
      $m = substr($date, -5, -3);
      $y = substr($date, 0, 4);
      $time_stamp = mktime(0, 0, 0, $m, $d, $y);
   } else {
      $time_stamp = time();
   }
       
   // initialize values
   $year = date("Y", $time_stamp);
   $month = date("n", $time_stamp);
   $day = date("j", $time_stamp);
    
   $current_month = MonthArray($month, $year);
       
   if ($month == "1") {
      $p_year = $year - 1;
      $p_month = 12;
      $prev_month = MonthArray($p_month, $p_year);
      $holiday1 = HolidayCalculatorArray($p_year);
      $holiday2 = HolidayCalculatorArray($year);
      $holidays = array_merge($holiday1, $holiday2);
   } else {
      $p_month = $month - 1;
      $prev_month = MonthArray($p_month, $year);
      $holidays = HolidayCalculatorArray($year);
   }
       
   // time saving loop determines if holidays should be checked for
   // keeps things in linear time when possible
   $check_for_holidays = 0;
   for ($i = 0; $i < sizeof($holidays); $i++) {
      $hol_month = substr($holidays[$i], -5, -3);
      if ($hol_month < 10) {
         $hol_month = substr($hol_month, 1);
      }
      if (($hol_month == $p_month) or ($hol_month == $month) ) {
         $check_for_holidays = 1;
         break;
      }
   }
    
   $prev_two_months = array_merge($prev_month, $current_month);
    
   // this is the correct place to start in the merged array
   $jump_in = ($day + sizeof($prev_month) - 1);
    
   $ret = $i = 0;
   while ($i <= $b_days) {
      $this_day = $prev_two_months[$jump_in][0];
      if (($this_day != "Saturday") and ($this_day != "Sunday")) {
         $potential_day = $prev_two_months[$jump_in][1];
         if ($check_for_holidays) {
            // avoid 2nd loop
            if (!in_array($potential_day, $holidays)) {
               $i += 1;
               $ret = $potential_day;
            }
         } else {
            $i += 1;
            $ret = $potential_day;
         }
      }
      $jump_in -= 1;
   }
   return $ret;
}
    
//-----------------------------------------------------------------------------
//
// Function: MonthArray
// Description:
//    Generates an array representation of a month.
// Parameters:
//    int $month	Month to generate.
//    int $year		Year of $month.
// Return Values:
//    Returns an 2D-array representation of a month as described below.
//    $a[m][0] = day_of_week from (m,t,w,th,fr,sa,su)
//    $a[m][1] = yyyy-mm-dd
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function MonthArray($month, $year) {
   $nextmonth = $month+1;
   $lastday = mktime(0, 0, 0, $nextmonth, 0, $year);
   $lastday = date(d, $lastday);
   $array = array();
    
   for($counter = 1; $counter <= $lastday; $counter++) {
      $day = mktime(0, 0, 0, $month, $counter, $year);
      $day_of_week = date(l, $day);
      $array[$counter][0] = $day_of_week;
       
      if ($month < 10) {
         $cop_month = "0$month";
      } else {
         $cop_month = $month;
      }
      if ($counter < 10) {
         $cop_count = "0$counter";
      } else {
         $cop_count = $counter;
      }
       
      $array[$counter][1] = "$year-$cop_month-$cop_count";
   }
   return $array;
}
    
//-----------------------------------------------------------------------------
//
// Function: FormatDate
// Description:
//    This function will pad single digit months/days and will pad years to
//    four digits using leading zeros.  It will then return the date formatted 
//    as YYYY-MM-DD.  If invalid lengths are passed (empty string or long
//    string) the current date is returned.
// Parameters:
//    string $year	Year
//    string $month	Numerical Month
//    string $day
// Return Values:
//    Correctly formatted date.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function FormatDate($year, $month, $day) {
   if (strlen($year) == 0 || strlen($month) == 0 || strlen($day) == 0)
      return date("Y-m-d");
   if (strlen($year) > 4 || strlen($month) > 2 || strlen($day) > 2)
      return date("Y-m-d");
   while (strlen($year) < 4) $year = "0".$year;
   while (strlen($month) < 2) $month = "0".$month;
   while (strlen($day) < 2) $day = "0".$day;
   return $year."-".$month."-".$day;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: HolidayCalculatorArray
// Description:
//    This function creates an array of all days that count as holidays to the
//    system.
// Parameters:
//    int $y	Year to calculate holidays for.
// Return Values:
//    Array of holidays.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function HolidayCalculatorArray($y = "") {
   if (!$y || ($y < 1583) || ($y > 4099)) {
      $y = date("Y", time());
   }
       
   $tg = GetHoliday($y, 11, 4, 3);
   // Thanksgiving (3rd thur nov)
   $rest = substr($tg, 0, -2);
   $day = substr($tg, -2);
   $day2 = $day + 1;
   $tg2 = "$rest$day2"; // Day After Thanksgiving
       
   $arr = array(
   FormatDate($y, 1, 1), // New Year's Day
   GetHoliday($y, 1, 1, 3), // MLK (3rd Monday in January)
   GetHoliday($y, 5, 1), // Memorial Day
   ObservedDay($y, 7, 4), // Independence Day Observed
   GetHoliday($y, 9, 1, 1), // Labor Day Observed
   $tg, // Thanksgiving (3rd thur nov)
   $tg2, // Day After Thanksgiving
   FormatDate($y, 12, 24), // Christmas Eve
   FormatDate($y, 12, 25), // Christmas Break
   FormatDate($y, 12, 26), // Christmas Break
   FormatDate($y, 12, 27), // Christmas Break
   FormatDate($y, 12, 28), // Christmas Break
   FormatDate($y, 12, 29), // Christmas Break
   FormatDate($y, 12, 30), // Christmas Break
   FormatDate($y, 12, 31) // Christmas Break
   );
    
   return $arr;
}

//-----------------------------------------------------------------------------
//
// Function: ObservedDay
// Description:
//    Gets the observed day of a holiday.  A holiday on Sunday is observed on
//    the following Monday, while a holiday on Saturday is observed on the 
//    previous Friday.
// Parameters:
//    int $year		Year of holiday.
//    int $month	Month of holiday.
//    int $day		Day of holiday.
// Return Values:
//    Returns the observed date of the holiday.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ObservedDay($year, $month, $day) {
   $day_unix_time = mktime(0,0,0,$month,$day,$year);
   if (intval(date("w", $day_unix_time)) == 0)
     return date("Y-m-d", ($day_unix_time + 86400));
   if (intval(date("w", $day_unix_time)) == 6)
     return date("Y-m-d", ($day_unix_time - 86400));
   return date("Y-m-d", $day_unix_time);
}

//-----------------------------------------------------------------------------
//
// Function: GetHoliday
// Description:
//    This function gets a date based on three or four parameters.
// Parameters:
//    int $year		Year of the date to get.
//    int $month	Month of the date to get.
//    int $target_day	Day of the date to get - 0 - Sun. through 6 - Sat.
//    int $week		Week of month $target_day is in.  Empty - last week. 
// Return Values:
//    Returns a date in the format YYYY-MM-DD.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetHoliday($year, $month, $target_day, $week = "") {
   $week = ($week - 1) % 5 + 1;
   $month = ($month - 1) % 12 + 1;
   $target_day = ($target_day) % 7;
   if (!$week) {
      $month_timestamp = mktime(0, 0, 0, $month, 1);
      $days_in_month = date("t", $month_timestamp);
      $week = date("W", mktime(0, 0, 0, $month, $days_in_month)) - date("W", $month_timestamp);
   }
   $earliest_day = 1 + 7 * ($week - 1);
   $earliest_weekday_timestamp = mktime(0, 0, 0, $month, $earliest_day);
   $earliest_weekday = date("w", $earliest_weekday_timestamp);
   if ($target_day == $earliest_weekday) $offset = 0;
   else {
      if ($target_day < $earliest_weekday )
         $offset = $target_day + (7 - $earliest_weekday);
      else $offset = ($target_day + (7 - $earliest_weekday)) - 7;
   }
   return date("Y-m-d", $earliest_weekday_timestamp + 86400 * $offset);
}
?>
