<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("functions/functions-admin-groups.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-ldap.php"); 
require_once("functions/ru_ldap_auth.php"); 

//
// Filename: functions-admin-users.php
// Description: Contains functions for user administration.
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: FindUsersHtml
// Description:
//    Generates HTML for the "Find Users" form.
// Parameters:
//    none.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function FindUsersHtml($disabled = "") {
   $output = Font("Find users where")
           .'<br><select name="UserField">'
           .'<option value="username">User Name</option>'
           .'<option value="email">Email</option>'
           .'<option value="firstname">First Name</option>'
           .'<option value="lastname">Last Name</option>'
           .'<option value="organization">Organization</option>'
           .'</select>'
           .'<select name="UserOp">'
           .'<option value="LIKE" selected>contains</option>'
           .'<option VALUE="NOT LIKE" >doesn\'t contain</option>'
           .'<option VALUE="=" >is</option>'
           .'<option VALUE="!=" >isn\'t</option>'
           .'</select>'
           .'<input size=15 name="UserString"><br><br>';
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetStaffForm
// Description:
//    Generates HTML for staff searches.
// Parameters:
//    string $hierarchy	Passed to CreateLinksFromArray
// Return Values:
//    Returns HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------    
function GetStaffForm($hierarchy = "") {
   global $UserString,$UserField,$UserOp,$user,$username;
   $output = "<table border=0><tr><td valign=\"top\"><form>"
           ."<input type=\"hidden\" name=\"item\" value=\"Users\">"
           .FindUsersHtml("disabled")
           ."<table align=\"center width=\"100%\"><td align=\"left\">";

   if (DoesUserHaveRight($username, "AdminUser")) {
      $output .= "<input type=\"submit\" value=\"Search\">"
              ."</td><td align=\"right\"><input type=\"submit\" name=\"user\" value=\"Create New User\">"
              ."</td>";
   }
   $output .= "</table></form></td><td valign=\"top\" alight=\"right\">"
              .FontBold("List&nbsp;of&nbsp;Users")."<br><ol>";

   if (!(empty($UserField) && empty($UserOp) && empty($user))) {
     if (!(isset($UserField))) $UserField = "username";
     if (!(isset($UserOp))) $UserOp = "LIKE";

     if (!(isset($user))) {
       $users = GetCertainUsers($UserString, $UserField, $UserOp);

       if ($users[0] == "Empty") {
         $output .= "No matches found.";
       }

       else {
         $output .= CreateLinksFromArray($users, $hierarchy, "<br>", $hl);
         if (count($users) == 1) $GetUser = $users[0];
       }
     }
   }
   $output .= "</ol></td></tr></table>";

   if (isset($GetUser)){
      return $GetUser;
   }
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: CommitUserProfile
// Description:
//    Updates a user's profile (admin page).
// Parameters:
//    array $staff_columns	column name => column value
//    array $update_type	update or insert.
// Return Values:
//    Returns "Error updating!" if the query fails.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function CommitUserProfile($staff_columns, $update_type) {
   $user = $staff_columns['username'];
   foreach ($staff_columns as $key => $value) {
      if ($update) {
         $update .= ",$key=\"$value\"";
      }

      else {
         $update = "$key=\"$value\"";
      }
   }

   if ($update_type == "update") {
      $query = "update staff set $update where username=\"$user\"";
   }
   else {
      $query = "SELECT COUNT(*) as num FROM staff WHERE username = '$user'";
      $result = mysql_query($query);
      if (mysql_result($result, 0) > 0) return "Duplicate username $user!<br>";
      $query = "insert into staff set $update";
   }
   $result = mysql_query($query);

   if (empty($result)) {
      return "Error updating!<br>";
      mysql_free_result($result);
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: CommitUserSignature
// Description:
//    Updates a user's signature in staffprefs table.
// Parameters:
//    string $user		username
//    string $signature		body of signature
//    string $updateType	update or insert
// Return Values:
//    "Error updating signature!" if query fails.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function CommitUserSignature($user, $signature, $updateType) {
   $query = "select username from staffprefs where username='$user'";
   $result = mysql_query($query);
   if (isset($result) && mysql_num_rows($result) < 1) $updateType = "insert";
   mysql_free_result($result);
   if ($updateType == "update")
      $query = "update staffprefs set signature='$signature' where username='$user'";
   else
      $query = "insert into staffprefs set signature='$signature', username='$user'";
   $result = mysql_query($query);
   if (empty($result)) return "Error updating signature!<br>";
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateUserProfileForm
// Description:
//    Generates the HTML for updating a user's profile (staff).
// Parameters:
//    string $item	not used
//    string $user	username
// Return Values:
//    Returns HTML to generate the form.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateUserProfileForm($item = "", $user) {
   global $username;
   $oldusername = $username;
   $staffColumns = array("username", "firstname", "lastname", "email", "nickname", "extra_info", "organization", "address1", "address2", "city", "state", "zip", "country", "home_phone", "work_phone", "mobile_phone", "pager", "access", "rights", "comments");
   $query = "select * from staff where username='$user'";
   $result = mysql_query($query);
   $sQuery = "select signature from staffprefs where username='$user'";
   $sResult = mysql_query($sQuery);
   while ($row = mysql_fetch_assoc($result)) {
      $UserProfile = $row;
   }
   while ($sRow = mysql_fetch_assoc($sResult)) {
      $signature = $sRow['signature'];
   }
   mysql_free_result($result);
   mysql_free_result($sResult);
   if (isset($UserProfile))
      foreach ($staffColumns as $title) $$title = $UserProfile[$title]; // import user information
   if (isset($firstname) && isset($lastname))
      $name = $firstname." ".$lastname; // join first and last name together
   if ($user == "Create New User") {
      $Action = $user;
      $user = "";
   }
   $output = "<form method='post'><table border=0 width=\"100%\"><tr valign=\"top\">"
           ."<td width=\"50%\">".OpenColorTable("green", "Identity: $user", "100%")
           ."<table cellpading=2 width=\"100%\">".OpenTable("Username:")
           ."<input name=\"user\" value=\"$user\" size=8>".Font("(* required)")
           .CloseTable().OpenTable("Email:")
           ."<input name=\"email\" value=\"$email\" size=24>"
           .CloseTable().OpenTable("Real Name:")
           ."<input name=\"name\" value=\"$name\" size=24>"
           .CloseTable().OpenTable("Nickame:")
           ."<input name=\"nickname\" value=\"$nickname\" size=24>"
           .CloseTable().OpenTable("Extra info:")
           ."<textarea name=\"extra_info\" cols=\"24\" rows=\"3\">"
           .$extra_info."</textarea>"
           .CloseTable()."</table>".CloseColorTable()
           ."</td ><td width=\"50%\">"

           .OpenColorTable("blue", "Location: $user", "100%")
           ."<table cellpading=2 width=\"100%\">"
           .OpenTable("Organization:")
           ."<input name=\"organization\" value=\"$organization\" size=24>"
           .CloseTable().OpenTable("Address 1:")
           ."<input name=\"address1\" value=\"$address1\" size=24>"
           .CloseTable().OpenTable("Address 2:")
           ."<input name=\"address2\" value=\"$address2\" size=24>"
           .CloseTable().OpenTable("City:")
           ."<input name=\"city\" value=\"$city\" size=24>"
           .CloseTable().OpenTable("State:")
           ."<input name=\"state\" value=\"$state\" size=3>"
           .CloseTable().OpenTable("Zip:")
           ."<input name=\"zip\" value=\"$zip\" size=6>"
           .CloseTable().OpenTable("Country:")
           ."<input name=\"country\" value=\"$country\" size=24>"
           .CloseTable()."</table>".CloseColorTable()

           ."</td></tr><tr valign=\"top\"><td width=\"50%\">"
           .OpenColorTable("blue", "Change Password: $user", "100%")
           ."<table cellpading=2 width=\"100%\">";

   global $AUTH_TYPE;
   if ($AUTH_TYPE == "local") {
     if (DoesUserHaveRight($oldusername, "LocalPasswords") || $oldusername == $username) {
       $output .= ChangePasswordFormAdmin($username, $_POST['passwd1'], $_POST['passwd2']);
     }
     else $output .= FontBold("No Permissions to change Local Authentication.");
   }
   else $output .= FontBold("Change Password Form only available using Local Authentication.");

           $output .= "</table>".CloseColorTable()
           ."</td><td width=\"50%\">"
           .OpenColorTable("green", "Phone Numbers: $user", "100%")
           ."<table cellpading=2 width=\"100%\">"
           .OpenTable("Home:")
           ."<input name=\"home_phone\" value=\"$home_phone\" size=12>"
           .CloseTable().OpenTable("Work:")
           ."<input name=\"work_phone\" value=\"$work_phone\" size=12>"
           .CloseTable().OpenTable("Mobile:")
           ."<input name=\"mobile_phone\" value=\"$mobile_phone\" size=12>"
           .CloseTable().OpenTable("Pager:")
           ."<input name=\"pager\" value=\"$pager\" size=12>"
           .CloseTable()."</table>"
           ."<input type=\"hidden\" name=\"commit\" value=\"yes\">"
           .CloseColorTable()."</td></tr><tr><td width=\"50%\">"
           .OpenColorTable("green", "Additional Comments: $user", "100%")
           ."<table cellpading=2 width=\"100%\">"
           .OpenTable("Comments:")
           ."<textarea name=\"comments\" cols=24 rows=4>$comments</textarea>"
           .CloseTable()."</table>".CloseColorTable()."</td><td>"

           .OpenColorTable("blue", "Signature: $user", "100%")
           ."<table cellpading=2 width=\"100%\">"
           .OpenTable("Signature:")
           ."<textarea name=\"signature\" cols=24 rows=4>$signature</textarea>"
           .CloseTable()."</table>".CloseColorTable()."</td></tr><tr>"
           ."<td width=\"50%\">&nbsp;</td><td width=\"50%\"><center>"
           .OpenColorTable("red", "Commit changes: $user", "100%")
           .Spacer(1)."<center>";

   if ($Action == "Create New User") {
      $output .= "<input type=hidden name=updateType value=\"insert\">\n";
      $user = "New User";
   }
   else $output .= "<input type=hidden name=updateType value=\"update\">\n";
   if (DoesUserHaveRight($oldusername, "AdminUser") || $username == $oldusername)
      $output .=  "<input type=\"submit\" value=\"Commit above changes for $user\">";
   if (DoesUserHaveRight($oldusername, "AdminUser"))
      $output .= "<input type=\"submit\" name=\"DeleteUser\" value=\"Delete This User\">";
   $output .= "</center>".Spacer(1).CloseColorTable()."</center></td></tr></table></form>";
   $username = $oldusername;
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: ChangePasswordFormAdmin
// Description:
//    Generates html to allow change of password.
// Parameters:
//    string $user	User Name
//    string $passwd1	New Password
//    string $passwd2	New Password Check.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Applies only to local accounts..
//-----------------------------------------------------------------------------
function ChangePasswordFormAdmin($username = "", $passwd1 = "", $passwd2 = "") {
   if ($passwd1 != "" && $passwd2 != "") {
      if ($passwd1 != $passwd2) $msg = "Passwords do not match.";
      else $msg = UpdateLocalPassword($username, $passwd1);
   }
   return FontBold($msg)
          .OpenTable("New Password:")
          ."<input name=\"passwd1\" type=\"password\" size=15>"
          .CloseTable().OpenTable("Retype Password:")
          ."<input name=\"passwd2\" type=\"password\" size=15>"
          .CloseTable()."<tr><td align=\"right\" colspan=2>"
          ."<input type=\"submit\" value=\"Change Password\"></td></tr>";
}


//-----------------------------------------------------------------------------
//
// Function: BulkAddUsersForm
// Description:
//    Framework for bulk addition of users.
// Parameters:
//    None.
// Return Values:
//    None.
// Remarks:
//    Framework only.
//-----------------------------------------------------------------------------

function BulkAddUsersForm($AUTH_TYPE, $username) {
  global $username;
  $output .= "<form action=\"$self\" method=\"POST\" enctype=\"multipart/form-data\">";
  $output .= "<table align=\"center\"><tr><td>";
  $output .= "Upload File:";
  $output .= "<input type=\"file\" name=\"csv_file\">";
  if($AUTH_TYPE == "local") $output .= " .CSV file with the following format: username, email, password";
  if($AUTH_TYPE == "extensible") $output .= " .CSV file with the following format: username, email";
  $output .= "</td></tr></table>";
  $output .= "<hr width=50% \>";
  $output .= "<table align=\"center\" cellpadding=\"5\" cellspacing=\"0\" border=\"0\">\n";
  for($i = 0; $i < 20; $i+=2) {
    $i2=$i+1;
    $output .= "<tr valign=\"top\">\n"
            . "	<td><b>NetID:</b> <input name=\"uid[$i]\" value=\"\" size=\"15\"></td>\n"
            . "	<td><b>Email:</b> <input name=\"email[$i]\" value=\"\" size=\"15\"></td>\n";
    if($AUTH_TYPE == "local"){
      $output .= "<td><b>Password:</b> <input name=\"password[". $i ."]\" value=\"\" size=\"15\"></td>\n";
    }
    $output .= " <td><b>NetID:</b> <input name=\"uid[". $i2 ."]\" value=\"\" size=\"15\"></td>\n"
            . "	<td><b>Email:</b> <input name=\"email[". $i2 ."]\" value=\"\" size=\"15\"></td>\n";
    if($AUTH_TYPE == "local"){
      $output .= "<td><b>Password:</b> <input name=\"password[". $i2 ."]\" value=\"\" size=\"15\"></td>\n";
    }
    $output .= "</tr>";
  }
  $output .= "</table>\n";
  $output .= "<br \>\n";
  $group_listing = GetGroups();
  
  $output .= "<table align=\"center\">";
  $output .= "<tr><td align=\"center\">";
  $output .= "<strong>Add to Groups: </strong>";
  $output .= "</td><td width=\"10\">";
  $output .= "</td><td>";
  $output .= "<select multiple name=\"group_listing[]\" size=\"10\">";
  $output .= CreateSelectOptions($group_listing, "close");
  $output .= "</select>";
  $output .= "</td></tr>";
  $output .= "</table>";
  if(DoesUserHaveRight($username, "AdminUser")){
    $output .= "<tr><td colspan=\"4\" align=\"center\"><input type=\"submit\" name=\"BulkAdd\" value=\"Add New Users\"> <input type=\"reset\" value=\"Reset Page\"> <td></tr>\n";
  }
    
    
  $output .= "</form>\n";
  $output .= "";
  return $output;
}

//----------------------------------------------------------------------
//
// Function:      BulkAddCheck
//
// Description:   Verifies data to be inserted is valid
//
// Parameters:    
//   $data:       array containing data (username, email,pass) to be inserted 
//   $AUTH_TYPE:  authentication type
//
// Return Values: 
//   $output      html containing successful and unsuccessful messages
//
// Remarks:       
//
//----------------------------------------------------------------------

function BulkAddCheck($AUTH_TYPE, $data, $group_list = ""){
  define("EMPTY_USER", 1);
  define("EMPTY_EMAIL", 2);
  define("EMPTY_PASS", 3);
  define("INVALID_USER", 4);
  define("INVALID_EMAIL", 5);
  define("INVALID_PASS", 6);
  if(!empty($data)){
	  foreach($data as $line){
		  if($AUTH_TYPE == "local") list($user,$email,$pass) = explode(",", $line);
		  else if($AUTH_TYPE == "extensible") list($user,$email) = explode(",", $line);
      if(!empty($user)) $user = trim($user);
      if(!empty($email)) {
        $email = strtolower($email);
        $email = trim($email);
      }
      if(!empty($pass)) $pass = trim($pass);
      if($AUTH_TYPE == "local" && !empty($epass)) $pass = trim($pass);

      if($AUTH_TYPE == "local" && (!empty($user) || !empty($email) || !empty($pass)) || 
       $AUTH_TYPE == "extensible" && (!empty($user) || !empty($email))){
        if(empty($user)) $error = EMPTY_USER;
        else if(empty($email)) $error = EMPTY_EMAIL;
        else if($AUTH_TYPE == "local" && empty($pass)) $error = EMPTY_PASS;
        else {
          //invalid checks
          if(preg_match("/username/", $user)) $error = INVALID_USER;
          if(!preg_match("/[a-z0-9][a-z0-9\-_\.]*@[a-z0-9][a-z0-9\.-]*[a-z0-9]\.[a-z]{2,4}/", $email)) $error = INVALID_EMAIL;
          //elseif(preg_match("/email/", $email)) $error = INVALID_EMAIL;
          elseif($AUTH_TYPE == "local" && preg_match("/pass/", $pass)) $error = INVALID_PASS;
          else $error = 0;
        }
  
        switch ($error) {
          case EMPTY_USER:
            if(!empty($email)) $invalid[] = "<font color=\"red\"> Please supply a username for email <b>$email</b>.</font>";
            else $invalid[] = "<font color=\"red\"> Please supply a username for entry <b>$line</b></font>";
            break;
  
          case EMPTY_EMAIL:
            if(!empty($user)) $invalid[] = "<font color=\"red\"> Please supply an email for username <b>$user</b>.</font>";
            else $invalid[] = "<font color=\"red\"> Please supply an email for entry <b>$line</b></font>";
            break;
  
          case EMPTY_PASS:
            if(!empty($user)) $invalid[] = "<font color=\"red\"> Please supply a password for username <b>$user</b></font>";
            else $invalid[] = "<font color=\"red\"> Please supply a password for entry <b>$line</b></font>";
            break;
  
          case INVALID_USER:
            $invalid[] = "<font color=\"red\"> Please supply a valid username for entry <b>$line</b></font>";
            break;
  
          case INVALID_EMAIL:
            $invalid[] = "<font color=\"red\"> Please supply a valid email for entry <b>$line</b></font>";
            break;
  
          case INVALID_PASS:
            $invalid[] = "<font color=\"red\"> Please supply a valid password for entry <b>$line</b></font>";
            break;
        
          default:
            //check to see if username exists in db already else insert
            if ($AUTH_TYPE == "extensible" && !StaffExistsInLdap($user)) {
              $invalid[] = "<font color=\"red\"> Username <b>$user</b> does not exist in LDAP!</font>"; 
            }else if (StaffExistsInLocal($user)) {
              $invalid[] = "<font color=\"red\"> Username <b>$user</b> exists in database!</font>"; 
            }else {
              $person = array("username" => $user, "email" => $email);
              CommitUserProfile($person, "inserttest");
              if($AUTH_TYPE == "local") {
                UpdateLocalPassword($user, $pass);
              }
              $valid[] = "Successfully added $user ($email)";
              if(!empty($group_list)) {
                SpaceInputValue($group_list);
                $errors = BulkAddGroup($group_list, $user);
                //if (count($errors) > 0 ) $invalid[] = "Warning: Could not add $user to group(s)";
              }
            }
        }
	    }
    }//for
  }//if

  if(count($invalid) > 0){
    $output .= OpenColorTable("red", "Unsuccessful", "100%");
    foreach($invalid as $value){
      $output .= $value."<br \>";
    }
    $output .= CloseColorTable();
  }

  $output .= "<br \>";

  if(count($valid) > 0){
    $output .= OpenColorTable("blue", "Successfully Added", "100%");
    foreach($valid as $value){
      $output .= $value."<br \>";
    }
    $output .= CloseColorTable();
  }
  return $output;

}

//----------------------------------------------------------------------
//
// Function:      StaffExistsInLdap
//
// Description:   Checks to see whether username exists in LDAP db
//
// Parameters:    
//   $username:   username of staff member
//
// Return Values: 
//   boolean value containing whether or not username exists
// Remarks:       
//
//----------------------------------------------------------------------

function StaffExistsInLdap($username){
  global $LDAP_ROOT_DN, $SERVER_ID;
  $oSearch = new RutgersLDAPAuth;
  $oSearch->Connect();
  $ldap_query = "uid=$username";
  $result_array = array();
  $array_attrib = array("uid");
  $oSearch->Search(&$result_array, $LDAP_ROOT_DN[$SERVER_ID], $ldap_query, $array_attrib);
  $uid = $result_array[0]['uid'][0];
  if (strtoupper($uid) == strtoupper($username)) return 1; 
  else return 0;
}

//----------------------------------------------------------------------
//
// Function:      StaffExistsInLocal
//
// Description:   Checks to see if username is in staff table
//
// Parameters:    
//   $username:   username
//
// Return Values: 
//
// Remarks:       
//
//----------------------------------------------------------------------

function StaffExistsInLocal($username){
  global $db;
  $query = "SELECT count(*) AS staff_exists FROM staff WHERE username='$username'";
  $result = mysql_query($query, $db);
  if ($result) $row = mysql_fetch_array($result);
  return $row['staff_exists'];
} 

//----------------------------------------------------------------------
//
// Function:      BulkStaffRemoval
//
// Description:   Removes staff from database
//
// Parameters:    
//   $staff_removal[]:  contains staff username(s) listing
//
// Return Values: 
//   $output:     html containing removal status
// Remarks:       
//
//----------------------------------------------------------------------

function RemoveStaff($staff_removal){
  foreach ($staff_removal as $staff){
    $query = "DELETE FROM staff WHERE username='$staff'";
    $query2 = "DELETE FROM ingroup WHERE username='$staff'";
    mysql_query($query);
    mysql_query($query2);
  }
}

//----------------------------------------------------------------------
//
// Function:      BulkStaffRemovalForm
//
// Description:   Form for staff removal
//
// Parameters:    
//   $staff_listing[]:  contains staff username(s) listing
//
// Return Values: 
//   $output:     html containing removal form
// Remarks:       
//
//----------------------------------------------------------------------

function BulkStaffRemovalForm($staff_listing){
  global $username;
  $output .= "<form action=\"$self\" method=\"POST\">";
  $output .= "<table align=\"center\">";
  $output .= "<tr><td align=\"center\">";
  $output .= "<strong>Remove Staff: </strong>";
  $output .= "</td><td width=\"10\">";
  $output .= "</td><td>";
  $output .= "<select multiple name=\"staff_removal[]\" size=\"10\">";
  $output .= CreateSelectOptions($staff_listing);
  $output .= "</select>";
  $output .= "</td></tr>";
  if (DoesUserHaveRight($username, "AdminUser")) {
    $output .= "<tr><td colspan=\"3\" align=\"center\">";
    $output .= "<input type=\"submit\" value=\"Remove Users\">";
    $output .= "</td></tr>";
  }
  $output .= "</table>";
  $output .= "</form>";
  return $output;
}

// -------------------------------------------------------- 
// Function:                UserGroups

// Description:             Lists all of the groups and privs 
//                          of a user

// Type:                    public

// Parameters:
//    [in] string $user     The username of the user

// Return Values:           
//    string                HTML displaying the groups
//                          and privs of the user

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function UserGroups($user) 
{
  $overview = <<<EOM
<p />
$user has the following rights for certain queues based 
on his/her groups (<em>global</em> means all queues): 
<p />
EOM;

  $table = <<<EOT
<table border="1" cellspacing="0" cellpadding="2">
<tr>
  <td><strong>Group</strong></td>
  <td><strong>Right</strong></td>
  <td><strong>Queue</strong></td>
</tr>
EOT;

  $sql = <<<EOQ
SELECT groupright.group_name,
       groupright.group_right,
       groupright.queue
FROM ingroup, groupright 
WHERE ingroup.group_name=groupright.group_name 
AND ingroup.username='$user' 
ORDER BY group_name, group_right, queue;
EOQ;
 
  $result = mysql_query($sql) or die(mysql_error()); 
  if (mysql_num_rows($result) == 0) {
    $table = "<strong>None</strong>";
  }
  else {
    while ($row = mysql_fetch_object($result)) {
      $table .= "<tr>\n";
      $table .= "  <td>". $row->group_name . "</td>\n";
      $table .= "  <td>". $row->group_right . "</td>\n";
      $table .= "  <td>". $row->queue . "</td>\n";
      $table .= "</tr>\n";
    }
    $table .= "</table>\n";
  }
  return $overview . $table;
}

?>