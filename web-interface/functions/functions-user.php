<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename:               functions-user.php
// Description:            Functions associated with user preferences.
// Supprted Language(s):   PHP 4.0+
//

require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");

//-----------------------------------------------------------------------------
//
// Function: EmailPrefsForm
// Description:
//    Generates HTMl to display email preferences form.
// Parameters:
//    string $username	Username.
// Return Values:
//    HTML to display.
// Remarks:
//    Only shown if $username is a supervisor lets them choose html or text */
//-----------------------------------------------------------------------------
function EmailPrefsForm($username) {
   global $db;
   $output = ""; 
   // 1.  update data if needed
   if (isset($_POST['html_email'])) {
      $html_email = $_POST['html_email'];
      $query = "update staff set html_email = '$html_email' ";
      $query .= "where username='$username'";
      $result = mysql_query($query, $db);
      if (!$result) {
         $output .= "<p>MySQL Error:  <i>$query</i>\n";
      } 
      else {
         $output .= "<p><b>Your E-mail Preferecnes have been changed. </b> ";
      }
   }
   $url = GetEmailInformationURL();
   $output .= "<p>You are a Queue Supervisor so you might get nightly email "
           ."regarding overdue tickets.  <br>"
           ."Use this form to configure the format of that email "
           ."(<a class=\"main\" target=\"link\" href=\"$url\">"
           ."HTML or Plain Text</a>).  ";
       
   // 2.  display data (up-to-date or original)
   $html_email = 0;
   $query = "select html_email from staff where username='$username'";
   $result = mysql_query($query, $db);
   if (!$result) $output .= "<p>MySQL Error:  <i>$query</i>\n";
   else {
      while ($row = mysql_fetch_object($result)) {
         if ($row->html_email == 1) $html_email = 1;
      }
   }

   // 3.  show them what we have and let them change it
   if ($html_email) $check1 = "checked";
   else $check2 = "checked";
   global $self;
   $output .= "<br><form action=\"$self\" method=\"POST\">\n"
           ."<p><input name=\"html_email\" value=\"1\" type=\"radio\" $check1>\n"
           ."I <u>do</u> want to receive HTML email.  \n"
           ."<p><input name=\"html_email\" value=\"0\" type=\"radio\" $check2>\n"
           ."I <u>do not</u> want to receive HTML email.  Just send Plain Text.\n"
           ."<p align=\"right\">\n"
           ."<input type=\"submit\" value=\"Change E-mail Preference\">\n"
           ."</form>\n";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: IsSupervisor
// Description:
//    Checks if username is a supervisor.
// Parameters:
//    string $username	Username.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function IsSupervisor($username) {
   global $db;
   $q = "select username from q_supervisor where username='$username'";
   $result = mysql_query($q, $db);
   if (!$result) print "<p>MySQL Error:  <i>$q</i>\n";
   else {
      while ($row = mysql_fetch_object($result)) {
         if ($row->username == $username) return 1;
      }
   }
   return 0;
}
    
//-----------------------------------------------------------------------------
//
// Function: ChangeSignatureForm
// Description:
//    Generates HTML to display the change signature form.
// Parameters:
//    string $username	Username.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ChangeSignatureForm($username = "", $new_signature = "") {
   global $db,$input,$self;
   $output = "";
   if (isSet($new_signature)) {
       if (mysql_num_rows(mysql_query("select * from staffprefs where username='$username'", $db)) > 0){
          $update_signature  = "update staffprefs set ";
          $update_signature .= "signature='$new_signature' where ";
          $update_signature .= "username='$username'";
       }
       else{
          $update_signature  = "insert into staffprefs set ";
          $update_signature .= "username='$username',";
	  $update_signature .= " signature='$new_signature'";
       }
       $new_result = mysql_query($update_signature, $db);
       $output .= Font("Your signature has been updated.");
       if (!$new_result) {
          $output .= "error";
       }
   }
   $query = "select signature from ";
   $query .= "staffprefs where username='$username'";
   $result = mysql_query($query, $db);
   if ($result) {
      $array = mysql_fetch_array($result);
      $signature = $array[signature];
   }
   else $output .= "error";
   $output .=  "<form action=\"$self\" method=\"POST\">"
           .'<table cellpading="2" width="100%">'
           .OpenTable("Signature:")
           .'<textarea name="new_signature" cols="70" rows="5">'
           ."$signature</textarea>".CloseTable()
           .'</table><p><div align="right">'
           .'<input type="submit" value="Change Signature">'
           .'</div></form>';
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: ChangeDisplayOverview
// Description:
//    Generates HTMl to display a form to choose whether the ticket overview is
//    displayed.
// Parameters:
//    string $username	Username.
//    string $display	Display or not.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ChangeDisplayOverview($username, $display){
   global $db;
   $output = "";
   if (isset($display)){
      if (mysql_num_rows(mysql_query("select * from staffprefs where username='$username'", $db)) > 0){
         $query = "update staffprefs set ";
         $query .= "display_overview=$display where ";
         $query .= "username='$username'";
      }
      else{
         $query = "insert into staffprefs values ";
         $query .= "('$username', '', $display)";
      }
      mysql_query($query, $db);
      $output .= "Your display preferences have been updated.";
   }
   $display = GetArrayFromTable("display_overview", "staffprefs where username='$username'");
   $output .= "<form method=post>";
   $output .= "<table border=0><tr><td>";
   $output .= "<b>Display Overview:</b></td><td>";
   $output .= "<input type='radio' name='display_overview' value=0 ";
   if (!$display[0]) $output .= "checked";
   $output .= ">Hide<input type='radio' name='display_overview' value=1 ";
   if ($display[0]) $output .= "checked";
   $output .= ">Display</td></tr></table>";
   $output .= "<div align='right'><input type='submit' value='Change Display'></div></form>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: ChangeHTMLComment
// Description:
//    Generates HTML to display a form to choose whether the ticket's 
//    HTML in the comments is interpretted by the browser.  
// 
// Parameters:
//    string $username	Username.
//    string $display	Display or not.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ChangeHTMLComment($username, $display){
   global $db;
   $output = "";
   if (isset($display)){
      if (mysql_num_rows(mysql_query("select * from staffprefs where username='$username'", $db)) > 0){
         $query = "update staffprefs set ";
         $query .= "show_html_in_comments=$display where ";
         $query .= "username='$username'";
      }
      else{
         $query = "insert into staffprefs values ";
         $query .= "('$username', '', $display)";
      }
      mysql_query($query, $db);
      $output .= "Your HTML comment display preferences have been updated.";
   }
   $display = GetArrayFromTable("show_html_in_comments", "staffprefs where username='$username'");
   $output .= "<form method=post>";
   $output .= "<table border=0><tr><td>";
   $output .= "<b>Display HTML in Comments:</b></td><td>";
   $output .= "<input type='radio' name='html_in_comments' value=1 ";
   if ($display[0]) $output .= "checked";
   $output .= ">HTML-entity-quote comments<br><br><input type='radio' name='html_in_comments' value=0 ";
   if (!$display[0]) $output .= "checked";
   $output .= ">Do not HTML-entity-quote comments (HTML in comments might be interpreted by your browser)</td></tr></table>";
   $output .= "<div align='right'><input type='submit' value='Change HTML'></div></form>";
   // $output .= "\n<p>Note that PHP&#039;s htmlentities (which is used for ";
   // $output .= "for the above) can vary by configuration </p>\n";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: ChangePasswordForm
// Description:
//    Generates html to allow change of password.
// Parameters:
//    string $user      User Name
//    string $passwd1   New Password
//    string $passwd2   New Password Check.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    Applies only to local accounts..
//-----------------------------------------------------------------------------
function ChangePasswordForm($username = "", $passwd1 = "", $passwd2 = "") {
   if ($passwd1 != "" && $passwd2 != "") {
      if ($passwd1 != $passwd2) $msg = "Passwords do not match.";
      else $msg = UpdateLocalPassword($username, $passwd1);
   }
   return FontBold($msg)
          ."<form method=\"post\">".OpenTable("New Password:")
          ."<input name=\"passwd1\" type=\"password\" size=15>"
          .CloseTable().OpenTable("Retype Password:")
          ."<input name=\"passwd2\" type=\"password\" size=15>"
          .CloseTable()."<tr><td align=\"right\" colspan=2>"
          ."<input type=\"submit\" value=\"Change Password\"></td></tr></form>";
}

//-----------------------------------------------------------------------------
//
// Function: RefreshForm
// Description:
//    Allows the refresh rate of the main page to be selected.
// Parameters:
//    string $rate      Selected refresh rate.
// Return Values:
//    Returns HTML to generate the form for selecting the refresh rate of page.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function RefreshForm($rate = "") {
   global $self, $username, $db;
   $output = "";
   $pref = GetValueFromTable("refresh", "staffprefs", "where username=\"$username\"");
   if ($rate != $pref && $rate != "") {
      if ($rate == "no") $rate = "";
      $update = "update staffprefs set refresh = \"$rate\" where username = \"$username\"";
      $result = mysql_query($update, $db);
      if (!$result) $output = FontBold("Refresh Time failed to update.");
      else $output = FontBold("Refresh Time updated successfully.");
   }
   $one = $two = $three = $four = $five = $six = $seven = "";
   if (empty($rate)) $one = "selected";
   if ($rate == "120") $two = "selected";
   if ($rate == "300") $three = "selected";
   if ($rate == "600") $four = "selected";
   if ($rate == "1200") $five = "selected";
   if ($rate == "3600") $six = "selected";
   if ($rate == "7200") $seven = "selected";
   $output .= "<form action=\"$self\" method=\"post\"><select name=\"rate\">"
          ."<option value=\"no\" $one>Don't refresh pages.</option>"
          ."<option value=\"120\" $two>Refresh pages every 2 minutes.</option>"
          ."<option value=\"300\" $three>Refresh pages every 5 minutes.</option>"
          ."<option value=\"600\" $four>Refresh pages every 10 minutes.</option>"
          ."<option value=\"1200\" $five>Refresh pages every 20 minutes.</option>"
          ."<option value=\"3600\" $six>Refresh pages every hour.</option>"
          ."<option value=\"7200\" $seven>Refresh pages every two hours.</option>"
          ."</select>&nbsp;<input type=submit value=\"Go!\"></form>";
   return $output;
}

?>
