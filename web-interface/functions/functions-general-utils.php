<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("functions-getters.php");
require_once("functions-widgets.php");
require_once("ldap.php");
//
// Filename: functions-general-utils.php
// Description: Contains general useful miscellaneous functions.
// Supprted Language(s):   PHP 4.0
//
global $self;
$self = $PHP_SELF;
    
//-----------------------------------------------------------------------------
//
// Function: BinarySearch
// Description:
//    Implements a binary search for an element in an array.
// Parameters:
//    array $array	Haystack.
//    various $element	Needle.
// Return Values:
//    returns 0 if not found, element if found.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function BinarySearch($array, $element) {
   $low = 0;
   $high = count($array) - 1;

   while ($low <= $high) {
      $mid = floor(($low + $high) / 2);

      if ($element == $array[$mid]) {
         return $array[$mid];
      }

      else {

         if ($element < $array[$mid]) {
            $high = $mid - 1;
         }

         else {
            $low = $mid + 1;
         }
      }
   }
   return 0;
}

//-----------------------------------------------------------------------------
//
// Function: EmailGood
// Description:
//    Uses regular expressions to verify if an email is good.
//    Valid numbers, letters @ valid numbers,letters . valid numbers letters
// Parameters:
//    string $email	email to verify.
// Return Values:
//    Boolean valid.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function EmailGood($email = "") {
   return eregi("^[A-Za-z0-9\_-]+@[A-Za-z0-9\_-]+.[A-Za-z0-9\_-]+.*", $email);
}
    
//-----------------------------------------------------------------------------
//
// Function: PhoneGood
// Description:
//    Uses regular expressions to verify if a phone number is good.
// Parameters:
//    string $phone	phone number to verify.
// Return Values:
//    Boolean valid.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function PhoneGood($phone = "") {
   return eregi("[0-9]{3}-[0-9]{3}-[0-9]{4}", $phone);
}
    
//-----------------------------------------------------------------------------
//
// Function: DateGood
// Description:
//    Uses regular expressions to verify if a date is good.
// Parameters:
//    date $date	Date to check.
// Return Values:
//    Boolean valid.
// Remarks:
//    Does not check for dates like February 30th.
//-----------------------------------------------------------------------------
function DateGood($date = "") {
   $month = substr($date, 5, -3);
   $day = substr($date, 8);
   return (eregi("[0-9]{4}-[0-9]{2}-[0-9]{2}", $date)
   and ($month < 13) and ($month >= 0)
   and ($day < 32) and ($day >= 0));
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateLastCommentId
// Description:
//    Updates the last_comment_id of a ticket.
// Parameters:
//    int $ticket_id	ID of ticket to update.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateLastCommentId($ticket_id) {
   global $db;
   $last_comment_id = GetLastCommentOfTicket($ticket_id);
   $query = "UPDATE ticket SET last_comment_id='$last_comment_id', ";
   $query .= "date_created = date_created";
   $query .= " WHERE id='$ticket_id'";
   mysql_query($query, $db);
   UpdateTicketStatus($ticket_id, $last_comment_id);
}

//-----------------------------------------------------------------------------
//
// Function: UpdateTicketStatus
// Description:
//    Changes the status of a ticket.
// Parameters:
//    int $ticket_id	ID of ticket to update.
//    int $comment_id	ID of comment to update. Defaults to newest comment.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateTicketStatus($ticket_id, $comment_id = "") {
   global $db;

   if ($comment_id == "") {
      $query = "select max(comment.id) as comment_id from ticket, comment ";
      $query .= "where comment.ticket_id=ticket.id and ticket.id='$ticket_id' ";
      $query .= "group by ticket.id";
      $result = mysql_query($query, $db);

      if (($result) && ($row = mysql_fetch_object($result))) {
         $comment_id = $row->comment_id;
      }
   }
   $query = "select comment.date_created, comment.status, current_status ";
   $query .= "from ticket, comment where ticket.id='$ticket_id' and ";
   $query .= "comment.id='$comment_id' limit 1";
   $row = mysql_fetch_object(mysql_query($query, $db));

   if (($row->status != "") && ($row->status != $row->current_status)) {
      $query = "update ticket set last_comment_id='$comment_id', ";
      $query .= "date_created=date_created, current_status='$row->status', ";
      $query .= "status_change_date='$row->date_created' ";
      $query .= "where id='$ticket_id'";
      mysql_query($query, $db);
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: UpdateFirstCommentId
// Description:
//    Updates first_comment_id for a ticket.
// Parameters:
//    int $ticket_id	ID of ticket to update.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UpdateFirstCommentId($ticket_id) {
   global $db;
   $first_comment_id = GetFirstCommentOfTicket($ticket_id);
   $query = "UPDATE ticket SET first_comment_id='$first_comment_id', ";
   $query .= "date_created = date_created";
   $query .= " WHERE id='$ticket_id'";
   mysql_query($query, $db);
}
    
//-----------------------------------------------------------------------------
//
// Function: PrintArray
// Description:
//    Creates a string of the values of an array.
// Parameters:
//    array $array	Array to print.
// Return Values:
//    Returns the string.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function PrintArray($array = "") {
   $output = "";
   for ($i = 0; $i < sizeof($array); $i++) {
      $output .= $array[$i]."<br>";
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: PrintCheckboxArray
// Description:
//    Prints a list of checkboxes labelled with the items in the arrays.
// Parameters:
//    string $array_name		Name to use for the checkboxes.
//    array or string $array_values	Either "combined" or checkbox values.
//    array or string $array_shows	If "", $array_values used.  Can be 2D.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function PrintCheckboxArray($array_name, $array_values = "", $array_shows = "") {
   if ($array_shows == "combined") {
      $array_shows = $array_values[0];
      $array_values = $array_values[1];
   }
       
   if ($array_shows == "") {
      $array_shows = $array_values;
   }
   $size = sizeof($array_values);
   $size2 = sizeof($array_shows);

   if ($size > $size2) {
      $size = $size2;
   }
   $output = "<ul>";
   if ($size == 0) {
      $output .= "<li><i>none</i></li>";
   }
       
   for ($i = 0; $i < $size; $i++) {
      $value = $array_values[$i];
      $show = $array_shows[$i];
      $name = $array_name."[".$i."]";
      $output .= "<input type=\"checkbox\" name=\"$name\" value =\"$value\">$show<br>";
   }
   $output .= "</ul>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: PrintSpecialFieldsCheckboxArray
// Description:
//    Prints a list of checkboxes labelled with the items in the arrays.
// Parameters:
//    string $array_name	Name of checkbox array.
//    array $array_values	Values of checkboxes.
//    int $diff_ques		Adds additional info.
// Return Values:
//    HTML to display the checkbox array.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function PrintSpecialFieldsCheckboxArray($array_name, $array_values = "", $diff_ques = "") {
   global $self;
   $queue = $_GET['queue'];
   $array_shows = $array_values;
   $size = mysql_num_rows($array_values);
   $output = "<ul>";

   if ($size == 0) {
      $output .= "<li><i>none</i></li>";
   }

   else {
      $link = "<a class=\"main\" href=\"$self"."?item=Queues&queue=$queue"."&sub_group=Special_Fields&field_edit=";
      $link_end = "\">";
      $link_close = "</a>";

      while ($row = mysql_fetch_object($array_values)) {
         $value = $row->field_name;

         if ($diff_ques == 1) {
            $display_queue = ", $queue";
         }
         $show = $link.$row->field_name.$link_end.$row->field_name."$link_close &lt;";
         $show .= $row->field_display_name.", ".$row->field_type;
         $show .= ", $row->required".$display_queue."&gt;";
         $name = $array_name."[".$i."]";

         if ($diff_ques == 1) {
            $value = $row->date_create;
         }
         $output .= "<input type=\"checkbox\" name=\"$name\" value=\"$value\">$show<br>";
      }
   }
   $output .= "</ul>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: FixVarName
// Description:
//    Fixes a variable name by removing certain characters.
// Parameters:
//    string $var	String to fix.
// Return Values:
//    Returns the fixed var name.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function FixVarName($var) {
   $search = array("?", "%", " ", "\$");
   $replace = array("", "", "_", "");
   $var = str_replace($search, $replace, $var);
   return $var;
}


//-----------------------------------------------------------------------------
//
// Function: PrintSelectArray
// Description:
//    Creates an HTML select box from array options.
// Parameters:
//    string $name			Name to use for the array.
//    array $array_values		Values for the select box
//    string or array $array_shows	"combined" "" or display for array.
//    string $firstvalue		Value of first item.
//    string $firstshow			Display for first item.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function PrintSelectArray($name, $array_values = "", $array_shows = "", $firstvalue = "", $firstshow = "") {
   $output = "";
   if ($firstshow == "") {
      $firstshow = $firstvalue;
   }

   if ($array_shows == "combined") {
      $array_shows = $array_values[0];
      $array_values = $array_values[1];
   }
       
   if ($array_shows == "") {
      $array_shows = $array_values;
   }
   $size = sizeof($array_values);
   $size2 = sizeof($array_shows);

   if ($size > $size2) {
      $size = $size2;
   }

   if ($size == 0) {
      $output .= "No Choices Available";
      return 0;
   }

   else {
      $output .= "<select name=\"$name\">";

      if ($firstvalue != "") {
         $output .= "<option value = \"$firstvalue\">$firstshow</option>";
      }
   }
       
   for ($i = 0; $i < $size; $i++) {
      $value = $array_values[$i];
      $show = $array_shows[$i];
      $output .= "<option value=\"$value\">$show</option>";
   }
   $output .= "</select>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetMoreEmail
// Description:
//    Splits strings of emails into individual emails.
// Parameters:
//    string $requester	Single email.
//    string $cc	Multiple emails, seperated buy ", "
//    string $admin_cc	Multiple emails, seperated buy ", "
//    int $combine	Combine arrays into one or not.
// Return Values:
//    Returns either 3 seperate arrays or 1 array of emails depending on value
//    of $combine.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetMoreEmail($requester = "", $cc = "", $admin_cc = "", $combine = 0) {
   $arr1 = $requester;
   $arr2 = split(', ', $cc);
   $arr3 = split(', ', $admin_cc);

   if (!($combine)) {
      $pre_arr = array_merge($arr1, $arr2, $arr3);
      $final_arr = array();

      for ($i = 0; $i < sizeof($pre_arr); $i++) {

         if (EmailGood($pre_arr[$i])) {
            array_push($final_arr, $pre_arr[$i]);
         }
      }
   }

   else {
      return array($arr1, $arr2, $arr3);
   }
   return $final_arr;
}

//-----------------------------------------------------------------------------
//
// Function: DateWarning
// Description:
//    Inform user how to enter a date.
// Parameters:
//    None.
// Return Values:
//    Returns HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DateWarning() {
   return Font("in a <b>yyyy-mm-dd</b> format");
}
    
//-----------------------------------------------------------------------------
//
// Function: ArrayToString
// Description:
//    Explodes an array's items as a string.
// Parameters:
//    array $array	Array to explode.
//    string $spacer	String to place between elements of array.
// Return Values:
//    Returns a string.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ArrayToString($array = "", $spacer = "") {
   $output = "";

   for($i = 0; $i < sizeof($array); $i++) {
      $output .= "$array[$i]$spacer";
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: LinkToTicket
// Description:
//    Generates a link to a specific ticket.
// Parameters:
//    int $ticket_id	ID of ticket to link to.
// Return Values:
//    Returns HTML to display the link.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LinkToTicket($ticket_id = "") {
   $url = "<a class=\"main\" href=\"ticket.php?id=$ticket_id\">";
   $url .= "Click here for info on ticket #$ticket_id</a>";
   return $url;
}

//-----------------------------------------------------------------------------
//
// Function: CreateLinksFromArray
// Description:
//    Used to create a list of links in a row.
// Parameters:
//    array $array	Contains the text and value to use for each link.
//    string $var_name	Variable name to use in link.
//    string $space	Added to the end of each link after </a>.
//    string $hl	If this matches an item in $array, mark with arrow.
// Return Values:
//    Returns HTML to display the links.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CreateLinksFromArray($array = "", $var_name = "", $space = "", $hl = "") {
   global $self;
   $special = 0;
   $image = "<img src=\"images/arrow.png\">";
   $output = $null = "";
   $size = sizeof($array);

   if ($size < 1) {
      $output .= "Array of zero size passed to create_links_from_array()";
      return $output;
   }

   if ($hl == "first") {
      $special = 1;
   }

   for($i = 0; $i < $size; $i++) {
      //$value = str_replace(" ", "_", $array[$i]);
      $value = $array[$i];
      if (($array[$i] == $hl || str_replace(" ", "_", $array[$i]) == $hl) or $special) {
         $output .= Font("<a class=\"main\" href=\"$self?$var_name=$value\">$array[$i]</a>$image");
         $special = 0;
      }

      else {
         $output .= Font("<a class=\"main\" href=\"$self?$var_name=$value\">$array[$i]</a>");
      }
      $output .= $space;
   }
   return $output;
}

//----------------------------------------------------------------------
//
// Function:      SpaceInputValue
//
// Description:   Converts all underscores in array into spaces
//
// Parameters:    
//   $input_array:  input array (containing underscores) by reference
//
// Return Values: 
//   None.
// Remarks:       
//   None.
//----------------------------------------------------------------------

function SpaceInputValue(&$input_array){
  for($i=0; $i<count($input_array); $i++) $input_array[$i] = str_replace("_", " ", $input_array[$i]);
}

//-----------------------------------------------------------------------------
//
// Function: CreateSelectOptions
// Description:
//    Creates HTML select box options from an array.
// Parameters:
//    array $array		Options.
//    string $open		Pass as "open" to leave off </option>.
//    string $selected_string	Matched to an item in the array, "selected".
//    string $username		Appended to the values in $array.
// Return Values:
//    Returns HTML to display the options.
// Remarks:
//    Only creates the options, not the actual form.
//-----------------------------------------------------------------------------
function CreateSelectOptions($array = "", $open = "", $selected_string = "", $show_array = "", $username = "") {
   $output = "";
   if ($username != "") $username = $username."---";
   $size = sizeof($array);
   if ($show_array == "") $show_array = $array;
   if ($size < 1) return "Array of zero size passed to create_select_options()";
   if ($open == "open") $tag = "";
   else $tag = "</option>";

   if (empty($selected_string)) {

      for ($i = 0; $i < $size; $i++) {
         $value = str_replace(" ", "_", $array[$i]);
         $output .= "<option value=\"$username$value\">".$array[$i].$tag;
      }
   }

   else {

      for ($i = 0; $i < $size; $i++) {
         $value = str_replace(" ", "_", $array[$i]);
         $show_value = $array[$i];
         if ($value == $selected_string) $output .= "<option value=\"$username$value\" selected>".$array[$i].$tag;
         else $output .= "<option value=\"$username$value\">".$array[$i].$tag;
      }
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: QueueItemLink
// Description:
//    Generates a link to search for tickets in queue.
// Parameters:
//    string $queue_string	name of queue to use in link.
// Return Values:
//    Returns HTML to display the link.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function QueueItemLink($queue_string = "") {
   $link_string = str_replace(" ", "%20", $queue_string);
   $link = "<a class=\"main\" href=\"search.php?QueueOp==&valueOfQueue=";
   $link .= "$link_string&Action=Show+Results&Reset=0\"> $queue_string</a>";
   return Font($link);
}
    
//-----------------------------------------------------------------------------
//
// Function: QueueStatusItemLink
// Description:
//    Generates a link to a search for tickets of status in queue.
// Parameters:
//    string $queue_string	Name of queue to use in link.
//    string $status_string	String to display for link.
//    string $status_state	State to include in link.
// Return Values:
//    Returns HTML to display the link.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function QueueStatusItemLink($queue_string = "", $status_string = "", $status_state = "") {
   $link_string = str_replace(" ", "%20", $queue_string);
   $status_link_string = str_replace(" ", "%20", $status_string);
   $link = "<a class=\"main\" href=\"search.php?QueueOp==&valueOfQueue=";
   $link .= "$link_string&StatusOp==&valueOfStatus=$status_state&Action=";
   $link .= "Show+Results&Reset=0\"> $status_string</a>";
   return Font($link);
}

//-----------------------------------------------------------------------------
//
// Function: AllowableDaysOfQ
// Description:
//    Fetches the number of days until a ticket is overdue in a specific queue.
// Parameters:
//    string $queue	Name of queue.
// Return Values:
//    Returns the number of days allowed.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function AllowableDaysOfQ($queue = "") {
   global $db;
   $query = "SELECT q_name, allowabledays FROM queue WHERE q_name='$queue'";
   $row = mysql_fetch_object(mysql_query($query,$db));
   $days_allowed = $row->allowabledays;
   return $days_allowed;
}

//-----------------------------------------------------------------------------
//
// Function: OverdueLinkNum
// Description:
//    Used for the overview display - linkifies the numbers.
// Parameters:
//    string $queue	Name of queue to include in link.
//    date $day		Date of newest overdue tickets.
//    int $num		Number to display.
// Return Values:
//    Returns the HTML to display the link.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OverdueLinkNum($queue, $day, $num = 0) {
   $link_string = str_replace(" ", "%20", $queue);
   $string = "<a class=\"main\" ";
   $string .= "href=\"search.php?";
   $string .= "QueueOp==";
   $string .= "&valueOfQueue=$link_string";
   $string .= "&StatusOp=!=";
   $string .= "&valueOfStatus=resolved";
   $string .= "&date_type=date_created";
   $string .= "&DateOp=<";
   $string .= "&valueOfDate=$day";
   $string .= "&Action=Show+Results";
   $string .= "&Reset=0\">";
   $string .= "$num</a>";
   return $string;
}

//-----------------------------------------------------------------------------
//
// Function: Refresh
// Description:
//    Allows the refresh rate of a page to be selected.
// Parameters:
//    string $rate	Selected refresh rate.
// Return Values:
//    Returns HTML to generate the form for selecting the refresh rate of page.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function Refresh($rate = "") {
   global $self, $username;
   if ($rate == "") $rate = GetValueFromTable("refresh", "staffprefs", "where username=\"$username\"");
   $one = $two = $three = $four = $five = $six = $seven = "";
   if (empty($rate)) $one = "selected";
   if ($rate == "120") $two = "selected";
   if ($rate == "300") $three = "selected";
   if ($rate == "600") $four = "selected";
   if ($rate == "1200") $five = "selected";
   if ($rate == "3600") $six = "selected";
   if ($rate == "7200") $seven = "selected";
   return "<form action=\"$self\"><select name=\"rate\">"
          ."<option value=\"\" $one>Don't refresh this page.</option>"
          ."<option value=\"120\" $two>Refresh this page every 2 minutes.</option>"
          ."<option value=\"300\" $three>Refresh this page every 5 minutes.</option>"
          ."<option value=\"600\" $four>Refresh this page every 10 minutes.</option>"
          ."<option value=\"1200\" $five>Refresh this page every 20 minutes.</option>"
          ."<option value=\"3600\" $six>Refresh this page every hour.</option>"
          ."<option value=\"7200\" $seven>Refresh this page every two hours.</option>"
          ."</select>&nbsp;<input type=submit value=\"Go!\"></form>";
}    

//-----------------------------------------------------------------------------
//
// Function: DeleteTicket
// Description:
//    Removes a ticket from the database completely.
// Parameters:
//    int $id	ID of ticket to delete.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DeleteTicket($id) {
   global $db;
   $query = "delete from ticket where id='$id'";
   $result = mysql_query($query, $db);
   $query = "delete from comment where ticket_id = '$id'";
   $result = mysql_query($query, $db);
   $query = "Delete from ticketwatcher where ticket_id = '$id'";
}
    
//-----------------------------------------------------------------------------
//
// Function: MassMail
// Description:
//    Sends mail to multiple users at a time.
// Parameters:
//    array $array	Emails to send mail to.
//    string $username	Staff member emails are from.  Signature attached.
//    string $queue	Not used.
//    int $ticket_id	ID of ticket emails are sent in relation to.
//    string $content	Body of email to be sent.
// Return Values:
//    Returns the output of mail() from attempt to send to $array.
// Remarks:
//    Will cause rejections from some mail servers if $to is too large.
//-----------------------------------------------------------------------------
function MassMail($array = "", $username = "", $queue = "", $ticket_id = "", $content = "") {
   $to = "";
   for ($i = 0; $i < sizeof($array); $i++) {
      $to .= "$array[$i],";
   }
   $to = substr($to, 0, -1); // strip last comma
   $subject = "Help Desk Ticket: $ticket_id";
   $sg = get_staff_signature($username);
   $url = "https://".$_SERVER['SERVER_NAME']."/ticket.php?id=$ticket_id";
   $message = "$content \r\n \r\n";
   $message .= "For more information go to:  ";
   $message .= "\r\n \r\n $url \r\n \r\n $sg";
   $sender = get_staff_email($username);
   $message = str_replace("&#039;", "'", $message);
   $message = str_replace("&#39;", "'", $message);
   return mail("$to", "$subject", "$message", "From: $sender\r\n");
}
?>