<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


/*-----------------------------------------------------------------------------
Extensible Authentication:
Set this to include the authentication file, which will have these functions:
Auth($username, $password)
 - Should return TRUE if $username and $password are correct.
 - Should return FALSE if they are not.
GetLastError()
 - This should return a string, the error message to display on screen.
-----------------------------------------------------------------------------*/

function Auth($username, $password) {
   global $db;
   $query = "select * from staff where username='$username' and password=password('$password')";
   $result = mysql_query($query, $db);
   if (!$result || mysql_num_rows($result) <= 0) return FALSE;
   else return TRUE;
}

function GetLastError() {
   return "An invalid username or password was supplied.";
}
?>
