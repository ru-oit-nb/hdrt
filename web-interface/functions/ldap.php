<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("ru_ldap_auth.php");
//
// Filename:            ldap.php
// Description:         Interface and implementation of class for handling
//                      LDAP requests
//
class LDAP {
   var $hLDAP;
   // Handle to LDAP connection, if any
   var $szLastError;
   // Last error message
   var $iError;
   // Last error number

   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::LDAP
   //
   // Description:      Class constructor function
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    N/A
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function LDAP() {
      $this->hLDAP = NULL;
      $this->szLastError = '';
      $this->iError = 0;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::SaveLastError
   //
   // Description:      Saves information about the last error to occur
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    None
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function SaveLastError() {
      if (!is_null($this->hLDAP)) {
         $this->iError = ldap_errno($this->hLDAP);
         $this->szLastError = ldap_err2str($this->iError);
      }
   }

   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::GetLastError
   //
   // Description:      Retrieves and returns the last error code to occur
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    integer           The last error code to occur
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function GetLastError() {
      return $this->iError;
   }

   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::GetLastErrorMessage
   //
   // Description:      Retrieves and returns the last error message to occur
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    string            A string pertaining to the last error to occur
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function GetLastErrorMessage() {
      return $this->szLastError;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::Connect
   //
   // Description:      Attempts to connect to an LDAP server
   //
   // Type:             non-static
   //
   // Parameters:
   //    [in] string $szServer      The URL of the LDAP server to connect to
   //    [in] string $szLogin       The DN of the user to authenticate as or
   //                               NULL to perform anonymous authentication
   //    [in] string $szPassword    The password to authenticate with
   //    [in] boolean $fUseV3       Use LDAPv3
   //
   // Return Values:
   //    integer           -1 if a general error occurred, 0 if the username
   //                      and/or password was incorrect, or 1 on success;
   //                      Call GetLastErrorMessage() to retrieve the last
   //                      error message or GetLastError() to retrieve the
   //                      last error code
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function Connect($szServer, $szLogin, $szPassword, $fUseV3 = true) {
   // Try and connect to the server
      $this->hLDAP = ldap_connect($szServer);
      if (is_null($this->hLDAP)) {
         $this->SaveLastError();
         return -1;
      }
          
   // Use LDAP protocol version 3
      if ($fUseV3) {
         if (!ldap_set_option($this->hLDAP, LDAP_OPT_PROTOCOL_VERSION, 3)) {
            $this->SaveLastError();
            $this->Close();
            return -1;
         }
      }
          
   // Perform the bind
      $fSuccess = false;
      if (is_null($szLogin)) {
         $fSuccess = @ldap_bind($this->hLDAP, NULL, NULL);
      } else {
         $fSuccess = @ldap_bind($this->hLDAP, $szLogin, stripslashes($szPassword));
      }
      if (!$fSuccess) {
         $this->SaveLastError();
         $this->Close();
      // Catch 'No such object' and 'Invalid credentials' errors which
      // indicate that either the user doesn't exist, or they entered
      // the wrong password - Don't clue hackers in to which part of
      // the authentication was wrong!
         if ($this->iError == 49 || $this->iError == 32)
            return 0;
         return -1;
      }
      return 1;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::ConnectID
   //
   // Description:      Attempts to connect to an LDAP server returns con_id
   //
   // Type:             non-static
   //
   // Parameters:
   //    [in] string $szServer      The URL of the LDAP server to connect to
   //    [in] string $szLogin       The DN of the user to authenticate as or
   //                               NULL to perform anonymous authentication
   //    [in] string $szPassword    The password to authenticate with
   //    [in] boolean $fUseV3       Use LDAPv3
   //
   // Return Values:
   //    Returns connection ID of ldap_connect
   //
   // Remarks:
   //    added by jfulton to expand Josh's library for LDAP lookups
   //
   //-------------------------------------------------------------------------
   function ConnectID($szServer, $szLogin, $szPassword, $fUseV3 = true) {
      // Try and connect to the server
      $this->hLDAP = ldap_connect($szServer);
      if (is_null($this->hLDAP)) {
         $this->SaveLastError();
         return -1;
      }
          
      // Use LDAP protocol version 3
      if ($fUseV3) {
         if (!ldap_set_option($this->hLDAP, LDAP_OPT_PROTOCOL_VERSION, 3)) {
            $this->SaveLastError();
            $this->Close();
            return -1;
         }
      }
          
      // Perform the bind
      $fSuccess = false;
      if (is_null($szLogin))
         $fSuccess = @ldap_bind($this->hLDAP, NULL, NULL);
      else
         $fSuccess = @ldap_bind($this->hLDAP, $szLogin, $szPassword);
      if (!$fSuccess) {
         $this->SaveLastError();
         $this->Close();
             
         // Catch 'No such object' and 'Invalid credentials' errors which
         // indicate that either the user doesn't exist, or they entered
         // the wrong password - Don't clue crackers in to which part of
         // the authentication was wrong!
         if ($this->iError == 49 || $this->iError == 32)
            return 0;
         return -1;
      }
      return $this->hLDAP;
      // only change by jfulton
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::Search
   //
   // Description:      Performs a search on the LDAP server
   //
   // Type:             non-static
   //
   // Parameters:
   //    [out] array &$arrayResults    Array to store results from search in
   //    [in] string $szBaseDN         The base to start searching below
   //    [in] string $szFilter         The filter to use in the search
   //    [in] array $arrayAttrib       The specific attributes to return
   //    [in] string $szSortAttrib     The attribute to sort on
   //    [in] boolean $fSub            true to search everything below the
   //                                  base or false to search only 1 level
   //
   // Return Values:
   //    integer           1 on success, -1 if more entries are available,
   //                      or 0 on an error; Call GetLastErrorMessage() to
   //                      retrieve the last error message or GetLastError()
   //                      to retrieve the last error code
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function Search(&$arrayResults, $szBaseDN, $szFilter, $arrayAttrib = NULL,
      $szSortAttrib = NULL, $fSub = true) {
      // Make sure we have an active connection
      if (is_null($this->hLDAP)) {
         $this->szLastError = "Not connected";
         $this->iLastError = -1;
         return 0;
      }
          
      // Search everything below the base
      if ($fSub) {
         if (!is_null($arrayAttrib)) {
            $hSearch = ldap_search($this->hLDAP, $szBaseDN, $szFilter,
               $arrayAttrib);
         }
         else
            $hSearch = ldap_search($this->hLDAP, $szBaseDN, $szFilter);
      }
          
      // Search only 1 level below the base
      else
      {
         if (!is_null($arrayAttrib)) {
            $hSearch = ldap_list($this->hLDAP, $szBaseDN, $szFilter,
               $arrayAttrib);
         }
         else
            $hSearch = ldap_list($this->hLDAP, $szBaseDN, $szFilter);
      }
          
      // If there was an error other than exceeding the size limit, we're done
      $iRC = 1;
      $iError = ldap_errno($this->hLDAP);
      if ($iError == 4)
         $iRC = -1;
      else if ($iError) {
         $this->SaveLastError();
         return 0;
      }
          
      // If requested, do a sort
      if (!is_null($szSortAttrib) && !ldap_sort($this->hLDAP, $hSearch,
         $szSortAttrib)) {
         $this->SaveLastError();
         return 0;
      }
          
      // Now that we have our search results, get the entries
      $arrayResults = ldap_get_entries($this->hLDAP, $hSearch);
      if (!$arrayResults) {
         $this->SaveLastError();
         return 0;
      }
      return $iRC;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         LDAP::Close
   //
   // Description:      Closes the LDAP connection if it is open
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    None
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function Close() {
      @ldap_close($this->hLDAP);
      $this->hLDAP = NULL;
   }
}
?>
