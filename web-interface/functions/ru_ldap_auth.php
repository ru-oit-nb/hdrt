<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("ldap.php");
//
// Filename:            ru_ldap_auth.php
// Description:         Interface and implementation of class for handling
//                      LDAP-based authentication using Rutgers LDAP server(s)
//
// NOTE:
// Just change the "Configurable information" variables to match what they
// need to be for the Rutgers LDAP server.  In your code, just create a
// new RutgersLDAPAuth object and call the Authenticate() member to
// authenticate a user.  Expand upon this class as necessary.
//
    
// Dependent libraries (You may need to change this path depending on your
// PHP configuration)

class RutgersLDAPAuth extends LDAP {
   //
   // Configurable information:
   //    'auth_file' => Full path to the name of the file containing the
   //                   LDAP authentication login and password information
   //    'server' => The server to use to authenticate folks
   //    'useVersion3' => true if you want to use LDAPv3 or false if not
   //    'searchBase' => The base of the LDAP 'tree' to search for users
   //                    and objects under
   //    'authSearchAttrib' => The attribute to search on when trying to
   //                          get the DN of a user based on their login
   //                          (ie: what attribute in LDAP corresponds to
   //                          the user's NetID?)
   //
   var $arrayConfig = array(
   'auth_file' => 'insert_your_file_here',
   'server' => 'ldaps://ldap.domain.tld',
      'useVersion3' => true,
      'searchBase' => 'ou=People,dc=rutgers,dc=edu',
      'authSearchAttrib' => 'uid',
      );

   //-------------------------------------------------------------------------
   //
   // Function:         RutgersLDAPAuth::RutgersLDAPAuth
   //
   // Description:      Class constructor function
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    N/A
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function RutgersLDAPAuth() {
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         RutgersLDAPAuth::ReadAuthFile
   //
   // Description:      Reads authentication information from an "encrypted"
   //                   file
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    array             An associative array with 'login' and 'password'
   //                      keys on success or NULL on failure; Call
   //                      GetLastErrorMessage() to retrieve the error
   //                      that last occurred
   //
   // Remarks:
   //    The file must contain 1 line that's base64 encoded and be in the
   //    format:
   //          authentication_dn:some_unique_text:password
   //
   //-------------------------------------------------------------------------
   function ReadAuthFile() {
      // Open the file
      $arrayFile = @file($this->arrayConfig['auth_file']);
      if (!$arrayFile) {
         $this->szLastError = "Cannot find or open the file '" . "{$this->arrayConfig['auth_file']}' for reading";
         return NULL;
      }
   
      // Decode the information
      $szAuth = base64_decode($arrayFile[0]);
      $arrayAuth = explode(":", $szAuth, 3);
      $arrayInfo = array('login' => $arrayAuth[0], 'password' => $arrayAuth[2],);
      return $arrayInfo;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         RutgersLDAPAuth::Connect
   //
   // Description:      Attempts to connect to LDAP using the configured
   //                   authentication information
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    boolean           true on success or false on failure; Call
   //                      GetLastErrorMessage() to retrieve the error
   //                      that last occurred
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function Connect() {
      // Grab the authentication information
      $arrayAuthInfo = $this->ReadAuthFile();
      if (is_null($arrayAuthInfo))
         return false;

      // Connect to LDAP
      $iRC = parent::Connect($this->arrayConfig['server'],
         $arrayAuthInfo['login'], $arrayAuthInfo['password'],
         $this->arrayConfig['useVersion3']);
      if ($iRC != 1)
         return false;
      return true;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         RutgersLDAPAuth::ConnectID
   //
   // Description:      Attempts to connect to LDAP using the configured
   //                   authentication information & returns connection ID
   //
   // Type:             non-static
   //
   // Parameters:
   //    None
   //
   // Return Values:
   //    connection_id     returns connection id of ldap_connect
   //
   // Remarks:
   //    added by jfulton for LDAP lookups
   //
   //-------------------------------------------------------------------------
   function ConnectID() {
      // Grab the authentication information
      $arrayAuthInfo = $this->ReadAuthFile();
      if (is_null($arrayAuthInfo))
         return false;

      // Connect to LDAP
      $iRC = parent::ConnectID($this->arrayConfig['server'],
         $arrayAuthInfo['login'], $arrayAuthInfo['password'],
         $this->arrayConfig['useVersion3']);
      return $iRC;
   }
       
   //-------------------------------------------------------------------------
   //
   // Function:         RutgersLDAPAuth::Authenticate
   //
   // Description:      Attempt to authenticate a user based on their login
   //                   and password
   //
   // Type:             non-static
   //
   // Parameters:
   //    [in] string $szLogin       The Rutgers NetID to use to perform the
   //                               authentication
   //    [in] string $szPassword    The corresponding password
   //
   // Return Values:
   //    boolean           true if the user authenticated successfully or
   //                      false if not; Call GetLastErrorMessage() to
   //                      retrieve the error that last occurred
   //
   // Remarks:
   //    None
   //
   //-------------------------------------------------------------------------
   function Authenticate($szLogin, $szPassword) {
      // Make sure the login is not empty - We don't want to authenticate as
      // an anonymous user
      $szLogin = trim($szLogin);
      if ($szLogin == '') {
         $this->szLastError = "No NetID was provided";
         return false;
      }

      // NetIDs should not contain * characters
      if (strpos($szLogin, "*") !== false) {
         $this->szLastError = "NetIDs cannot contain the * character";
         return false;
      }

      // First connect to the database
      if (!$this->Connect())
         return false;

      // Now search for the user in the LDAP database - We ask that the DN
      // be returned (The DN should always be returned without asking for it
      // explicitly, but we just want to be safe...)
      $iRC = $this->Search($arrayResults, $this->arrayConfig['searchBase'],
         "{$this->arrayConfig['authSearchAttrib']}=$szLogin", array('dn'));
      $this->Close();
      if ($iRC != 1)
         return false;

      // No match was found, so return with a semi-generic error so that
      // we don't give away that the login was wrong
      else if (!$arrayResults['count']) {
         $this->szLastError = "An invalid username and/or password was " . "supplied";
         return false;
      }
      $szDN = $arrayResults[0]['dn'];

      // We found the user, so now we need to authenticate them
      $iRC = parent::Connect($this->arrayConfig['server'], $szDN, $szPassword,
         $this->arrayConfig['useVersion3']);
      $this->Close();
      if ($iRC == -1)
         return false;
      else if (!$iRC) {
         $this->szLastError = "An invalid username and/or password was " . "supplied";
         return false;
      }
      return true;
   }
}
?>
