<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");
//
// Filename: functions-admin-groups.php
// Description: Group Administration Functions
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: ModifyGroupForm
// Description:
//    Generates form to modify group name, description, etc.
// Parameters:
//    string $name	Group Name
//    string $description	Group Description
// Return Values:
//    String of HTML to display form.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function ModifyGroupForm($name = "", $description = "") {
   global $username, $db;
   $name = str_replace("_", " ", $name);
   $check_group_type = "SELECT * FROM groups WHERE group_name='$name'";
   $result = mysql_query($check_group_type, $db);
   $row = mysql_fetch_object($result);
   $type = $row->type;
   $output = "<form><input type=\"hidden\" name=\"id\" value=8>"
             ."<table border=\"0\" width=\"100%\"><tr><td align=\"right\">"
             .Font("Name:")."</td><td>";
   if ($type == "pseudo"){
      $output .= "<input type='hidden' name='name' value='$name'>$name"; 
   }
   else{
      $output .= "<input type='text' name='name' value='$name'>";
   }
   $output .= '</td></tr><tr><td align="right">'
           .Font("Description:").'</td><td colspan="3">';
   if ($name=="")
      $output .= "<input type=hidden name=NewGroup value=yes>\n"
              ."<input name=\"description\" value=\"$description\" "
              ."size=60></td></tr><tr>";
   else {
      $output .= "<input name=\"description\" value=\"$description\" "
              ."size=60></td></tr><tr>";
   }
   if ($type != "pseudo"  && $name != ""){
      $output .= '<td align="right">'.font("Delete Group:")
              .'</td><td align="left">'
              .'<input type=checkbox name=DeleteGroup></td>';
   }
   $output .='</tr><tr><td></td><td align="right">';
   if (DoesUserHaveRight($username, "AdminGroup")){
      $output .= "<input type=\"submit\" value=\"Submit\">";
   }
   $output .= "</td></tr></table></form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: ModifyGroupMemberForm
// Description:
//    Generates a form for adding and deleting group members.
// Parameters:
//    string $group	Group Name
//
// Return Values:
//    String of HTML to display form.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function ModifyGroupMemberForm($group = "") {
   global $sub_group,$username;
   $output = "";
   if (empty($group)) {
      $output .= Spacer(1)
              .FontBold("Please Create the Group before you add members.")
              .Spacer(2);
      return $output;
   }
   $usr_array = GetStaff();
   $output .= "<form><input type=\"hidden\" name=\"group\" value=\"$group\""
           .'<table border="0" width="100%"><tr><td width="33%">'
           .FontBold("Add Members:").'</td><td width="33%">'
           .FontBold("Current Members:")
           .Font("<br>(Check box to delete group member)")
           .'</td><td width="33%">&nbsp;</td></tr><tr><td valign="top">'
           .'<select multiple name="AddMembers[]" size=10>'
           .CreateSelectOptions($usr_array, "open")
           .'</select></td><td align="left"><ol>'
           .GetSubMembers($group)
           .'</ol></td><td valign="bottom" align="right">'
           ."<input type=\"hidden\" name=\"sub_group\" value=\"$sub_group\">";

   if (DoesUserHaveRight($username, "AdminGroup")) {
      $output .= "<input type=\"submit\" value=\"Submit\">";
   }
   $output .= '</td></tr></table></form>';
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetSubMembers
// Description:
//    Returns members of a group as a list.
// Parameters:
//    string $group	Group Name
//
// Return Values:
//    string $output	printable HTML checkbox list of group members
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function GetSubMembers($group = "") {
   global $db;
   $query = "select username from ingroup where group_name='$group'";
   $result = mysql_query($query, $db);
   $array = array();
   $output = "";

   while ($row = mysql_fetch_assoc($result)) {
      array_push($array, $row['username']);
   }
   mysql_free_result($result);

   foreach ($array as $element) {
      $output .= Font("<li><input type=checkbox name=DelMembers[] value=\"$element\">$element<br>");
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetCertain Users
// Description:
//    Retrieves a list of users based on certain criteria.
// Parameters:
//    string $string	string to search for in $field
//    string $field	field to examine in database
//    string $type 	type of search to perform (LIKE, NOT LIKE)
// Return Values:
//    array of users or "Empty" if no users found.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function GetCertainUsers($string, $field, $type) {
   if ($string == NULL) $string = "";
   if ($type == "LIKE" || $type == "NOT LIKE") $string = "%$string%";
   $query = "select username from staff where $field $type '$string'";
   $result = mysql_query($query);
   if (isset($result) && mysql_num_rows($result) > 0) {
      $newusers = array();
      while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
         array_push($newusers, $row['username']);
      }
   }
   if (is_array($newusers)) {
      return $newusers;
   }

   else {
      return array("Empty");
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: AddGroupMember
// Description:
//    Adds members to specified group.
// Parameters:
//    string $group	Group Name
//    array $AddMembers	Members to add to $group
//
// Return Values:
//    Returns errors.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function AddGroupMember($group = "", $AddMembers = "") {
   $error = array();
   foreach ($AddMembers as $member) {
      $query = "insert into ingroup set group_name='$group', username='$member'";
      $result = mysql_query($query);
      if (empty($result)) array_push($error, $member);
   }
   return($error);
}

//----------------------------------------------------------------------
//
// Function:      BulkAddGroup
//
// Description:   Adds member to specified groups
//
// Parameters:    
//   $group_list:  array containing list of groups
//   $username:    username 
//
// Return Values: 
//   Returns errors.
// Remarks:       
//   None.
//----------------------------------------------------------------------

function BulkAddGroup($group_list = "", $username){
   $error = array();
   foreach ($group_list as $group) {
      $query = "INSERT INTO ingroup SET group_name='$group', username='$username'";
      $result = mysql_query($query);
      if (empty($result)) array_push($error, $member);
   }
  return($error);

}
    
//-----------------------------------------------------------------------------
//
// Function: DelGroupMember
// Description:
//    Removes members from group.
// Parameters:
//    string $group	Group Name
//    array $DelMembers	Members to delete from $group.
//
// Return Values:
//    Returns errors.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DelGroupMember($group = "", $DelMembers = "") {
   $error = array();
   foreach ($DelMembers as $member) {
      $query = "delete from ingroup where group_name='$group' and username='$member'";
      $result = mysql_query($query);
      if (empty($result)) array_push($error, $member);
   }
   return($error);
}
    
//-----------------------------------------------------------------------------
//
// Function: ChangeGroupDescription
// Description:
//    Updates a group description.  Also used to create a group.
// Parameters:
//    string $group	Group Name
//    string $description	Group Description
//    string $new	If yes, create instead of update.
//
// Return Values:
//    Error if update failed.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function ChangeGroupDescription($group = "", $description = "", $new = "") {
   if ($new != "yes" )
      $query = "update groups set description='$description' where group_name='$group'";
   else
      $query = "insert into groups set group_name='$group', description='$description'";
   $result = mysql_query($query);
   if ($new != "yes") {
      if (empty($result)) return ("Error updating $group to set description=$description.");
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetGroupDescription
// Description:
//    Retrieves the description of the specified group from the database.
// Parameters:
//    string $group	Group Name
//
// Return Values:
//    string	Description
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function GetGroupDescription($group = "") {
   $query = "select description from groups where group_name='$group'";
   if (empty($group)) return ("Could not process group name.  Please try again or contact the webmaster.");
      $result = mysql_query($query);
   if (isset($result))
      while ($row = mysql_fetch_assoc($result)) {
      $description = $row['description'];
   }
   mysql_free_result($result);
   return ($description);
}
    
//-----------------------------------------------------------------------------
//
// Function: DeleteGroup
// Description:
//    Deletes the specified group from the database.
//
// Parameters:
//    string $group	Group Name
//
// Return Values:
//    Error if group was not successfully deleted.
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DeleteGroup($group = "") {
   $group = str_replace("_", " ", $group);
   $query = "delete from groups where group_name='$group'";
   $result = mysql_query($query);
   if (empty($result)) return ("Could not delete group $group.");
}
?>
