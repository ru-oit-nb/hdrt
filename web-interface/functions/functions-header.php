<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.


// Filename:                functions-header.php
// Description:             Library for site security
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-01-28 16:29:43 jfulton> 
// -------------------------------------------------------- 
// Extended Description:  
// 
// Called by "../security.php" (see that for more info) 
// -------------------------------------------------------- 
// Assumes you have two SQL tables:
// 
// I.  session
//
// Assumes you have an SQL table called session:  
// 
// create table session (
//   session_code varchar(15) NOT NULL default '',
//   username varchar(15) NOT NULL default '',
//   primary key  (username)
// );
// 		      
// session_codes are stored in DB only for security purposes.  
// I.e. users could change their cookies.  
// 
// Remember to edit SessionSalt() below.  
// 
// II. account
// 
// It also assumes you have a table called "account" that 
// has a field called username that stores the NetID of the 
// person that is logged in.  
// 
// A valid login is an RU NetID that will survive an LDAP 
// NetID/Password check AND be in the "account" table.  
// Normally we then provide an "administrator" an interface 
// to add NetIDs to the account table.  
// 
// If you don't want to use NetIDs simply alter the "account"
// table to have a password field and check that the provided 
// password matches the one in the table.  
// -------------------------------------------------------- 
// Depends on the following:  
//require_once("ldap.php");
//require_once("ru_ldap_auth.php");

// -------------------------------------------------------- 
// Function:                SessionValid
// Description:             Verifies if the session code is 
//                          valid based on $_SESSION['username']
// Type:                    public
// Parameters:
//    None                  
// Return Values:           
//    boolean               True if session code is valid 
// Remarks:                 
//    None                  
// -------------------------------------------------------- 
function SessionValid() {
  if (!isset($_SESSION['session_code'])) return 0;
  else {
    $sql  = "SELECT count(username) as boolean ";
    $sql .= "FROM session WHERE ";
    $sql .= "username='";
    $sql .= $_SESSION['username'];
    $sql .= "' AND session_code='";
    $sql .= $_SESSION['session_code'];
    $sql .= "'";
    $result = mysql_query($sql) or die(mysql_error());
    $obj = mysql_fetch_object($result);
    return $obj->boolean;
  }
}

// -------------------------------------------------------- 
// Function:                SessionExpired
// Description:             Verifies if the session code is 
//                          $SECONDS (global) too old 
// Type:                    public
// Parameters:
//    None                  
// Return Values:           
//    boolean               False if session is $SECONDS too old 
// Remarks:                 
//    None                  
// -------------------------------------------------------- 
function SessionExpired() {
  $seconds = GetSessionExpireTime();
  $current_time = time();
  if (($_SESSION['time'] + $seconds) < $current_time) {
    session_unset();
    return 1;
  }
  else {
    $_SESSION['time'] = $current_time;  // reset clock 
    return 0;
  }
}

// -------------------------------------------------------- 
// Function:                GetSessionExpireTime
// Description:             Gets the # of seconds a session 
//                          should last 
// Type:                    public
// Parameters:
//    None                  
// Return Values:           
//    int               Number of seconds 
// Remarks:                 
//    None                  
// -------------------------------------------------------- 
function GetSessionExpireTime() {
  global $db;
  $query = "SELECT val FROM system_variables WHERE var = ";
  $query .= "'session_expire' limit 1";
  $result = mysql_query($query, $db);
  if (!$result) return 0;
  $object = mysql_fetch_object($result);
  return $object->val;
}

// -------------------------------------------------------- 
// Function:                RegisterSession
// Description:             Registers session vars with 
//                          session and db
// Type:                    public
// Parameters:
//    None                  
// Return Values:           
//    None                  
// Remarks:                 
//    None                  
// -------------------------------------------------------- 
function RegisterSession() {
  $_SESSION['username'] = $_POST['username']; 
  $current_time = time();
  $_SESSION['time'] = $current_time;
  $_SESSION['session_code'] = substr(crypt($current_time, SessionSalt()), 0, 32);
  $sql  = "REPLACE INTO session SET ";
  $sql .= "username='" . $_SESSION['username'] . "', ";
  $sql .= "session_code='" . $_SESSION['session_code'] . "'";
  mysql_query($sql) or die(mysql_error());
}

// -------------------------------------------------------- 
// Function:                ShowCredentials
// Description:             Makes sure user can stay, if so 
//                          registers them
// Type:                    public
// Parameters:
//    None                  
// Return Values:           
//    None                  
// Remarks:                 
//    None                  
// -------------------------------------------------------- 
/* -----------------------------------------------------------------------------
Extensible Authentication:
Set this to include the authentication file, which will have these functions:
Auth($username, $password)
 - Should return TRUE if $username and $password are correct.
 - Should return FALSE if they are not.
GetLastError()
 - This should return a string, the error message to display on screen.
----------------------------------------------------------------------------- */
function ShowCredentials($msg=""){
  global $AUTH_TYPE;
  if (!isset($_POST['username'])) { 
    // user has first arrived
    print Head() . $msg . LoginForm() . Foot();
    exit;
  }
  else {
    // see if user filled out form correctly 
    $errmsg = CheckInput();
    if (!empty($errmsg)) {
      print Head() . ErrorReport($errmsg) . LoginForm() . Foot();
      exit;
    }
    else {
      if (!HasAccount()) {
	$errmsg = "An invalid username and/or password was supplied";
	print Head() . ErrorReport($errmsg) . LoginForm() . Foot();
	exit;
      }
      else {
        if ($AUTH_TYPE == "local") $auth_filename = "functions/functions-auth-local.php";
        else $auth_filename = "functions/functions-auth-ru-ldap.php";
        require_once($auth_filename);
        if (!Auth($_POST['username'], $_POST['password'])) {
	  print Head() . ErrorReport(GetLastError()) . LoginForm() . Foot();
          exit;
	}
	else {
	  // auth good, so register session vars 
	  RegisterSession();
	}
      }
    }
  }
}

// -------------------------------------------------------- 
// Function:                HasAccount

// Description:             Verifies if $_POST['username'] 
//                          has an account

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    boolean               True if $username has an account

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function HasAccount() 
{
  $sql = "SELECT COUNT(username) AS boolean FROM staff ";
  $sql .= "WHERE username='" . $_POST['username'] . "'";
  $result = mysql_query($sql) or die(mysql_error());
  $obj = mysql_fetch_object($result);
  return $obj->boolean;
}

// -------------------------------------------------------- 
// Function:                ShowCredentials

// Description:             Displays login form

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    None                  

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function LoginForm() 
{
  $username = $_POST['username'];
  global $self, $id;
  foreach ($_GET as $field => $value) {
    if ($field != "logout") {
      if ($field != "id" || empty($id)) $gets .= "$field=$value&";
    }
  }
  if (!empty($gets)) $gets = substr($gets, 0, -1);
  return "<center>\n".OpenColorTable("blue", "Login", "50%")
         ."<form action=\"$self?$gets\" method=\"post\">\n"
         ."<table border=0 width=\"100%\"><tr align=\"right\">\n"
         ."<td align=\"right\">".Font("Username:")
         ."</td>\n"
         ."<td align=\"left\">"
         ."<input type=\"text\" name=\"username\" value=\"$username\" size=15>"
         ."</td>\n</tr>\n<tr>\n<td align=\"right\">".Font("Password:")
         ."</td>\n<td align=\"left\">"
         ."<input type=\"password\" name=\"password\" value=\"\" size=15>"
         ."</td>\n</tr>\n<tr>\n<td colspan=2 align=\"right\">"
         ."<input type=\"submit\" Value=\"Login\">"
         ."</td>\n</tr>\n</table>\n</form>\n".CloseColorTable()."\n</center>\n"
         ."<br>\n";
}

// -------------------------------------------------------- 
// Function:                CheckInput

// Description:             Checks that LoginForm is filled 
//                          in correctly 

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    string               A description of the error

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function CheckInput() 
{
  $str = "";
  if (empty($_POST['username'])) {
    $str .= "<li>Please enter a <em>Username</em></li>\n";
  }
  if (empty($_POST['password'])) {
    $str .= "<li>Please enter a <em>Password</em></li>\n";
  }
  if (empty($str)) return "";
  else return "\n<ul>\n" . $str . "</ul>\n";
}

// -------------------------------------------------------- 
// Function:                SessionSalt

// Description:             Returns "salt" to make session 
//                          unique

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    string                A string for the crypt function

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function SessionSalt() 
{
  global $db;
  $sql = "SELECT val FROM system_variables WHERE var='session_salt'";
  $result = mysql_query($sql, $db) or die(mysql_error());
  $row = mysql_fetch_object($result);
  return $row->session_salt;
}

// -------------------------------------------------------- 
// From functions header 
// -------------------------------------------------------- 



//-----------------------------------------------------------------------------
//
// Function: ReplaceMysql
// Description:
//    Forms and runs a mysql replace query.
// Parameters:
//    string $tablename		Table to replace into.
//    string $set_string	Query after "set".
// Return Values:
//    Returns the result of the query.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ReplaceMysql($tablename, $set_string) {
   global $db;
   $query = "Replace into $tablename set ".$set_string;
   $result = mysql_query($query, $db);
   return $result;
}
    
//----------------------------------------------------------------------------- 
// 
// Function: SetNoBack 
// Description: 
//    Prevents the user from going back. 
// Parameters: 
//    string $uniqueName 	Test value. 
//    string $uniqueValue 	Test value. 
// Return Values: 
//    1 if allowed, 0 if not. 
// Remarks: 
//    This function may not behave as expected.
//----------------------------------------------------------------------------- 
function SetNoBack($uniqueName,$uniqueValue) {
   if (!isset($_SESSION[$uniqueName]) || ($_SESSION[$uniqueName] != $uniqueValue)) {
      $_SESSION[$uniqueName] = $uniqueValue;
      return 1;
   }
   elseif ($_SESSION[$uniqueName] == $uniqueValue) return 0;
}
    
//-----------------------------------------------------------------------------
//
// Function: CheckScheduling
// Description:
//    Checks to see if the scheduling module is enabled.
// Parameters:
//    None.
// Return Values:
//    1 if enabled, 0 if not.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CheckScheduling() {
   global $db;
   $result = mysql_query("select use_module from modules where module_name='Scheduling'");
   $row = mysql_fetch_object($result);
   return $row->use_module;
}

//-----------------------------------------------------------------------------
//
// Function: CheckAuthType
// Description:
//    Checks to see the type of authentication to use.
// Parameters:
//    None.
// Return Values:
//    local if DB, extensible if other.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CheckAuthType() {
   global $db;
   $result = mysql_query("select val from system_variables where var='authentication_type'");
   $row = mysql_fetch_object($result);
   return $row->val;
}

//-----------------------------------------------------------------------------
//
// Function: CheckLookupType
// Description:
//    Checks to see the type of lookup to use.
// Parameters:
//    None.
// Return Values:
//    local if DB, extensible if other.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CheckLookupType() {
   global $db;
   $result = mysql_query("select val from system_variables where var='user_lookup_type'");
   $row = mysql_fetch_object($result);
   return $row->val;
}
?>
