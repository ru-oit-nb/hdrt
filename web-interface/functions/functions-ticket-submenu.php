<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-setters.php");
require_once("functions/functions-widgets.php");
//
// Filename: functions-ticket-submenu.php
// Description: Contains functions for updating tickets.
// Supprted Language(s):   PHP 4.0
//
global $self;
$self = $PHP_SELF;

//-----------------------------------------------------------------------------
//
// Function: ThisTicketSubmenuForm
// Description:
//    Generates HTML to display the ticket update form.
// Parameters:
//    int $id		Ticket ID.
//    string $provision	Provision that is set.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ThisTicketSubmenuForm($id, $provision = "") {
   global $username,$self,$force_resolve;
   $output = OpenColorTable("blue", "Update ticket #$id", "100%")
           ."<form method=\"post\" action=\"$self?id=$id\">";
   $watchers = "$self?id=$id&menu=People";
   $link = "<a class=\"main\" href=\"$watchers\">(more info)</a>";
   $last_comment_row = GetLastCommentRowOfTicket($id);
   $ticket = GetFirstRowFromTable("ticket where id = '$id'");
   $queue = $ticket->queue;
   if ($provision == "OnCorrespond") $reply_selected = "selected";
   if ($force_resolve) $last_comment_row->status = "Resolved";
   $output .= "<h3>Ticket Watchers: ".Font($link)."</h3><table>"
           .OpenTable("Requester")
           .PrintArray(GetTicketWatchers($id, "Requester"))
           .CloseTable().OpenTable("Cc")
           .PrintArray(GetTicketWatchers($id, "Cc"))
           .CloseTable().OpenTable("AdminCc")
           .PrintArray(GetTicketWatchers($id, "AdminCc"))
           .CloseTable().Spacer(1).OpenTable("<h3>Queue Watchers:</h3>")
           .CloseTable().OpenTable(" ")
           .PrintArray(GetQueueWatchers($queue)).CloseTable()
           .Spacer(1).OpenTable("<h3>Update Form:</h3>").CloseTable()
           .OpenTable("Update Type")
           ."<select name=\"provision\">"
           ."<option value=\"OnComment\">Comments (Not sent to requesters)"
           ."</option><option value=\"OnCorrespond\" $reply_selected>"
           ."Response to requesters</option></select>"
           .CloseTable().OpenTable("Status")
           ."<select name=\"status\">"
           .CreateSelectOptions(GetStatus(), "", $last_comment_row->status)
           ."</select>".CloseTable().OpenTable("Owner")
           ."<select name=\"owner\"><option value=\"$ticket->owner\">"
           ."$ticket->owner</option>"
           .CreateSelectOptions(GetStaff(), "", $ticket->owner)
           ."</select>".CloseTable().OpenTable("Worked")
           ."<input type=\"text\" size=8 name=\"time_worked\" value=\"$time_worked\">"
           .Font("minutes").CloseTable().OpenTable("Subject")
           ."<input type=\"text\" size=50 name=\"subject\" value=\"$subject\">"
           .CloseTable().OpenTable("Additional <br>Comments")
           ."<input type=\"hidden\" name=\"id\" value=\"$id\">"
           ."<input type=\"hidden\" name=\"form_submitted\" value=\"comment\">"
           ."<textarea cols=72 rows=15 wrap=\"hard\" name=\"body\">$body</textarea>"
           .CloseTable().OpenTable("If Resolving Ticket, Send <br>Preset Email Message To User")
           ."<input type=\"checkbox\" name=\"email_user\" value=1 checked>"
           .CloseTable()."</table><p align=\"right\">";

   if (DoesUserHaveRight($username, "CommentOnTicket", $queue, $id)){
       $output .= "<input type=\"submit\" value=\"Save Changes\">";
   }
   $output .= "</form>".CloseColorTable();
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketComment
// Description:
//    Generates HTML to display the ticket update form.
// Parameters:
//    int $id		Ticket ID.
// Return Values:
//    HTML to Display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketComment($id) {
   return OpenColorTable("green", "User", "100%")
         .TicketStaticUser($id).CloseColorTable()
         ."<br>"
         .ThisTicketSubmenuForm($id);
}
    
    
//-----------------------------------------------------------------------------
//
// Function: TicketReply
// Description:
//    Generates HTML to display the ticket update form.
// Parameters:
//    int $id		Ticket ID.
//    string $user	Not used.
// Return Values:
//    HTML to Display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketReply($id, $user) {
   return OpenColorTable("green", "User", "100%")
         .TicketStaticUser($id).CloseColorTable()
         ."<br>"
         .ThisTicketSubmenuForm($id, "OnCorrespond");
}

//-----------------------------------------------------------------------------
//
// Function: TicketSetNewStatus
// Description:
//    Updates a ticket's status.
// Parameters:
//    int $id			Ticket ID.
//    string $user		Staff username.
//    string $new_status	New status.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketSetNewStatus($id, $user, $new_status) {
   global $db;
   $query = "SELECT status, date_created, priority, staff, subject ";
   $query .= "FROM comment WHERE ticket_id='$id' ";
   $query .= "ORDER BY date_created DESC";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_array($result);
   $old_status = $row["status"];
   if ($old_status == $new_status) {
      $errmsg = "This ticket already has a status of $old_status.  ";
      $errmsg .= "No changes were made.  ";
      return ErrorReport($errmsg, "50%");
   }
   else {
     $priority = $row["priority"];
     $staff = $user;
     $subject = $row["subject"];
     $subject = htmlentities($subject);
     $comment_id = AddComment($id, $new_status, $priority, $staff, addslashes($subject));
     if (is_int($comment_id)) {
       return "<p><center>".OpenColorTable("red", "Results", "50%")
	 .Font("Status changed from ").FontBold($old_status)
	 .Font(" to ").FontBold($new_status).Font(" by ")
	 .FontBold($user).".".CloseColorTable()."</center>";
     }
     else {
       $errmsg = "Internal error.  Unable to change status";
       return ErrorReport($errmsg, "50%");
     }
   }
}

//-----------------------------------------------------------------------------
//
// Function: TicketOpen
// Description:
//    Sets the status of a ticket to Open.
// Parameters:
//    int $id		Ticket ID.
//    string $user	Staff username.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketOpen($id, $user) {
   if (!(DoesUserHaveRight($user, "ModifyTicket", "", $id))) {
      $errmsg = "Sorry, you do not have the required permission! &nbsp;&nbsp;";
      $errmsg .= "No changes were made.";
      return ErrorReport($errmsg, "50%");
   }
   return TicketSetNewStatus($id, $user, "Open");
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketResolve
// Description:
//    Resolves a ticket.
// Parameters:
//    int $id		Ticket ID.
//    string $user	Staff username.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketResolve($id, $user) {
   if (!(DoesUserHaveRight($user, "ModifyTicket", "", $id))) {
      $errmsg = "Sorry, you do not have the required permission! &nbsp;&nbsp;";
      $errmsg .= "No changes were made.";
      return ErrorReport($errmsg, "50%");
   }
   return TicketSetNewStatus($id, $user, "Resolved");
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketSteal
// Description:
//    Changes the owner of a ticket to $new_owner.
// Parameters:
//    int $id			Ticket ID.
//    string $new_owner		Staff username.
//    string $suppress_result	Suppress result?
// Return Values:
//    HTML to display if $supress_result empty.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketSteal($id, $new_owner, $supress_result = "") {
   global $db;
   $query = "SELECT STATUS, date_created, priority, staff, subject ";
   $query .= "FROM comment WHERE ticket_id='$id' ";
   $query .= "ORDER BY date_created desc";
   $query2 = "SELECT owner FROM ticket WHERE id='$id'";
   $result = mysql_query($query, $db);
   $ticket_result = mysql_query($query2, $db);
   $row = mysql_fetch_array($result);
   $ticket_row = mysql_fetch_array($ticket_result);
   $old_owner = $ticket_row["owner"];
   if ($old_owner == $new_owner) {
      $errmsg = "You already own this ticket.  ";
      $errmsg .= "No changes were made.  ";
      return ErrorReport($errmsg, "50%");
   }
   else {
     $provision = "OnOwnerChange";
     $priority = $row["priority"];
     $staff = $row["staff"];
     $subject = $row["subject"];
     $new_status = "Open";
     $comment_id = AddComment($id, $new_status, $priority, $new_owner, 
				    'Ownership Change', "", 
				    "Ownership has changed to $new_owner");
     $result2 = UpdateTicket($id, "owner='$new_owner'");
     if (supress_result != "") return 1;
     if (is_int($comment_id) && $result2) {
       return "<p><center>".OpenColorTable("red", "Results", "50%")
	 .Font("Owner changed from ").FontBold($old_owner)
	 .Font(" to ").FontBold($new_owner)."."
	 .CloseColorTable()."</center>";
     }

     else {
       $errmsg = "Internal error.  Unable to change status";
       return ErrorReport($errmsg, "50%");
     }
   }
}
?>