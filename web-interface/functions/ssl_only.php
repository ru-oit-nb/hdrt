<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


// Filename:                ssl_only.php
// Description:             Forces viewer to use https
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-01-21 11:09:21 jfulton> 
// -------------------------------------------------------- 
// This page can be included in any PHP page, that you need
// to use only "https" and NOT "http".  
// 
// Ideally this should be handled by an httpd.conf file but 
// this is an extra level of security 

if (empty($_SERVER["HTTPS"])) {
  $url = "https://" . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF'];
  MoveThem($url);
  // AskThem($url);
  /* uncomment the above accordinly */
}

// -------------------------------------------------------- 
// Function:                MoveThem

// Description:             Tries to redirect user to secure page

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    None                  

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function MoveThem($url="") {
  header("Location:  $url");
}

// -------------------------------------------------------- 
// Function:                AskThem

// Description:             Prints link of https version and 
//                          asks user to go to it.  

// Type:                    public

// Parameters:
//    None                  

// Return Values:           
//    None                  

// Remarks:                 
//    Exits at end to prvent them from entring data that 
//    Should have gone over SSL
// -------------------------------------------------------- 

function AskThem($url="") {
  print "Please use our secure site:  \n";
  print "<a href=\"$url\">$url</a>\n";
  exit;  // very important
}

?>
