<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
//
// Filename:               functions-search.php
// Description:            Functions associated with the search.php file.
// Supprted Language(s):   PHP 4.0+
//

//-----------------------------------------------------------------------------
//
// Function: SearchParametersForm
// Description:
//    Generates HTML to display the search forms.
// Parameters:
//    None.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SearchParamatersForm() {
   $output = "<tr><td><input type=\"hidden\" name=\"CompileRestriction\" value=\"1\"><ul><li>"
           .Font("Owner is&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"OwnerOp\">"
           ."<select name =\"OwnerOp\"><option value=\"=\" selected>is</option>"
           ."<option value=\"!=\" >isn't</option></select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfOwner\">"
           ."<select name=\"valueOfOwner\">"
           ."<option value=\"\">-</option>"
           .CreateSelectOptions(GetStaff())
           ."</select></li><li>"
           .Font("Requester email address&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"RequesterOp\">"
           ."<select name =\"RequesterOp\">"
           ."<option value=\"LIKE\" selected>contains</option>"
           ."<option value=\"NOT LIKE\" >doesn't contain</option>"
           ."<option value=\"=\" >is</option>"
           ."<option value=\"!=\" >isn't</option></select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfRequester\">"
           ."<input name=\"valueOfRequester\" size=\"20\"></li><li>"
           .Font("Subject&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"SubjectOp\">"
           ."<select name =\"SubjectOp\">"
           ."<option value=\"LIKE\" selected>contains</option>"
           ."<option value=\"NOT LIKE\" >doesn't contain</option>"
           ."<option value=\"=\" >is</option>"
           ."<option value=\"!=\" >isn't</option></select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfSubject\">"
           ."<input type=text name=\"valueOfSubject\"></li><li>"
           .Font("Queue&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"QueueOp\">"
           ."<select name=\"QueueOp\">"
           ."<option value=\"=\">is</option>"
           ."<option value=\"!=\">is not</option></select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfQueue\">"
           ."<select name =\"valueOfQueue\">"
           ."<option value=\"\">-</option>"
           .CreateSelectOptions(GetQueue())
           ."</select></li><li>"
           .Font("Priority&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"PriorityOp\">"
           ."<select name =\"PriorityOp\">"
           ."<option value=\"&gt;\" SELECTED>exceeds</option>"
           ."<option value=\"&lt;\" >is less than</option></select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfPriority\">"
           ."<input name=\"valueOfPriority\" SIZE=5></li><li>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"date_type\">"
           ."<select name=\"date_type\">"
           ."<option value=\"date_created\">Created</option>"
           ."<option value=\"started\">Started</option>"
           ."<option value=\"Resolved\">Resolved</option>"
           ."<option value=\"last_contact\">Last Contacted</option>"
           ."<option value=\"LastUpdated\">Last Updated</option>"
           ."<option value=\"starts\">Starts By</option>"
           ."<option value=\"due\">Due</option>"
           ."</select>"
           ."<select name =\"DateOp\">"
           ."<option value=\"&gt;\">After</option>"
           ."<option value=\"=\">On</option>"
           ."<option value=\"&lt;\">Before</option>"
           ."</select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfDate\">"
           ."<input name=\"valueOfDate\" value=\"\" size=16>YYYY-MM-DD</li>"
           ."<li><input type=\"hidden\" name=\"field_list[]\" value=\"date_type2\">"
           ."<select name=\"date_type2\">"
           ."<option value=\"date_created\">Created</option>"
           ."<option value=\"started\">Started</option>"
           ."<option value=\"Resolved\">Resolved</option>"
           ."<option value=\"last_contact\">Last Contacted</option>"
           ."<option value=\"LastUpdated\">Last Updated</option>"
           ."<option value=\"starts\">Starts By</option>"
           ."<option value=\"due\">Due</option>"
           ."</select>"
           ."<select name =\"DateOp2\">"
           ."<option value=\"&lt;\">Before</option>"
           ."<option value=\"=\">On</option>"
           ."<option value=\"&gt;\">After</option>"
           ."</select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfDate2\">"
           ."<input name=\"valueOfDate2\" value=\"\" size=16>YYYY-MM-DD</li><li>"
           .Font("Ticket content&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"ContentOp\">"
           ."<select name =\"ContentOp\">"
           ."<option value=\"LIKE\" SELECTED>matches</option>"
           ."<option value=\"NOT LIKE\" >does not match</option>"
           ."</select>"
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"valueOfContent\">"
           ."<input name=\"valueOfContent\" Size=20></li><li>"
           .Font("Status&nbsp;")
           ."<input type=\"hidden\" name=\"field_list[]\" value=\"StatusOp\">"
           ."<select name =\"StatusOp\">"
           ."<option value=\"=\" SELECTED>is</option>"
           ."<option value=\"!=\" >isn't</option>"
           ."</select>"
           ."<select name =\"valueOfStatus\">"
           ."<option value=\"\">-</option>"
           ."<option value=\"new\" >new</option>"
           ."<option value=\"open\" >open</option>"
           ."<option value=\"stalled\" >stalled</option>"
           ."<option value=\"resolved\" >resolved</option>"
           ."<option value=\"dead\" >dead</option>"
           ."</select>";

   $table = "q_specific_fields group by field_name";
   $field_names = GetArrayFromTable("field_name", $table);
   $num_fns = sizeof($field_names);
   global $nssf;
   $output .= "<hidden name=\"nssf\" value=\"$nssf\">";
   for($i = 0; $i < $nssf; $i++) {
      $output .= "</li><li>"
              .Font("Special Field ".($i+1))
              ."&nbsp;<input type=\"hidden\" name=\"field_list[]\" value=\"special_field_name".($i+1)."\">"
              ."<select name =\"special_field_name".($i+1)."\">"
              ."<option value=\"\">-</option>";
      for($j = 0; $j < $num_fns; $j++) {
         $output .= "<option value=\"$field_names[$j]\">$field_names[$j]</option>";
      }
      $output .= "</select>"
              ."<input type=\"hidden\" name=\"field_list[]\" value=\"special_field_op".($i+1)."\">"
              ."<select name =\"special_field_op".($i+1)."\">"
              ."<option value=\"LIKE\" selected>contains</option>"
              ."<option value=\"NOT LIKE\" >doesn't contain</option>"
              ."<option value=\"=\" >is</option>"
              ."<option value=\"!=\" >isn't</option></select>"
              ."<input type=\"hidden\" name=\"field_list[]\" value=\"special_field_val".($i+1)."\">"
              ."<input name=\"special_field_val".($i+1)."\" Size=20>";
   }
   $output .= "</ul></td></tr><tr align=\"right\"><td colspan=4>"
           ."<input type=\"submit\" name=\"Action\" value='Search'></td></tr>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: OrderingAndSorting
// Description:
//    Generates HTML to display the ordering and sorting search options.
// Parameters:
//    None.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OrderingAndSorting() {
   return "<ul><li>"
         .Font("Results per page")
         ."<input type=\"hidden\" name=\"field_list[]\" value=\"rows_per_page\">"
         ."<select name =\"rows_per_page\">"
         ."<option value=\"1000\">Unlimited</option>"
         ."<option value=\"10\">10</option>"
         ."<option value=\"25\">25</option>"
         ."<option value=\"50\" SELECTED>50</option>"
         ."<option value=\"100\">100</option>"
         ."</select></li><li>"
         .Font("Sort results by")
         ."<input type=\"hidden\" name=\"field_list[]\" value=\"tickets_sort_by\">"
         ."<select name=\"tickets_sort_by\">"
         ."<option value=\"ticket.id\">id</option>"
         ."<option value=\"ticket.current_status\">Status</option>"
         ."<option value=\"ticket.owner\">Owner</option>"
         ."<option value=\"ticket.date_created\">Created</option>"
         ."<option value=\"ticket.due\">Due</option>"
         ."<option value=\"ticket.starts\">Starts</option>"
         ."<option value=\"ticket.started\">Started</option>"
         ."<option value=\"last_comment.date_created\">Resolved</option>"
         ."<option value=\"ticket.last_contact\">LastUpdated</option>"
         ."<option value=\"ticket.priority\">Priority</option>"
         ."<option value=\"ticket.time_worked\">TimeWorked</option>"
         ."<option value=\"ticket.time_left\">TimeLeft</option>"
         ."</select><input type=\"hidden\" name=\"field_list[]\" value=\"tickets_sort_order\">"
         ."<select name=\"tickets_sort_order\">"
         ."<option value=\"DESC\">Descending</option>"
         ."<option value=\"ASC\">Ascending</option></select></ul>";
}
    
//-----------------------------------------------------------------------------
//
// Function: OutputSettings
// Description:
//    Generates HTML to display search output settings.
// Parameters:
//    None.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OutputSettings() {
   $output = "<table border=\"1\" bordercolor=\"#99CC99\" width=\"100%\"><tr>"
            ."<td align=\"center\" valign=\"middle\">Normal Search<br>"
            ."<input type=\"hidden\" name=\"field_list[]\" value=\"output_type\">"
            ."<input type=\"radio\" name=\"output_type\" value=\"normal\" checked></td>"
            ."<td align=\"center\" valign=\"middle\">Text Delimitated<br>"
            ."<input type=\"radio\" name=\"output_type\" value=\"text\"></td>"
            ."</tr><tr><td colspan=3 bgcolor=\"#cccccc\">"
            ."<strong>Text Delimitated Options:</strong></td></tr>"
            ."<tr><td>Delimitating Text</td><td colspan=2>"
            ."<input type=\"text\" name=\"delim\" value=\",\" size=5>"
            ."(&lt;tab&gt; will be replaced with the tab character)"
            ."</td></tr><td>Start with field labels row</td><td colspan=2>"
            ."<select name=\"labels\"><option value=1 selected>Yes</option>"
            ."<option value=0>No</option></select></td></tr><tr><td>"
            ."Fields to output:</td><td colspan=2>";
   global $db;
   $field_choice_array = GetFieldChoiceArray();
   global $nssf;
   for ($j = 1; $j <= $nssf; $j++) 
     array_push($field_choice_array, "sf$j.value");
   $fca_size = sizeof($field_choice_array);
   global $num_field_choices;
   if (!($num_field_choices)) $num_field_choices = 5;
   $output .= "<ol>";
   for($j = 0; $j < $num_field_choices; $j++) {
      $output .= "</li><li><select name=\"output_field[]\">";
      for($i = 0; $i < $fca_size; $i++) {
         $output .= "<option value=\"".$field_choice_array[$i]."\">";
         $output .= $field_choice_array[$i]."</option>";
      }
      $output .= "</select></li>";
   }
   $output .= "</ol></td></tr></table>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: GetFieldChoiceArray
// Description:
//    Generates Array of field choices for advanced search
// Parameters:
//    None.
// Return Values:
//    Array of strings
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFieldChoiceArray() {
   global $db;
   $field_choice_array = array();
   $displayable_tables = array(
			       "ticket" => "ticket",
			       "last_comment" => "comment",
			       "first_comment" => "comment"
			       );
   foreach($displayable_tables as $menu_value => $current_table) {
     $sql = "SHOW COLUMNS FROM $current_table";
     $result = mysql_query($sql) or die(mysql_error());
     while ($row = mysql_fetch_object($result)) {
       array_push($field_choice_array, $menu_value . '.' . $row->Field);
     }
   }
   return $field_choice_array;
}

?>