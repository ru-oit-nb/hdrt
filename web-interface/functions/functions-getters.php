<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-setters.php");
require_once("functions-widgets.php");
//
// Filename: functions-getters.php
// Description: Contains functions for getting information (DB).
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: GetArrayFromTable
// Description:      
//    Retrieves information from a table.
// Parameters:
//    string $field    database field to be returned
//    string $table    table to select information from
//    string $where    optional extensions to query
// Return Values:
//    Returns array of values for the field.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetArrayFromTable ($field, $table, $where = '') {
   global $db;
   $result = mysql_query("select $field from $table $where", $db);
   if ($result && mysql_num_rows($result) > 0) {
      $i = 0;
      while ($row = mysql_fetch_array($result)) {
         $field_array[$i] = $row[0];
         $i++;
      }
      return $field_array;
   }
   else return array();
}

//-----------------------------------------------------------------------------
//
// Function:  GetStaff
// Description:  
//    Returns a list of all staff netids
// Parameters:
//    None.
// Return Values:
//    Returns an array of netids.
// Remarks:
//    None.
//
//-----------------------------------------------------------------------------
function GetStaff() {
   return GetArrayFromTable('username', 'staff');
}

//-----------------------------------------------------------------------------
//
// Function: GetCountFromTable
// Description:
//    Counts the number of rows in a table.
// Parameters:
//    string $table	Table name to count rows.
// Return Values:
//    Number of rows.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetCountFromTable($table) {
   global $db;
   $query = "Select count(*) as num from $table";
   $result = mysql_query($query, $db);
   if ($table_row = mysql_fetch_object($result)) {
      return $table_row->num;
   }
   return 0;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetRowsFromTable
// Description:
//    Retrieves rows from a mysql table.
// Parameters:
//    string $table_name	Table Name.
// Return Values:
//    Mysql result.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetRowsFromTable($table_name) {
   global $db;
   $result = mysql_query("Select * From ".$table_name, $db);
   return $result;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetFirstRowFromQuery
// Description:
//    Gets the first row from a mysql query.
// Parameters:
//    string $query	Query to run on the database.
// Return Values:
//    Returns a mysql object of the first row in the query.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFirstRowFromQuery($query) {
   global $db;
   $result = mysql_query($query, $db);
   if ($result) {
      return mysql_fetch_object($result);
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetFieldFromQuery
// Description:
//    Retrieves the value of a specific field from the database.
// Parameters:
//    string $query	Query to run on the database.
//    string $field	Field to retrieve.
// Return Values:
//    Returns the value of the requested field.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFieldFromQuery($query, $field) {
   $row = GetFirstRowFromQuery($query);
   return $row->{$field};
}
    
//-----------------------------------------------------------------------------
//
// Function: GetFirstRowFromTable
// Description:
//    Retrieves the first row of a table.
// Parameters:
//    string $table_name	Name of the table to use.
// Return Values:
//    Returns a mysql object of the frist row of the table.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFirstRowFromTable($table_name) {
   return mysql_fetch_object(GetRowsFromTable($table_name));
}
    
//-----------------------------------------------------------------------------
//
// Function: GetArrayFromQuery
// Description:
//    Returns as array of the values of $field_name.
// Parameters:
//    string $query		Query to run on the database.
//    string $field_name	Field name to retrieve.
// Return Values:
//    Returns an array of objects.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetArrayFromQuery($query, $field_name) {
   global $db;
   $result = mysql_query($query, $db);
   $arr = array();
   $i = 0;
   while (($result) && ($row = mysql_fetch_object($result))) {
      $arr[$i] = $row-> {
         $field_name };
      $i++;
   }
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetQueue
// Description:
//    Retrieves a list of the queues available in the system.
// Parameters:
//    None.
// Return Values:
//    Returns an array of queue names.
// Remarks:
//    Redone by jfulton to exclude disabled queues on Dec. 20, 2004
//
//-----------------------------------------------------------------------------
function GetQueue() {
  $sql = "SELECT q_name FROM queue WHERE enabled > 0 ORDER BY q_name";
  $result = mysql_query($sql) or die(mysql_error());
  $arr = array();
  $i = 0;
  while ($row = mysql_fetch_object($result)) {
    $arr[$i++] = $row->q_name;
  }
  return $arr;
}

//-----------------------------------------------------------------------------
//
// Function: GetAllQueues
// Description:
//    Retrieves a list of all the queues available in the system.
// Parameters:
//    None.
// Return Values:
//    Returns an array of queue names.
// Remarks:
//    Added for admin page.
//
//-----------------------------------------------------------------------------
function GetAllQueues() {
  $sql = "SELECT q_name FROM queue";
  $result = mysql_query($sql) or die(mysql_error());
  $arr = array();
  $i = 0;
  while ($row = mysql_fetch_object($result)) {
    $arr[$i++] = $row->q_name;
  }
  return $arr;
}

//-----------------------------------------------------------------------------
//
// Function: GetQueueWatchers
// Description:
//    Retrieves the watchers for a queue.
// Parameters:
//    string $queue	Queue name.
// Return Values:
//    Returns an array of watcher usernames.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueueWatchers($queue) {
   return GetArrayFromTable("username", "watcher where q_name='$queue'");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetQueueSupervisors
// Description:
//    Retrieves the supervisors for a queue.
// Parameters:
//    string $queue	Queue name.
// Return Values:
//    Returns an array of supervisor usernames.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueueSupervisors($queue) {
   return GetArrayFromTable("username", "q_supervisor where q_name='$queue'");
}

//-----------------------------------------------------------------------------
//
// Function: GetQueueGreeting
// Description:
//    Retrieves the greeting for a queue.
// Parameters:
//    string $queue	Queue name.
// Return Values:
//    Returns an object with the row information.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueueGreeting($queue) {
   return GetFirstRowFromTable("queue where q_name='$queue'", "greeting");
}

//-----------------------------------------------------------------------------
//
// Function: GetTicketWatchers
// Description:
//    Retrieves the watcher for a ticket.
// Parameters:
//    int $id			Ticket ID.
//    string $type		Type of watcher.
//    string $with_delete_array	Delete if not "".
// Return Values:
//    Returns array of watchers.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetTicketWatchers($id, $type = "", $with_delete_query_array = "") {
   global $db;

   if ($type != "") {
      $type = " and type='$type'";
   }
   $query = "select * from ticketwatcher where ticket_id = '$id'".$type;
   $i = 0;
   $result = mysql_query($query, $db);

   if ($with_delete_query_array == "") {

      while ($table_row = mysql_fetch_object($result)) {
         $array[$i] = $table_row->email;
         $i++;
      }
      return $array;
   }
       
   while ($table_row = mysql_fetch_object($result)) {
      $arr[$i] = $table_row->email;
      $query = "Delete from ticketwatcher where ";
      $query .= "ticket_id = '$table_row->id' and type = '$table_row->type' ";
      $query .= " and email = '$table_row->email'";
      $delete_array[$i] = $query;
      $i++;
   }
   $return_array[0] = $arr;
   $return_array[1] = $delete_array;
   return $return_array;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetPseudoGroups
// Description:
//    Retrieves a list of pseudogroups.
// Parameters:
//    None.
// Return Values:
//    Array of pseudogroups.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetPseudoGroups() {
   return GetArrayFromTable("group_name", "groups where type Like 'pseudo' order by group_name");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetGroups
// Description:
//    Retrieves a list of groups.
// Parameters:
//    None.
// Return Values:
//    Array of groups.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetGroups() {
   return GetArrayFromTable("group_name", "groups where type IS NULL order by group_name");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetGroupPropertiesArray
// Description:
//    Gets the menu items for group properties.
// Parameters:
//    None.
// Return Values:
//    Array of properties.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetGroupPropertiesArray() {
   return array("Basics", "Members");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetPseudoGroupPropertiesArray
// Description:
//    Gets the menu items for pseudogroup properties.
// Parameters:
//    None.
// Return Values:
//    Array of properties.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetPseudogroupPropertiesArray() {
   return array ("Basics");
}

//-----------------------------------------------------------------------------
//
// Function: GetQueuePropertiesArray
// Description:
//    Gets the menu items for queue properties.
// Parameters:
//    None.
// Return Values:
//    Array of properties.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueuePropertiesArray() {
   global $SCHEDULING;
   $arr = array("Basics","Greetings","Watchers","Supervisors","Special Fields",
                "Scripts","Group Rights","User Rights","Allow Mail Comments");
   if ($SCHEDULING) {
      $arr[9] = "Scheduling Permissions";
   }
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetGlobalPropertiesArray
// Description:
//    Gets the menu items for global properties.
// Parameters:
//    None.
// Return Values:
//    Array of properties.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetGlobalPropertiesArray() {
   global $SCHEDULING;
   $arr = array("Options", "Scripts","Group Rights","User Rights","Modules");
   if ($SCHEDULING) {
      $arr[5] = "Scheduling Options";
      $arr[6] = "Scheduling Permissions";
      $arr[7] = "Buildings";
      $arr[8] = "User Building Permissions";
   }
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetWatchers
// Description:
//    Retrieves the watchers for a queue.
// Parameters:
//    string $q_name	Queue name.
// Return Values:
//    Array of watcher usernames.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetWatchers($q_name = "") {
   if ($q_name != "") {
      return GetArrayFromTable("username", "watcher WHERE q_name='$q_name'");
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetSupervisors
// Description:
//    Retrieves the supervisors for a queue.
// Parameters:
//    string $q_name	Queue name.
// Return Values:
//    Array of supervisor usernames.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetSupervisors($q_name = "") {
   if ($q_name != "") {
      return GetArrayFromTable("username", "q_supervisor WHERE q_name='$q_name'");
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetSpecialFields
// Description:
//    Retrieves special field names and types.
// Parameters:
//    string $q_name	Queue name.
//    string $not_queue	Queue to exclude if $q_name blank.
// Return Values:
//    Rows of special fields table.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetSpecialFields($q_name = "", $not_queue = "") {
   if ($q_name != "") {
      return GetRowsFromTable("q_specific_fields WHERE q_name='$q_name' order by date_create");//date_create");
   }

   else {

      if ($not_queue != "") {
         $limit_by = "where  q_name not like '$not_queue' order by date_create,q_name";
      }
      return GetRowsFromTable("q_specific_fields $limit_by");
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetRights
// Description:
//    Gets the possible rights that can be granted.
// Parameters:
//    None.
// Return Values:
//    Array of rights.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetRights() {
  $arr = array (//"AdminKeywordSelects",
                 "AdminQueue",
                 "AdminGroup",
                 "AdminUser",
                 "AdminGlobal",
                 "CommentOnTicket",
                 "CreateTicket",
                 "DeleteTicket",
		 "LocalPasswords",
                 "ModifyACL",
                 //"ModifyQueueWatchers",
                 //"ModifyScripts",
                 //"ModifyTemplate",
                 "ModifyTicket",
                 //"OwnTicket",
                 //"ReplyToTicket",
                 "SeeQueue",
                 // "ShowACL",
		 //  "ShowScripts",
		 //  "ShowTemplate",
		 //  "ShowTicket",
		 //  "ShowTicketComments",
		 //  "Watch",
                 "WatchAsAdminCc",
                 "AllRights");
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetStatus
// Description:
//    Gets the possible ticket statuses.
// Parameters:
//    None.
// Return Values:
//    Array of statuses.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetStatus() {
   return array("New", "Open", "Stalled", "Resolved");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetRightsOf
// Description:
//    Gets the rights of the specified staff member or group.
// Parameters:
//    string $username		Username or group to get info for.
//    string $queue		Queue name.
//    string $group_or_user	Determines if the rights are for user or group.
// Return Values:
//    Returns an array of rights.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetRightsOf($username = "", $queue = "", $group_or_user = "staffright") {
   if ($group_or_user == "staffright") {
      $rights = "staff_right";
      $name = "username";
   }

   else {
      $rights = "group_right";
      $name = "group_name";
   }
   $queue = str_replace("_", " ", $queue);
   return GetArrayFromTable($rights, "$group_or_user where $name Like '$username' and queue='$queue'");
}
    
//-----------------------------------------------------------------------------
//
// Function: GetStaffEmail
// Description:
//    Gets the email of a staff member.
// Parameters:
//    string $username	Username of staff member.
// Return Values:
//    Returns an email address.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetStaffEmail($username = "") {
   $table_name = "staff where username like '$username'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->email != "") {
      return $row->email;
   }

   if ($username != "") {
      return $username. GetStaffEmailDomain();
   }
   return "";
}
    
//-----------------------------------------------------------------------------
//
// Function: IsEmailSet
// Description:
//    Checks if a staff member has an email address set.
// Parameters:
//    string $username	Username to check.
// Return Values:
//    Boolean set.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function IsEmailSet($username) {
   if ($username == "") return 1;
   $table_name = "staff where username like '$username'";
   $row = GetFirstRowFromTable($table_name);
   if ($row->email != "") return 1;
   return 0;
}
    
    
    
//-----------------------------------------------------------------------------
//
// Function: GetStaffEmailArray
// Description:
//    Retrieves multiple staff emails.
// Parameters:
//    array $username_array	Array of usernames to get emails for.
// Return Values:
//    Returns an array of emails.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetStaffEmailArray($username_array) {
   $i = 0;
   if ($username_array) {
      foreach ($username_array as $username) {
         $email_array[$i] = GetStaffEmail($username);
         $i++;
      }
   }
   return $email_array;
}

//-----------------------------------------------------------------------------
//
// Function: GetStaffSignature
// Description:
//    Gets the signature of a staff member.
// Parameters:
//    string $username	Username to retrieve the signature of.
// Return Values:
//    Signature of staff member.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetStaffSignature($username = "") {
      $table_name = "staffprefs where username like '$username'";
      $row = GetFirstRowFromTable($table_name);
      return $row->signature;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetTicket
// Description:
//    Gets a ticket's row from the database.
// Parameters:
//    int $id	Ticket ID
// Return Values:
//    Object containing ticket info.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetTicket($id) {
   global $db;
   $query = "select * from ticket where id=$id";
   $result = mysql_query($query, $db);
   if ($result) $table_row = mysql_fetch_object($result);
   return $table_row;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetUser
// Description:
//    Gets a user's row from the database.
// Parameters:
//    int $id	User ID
// Return Values:
//    Object containing user info.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetUser($id) {
   global $db;
   $query = "select * from user where id=$id";
   $result = mysql_query($query, $db);
   if ($result) $table_row = mysql_fetch_object($result);
   return $table_row;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: GetQueueRow
// Description:
//    Gets a queue's row from the database
// Parameters:
//    string $q_name	Queue name.
// Return Values:
//    Object containing queue info.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueueRow($q_name) {
   global $db;
   $query = "select * from queue where q_name='$q_name'";
   $result = mysql_query($query, $db);
   if ($result) $table_row = mysql_fetch_object($result);
   return $table_row;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetHighestPriorityTicketsFor
// Description:
//    Retrieves the tickets with highest priority where $fieldname=$fieldvalue.
// Parameters:
//    string $fieldname		Name of field.
//    string $fieldvalue	Value of field.
// Return Values:
//    Mysql result.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetHighestPriorityTicketsFor($fieldname, $fieldvalue) {
   global $db;
   $query = "SELECT owner, ticket.id, first_comment.subject, ";
   $query .= "ticket.current_status, queue ";
   $query .= "from ticket, comment as first_comment, ";
   $query .= "comment as last_comment, queue ";
   $query .= "where first_comment.id = ticket.first_comment_id ";
   $query .= "and last_comment.id = ticket.last_comment_id and ";
   $query .= "ticket.queue = queue.q_name and ";
   $query .= "$fieldname=\"$fieldvalue\" and ";
   $query .= "ticket.current_status not like \"%resolved%\" ";
   $query .= "order by last_comment.priority desc, ticket.id desc";
   $result = mysql_query($query, $db);
   return $result;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetHighestPriorityTicketsWatcher
// Description:
//    Retrieves the tickets with highest priority where $fieldname=$fieldvalue.
// Parameters:
//    string $fieldname		Name of field.
//    string $fieldvalue	Value of field.
//    string $type		Type of watcher.
// Return Values:
//    Mysql result.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetHighestPriorityTicketsWatcher($fieldname, $fieldvalue, $type) {
   global $db;
   $query = "SELECT owner, ticket.id, first_comment.subject, ";
   $query .= "ticket.current_status, queue ";
   $query .= "from ticket,ticketwatcher, comment as first_comment, ";
   $query .= "comment as last_comment, queue ";
   $query .= "where first_comment.id = ticket.first_comment_id ";
   $query .= "and last_comment.id = ticket.last_comment_id and ";
   $query .= "ticket.queue = queue.q_name and ";
   $query .= "$fieldname=\"$fieldvalue\" and ";
   $query .= "ticket.current_status not like \"%resolved%\" and ";
   $query .= "ticketwatcher.ticket_id=ticket.id and ticketwatcher.type=\"$type\" ";
   $query .= "order by last_comment.priority desc";
   $result = mysql_query($query, $db);
   return $result;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetOriginalSubjectOfTicket
// Description:
//    Retrieves the subject of the first comment of a ticket.
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Returns the subject.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetOriginalSubjectOfTicket($ticketId) {
   global $db;
   $id = GetFirstCommentOfTicket($ticketId);
   $query = "select subject from comment where id = '$id'";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   return $tablerow->subject;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetFirstCommentOfTicket
// Description:
//    Gets the lowest comment id associated with a ticket
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Returns the comment's ID.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFirstCommentOfTicket($ticketId) {
   global $db;
   $query = "select Min(comment.id) as id from ticket, comment where ticket.id = ticket_id and ticket_id='$ticketId' group by ticket_id";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   $id = $tablerow->id;
   return $id;
}

//-----------------------------------------------------------------------------
//
// Function:  GetFirstCommentRowOfTicket
// Description:
//    Gets the first comment of a ticket as an object.
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Comment as object.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetFirstCommentRowOfTicket($ticketId) {
   global $db;
   $first_comment_id = GetFirstCommentOfTicket($ticketId);
   $query = "select * from comment where id = '$first_comment_id'";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   return $tablerow;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetLastCommentOfTicket
// Description:
//    Gets the highest comment id associated with a ticket
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Returns the comment's ID.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetLastCommentOfTicket($ticketId) {
   global $db;
   $query = "select Max(comment.id) as id from ticket, comment where ticket.id = ticket_id and ticket_id='$ticketId' group by ticket_id";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   $id = $tablerow->id;
   return $id;
}
    
//-----------------------------------------------------------------------------
//
// Function:  GetLastCommentRowOfTicket
// Description:
//    Gets the last comment of a ticket as an object.
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Comment as object.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetLastCommentRowOfTicket($ticketId) {
   global $db;
   $last_comment_id = GetLastCommentOfTicket($ticketId);
   $query = "select * from comment where id = '$last_comment_id'";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   return $tablerow;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetCurrentStatusOfTicket
// Description:
//    Gets the current status of a ticket.
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Current status as string.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetCurrentStatusOfTicket($ticketId) {
   global $db;
   $query = "select current_status from ticket where id= '$ticketId'";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   return $tablerow->current_status;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetCurrentPriorityOfTicket
// Description:
//    Gets the current priority of a ticket.
// Parameters:
//    int $ticketId	Ticket ID
// Return Values:
//    Current priority as string.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetCurrentPriorityOfTicket($ticketId) {
   global $db;
   $id = GetLastCommentOfTicket($ticketId);
   $query = "select priority from comment where id = '$id'";
   $result = mysql_query($query, $db);
   $tablerow = mysql_fetch_object($result);
   return $tablerow->priority;
}
    
//-----------------------------------------------------------------------------
//
// Function: DoesGroupHaveRight
// Description:
//    Checks whether or not a group has a specific right.
// Parameters:
//    string $group	Group name.
//    string $right	Right to check for.
// Return Values:
//    0 if no, >0 if yes.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DoesGroupHaveRight($group, $right) {
   return GetCountFromTable("groupright where group_name='$group' and group_right='$right'");
}
    
    
//-----------------------------------------------------------------------------
//
// Function: WhatPseudoGroupsIsUserIn
// Description:
//    Retrieves the pseudo groups a staff member is in.
// Parameters:
//    string $user	Username.
//    int $ticket_id	Ticket ID.
// Return Values:
//    Returns an array of pseudogroups.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function WhatPseudoGroupsIsUserIn($user, $ticket_id) {
   $email = GetStaffEmail($user);

   if (GetCountFromTable("ticket where id='$ticket_id' and owner='$user'")) {
      $arr[0] = "Owner";
   }
   return array_merge($arr, GetArrayFromTable("type", "ticketwatcher where ticket_id='$ticket_id' and email='$email'"));
}
    
//-----------------------------------------------------------------------------
//
// Function: DoesUserHaveRight
// Description:
//    Checks whether a user has a specific right.
// Parameters:
//    string $username	Username.
//    string $right	Right to check for.
//    string $queue	Queue to check in.
//    int $ticket_id	Ticket to check for.
// Return Values:
//    1 if yes, 0 if no.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DoesUserHaveRight($username, $right, $queue = "", $ticket_id = "") {
   global $db;
   if ($queue == "Create_a_new_queue") {
      $queue = "global";
   }
   if ($ticket_id != "") {
      $pseudo = WhatPseudoGroupsIsUserIn($username, $ticket_id);
        
      if ($pseudo) {
         foreach($pseudo as $group) {
            if (DoesGroupHaveRight($group, $right)) return 1;
         }
      }
   }
   if (($queue == "") && ($ticket_id != "")) {
      $ticket = GetFirstRowFromTable("ticket where id='$ticket_id'");
      $queue = $ticket->queue;
   }
   if (($queue == "") && ($ticket_id == "")) $queue = "global";
   $queue = str_replace("_", " ", $queue);
   $query = "Select username from staffright where username = '$username'";
   $query = $query." and (staff_right = '$right' or staff_right='AllRights') and ";
   $query = $query." (queue like 'global' or queue like '$queue')";
   $result = mysql_query($query, $db);
   if ($result) {
      if (mysql_num_rows($result) > 0) return 1;
   }
   $groups = GetArrayFromTable("group_name", "ingroup where username='$username'");
     
   if ($groups != "") {
      foreach($groups as $group_name) {
         $group_name = str_replace("_", " ", $group_name);
         $query = "Select group_name from groupright where group_name = '$group_name'";
         $query = $query." and (group_right = '$right' or group_right='AllRights') and ";
         $query = $query." (queue like 'global' or queue like '$queue')";
         $result = mysql_query($query, $db);
         if (mysql_num_rows($result) > 0)  return 1;
      }
   }
   return 0;
}
    
//-----------------------------------------------------------------------------
//
// Function: DoesUserHaveAnyRelatedTickets
// Description:
//    Checks if a user has any other tickets in the system.
// Parameters:
//    int $user_id	User ID
// Return Values:
//    0 if no, >0 if yes.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DoesUserHaveAnyRelatedTickets($user_id = "") {
   global $db;
   $query = "Select user_id from ticket where user_id = '$user_id' limit 1";
   $result = mysql_query($query, $db);
   return mysql_num_rows($result);
}
    
    
//-----------------------------------------------------------------------------
//
// Function: UserExists
// Description:
//    Checks if a user exists.
// Parameters:
//    int $user_id	User ID.
// Return Values:
//    <=0 if no, >0 if yes.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UserExists($user_id = "") {
   global $db;
   $query = "Select id from user where id = '$user_id' limit 1";
   $result = mysql_query($query, $db);
   return mysql_num_rows($result);
}
    
//-----------------------------------------------------------------------------
//
// Function: TicketExists
// Description:
//    Check if a ticket exists.
// Parameters:
//    int $id	Ticket ID.
// Return Values:
//    <=0 if no, >0 if yes.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketExists($id = "") {
   global $db;
   $query = "Select * from ticket where id = '$id' limit 1";
   $result = mysql_query($query, $db);
   return mysql_num_rows($result);
}
    
//-----------------------------------------------------------------------------
//
// Function: GetScriptActions
// Description:
//    Gets the actions a script can perform.
// Parameters:
//    string $provision	Script event.
//    string $queue	Queue name.
// Return Values:
//    Returns an array of actions.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetScriptActions($provision, $queue) {
  $actions = array();
  global $db;
  $sql = "SELECT action FROM script WHERE (queue = '$queue' OR queue = 'global') ";
  if ($provision != "OnTransaction") {
    $sql .= "AND (provision = '$provision' OR provision='OnTransaction') ";
  }
  $sql .= "GROUP BY action";
  $result = mysql_query($sql, $db) or die(mysql_error());
  while ($row = mysql_fetch_object($result)) {
    array_push($actions, $row->action);
  }
  return $actions;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetActionArray
// Description:
//    Gets the actions a script can perform.
// Parameters:
//    None.
// Return Values:
//    Array of actions.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetActionArray() {
   $arr = array("NotifyRequesters",
                "NotifyOwner",
                "NotifyAdminCc",
                "NotifyCc",
                "NotifyTicketWatchers",
                "NotifyQueueWatchers",
                "NotifyAllWatchers",
                "NotifyAllRelated");
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetProvisionArray
// Description:
//    Gets the provisions a script can be executed on.
// Parameters:
//    None.
// Return Values:
//    Array of provisions.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetProvisionArray() {
   $arr = array("OnCreate",
                "OnTransaction",
                "OnCorrespond",
                "OnComment",
                "OnStatus",
                "OnResolve",
                "OnOwnerChange",
                "OnQueueEnter",
                "OnQueueExit");
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetTemplateArray
// Description:
//    Gets the list of possible templates.
// Parameters:
//    None.
// Return Values:
//    Array of templates.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetTemplateArray() {
   $arr = array ("None");
   return $arr;
}
    
//-----------------------------------------------------------------------------
//
// Function: UserGetEntries
// Description:
//    Searches the user table for users like the search terms.
// Parameters:
//    string $user	List of terms to search for.
//    string $id	Id to search for.
// Return Values:
//    Found objects.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UserGetEntries($user = "", $id = "") {
   $user_split_array = explode(",", $user);

   if (sizeof($user_split_array) > 1) {
      $user = trim($user_split_array[0]);
      $user_first_name = trim($user_split_array[1]);
   }
   $where = "((id like '%$user%' or id like '%$user%' or uid like '%$user%' ";
   $where .= "or name like '%$user%' or email like '%$user%') or id like '$id' ";
   $where .= ") and ((id=duplicate_of) or (iid like '') or ";
   $where .= "(duplicate_of is null))";
   $table = "user where $where";
   return GetRowsFromTable($table);
}
    
//-----------------------------------------------------------------------------
//
// Function: GetTimeWorked
// Description:
//    Gets the total time worked on a ticket.
// Parameters:
//    int $ticket_id	Ticket ID.
// Return Values:
//    Time worked as int.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetTimeWorked($ticket_id) {
   global $db;
   $query = "select sum(time_worked) as total from comment where ticket_id = '$ticket_id'";
   $result = mysql_query($query, $db);
   $row = mysql_fetch_object($result);
   $total = $row->total;
   return $total;
}
    
//-----------------------------------------------------------------------------
//
// Function: AdjustTimeWorked
// Description:
//    Updates the time worked for a ticket.
// Parameters:
//    int $ticket_id	Ticket ID.
//    int $time		Time worked.
// Return Values:
//    Adjustment in time worked.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function AdjustTimeWorked($ticket_id, $time = "") {
   $true_time_worked = GetTimeWorked($ticket_id);
   if ($time == "") return 0;
   else {
      $adjust = $time-GetTimeWorked($ticket_id);
      UpdateTicket($ticket_id, "time_worked='$time'");
      return $adjust;
   }
}

//-----------------------------------------------------------------------------
//
// Function: GetValueFromTable
// Description:  Retrieves a specific value of a specific field in a table
//
// Parameters:
//    string $field_name  field to return information from
//    string $table       table to select from
//    string $where       option extensions to query
//
// Return Values:
//    Returns value for the field.
//
// Remarks:
//    None
//
//-----------------------------------------------------------------------------
function GetValueFromTable($field_name, $table, $where) {
   global $db;
   $result = mysql_query("select $field_name from $table $where limit 1");
   if ($result && mysql_num_rows($result) > 0) {
      $field = mysql_fetch_object($result);
      return $field->$field_name;
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: GetCommentEmail
// Description:
//    Gets the email address that comments are sent to.
// Parameters:
//    None.
// Return Values:
//    Returns an email address.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetCommentEmail() {
   $table_name = "system_variables WHERE var='comment_email_address'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->val != "") {
      return $row->val;
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: GetHelpEmail
// Description:
//    Gets the email address that help questions are sent to.
// Parameters:
//    None.
// Return Values:
//    Returns an email address.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetHelpEmail() {
   $table_name = "system_variables WHERE var='help_email_address'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->val != "") {
      return $row->val;
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: GetStaffEmailDomain
// Description:
//    Gets the email domain that all staff are located on
// Parameters:
//    None.
// Return Values:
//    Returns an email address domain.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetStaffEmailDomain() {
   $table_name = "system_variables WHERE var='staff_email_domain'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->val != "") {
      return $row->val;
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: GetSurveyURL
// Description:
//    Gets the URL for the help desk survey
// Parameters:
//    None.
// Return Values:
//    Returns a URL in string form.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetSurveyURL() {
   $table_name = "system_variables WHERE var='survey_url'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->val != "") {
      return $row->val;
   }
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: GetEmailInformationURL
// Description:
//    Gets the URL for information about html and plain text emails
// Parameters:
//    None.
// Return Values:
//    Returns a URL in string form.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetEmailInformationURL() {
   $table_name = "system_variables WHERE var='email_information_url'";
   $row = GetFirstRowFromTable($table_name);

   if ($row->val != "") {
      return $row->val;
   }
   return "";
}

?>