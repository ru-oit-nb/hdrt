<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions/functions-form-objects.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-header.php");
require_once("functions/functions-index.php");
//
// Filename: functions-new-ticket.php
// Description: 
//    Contains functions for creating a new ticket 
//    and checking input prior to creation of a new ticket.
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: NewTicketForm
// Description:
//    Generates HTML for creating a new ticket.
// Parameters:
//    string $queue		Queue to create in.
//    string $requester		Requester of new ticket.
//    string $status		Status to create with.
//    string $owner		Owner of ticket.
//    string $cc		CC of ticket.
//    string $admin_cc		Admin CC of ticket.
//    string $subject		Subject of ticket.
//    string $attach		Not Used.
//    string $content		Initial comment of ticket.
//    string $email_user	Send email to user?
// Return Values:
//    Returns HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function NewTicketForm($queue = "", $requester = "", $status = "", $owner = "",
                         $cc = "", $admin_cc = "", $subject = "", $attach = "",
                         $content = "", $email_user = "") {
       
   global $user_id, $username, $allqueues, $limitqueues;
   $clean_queue = str_replace("_", " ", $queue);
   $disabled = "disabled";
   $output = "<table border=0>".Input("", "normal", "form_type", "hidden", "", "");

   if (empty($queue) || !empty($allqueues) || !empty($limitqueues)) {
      $output .= OpenTable("Queue")."<select name=\"queue\">";
      if (!empty($allqueues)) {
         $output .= CreateSelectOptions(GetQueue(), "", $queue)."</select>";
         $status = "New";
         $output .= '<input type="submit" name="limitqueues" value="Show Limited Queues">';
      }
      else {
         $output .= CreateSelectOptions(GetQueuesUserCanSee($username), "", $queue)."</select>";
         $output .= '<input type="submit" name="allqueues" value="Show All Queues">';
      }
   }	
   if(!empty($queue) && empty($allqueues) && empty($limitqueues)) $output .= OpenTable("Queue").FontBold($queue);

   $output .= CloseTable().OpenTable("ID of user").FontBold($user_id)
           .CloseTable().Input("ID of user", $user_id, "user_id", "hidden", "")
           .OpenTable("Requester")
           ."<input type=\"text\" size=30 name=\"requester\" value=\"$requester\">"
           .CloseTable().OpenTable("Status")."<select name=\"status\">";
   if (empty($status)) $status = "New";
   $output .= CreateSelectOptions(GetStatus(), "", $status)."</select>"
           .CloseTable().OpenTable("Owner")
           ."<select name=\"owner\"><option value=\"\">-</option>"
           .CreateSelectOptions(GetStaff(), "", $owner)."</select>"
           .CloseTable().OpenTable("Cc")
           ."<input type=\"text\" size=30 name=\"cc\" value=\"$cc\">"
           .Font("<i>(use a space and a comma to add others)</i>")
           .CloseTable().OpenTable("Admin Cc")
           ."<input type=\"text\" size=30 name=\"admin_cc\" value=\"$admin_cc\">"
           .Font("<i>(use a space and a comma to add others)</i>")
           .CloseTable().OpenTable("Subject")
           ."<input type=\"text\" size=30 name=\"subject\" value=\"$subject\">"
           .CloseTable().OpenTable("Describe <br>the Issue")
           ."<textarea cols=72 rows=15 wrap=\"hard\" name=\"content\">$content</textarea>"
           .CloseTable().OpenTable("Send Preset Email Message To User")
           ."<input type=\"checkbox\" name=\"email_user\" value=1 checked>"
           .CloseTable()."</table>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: NewTicketSpecialFieldsForm
// Description:
//    Generate HTML to allow input of special fields on ticket creation.
// Parameters:
//    string $queue			Queue of ticket.
//    string $requester			Requester of ticket.
//    string $status			Ticket status.
//    string $owner			Owner of ticket.
//    string $cc			CC of ticket.
//    string $admin_cc			Admin CC of ticket.
//    string $subject			Subject of ticket.
//    string $attach			Not Used.
//    string $content			Initial comment for ticket.
//    string $special_fields_count	Number of special fields.
//    string $email_user		Email user?
// Return Values:
//
// Remarks:
//
//-----------------------------------------------------------------------------
function NewTicketSpecialFieldsForm($queue = "", $requester = "", $status = "",
                                    $owner = "", $cc = "", $admin_cc = "", 
                                    $subject = "", $attach = "", $content = "",
                                    $special_fields_count = "", $email_user = "") {
          
   global $user_id;
   $clean_queue = str_replace("_", " ", $queue);
   $disabled = "disabled";
   $output = "<table border=0>";

   if ($special_fields_count > 0) {
      $output .= Input("", $email_user, "email_user", "hidden", "", "")
              .Input("", "special", "form_type", "hidden", "", "")
              .Input("", $queue, "queue", "hidden", "", "")
              .Input("", $user_id, "user_id", "hidden", "", "")
              .Input("", $requester, "requester", "hidden", "", "")
              .Input("", $status, "status", "hidden", "", "")
              .Input("", $owner, "owner", "hidden", "", "")
              .Input("", $cc, "cc", "hidden", "", "")
              .Input("", $admin_cc, "admin_cc", "hidden", "", "")
              .InputSpecialFields($queue)
              .OpenTable("Subject")
              ."<input type=\"text\" size=30 name=\"subject\" value=\"$subject\">"
              .CloseTable().OpenTable("Describe <br>the Issue")
              ."<textarea cols=72 rows=15 wrap=\"hard\" name=\"content\">$content</textarea>"
              .CloseTable();
   }

   else {
      $output .= "<b>Ticket about to be created by $requester</b>.<br>"
            .Input("", $queue, "queue", "hidden", "", "")
            .Input("", $user_id, "user_id", "hidden", "", "")
            .Input("", $requester, "requester", "hidden", "", "")
            .Input("", $status, "status", "hidden", "", "")
            .Input("", $owner, "owner", "hidden", "", "")
            .Input("", $cc, "cc", "hidden", "", "")
            .Input("", $admin_cc, "admin_cc", "hidden", "", "")
            .Input("", $subject, "subject", "hidden", "", "")
            .Input("", $content, "content", "hidden", "", "");
   }
   $output .= "</table>";
   return $output;
}
       
//-----------------------------------------------------------------------------
//
// Function: TicketFormBasics
// Description:
//    Allows editing of ticket "basics".
// Parameters:
//    string $priority		Current priority of ticket.
//    string $final_priority	Current final priority of ticket.
//    string $time_worked	Current time worked of ticket.
//    string $time_left		Current time left of ticket.
// Return Values:
//    Returns HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketFormBasics($priority = "", $final_priority = "",
                            $time_worked = "", $time_left = "") {
   return "<table border=\"0\">".OpenTable("Priority")
          ."<input type=\"text\" name=\"priority\" size=6 value=\"$priority\">"
          .CloseTable().OpenTable("Final Priority")
          ."<input type=\"text\" name=\"final_priority\" size=6 value=\"$final_priority\">"
          .CloseTable().OpenTable("Time Worked")
          ."<input type=\"text\" name=\"time_worked\" size=6 value=\"$time_worked\">"
          .CloseTable().OpenTable("Time Left")
          ."<input type=\"text\" name=\"time_left\" size=6 value=\"$time_left\">"
          .CloseTable()."</table>";
}

//-----------------------------------------------------------------------------
//
// Function: TicketFormDates
// Description:
//    Allows editing of ticket dates.
// Parameters:
//    string $starts	Current start date.
//    string $due	Current due date.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketFormDates($starts = "", $due = "") {
   return "<table border=0>".OpenTable("Starts")
          ."<input type=\"text\" name=\"starts\" size=10 value=\"$starts\">"
          .DateWarning().CloseTable().OpenTable("Due")
          ."<input type=\"text\" name=\"due\" size=10 value=\"$due\">"
          .DateWarning().CloseTable()."</table>";
}

//-----------------------------------------------------------------------------
//
// Function: TicketFormRelationships
// Description:
//    Allows editing of ticket relationships.
// Parameters:
//    string $depends_on	Current depends on.
//    string $depended_on_by	Current depended on by.
//    string $parents		Current parents.
//    string $children		Current children.
//    string $refers_to		Current refers to.
//    string $refered_to_by	Current refered to by.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function TicketFormRelationships($depends_on = "", $depended_on_by = "",
                                   $parents = "", $children = "",
                                   $refers_to = "", $refered_to_by = "") {
   return "<table border=0>".OpenTable("Depends on")
          ."<input type=\"text\" name=\"depends_on\" size=10 value=\"$depends_on\">"
          .CloseTable().OpenTable("Depended on by")
          ."<input type=\"text\" name=\"depended_on_by\" size=10 value=\"$depended_on_by\">"
          .CloseTable().OpenTable("Parents")
          ."<input type=\"text\" name=\"parents\" size=10 value=\"$parents\">"
          .CloseTable().OpenTable("Children")
          ."<input type=\"text\" name=\"children\" size=10 value=\"$children\">"
          .CloseTable().OpenTable("Refers to")
          ."<input type=\"text\" name=\"refers_to\" size=10 value=\"$refers_to\">"
          .CloseTable().OpenTable("Refered to by")
          ."<input type=\"text\" name=\"refered_to_by\" size=10 value=\"$refered_to_by\">"
          .CloseTable()."</table>";
}

//-----------------------------------------------------------------------------
//
// Function: NewTicketShowForm
// Description:
//    Generates the HTML to display to create a new ticket.
// Parameters:
//    string $username			Username.
//    string $id			ID of user.
//    string $button			Label of additional button, if any.
//    string $queue			Queue of ticket.
//    string $requester			Requestor of ticket.
//    string $status			Status of ticket.
//    string $owner			Owner of ticket.
//    string $cc			CC of ticket.
//    string $admin_cc			AdminCC of ticket.
//    string $subject			Subject of first comment.
//    string $attach			Not used.
//    string $content			Body of first coment.
//    string $priority			Priority of ticket.
//    string $final_priority		Final Priority of ticket.
//    string $time_worked		Time worked on ticket.
//    string $time_left			Time remaining on ticket.
//    string $starts			Date ticket starts.
//    string $due			Date ticket due.
//    string $depends_on		Tickets ticket depends on.
//    string $depended_on_by		Tickets ticket depended on by.
//    string $parents			Parents of ticket.
//    string $children			Children of ticket.
//    string $refers_to			Tickets ticket refers to.
//    string $refered_to_by		Tickets ticket is refered to by.
//    string $special_fields_count	Number of special fields for $queue.
//    string $email_user		Email user?
//    string $form_type			Type of form to show.
// Return Values:
//    HTML to output.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function NewTicketShowForm($username = "", $id = "",
                              $button = "", $queue = "", $requester = "",
                              $status = "", $owner = "", $cc = "", $admin_cc = "",
                              $subject = "", $attach = "", $content = "",
                              $priority = "", $final_priority = "",
                              $time_worked = "", $time_left = "", $starts = "",
                              $due = "", $depends_on = "", $depended_on_by = "",
                              $parents = "", $children = "", $refers_to = "",
                              $refered_to_by = "", $special_fields_count = "",
                              $email_user = "", $form_type = "") {
   global $self,$detail_state,$user_id;
   $queue = str_replace(" ", "_", $queue);
   // $output = "<form action=\"$self?queue=$queue&id=$user_id\" method=\"post\">"
   if ($special_fields_count > 0) {
     $id = " id='special_fields' ";
   }

   $output = "\n\n<form $id action=\"$self?id=$user_id\" method=\"post\">\n";
   $output .= "\n<table border=0 width=\"100%\"><tr valign=\"top\"><td width=\"100%\">\n"
     .OpenColorTable("green", "Create a new ticket", "100%")."\n";

   $requester = GetStaffEmail($username);
   if (empty($requester)) $requester = $username;

   if (empty($button)) {
      $output .= NewTicketForm($queue, $requester, $status,$owner, $cc, $admin_cc,
                      $subject, $attach, $content, $email_user);
   }

   elseif($button == "Show-Details" or $button == "Hide-Details") {
      $errmsg = CheckInputNewTicket($username, $status, $subject, $content,
                                       $starts, $due, $requester, $cc, 
                                       $admin_cc, $user_id);
      if (!empty($errmsg)) {
         if (empty($subject) or empty($content)) {
            $output .= NewTicketForm($queue, $requester, $status, $owner,
                                     $cc, $admin_cc, $subject,
                                     $attach, $content, $email_user);
         } 

         elseif(!empty($subject) and !empty($content) and $form_type == "normal" 
                and ($button == "Show-Details" or $button == "Hide-Details")) {
            $output .= NewTicketForm($queue, $requester, $status,
                                       $owner, $cc, $admin_cc, $subject,
                                       $attach, $content, $email_user);
         } 

         else {
               $output .= NewTicketSpecialFieldsForm($queue, $requester,
                                                     $status, $owner, $cc, 
                                                     $admin_cc, $subject,
                                                     $attach, $content, 
                                                     $special_fields_count,
                                                     $email_user);
         }
      } 

      elseif ($form_type == "special" and empty($errmsg)) {
         $output .= NewTicketSpecialFieldsForm($queue, $requester, $status,
                                               $owner, $cc, $admin_cc, $subject,
                                               $attach, $content,
                                               $special_fields_count,
                                               $email_user);
      }

      else {
         $output .= NewTicketForm($queue, $requester, $status, $owner, $cc,
                                  $admin_cc, $subject, $attach, $content,
                                  $email_user);
      }
   }
       
   elseif($button == "Continue") {
      $errmsg = CheckInputNewTicket($username, $status, $subject, $content,
                                    $starts, $due, $requester,
                                    $cc, $admin_cc, $user_id);
          
      if (!empty($errmsg)) {
         if (empty($subject) or empty($content)) {
            $output .= NewTicketForm($queue, $requester, $status,
                                     $owner, $cc, $admin_cc, $subject,
                                     $attach, $content, $email_user);
         } 

         else {
            $output .= NewTicketSpecialFieldsForm($queue, $requester, $status,
                                                  $owner, $cc, $admin_cc, 
                                                  $subject, $attach, $content, 
                                                  $special_fields_count,
                                                  $email_user);
         }
      }

      else {
         $output .= NewTicketSpecialFieldsForm($queue, $requester, $status,
                                               $owner, $cc, $admin_cc, $subject,
                                               $attach, $content, 
                                               $special_fields_count, $email_user);
      }
   }

   elseif($button == "Create Ticket") {
      $errmsg = CheckInputNewTicket($username, $status, $subject, $content,
                                    $starts, $due, $requester, $cc, 
                                    $admin_cc, $user_id);
          
      if (!empty($errmsg)) {
         $output .= NewTicketSpecialFieldsForm($queue, $requester, $status,
                                               $owner, $cc, $admin_cc, 
                                               $subject, $attach, $content, 
                                               $special_fields_count,
                                               $email_user);
      }
   }
   $output .= CloseColorTable()."</td></tr></table>"
           ."<input type=\"hidden\" name=\"detail_state\" value=\"$detail_state\">";

   if ($detail_state == "Show-Details") {
      $output .= '<table border="0" width="100%"><tr valign="top"><td width="50%">'
              .OpenColorTable("yellow", "The Basics", "100%")
              .TicketFormBasics($priority, $final_priority,$time_worked, $time_left)
              .CloseColorTable()."</td><td>"
              .OpenColorTable("blue", "Dates", "100%")
              .TicketFormDates($starts, $due)
              .CloseColorTable()
              .'</td></tr><tr valign="top"></tr></table>';
   }

   else {
      $output .= "<input type=\"hidden\" name=\"priority\" value=\"$priority\">"
              ."<input type=\"hidden\" name=\"final_priority\" value=\"$final_priority\">"
              ."<input type=\"hidden\" name=\"time_worked\" value=\"$time_worked\">"
              ."<input type=\"hidden\" name=\"time_left\" value=\"$time_left\">" 
              ."<input type=\"hidden\" name=\"starts\" value=\"$starts\">"
              ."<input type=\"hidden\" name=\"due\" value=\"$due\">"
              ."<input type=\"hidden\" name=\"depends_on\" value=\"$depends_on\">"
              ."<input type=\"hidden\" name=\"depended_on_by\" value=\"$depended_on_by\">"
              ."<input type=\"hidden\" name=\"parents\" value=\"$parents\">"
              ."<input type=\"hidden\" name=\"children\" value=\"$children\">"
              ."<input type=\"hidden\" name=\"refers_to\" value=\"$refers_to\">"
              ."<input type=\"hidden\" name=\"refered_to_by\" value=\"$refered_to_by\">";
   }
     
   $output .= "<center><table border=0 width=\"50%\"><tr valign=\"top\">"
           ."<td width=\"50%\" align=\"right\">"
           .OpenColorTable("red", "Control", "100%");
       
   if ($detail_state == "Hide-Details") {
      $output .= '<input align="left" type="submit" name="button" value="Show-Details">';
   } 

   elseif ($detail_state == "Show-Details") {
      $output .= '<input align="left" type="submit" name="button" value="Hide-Details">';
   }
   SetNoBack("CreateTicket", "reset");

   if (empty($queue)) {
      $output .= '<input align="right" type="submit" name="button" value="Continue">';
   }

   else {
      if ($form_type == "normal" and $button == "Continue" and !empty($subject) and !empty($content)) {
	$jscript = "onClick=\"enable()\"";  
	$output .= '<input align="right" type="submit" name="button" ' . $jscript . ' value="Create Ticket">';
      }

      else if($form_type == "special") {
	$jscript = "onClick=\"enable()\"";  
	$output .= '<input align="right" type="submit" name="button" ' . $jscript . ' value="Create Ticket">';
      }
          
      else {
         $output .= '<input align="right" type="submit" name="button" value="Continue">';
      }
   }
   $output .= CloseColorTable()."</td></tr></table></center></form>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: CheckInputNewTicket
// Description:
//    Checks the input for a new ticket.
// Parameters:
//    string $requester		Requester of new ticket.
//    string $status		Status to create with.
//    string $subject		Subject of ticket.
//    string $content		Initial comment of ticket.
//    string $starts		Date ticket start.
//    string $due		Date ticket due.
//    string $requester		Requester of new ticket.
//    string $cc		CC of ticket.
//    string $admin_cc		Admin CC of ticket.
//    string $user_id		User ID.
//    string $queue		Queue to create in.
// Return Values:
//    HTML to output (Includes errors if need be).
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CheckInputNewTicket($requester = "", $status = "", $subject = "",
                             $content = "", $starts = "", $due = "",
                             $requester = "", $cc = "", $admin_cc = "",
                             $user_id = "", $queue = "") {
   $output = "";
   if (empty($requester)) $output .= "Please fill in the <b>requester</b> field.<br>";
   if (empty($queue)) $output .= "Please select a <b>queue</b>.<br>";
   if (empty($status)) $output .= "Please select a <b>status</b>.<br>";
   if (empty($subject)) $output .= "Please fill in the <b>subject</b> field.<br>";
   if (empty($content)) $output .= "Please <b>describe</b> the issue.<br>";
   if ((empty($user_id)) || (!UserExists($user_id))) 
      $output .= "Please enter a correct <b>user id</b> <br>";

   if (!empty($requester)) {
      if (!EmailGood($requester)) {
         $output .= "Requester: <b>$requester</b> is not a valid ";
         $output .= "email address.<br>";
      }
   }

   if (!empty($output)) {
      global $button;
      $button = "";
   }
       
   if (!empty($cc)) {
      $cc = str_replace(" ", "*", $cc);
      if ($cc != "**") {
         if (!EmailGood($cc)) {
            $output .= "Cc: <b>$cc</b> is not a valid ";
            $output .= "email address.<br>";
         }
      }
   }

   if (!empty($admin_cc)) {
      $admin_cc = str_replace(" ", "*", $admin_cc);
      if ($admin_cc != "**") {
         if (!EmailGood($admin_cc)) {
            $output .= "Admin Cc: <b>$admin_cc</b> is not a valid ";
            $output .= "email address.<br>";
         }
      }
   }
       
   $flag = 0;
   if (!empty($starts)) {
      if (!date_good($starts)) {
         $flag = 1;
         $output .= "<b>$starts</b> does not conform to ";
         $output .= "<b>yyyy-mm-dd</b> format.<br>";
      }
   }
       
   if (!empty($due)) {
      if (!date_good($due)) {
         $flag = 1;
         $output .= "<b>$due</b> does not conform to ";
         $output .= "<b>yyyy-mm-dd</b> format.<br>";
      }
   }

   if ($flag) {
      global $detail_state;
      if ($detail_state == "Hide-Details") {
         $output .= "<p>Click the \"Show-Details\" button ";
         $output .= "so you can fix the date format problem.";
      }
   }
   $output .= CheckRequiredFields();
   return $output;
}
?>