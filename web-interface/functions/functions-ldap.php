<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("functions-form-objects.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");
require_once("functions-header.php");
//
// Filename: functions-ldap.php
// Description: Contains functions for contecting, using, searching LDAP.
// Supprted Language(s):   PHP 4.0
//
require_once("ru_ldap_auth.php");

//-----------------------------------------------------------------------------
//
// Function: ShowCreateTicketForm
// Description:
//    Show a form for searching for users to create new tickets.
// Parameters:
//    None.
// Return Values:
//    HTML to display the table.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function ShowCreateTicketForm() {
   global $LOOKUP_TYPE;
   $output = '<table><form action="lookup_name.php" method="get"><tr><td nowrap>'
          .'<input type=submit value="Lookup User (Name)">&nbsp;'
          .'<input type="text" size="12" name="user" value="">'
          .'</td></form><form action="ticket.php"><td nowrap>'
          .'<input type=submit value="Go To Ticket">&nbsp;'
          .'<input size=7 name="id"></td></form></tr>';
   if ($LOOKUP_TYPE != "local") {
      session_unregister("ldap_insert");
      $output .= '<form action="lookup_netid.php" method="get"><tr><td nowrap>'
          .'<input type=submit value="Lookup User (NetID)">&nbsp;'
          .'<input type="text" size="12" name="uid" value="">';
   }
   $output .= '</td></tr></form></table>';
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: LdapShowForm
// Description:
//    Generates forms to show LDAP info and add the user to the system.
// Parameters:
//    string $cn	Common Name
//    string $iid	IID of user.
//    string $email	Email
//    string $uid	User ID
//    string $type	Type of user.
//    string $phone	Phone Number
//    string $fax	Fax Number
//    string $location	Location
//    string $addr	Address
//    string $rcpid	RCP ID
//    string $objclass	Object Class
//    string $ruservice	RU-Service
//    string $ruenable  RU-Enable
//    string $rudisable	RU-Disable
//    string $id	User ID
//    string $edit      Not Used.
//    string $queue	Not Used.
// Return Values:
//    Returns HTML to display the table.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LdapShowForm($cn = "", $iid = "", $email = "", $uid = "", $type = "",
                      $phone = "", $fax = "", $location = "", $addr = "", 
                      $rcpid = "", $objclass = "", $ruservice = "", $ruenable = "", 
                      $rudisable = "", $id = "", $edit = "", $queue = "") {
   global $self;
   if (($iid) && ($id == "")) {
      $query = "Select id from user where iid='$iid' order by id limit 1";
      $id = GetFieldFromQuery($query, "id");
   }
   $output = "<table border=0><tr><td valign=\"top\">";

   if ($id != "") {
      $output .= "<form action=\"new_ticket.php\" method=\"get\">";
      $output .= "<p><input type=submit value=\"Create New Ticket For User # $id\"></P>";
      $output .= Input("", $id, "user_id", "hidden");
      $output .= "</form>";
   }
   $output .= "<form action=\"add_user.php?id=$id\" method=\"get\">";

   if ($id) {
      $output .= "<p><input type=submit name=\"do\" value=\"Save changes\"></P>";
      $output .= SetInput("", $id, "id", "hidden");
   }
   else $output .= "<p><input type=submit name=\"do\" value=\"Add this user to the system\"></P>";
   $phone = str_replace("+", "\r\n+", $phone);
   $fax = str_replace("+", "\r\n+", $fax);
   $addr = str_replace("<br>", "\r\n", $addr);
   $output .= "<table border=0 style=\"border: 1px solid black\" cellspacing=3 cellpadding=0>"
           .SetInput("", $id, "id", "hidden")
           .Input("", 1, "edit", "hidden")
           .SetInput("Name", $cn).SetInput("IID", $iid)
           .SetInput("E-mail", $email)
           .SetInput("UID", $uid).SetInput("Type", $type)
           .SetInput("Phone", $phone, "", "textarea")
           .SetInput("Fax", $fax).SetInput("Location", $location)
           .SetInput("Address", $addr, "address", "textarea")
           .SetInput("RU-service", $ruservice, "", "textarea")
           .SetInput("RU-enable", $ruenable).SetInput("RU-disable", $rudisable)
           ."</table></form></td><td>";

   if ($id) {
      $output .= "Tickets Related To User # $id <br>"
              ."<table border = 1>"
              .RelatedTicketsToUser($id)
              ."</table>";
   }
   $output .= "</td></tr></table>";
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: LdapPrintAll
// Description:
//    Generates a string from an array of LDAP results.
// Parameters:
//    array $result_array	LDAP results to print.
//    string $string		Dimension 2 of the array to use.
// Return Values:
//    String to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LdapPrintAll($result_array, $string) {
   for ($i = 0; ; ) {
      $cat_new = $result_array[0]["$string"][$i++];
      if (empty($cat_new)) break;
      else $cat .= "$cat_new ";
   }
   if (empty($cat)) return "&nbsp;";
   return $cat;
}
    
//-----------------------------------------------------------------------------
//
// Function: LdapPrintResultList
// Description:
//    Generates HTML to display each type of LDAP rsults.
// Parameters:
//    array $result_array	LDAP results.
//    string $id		Passed to LdapShowForm
//    string $edit		Passed to LdapShowForm
//    string $queue		Passed to LdapShowForm
// Return Values:
//    Returns HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LdapPrintResultList($result_array, $id = "", $edit = "", $queue = "") {
   $dn = LdapPrintAll($result_array, "dn");
   $cn = LdapPrintAll($result_array, "cn");
   $fname = ucwords(LdapPrintAll($result_array, "givenname"));
   $lname = ucwords(LdapPrintAll($result_array, "sn"));
   $email = $result_array[0]["mail"][0];
   $iid = strtoupper(LdapPrintAll($result_array, "rutgerseduiid"));
   $uid = LdapPrintAll($result_array, "uid");
   $phone = LdapPrintAll($result_array, "telephonenumber");
   $fax = LdapPrintAll($result_array, "facsimiletelephonenumber");
   $addr = str_replace("$" , "<br>",LdapPrintAll($result_array, "postaladdress"));
   $type = LdapPrintAll($result_array, "employeetype");
   $rcpid = LdapPrintAll($result_array, "rcpid");
   $objclass = LdapPrintAll($result_array, "objectclass");
   $ruservice = LdapPrintAll($result_array, "ruservice");
   $ruenable = LdapPrintAll($result_array, "ruenable", "textarea");
   $rudisable = LdapPrintAll($result_array, "rudisable");
   $location = LdapPrintAll($result_array, "location");
       
      if (!empty($cn)) {
         return LdapShowForm($cn, $iid, $email, $uid, $type, $phone, $fax, $location, $addr, $rcpid, $objclass, $ruservice, $ruenable, $rudisable, $id, $edit, $queue);
      }
   }
    
//-----------------------------------------------------------------------------
//
// Function: LdapShowTable
// Description:
//    Generates HTML to allow searching for users by last name.
// Parameters:
//    string $iid		Not used.
//    string $passwd		Not used.
//    string $user		Not used.
//    string $username		Not used.
// Return Values:
//    Returns HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LdapShowTable($iid = "", $passwd = "", $user = "", $username = "") {
   global $self,$queue;
   return "<form action=\"$self\" method=\"GET\">"
         ."<table border=0>".OpenTable("User's Last Name")
         ."<input type=\"text\" name=\"user\" value=\"$user\">"
         .CloseTable().OpenTable()."<input type=\"submit\" value=\"Search\">"
         .CloseTable()."</table></form>\n";
}
    
//-----------------------------------------------------------------------------
//
// Function: UserPrintResultList
// Description:
//    Generates HTML to display information about a user in the database.
// Parameters:
//    mysql result $user_result_set	User information.
//    string $queue			Not used.
// Return Values:
//    Returns HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UserPrintResultList($user_result_set, $queue = "") {
   if ($user_result_set) {
      $output = "";
      $num_rows = mysql_num_rows($user_result_set);

      if ($user_result_set && $num_rows) {
         $top = array ("ID", "Name", "E-mail", "IID", "Create Ticket For This User", "Edit This User");
         $output = "<p>Users currently in system:</p><table border=1>".TableRowBold($top);

         while ($user_row = mysql_fetch_object($user_result_set)) {
            $delete = "";
            $id = $user_row->id;
            $ticket = "<a class=\"main\" href=\"new_ticket.php?user_id=$id\">click here</a>";
            $edit = "<a class=\"main\" href=\"add_user.php?id=$id\">click here</a>";
            $row = array ($id, $user_row->name, $user_row->email, $user_row->iid, $ticket, $edit);
            $output .= TableRow($row);
         }
         $output .= "</table>";
      }
      return $output;
   }
}

//-----------------------------------------------------------------------------
//
// Function: LdapPrintResultListLdap1
// Description:
//    Generates HTML to display each type of LDAP rsults.
// Parameters:
//    array $result_array	LDAP results.
//    string $common		First name.
//    string $queue		Not used.
// Return Values:
//    Returns HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function LdapPrintResultListLdap1($result_array, $common, $queue = "") {
   $count = count($result_array);
       
   if ($count < 2) {
      $output = "<p>No users were found with the last name of <b>$common</b>.  "
                .ButtonLink("add the user manually", "add_user.php").LdapShowTable();
   }
   else {
      for($i = 0; $i < $count; $i++) {
         $format_array[$i][0] = strtolower($result_array[$i]["cn"][0]);
         $format_array[$i][1] = $result_array[$i]["dn"];
         $format_array[$i][2] = strtolower($result_array[$i]["givenname"][0]);
         $format_array[$i][3] = strtolower($result_array[$i]["sn"][0]);
         $format_array[$i][4] = strtolower($result_array[$i]["mail"][0]);
         $format_array[$i][5] = $result_array[$i]["rutgerseduiid"][0];
      }
      sort($format_array);
      $count_mod = $count - 1;
      $name = ucfirst($common);
      if ($count_mod == 1) $output = "<p>The following person from LDAP has the last name <b>$name</b>.";
      else {
         $output = "<p>The following <b>$count_mod</b> people from LDAP have the last name <b>$name</b>.";
             
         if ($count_mod >= 50) {
            $output .= "<br><font color=\"red\"> Please note that only the first 50 results can "
                    ."be displayed. You can narrow your ldap search by using \"LASTNAME, FIRSTNAME\".";
         }
      }
      $output .= "<table border=0 cellpadding=1 cellspacing=1>"
              ."<tr><td valign=\"top\"><p><b>Select</b> "
              ."the user you want to add from below <b>or</b>&nbsp;"
              ."</td><td align=\"left\" valign=\"top\">"
              .ButtonLink("add the user manually", "add_user.php")
              ."</td></tr></table><p><table border=1><tr><td>"
              ."<font face=\"arial, helvetica\" size=2><b>No.</b></font>"
              ."</td><td>"
              ."<font face=\"arial, helvetica\" size=2><b>Name</b></font>"
              ."</td><td>"
              ."<font face=\"arial, helvetica\" size=2><b>E-mail</b></font>"
              ."</td><td><font face=\"arial, helvetica\" size=2><b>IID</b></font>"
              ."</td><td>"
              ."<font face=\"arial, helvetica\" size=2><b>Add this user</b></font>"
              ."</td></tr>";

      for($i = 0; $i < $count; $i++) {
         $cn = $format_array[$i][0];
         $dn = $format_array[$i][1];
         $fname = ucwords($format_array[$i][2]);
         $lname = ucwords($format_array[$i][3]);
         $email = $format_array[$i][4];
         $iid = strtoupper($format_array[$i][5]);

         if (!empty($cn)) {
            $output .= "<tr><td><font face=\"arial, helvetica\" size=2>$i</font>"
                    ."</td><td><font face=\"arial, helvetica\" size=2>"
                    ."$fname $lname</font></td><td>"
                    ."<font face=\"arial, helvetica\" size=2>";
         if (!empty($email)) $output .= "<a class=\"main\" href=\"mailto:$email\">$email</a>";
         else $output .= "&nbsp";
         $output .= "</font></td><td>"
                 ."<font face=\"arial, helvetica\" size=2>$iid</font>"
                 ."</td><td><font face=\"arial, helvetica\" size=2>"
                 ."<a class=\"main\" href=\"add_user.php?iid=$iid\">click here</a>"
                 ."</td></tr>";
         }
      }
      $output .= "</table>";
   }
   return $output;
}

function LdapCreateQuery($user = "") {
   $user_split_array = explode(",", $user);
   if (sizeof($user_split_array) > 1) {
      $user = trim($user_split_array[0]);
      $user_first_name = trim($user_split_array[1]);
   }

   if ($user_first_name != "") {
      return "(&(sn=$user)(givenname=$user_first_name*))";
   }
   return "sn=$user";
}
?>
