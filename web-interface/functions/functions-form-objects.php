<?php
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2005, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

 
require_once("functions-getters.php");
require_once("functions-widgets.php");
require_once("functions-ticket.php");
//
// Filename: functions-form-objects.php
// Description: Contains functions for generating HTML forms.
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: SetInput
// Description:
//    Creates most form items in the theme of the site.
// Parameters:
//    string $title			Title of form.
//    string $value			Default Value.
//    string $name			Name of form.
//    string $type			Type of form; defaults to "text".
//    int $size				Size of form.
//    string $disabled			Enable or disable part of form.
//    string $columns_or_selected_item	Depends on type of form.  
//    string $wrap_or_display_array	
//    string $set_name			"array" for input; default: "set".
//    string $required			Not Used.
// Return Values:
//    Returns HTML for form.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SetInput($title = "",
                  $value = "",
                  $name = "",
                  $type = "text",
                  $size = "",
                  $disabled = "",
                  $columns_or_selected_item = "", 
                  $wrap_or_display_array = "",
                  $set_name = "set",
                  $required = "") {

   $output = Input($title, $value, $name, $type, $size, $disabled, 
                   $columns_or_selected_item, $wrap_or_display_array);

   if ($name == "") {
      $name = strtolower(str_replace("-", "", $title));
   }
   $output .= Input("", $name, $set_name."[]", "hidden");
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: SetNameSetInput
// Description:
//    Wrapper for SetInput.
// Parameters:
//    string $set_name			"array" for input; default: "set".
//    string $title			Title of form.
//    string $value			Default Value.
//    string $name			Name of form.
//    string $type			Type of form; defaults to "text".
//    int $size				Size of form.
//    string $disabled			Enable or disable part of form.
//    string $columns_or_selected_item	Depends on type of form.  
//    string $wrap_or_display_array	
// Return Values:
//    Returns output of SetInput.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SetNameSetInput($set_name = "set",
                         $title = "",
                         $value = "",
                         $name = "",
                         $type = "text",
                         $size = "",
                         $disabled = "",
                         $columns_or_selected_item = "",
                         $wrap_or_display_array = "") {
      return SetInput($title, $value, $name, $type, $size, $disabled,
                      $columns_or_selected_item, $wrap_or_display_array, $set_name);
   }
    
    
//-----------------------------------------------------------------------------
//
// Function: Input
// Description:
//    Creates most form items in the theme of the site.
// Parameters:
//    string $title			Title of form.
//    string $value			Default Value.
//    string $name			Name of form.
//    string $type			Type of form; defaults to "text".
//    int $size				Size of form.
//    string $disabled			Enable or disable part of form.
//    string $columns_or_selected_item	Depends on type of form.  
//    string $wrap_or_display_array	
// Return Values:
//    Returns HTML for form.
// Remarks:
// The title sets what is displayed to the user next to the field
// value is the default value of a text field
// value array is the values of a menu or radio group
// name is the name of the field
// type could be "menu", "text", "textarea", or "radio" for radio button group
// if disabled is set to 1 then the item is disabled
// columns is the number of columns in a text area
// selected item is the item selected in a menu or radio group based on its value
// wrap is to set wether a textarea has a special type of wrap
// display array is what is desplayed in the menu or radio group. 
// It is set to the same as the value array if left empty
// it must be the same size as the value array
// fix the values for simple use
//-----------------------------------------------------------------------------
function Input($title = "",
               $value_or_value_array = "",
               $name = "",
               $type = "text",
               $size = "",
               $disabled = "",
               $columns_or_selected_item = "",
               $wrap_or_display_array = "") {

   $output = "";
   $wrap = $wrap_or_display_array;
   $display_array = $wrap_or_display_array;
   $select = $columns_or_selected_item;
   $columns = $columns_or_selected_item;
   $value_array = $value_or_value_array;
   $value = $value_or_value_array;
   
   if ($display_array == "") {
      $display_array = $value_array;
   }

   if (sizeof($display_array) < sizeof($value_array)) {
      $array_size = sizeof($display_array);
   }

   else {
      $array_size = sizeof($value_array);
   }

   if ($name == "") {
      $name = strtolower(str_replace("-", "", $title));
   }
       
   if ($disabled == 1) {
      $disabled = "disabled";
   }

   if ($name) {
      $name = "name=\"".$name."\" id=\"".$name."\"";
   }

   if (($size == "") && ($type == "text")) {
      $size = "size=\"35\"";
   }

   elseif ($size != "") {
      $size = "size=\"$size\"";
   }
    
   if ($type != "hidden") {
      $output .= OpenTable($title);
   }

   if ($type == "textarea") {

      if ($columns == "") {
         $columns = "cols=\"40\"";
      }

      if ($wrap == "") {
         $wrap = "wrap=\"hard\"";
      }

      if ($size == "") {
         $size = "3";
      }
      $output .=  "<textarea $columns rows=\"$size\" $wrap $name>$value</textarea>";
   }

   elseif($type == "menu") {

      if ($size != "") {
         $size = "size=\"$size\"";
      }
      $selected = "";
      $output .= "<select $name $size $disabled><option value=\"\">---</option>";

      for($i = 0; $i < $array_size; $i++) {
         if ($value_array[$i] == $select) $selected = "selected";
         $output .= "<option value=\"".$value_array[$i]."\" $selected>".$display_array[$i]."</option>";
         $selected = "";
      }
      $output .= "</select>";
   }

   elseif($type == "radio") {
      $output .= "<p>";

      for($i = 0; $i < $array_size; $i++) {
         if ($value_array[$i] == $select) $selected = "checked";
         $output .= "<label>"
                 ."<input type=\"radio\" $name value=\"".$value_array[$i]."\" $selected>"
                 .$display_array[$i]."</label><br>";
      
         $selected = "";
      }
      $output .= "</p>";
   }
   else {
      if ($type) {
         $type = "type=\"".$type."\"";
      }
      if ($value) {
         $value = "value=\"".$value."\"";
      }
      $output .= "<input $type $name $value $size $disabled $select>";
   }
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: SpecialFieldsCount
// Description:
//    Counts the number of special fields for a queue.
// Parameters:
//    string $queue	Queue to count special fields for
// Return Values:
//    number of special fields
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function SpecialFieldsCount($queue = "") {
   if ($queue == "") return 0;
   $queue = str_replace("_", " ", $queue);
   return GetCountFromTable("q_specific_fields where q_name='$queue'");
}
    
//-----------------------------------------------------------------------------
//
// Function: InputSpecialFields
// Description:
//    Generates a form to input speical fields.
// Parameters:
//    string $queue	Queue ticket is in.
//    int $ticket_id	Id of Ticket
// Return Values:
//    returns HTML for the form.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function InputSpecialFields($queue = "", $ticket_id = "") {
  global $db;
  if ($queue == "") return 0;
  $queue = str_replace("_", " ", $queue);
  $output = "";

  if ($ticket_id == "") {  
    $query = "SELECT * FROM q_specific_fields WHERE q_name='$queue' ORDER BY date_create";
  } 
  else {  
    $query = "
      SELECT * FROM q_specific_fields  
      LEFT OUTER JOIN q_specific_ticket_value 
        ON q_specific_fields.field_name=q_specific_ticket_value.field_name 
        AND ticket_id='$ticket_id' 
      WHERE q_name='$queue' 
      ORDER BY date_create";
  }
  $fields = mysql_query($query, $db);
     
  if ($fields) {
    $has_required = 0;
    while ($field = mysql_fetch_object($fields)) {
      $has_required = $has_required + $field->required;
      $output .= DisplaySpecialField($queue, $field, $ticket_id);  
    }
  }
       
  if ($has_required > 0) {
    $output .= OpenTable("&nbsp;");
    $output .= "<font color=\"red\">*</font> indicates a required field";
    $output .= CloseTable();
  }
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: IncompleteSpecialFields
// Description:
//    Checks values of required special fields for a ticket.
// Parameters:
//    int $ticket_id	Id of ticket to check.
// Return Values:
//    returns text indicating the missing fields.
// Remarks:
//    none.
//-----------------------------------------------------------------------------
function IncompleteSpecialFields($ticket_id = "") {
   if ($ticket_id == 0) return "";
   $ticket = GetFirstRowFromTable("ticket where id='$ticket_id'");
   $queue = $ticket->queue;
   $fields = GetArrayFromTable("field_name", "q_specific_fields where q_name='$queue'");
   $size = sizeof($fields);
   $j = 0;

   for($i = 0; $i < $size; $i++) {
      $field = $fields[$i];
      $value_row = GetFirstRowFromTable("q_specific_ticket_value where field_name='$field' and ticket_id='$ticket_id'");
      if ($value_row->value == "") {
         $missing_fields[$j] = $field;
         $j++;
      }
   }
   if ($j == 0) return "";
   $output = "You are missing values for ";
   for($i = 0; $i < $j; $i++) {
      $diff = $j-$i;
      $next_string = "";

      if ($diff == 1) {
         $next_string = ".";
      }

      if (($diff > 1) && ($j > 2)) {
         $next_string = ", ";
      }

      if ($diff == 2) {
         $next_string .= " and ";
      }

      if ($diff > 2) {
         $next_string = ", ";
      }
      $output .= $missing_fields[$i].$next_string;
   }

   $output .= " Click <a class=\"main\" href=\"ticket.php?id=$ticket_id"."&menu=Special_Fields\">Special Fields</a> to add data for these fields.<br>";
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: DisplaySpecialField
// Description:
//    Displays a special field.
// Parameters:
//    string $queue	Name of queue.
//    string $field	Name of field.
//    int $ticket_id	Id of Ticket
// Return Values:
//    0 if missing param.
//    HTML otherwise.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function DisplaySpecialField($queue, $field = "", $ticket_id = "") {
   if (($field == "") || ($queue == "")) return 0;
   $set_name = "q_specific_fields";
   $title = $field->field_display_name;
   $type = $field->field_type;
   $name = $field->field_name;

   if ($ticket_id != "") {
      $val_row = GetFirstRowFromTable("q_specific_ticket_value where ticket_id='$ticket_id' and field_name='$name'");
   }
   $value = GetArrayFromTable("item_value", "q_specific_menu where field_name='$name' and  q_name='$queue'");

   if ((!(($type == "menu") || ($type == "radio"))) && ($value != "")) {
      $value = $value[0];
      if ($val_row) $value = $val_row->value;
   }
   elseif($val_row) $columns_or_selected_item = $val_row->value;

   if ($field->required) {
      $output .= Input("", $name, required_list."[]", "hidden");  
      $title .= " <font color=\"red\">*</font>";
   }
   if (($field->value != "") && ($type == "text")) {
      $value = $field->value;
   }
   elseif ($type == "text" || $type == "textarea") $value = $_POST[$name];
   elseif (empty($columns_or_selected_item)) $columns_or_selected_item = $_POST[$name];
   $output .= SetNameSetInput($set_name, $title, $value, $name, $type, $size, $disabled, $columns_or_selected_item, $wrap_or_display_array);
   if ($type == "dorm_menu") {
     $title = "Campus/Building";
     if ($field->required) $title .= " <font color=\"red\">*</font>";
     $output = OpenTable($title);
     $output .= DormMenu();
     $output .= CloseTable();
   }
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: DormMenu
// Description:
//    Displays container HTML for javascript to create dorm_menu
// Parameters:
//    none
// Return Values:
//    HTML (mostly just divs) 
// Remarks:
//    None.
//-----------------------------------------------------------------------------

function DormMenu() 
{
  $menu = <<<EOD
<div id='trigger'> 
  <div id='trig_campus_div'><em>disabled</em></div>
  <div id='trig_building_div'></div>
</div>
<br /><br />
EOD;
  return $menu;
}

//-----------------------------------------------------------------------------
//
// Function: SetCustomizedFieldsValues
// Description:
//    Inserts special fields into the database.
// Parameters:
//    int $ticket_id	Id of ticket.
// Return Values:
//    Text indicating what was done.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SetCustomizedFieldsValues($ticket_id = "") {
   global $q_specific_fields, $db, $queue, $q_name, $req_err;
   if (($q_specific_fields == "") || ($ticket_id == "")) return 0;
   if (($queue == "") && ($q_name != "")) $queue = $q_name;

   if ($queue == "") {
      $ticket = GetFirstRowFromTable("ticket where id='$ticket_id'");
      $queue = $ticket->queue;
   }
   $queue = str_replace("_", " ", $queue);
   $req_err = CheckRequiredFields();
   if ($req_err) return $req_err;
   $size = sizeof($q_specific_fields);

   for($i = 0; $i < $size; $i++) {
      $fieldname = $q_specific_fields[$i];
      global ${$fieldname};
      $fieldvalue = ${$fieldname};

      if (!(($fieldvalue == "") && (RequiredField($fieldname)))) {
         $query = "Replace into q_specific_ticket_value set ticket_id='$ticket_id', field_name='$fieldname', value='$fieldvalue'";
         mysql_query($query, $db);
         $output .= "$fieldname set to $fieldvalue<br>";
      }
   }
   return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: GetValueOfTicketSpecialField
// Description:
//    Returns the value for a specific special field for a ticket.
// Parameters:
//    string $field_name	Name of field to return.
//    int $ticket_id		Id of ticket.
// Return Values:
//    Returns the value for a specific special field for a ticket.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetValueOfTicketSpecialField($field_name, $ticket_id) {
   $row = GetFirstRowFromTable("q_specific_ticket_value where ticket_id='$ticket_id' and field_name='$field_name'");
   if ($row) {
      return $row->value;
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: GetQueueSpecificValuesOfTicketSpecialFields
// Description:
//    Gets the names and values of special fields that are valid for the queue
//    the specified ticket is currently in.
// Parameters:
//    int $ticket_id	Id of Ticket
// Return Values:
//    Returns an array of names and an array of values in an array.  Use list
//    to retrieve them as seperate arrays.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetQueueSpecificValuesOfTicketSpecialFields($ticket_id = "") {
   global $db;

   if ($ticket_id) {
      $ticket = GetFirstRowFromTable("ticket where id='$ticket_id'");
      $queue = $ticket->queue;
      $query = "select field_display_name,value from ";
      $query .= "q_specific_ticket_value,q_specific_fields ";
      $query .= "where q_specific_ticket_value.field_name=q_specific_fields.field_name ";
      $query .= "and q_name='$queue' and ticket_id='$ticket_id' ";
      $query .= "order by q_specific_fields.date_create";
      $result = mysql_query($query, $db);
      $i = 0;
      $names = $values = array();
      if ($result) {
         while ($table_row = mysql_fetch_object($result)) {
            $names[$i] = str_replace("_", " ", $table_row->field_display_name);
            $values[$i] = $table_row->value;
            $i++;
         }
      }

      return array($names, $values);
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: SpecialFieldsEmailOutput
// Description:
//    Formats the special fields of a ticket to be sent in an email.
// Parameters:
//    int $ticket_id	Id of Ticket
// Return Values:
//    Returns email body formatted list of special fields.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SpecialFieldsEmailOutput($ticket_id = "") {
   $arrs = GetQueueSpecificValuesOfTicketSpecialFields($ticket_id);
   $output = "";
   $field_name_array = $arrs[0];
   $field_value_array = $arrs[1];
   $size = sizeof($field_name_array);
   $size2 = sizeof($field_value_array);
   if ($size2 < $size) $size = $size2;
   for($i = 0; $i < $size; $i++) {
      $num_of_spaces = 13-strlen($field_name_array[$i]);
      if ($num_of_spaces < 0) $num_of_spaces = 0;
      for($j = 0; $j < $num_of_spaces; $j++) {
         $spaces = " ";
      }
      $output .= $spaces."$field_name_array[$i]: $field_value_array[$i]\r";
   }
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: RequiredField
// Description:
//    Determines if a field is in the required list.
// Parameters:
//    string $field_name	Name of field to check.
// Return Values:
//    Boolean required.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function RequiredField($field_name) {
   global $required_list;
   if (@in_array ($field_name, $required_list)) {
      return 1;
   }
   else {
      return 0;
   }
}
    
//-----------------------------------------------------------------------------
//
// Function: CheckRequiredFields
// Description:
//    Checks that all required fields are filled in.
// Parameters:
//    None.
// Return Values:
//    Returns html if needed.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CheckRequiredFields() {
   global $required_list, $queue, $db;
   if ($required_list == "") return "";
   $output = "";
   $start = "Please fill in the <b>";
   $end = "</b> field.<br>";
   $size = sizeof($required_list);
   if ($queue != "") {
      $queue = str_replace("_", " ", $queue);

      for ($i = 0; $i < $size; $i++) {
         $fieldname = $required_list[$i];
         global ${$fieldname};
         $fieldvalue = ${$fieldname};

         if ($fieldvalue == "") {
            $field_row = GetFirstRowFromTable("q_specific_fields where q_name='$queue' and field_name='$fieldname'");
            $display_name = $field_row->field_display_name;
            $output .= $start.$display_name.$end;
         }
      }
   }

   else {

      for ($i = 0; $i < $size; $i++) {
         $fieldname = $required_list[0];
         global ${$fieldname};
         $fieldvalue = ${$fieldname};

         if ($fieldvalue == "") {
            $output .= $start.$fieldname.$end;
         }
      }
   }
   return $output;
}
?>