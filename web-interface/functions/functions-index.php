<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
require_once("functions-date.php");
require_once("functions-general-utils.php");
require_once("functions-getters.php");
require_once("functions-widgets.php");
//
// Filename: functions-index.php
// Description: Contains functions related to the index page display.
// Supprted Language(s):   PHP 4.0
//
global $self;
$self = $PHP_SELF;

//-----------------------------------------------------------------------------
//
// Function: GetTicketTable
// Description:
//    Display a list of tickets in a table, including queue, status, and owner
// Parameters:
//    int $number	Number of tickets to list.
//    string $user	Username to check.
//    string $requested	Type of tickets.
// Return Values:
//    Returns HTML to display the table.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetTicketTable($number = "", $user = "", $requested = "") {
   if ($requested == "requested") {
      $final = "Owner";
      $tr = GetFirstRowFromTable("staff where username = '$user'");
      $email = $tr->email;
      $result = GetHighestPriorityTicketsFor("ticket.requester", $email);
   } 

   elseif ($requested == "admincc") {
      $final = "Owner";
      $tr = GetFirstRowFromTable("staff where username = '$user'");
      $email = $tr->email;
      $result = GetHighestPriorityTicketsWatcher("ticketwatcher.email", $email, "AdminCC");
   }
       
   elseif ($requested == "cc") {
      $final = "Owner";
      $tr = GetFirstRowFromTable("staff where username = '$user'");
      $email = $tr->email;
      $result = GetHighestPriorityTicketsWatcher("ticketwatcher.email", $email, "CC");
   }
       
   else{
      $final = "&nbsp;";
      $result = GetHighestPriorityTicketsFor("owner", $user);
   }
       
   $output = '<table border=0 cellspacing=0 cellpadding=1 width=100%>'
           .'<tr><th align=left>'.Font("#")
           .'</th><th align=left>'.Font("Subject")
           .'</th><th align=left>'.Font("Queue")
           .'</th><th align=left>'.Font("Status")
           .'</th><th align=left>'.Font("$final").'</th></tr>';

   for ($i = 0;($i < $number+1) && ($tablerow = mysql_fetch_object($result));$i++) {
      $url = "<a class=\"main\" href=\"ticket.php?id=$tablerow->id\">";
      $subject = htmlentities(GetOriginalSubjectOfTicket($tablerow->id));
      $output .= '<tr><th align=left>'.Font($url.$tablerow->id."</a>")
              .'</th><th align=left>'.Font($url.$subject."</a>")
              .'</th><th align=left>'.Font($tablerow->queue)
              .'</th><th align=left>'.Font($tablerow->current_status);

      if ($requested == "requested" || $requested == "admincc" || $requested == "cc") {
	$output .= '<th align=left>'.font($tablerow->owner);
      }
      $output .= '</th></tr>';
   }
   $output.='</table>';
   return $output;
}

//-----------------------------------------------------------------------------
//
// Function: CacheOverdue
// Description:
//    Checks if the OverviewCache has expired.
// Parameters:
//    None.
// Return Values:
//    1 if the cache should be refreshed, 0 if not.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function CacheOverdue(){
   global $db;
   $num_min=15; //max number of minutes between cache

   if($min_ago=="error"){
      return 1;
   }
   $min_ago= GetCacheAge();
   if($min_ago<$num_min){
      return 0;
   }
   return 1;
}

//-----------------------------------------------------------------------------
//
// Function: GetCacheAge
// Description:
//    Gets the age of the OverviewCache.
// Parameters:
//    None.
// Return Values:
//    Returns the minutes passed since the Overview was last updated.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function GetCacheAge(){
   global $db;
   $query = "Select date_format(val, '%i') cache_min, date_format(now(), '%i') ";
   $query .= "as now_min from system_variables where var='overview_cache_date'";
   $result = mysql_query($query, $db);

   if(($result)&&($row=mysql_fetch_object($result))){
      if ($row->cache_min > $row->now_min) $row->cache_min=$row->cache_min-60;
      $min_ago = $row->now_min-$row->cache_min;    
      return $min_ago;
   }
   return "error";
}

//-----------------------------------------------------------------------------
//
// Function: OverviewList
// Description:
//    Displays the Overview on the main page.
// Parameters:
//    None.
// Return Values:
//    HTML to display.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OverviewList() {
   global $self;
   if (($_GET['recache'])||(CacheOverdue())) OverviewCaching();
   $output = OverviewDisplay();
   $output .= "<br>Overview cache created ".GetCacheAge();
   $output .= " minutes ago. <br><a class=\"main\" href=\"";
   $output .= $self."?recache=1\">Force re-cache?</a>&nbsp;";
   if ($_GET['allqueues'] != "show") $output .= "<a class=\"main\" href=\"$self?allqueues=show\">Show all.</a>";
   else $output .= "<a class=\"main\" href=\"".$_SERVER['SCRIPT_NAME']."\">Show limited.</a>";
   return $output;
}


//-----------------------------------------------------------------------------
//
// Function: OverviewCaching
// Description:
//    This function performs the calculations for overdue tickets, etc for each
//    queue.  The results are then inserted into a seperate table for quick
//    access.
// Parameters:
//    None.
// Return Values:
//    None.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OverviewCaching() {
   global $db, $debug_query;
   $allow_days_query = "select allowabledays from queue group by allowabledays";
   $result = mysql_query($allow_days_query, $db);
   $delete_query = "delete from overview_cache";
   mysql_query($delete_query, $db);
   $query = "select queue, current_status, ";
   $query .= "SUM(IF(current_status='New',1,0)) as new, ";
   $query .= "SUM(IF(current_status='Open',1,0)) as open, ";
   $query .= "SUM(IF(current_status='Stalled',1,0)) as stalled, ";
   $query .= "SUM(IF(current_status='Resolved',1,0)) as resolved, ";
   $query .= "SUM(IF((current_status!='Resolved') && (IF(( ";
   $or = "";

   while($row=mysql_fetch_object($result)){
      $days=$row->allowabledays;
      $date_ago=NBDaysAgo($days);
      $date_ago=str_replace("-","", $date_ago)."000000";
      $query .= "$or((allowabledays='$days')&&(date_created<'$date_ago'))";
      $or="||";
   }
   $query .= "),1,0)),1,0)) as overdue, ";
   $query .= "SUM(IF((date_created < '".NBDaysAgo(10)."'),1,0)) as days_ago, ";
   $query .= "allowabledays from ticket,queue where q_name=queue and ";
   $query .= "queue != '' and enabled > 0 group by queue order by queue";
   $result=mysql_query($query, $db);

   while(($result)&&($over=mysql_fetch_object($result))){
      $days=$over->allowabledays;
      $date_ago=NBDaysAgo($days);
      $query = "Replace into overview_cache set ";
      $query .= "queue='$over->queue', ";
      $query .= "new='$over->new', ";
      $query .= "open='$over->open', ";
      $query .= "stalled='$over->stalled', ";
      $query .= "resolved='$over->resolved', ";
      $query .= "overdue='$over->overdue', ";
      $query .= "d_day='$date_ago'";
      mysql_query($query, $db);	 
   }
   $query = "Replace into system_variables set ";
   $query .= "var='overview_cache_date', val=now()";
   mysql_query($query, $db);
}
	



//-----------------------------------------------------------------------------
//
// Function: OverviewDisplay
// Description:
//    Generates HTMl to display the overview.
// Parameters:
//    None.
// Return Values:
//    HTML to display the overview.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function OverviewDisplay() {
   global $db, $username;
   $new_color = "#DCDCDC"; // grey (gainsboro)
   $open_color = "#FFFFFF"; // white
   $stalled_color = "#DCDCDC"; // grey (gainsboro)
   $resolved_color = "#FFFFFF"; // white
   $overdue_color = "#DCDCDC"; // grey (gainsboro)
   $output = '<table border="0" style="border: 1px solid black" cellspacing="0" cellpadding="2" width="100%">'
            .'<tr><th align="left">'.Font("Queue")."</th><th bgcolor=\"$new_color\" align=\"left\">"
            .Font("New")."</th><th bgcolor=\"$open_color\" align=\"left\">".Font("Open")
            ."</th><th bgcolor=\"$stalled_color\" align=\"left\">".Font("Stalled")
            ."</th><th bgcolor=\"$resolved_color\" align=\"left\">".Font("Resolved")
            ."</th><th bgcolor=\"$overdue_color\" align=\"left\">".Font("Overdue")
            .'</th></tr>';

   if ($_GET['allqueues'] != "show") {
      $queues_array = GetQueuesUserCanSee($username);
      $queues = "('')";
      if (sizeof ($queues_array)>0){
         $queues = "(";
         foreach ($queues_array as $view_queue){
            $queues .= "'$view_queue',";
         }
         $queues = str_replace("'$view_queue',", "'$view_queue')", $queues);
      }
      $query = "Select * from overview_cache where queue in $queues order by queue";
   }
   else $query = "SELECT * FROM overview_cache";
   $result = mysql_query($query, $db);
   if (!$result) {
      print "No result for <i>$query</i>";
   }
   else {
     while ($row = mysql_fetch_object($result)) {
        $output .='<tr>'
        //Queue
                ."<th style=\"border-top:1px solid black\" align=\"left\">"
                .QueueItemLink($row->queue).'</th>'
        //New
                ."<th style=\"border-top:1px solid black\" bgcolor=\"$new_color\" align=\"right\">"
                .QueueStatusItemLink($row->queue, $row->new, "new").'</th>'
        //Open
                ."<th style=\"border-top:1px solid black\" bgcolor=\"$open_color\" align=\"right\">"
                .QueueStatusItemLink($row->queue, $row->open, "open").'</th>'                       
        //Stalled
                ."<th style=\"border-top:1px solid black\" bgcolor=\"$stalled_color\" align=\"right\">"
                .QueueStatusItemLink($row->queue, $row->stalled, "stalled").'</th>'
        //Resolved
                ."<th style=\"border-top:1px solid black\" bgcolor=\"$resolved_color\" align=\"right\">"
                .QueueStatusItemLink($row->queue, $row->resolved, "resolved").'</th>'
        //Overdue
                ."<th style=\"border-top:1px solid black\" bgcolor=\"$overdue_color\" align=\"right\">";
        $str = OverdueLinkNum($row->queue, $row->d_day, $row->overdue );
        $output .= Font($str).'</th></tr>';                
     }
   }
   $output .='</table>';
   return $output;
}
       
       
       
//-----------------------------------------------------------------------------
//
// Function: GetQueuesUserCanSee
// Description:
//    Retrieves a list of queues that a user has SeeQueue in
// Parameters:
//    string $username  username to retrieve queues for
// Return Values:
//    Returns array of queues.
// Remarks:
//    Ignores Queue Groups
//-----------------------------------------------------------------------------

function GetQueuesUserCanSee($username) {
  global $db;
  $queues = array();

  if (empty($username)) {
    return array();
  }
  $ingroups = GetDistinctArrayFromTable("group_name", 
					"ingroup", 
					"WHERE username='$username'");
  if (sizeof($ingroups) > 0) {
    $ingroups = str_replace("_"," ",$ingroups);
    $groups = "(";
    foreach ($ingroups as $group) {
      $groups .= "'$group',";
    }
    $groups = str_replace("'$group',", "'$group')", $groups);
  }
  $query1 = "SELECT * FROM staffright WHERE ";
  $query1 .= "(staff_right=\"AllRights\" OR staff_right=\"SeeQueue\") ";
  $query1 .= "AND staffright.queue=\"global\" ";
  $query1 .= "AND username=\"$username\" LIMIT 1";
  $result1 = mysql_query($query1, $db);
  if (mysql_num_rows($result1) > 0) {
    return GetDistinctArrayFromTable("q_name", "queue", "WHERE enabled > 0");
  }

  $query2 = "SELECT * FROM groupright WHERE ";
  $query2 .= "(group_right=\"AllRights\" OR (group_right=\"SeeQueue\" ";
  $query2 .= "AND groupright.queue=\"global\")) ";
  $query2 .= " AND group_name IN $groups LIMIT 1";
  $result2 = mysql_query($query2, $db);
  if (!empty($result2)) {
    if (mysql_num_rows($result2) > 0) {
      $priv_view_limit = "WHERE enabled > 0 ORDER BY q_name";  
      return GetDistinctArrayFromTable("q_name", "queue", $priv_view_limit);
    }
  }

  $group_queues_sql = "WHERE (group_right=\"SeeQueue\" ";
  $group_queues_sql .= "OR group_right=\"AllRights\") ";
  $group_queues_sql .= "AND group_name IN $groups AND enabled > 0 ";
  $group_queues_sql .= "AND groupright.queue=queue.q_name";
  $group_queues = GetDistinctArrayFromTable("groupright.queue", 
					    "groupright,queue", 
					    $group_queues_sql);

  $staff_queues_sql = "WHERE (staff_right=\"SeeQueue\" ";
  $staff_queues_sql .= "OR staff_right=\"Allrights\") ";
  $staff_queues_sql .= "AND username=\"$username\" AND enabled > 0 ";
  $staff_queues_sql .= "AND staffright.queue = queue.q_name";
  $staff_queues = GetDistinctArrayFromTable("staffright.queue", 
					    "staffright,queue", 
					    $staff_queues_sql);
  if (sizeof($staff_queues) > 0) {
    foreach ($staff_queues as $amend) {
      if (!in_array($amend, $group_queues)) {
	$group_queues[sizeof($group_queues)] = $amend;
      }
    }
  }
  if (sizeof($group_queues) > 0) {
    return $group_queues;
  }
  return array();
}

?>