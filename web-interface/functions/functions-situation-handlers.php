<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//require_once("functions/functions-form-objects.php");
//require_once("functions/functions-getters.php");
//require_once("functions/functions-widgets.php");

//
// Filename: functions-situation-handlers.php
// Description: Contains functions for dealing with script triggers.
// Supprted Language(s):   PHP 4.0
//

//-----------------------------------------------------------------------------
//
// Function: SituationHandler
// Description:
//    Determines what to do for each situation.
// Parameters:
//    string $provision	Provision that is set.
//    int $ticket_id	ID of ticket.
//    string $username	Staff username.
//    int $email_user	Email user?
// Return Values:
//    String.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function SituationHandler($provision, $ticket_id, $username, $email_user = 1) {
   $nl = "\r";

   // build ticket_row object with necessary attributes
   $what = "id, queue, requester, owner, user_id, emailed_resolution, ";
   $what .= "DATE_FORMAT(date_created, '%Y%m%d%H%i%s') AS date_created ";
   $sql = "SELECT $what FROM ticket WHERE id='$ticket_id'";
   $result = mysql_query($sql) or die(mysql_error());
   $ticket_row = mysql_fetch_object($result);

   if ($email_user != "0") $email_user = 1;

   $queue = $ticket_row->queue;
   $owner = $ticket_row->owner;
   $date = date("F j, Y, g:i a");
   $from = GetStaffEmail($username);

   $first_comment_row = GetFirstCommentRowOfTicket($ticket_id);  
   $ticket_subject = stripslashes($first_comment_row->subject);
   $ticket_body = stripslashes($first_comment_row->body);

   $last_comment_row = GetLastCommentRowOfTicket($ticket_id);  
   $comment_subject = stripslashes($last_comment_row->subject);
   $comment_body = stripslashes($last_comment_row->body);   
   $status = $last_comment_row->status;

   $requesters_email = BuildToEmailString(GetTicketWatchers($ticket_id, "Requester"));

   $subject = "Ticket: $ticket_id $ticket_subject -- ruQueue";  
   $url = $_SERVER['SERVER_NAME']."/ticket.php?id=$ticket_id";  
   $sg = stripslashes(GetStaffSignature($username));   

   if ($provision != "OnCreate") {
      $firstline = "On $date Ticket $ticket_id was acted upon by ";
      $data = "Comment Subject: ".$comment_subject."$nl";
      $data .= "Comment Body: $nl".$comment_body."$nl";
      $data .= "----------------------------------------------------------$nl";
      $data .= "Original Subject: $ticket_subject$nl";
      $data .= "Original Body: $nl$ticket_body$nl";
   }
   else {
      $firstline = "On $date Ticket $ticket_id was created by ";
      $data = "Subject: $ticket_subject$nl";
      $data .= "Body: $nl$ticket_body$nl";
   }

   $body = $firstline . GetStaffEmail($username) . "<".$username.">$nl";
   $body .= "        Queue: $queue$nl";
   $body .= "   Ticket URL: https://$url$nl";
   $body .= "        Owner: $owner$nl";
   $body .= "       Status: $status$nl";
   $body .= "   Requesters: $requesters_email$nl";
   $body .= SpecialFieldsEmailOutput($ticket_id);
   $body .= "----------------------------------------------------------$nl";
   $body .= "$data$nl";

   if ($sg) {
      $body .= "----------------------------------------------------------$nl";
      $body .= $sg . $nl;
      $body .= "----------------------------------------------------------$nl";
   }

   if ($provision == "OnDelete") {
      $provision = "OnTransaction";
      $body = "Ticket $ticket_id has been deleted by $username";
   }
   $actionArray = GetScriptActions($provision, $queue);
   
   if ($provision == "OnCreate") {
      $subject = "New ".$subject;
      $user_message_type = "greeting";
   }
   elseif($provision == "OnTransaction") {
      $subject = "A Transaction On ".$subject;
   }
   elseif($provision == "OnCorrespond") {
     $subject = "Correspondence For ".$subject;
   }
   elseif($provision == "OnComment") {
     $subject = "New Comment On ".$subject;
   }
   elseif($provision == "OnStatus") {
      $status = $last_comment_row->status;
      $subject = "Status Is Now $status For ".$subject;
   }
   elseif($provision == "OnOwnerChange") { 
     $subject = "Owner for ".$subject." is now ".$owner.".";
   }
   elseif($provision == "OnQueueEnter")  { 
     $subject = $subject." has entered ".$queue.".";
   }
   elseif($provision == "OnQueueExit") {
     $subject = $subject." has exited ".$queue.".";
   }
   elseif($provision == "OnResolve") {
      $subject = "$username has Resolved ".$subject;
      $user_message_type = "resolution";
   }
   elseif($provision == "OnTransaction") {
     $subject = "A transaction has been placed by $username ".$subject;
   }
   elseif($provision == "OnDelete") {
     $subject = "Ticket $ticket_id was deleted by $username";
   }
   else return 0;
       
   if (($user_message_type != "") && ($email_user)) {
      $email_to_user = EmailQueueMessageToUser($ticket_row, $first_comment_row, $user_message_type);
   }


       
   if ($actionArray) {
      if (in_array("NotifyAllRelated", $actionArray)) {
         $actionArray = array("NotifyAllWatchers", "NotifyOwner", "NotifyReqesters");
      }
      foreach ($actionArray as $action) {
         if ($action == "NotifyRequesters") $requesters = GetTicketWatchers($ticket_id, "Requester");
         if ($action == "NotifyOwner")  $owner = GetStaffEmail($ticket_row->owner);
         if ($action == "NotifyAdminCc") $admincc = GetTicketWatchers($ticket_id, "AdminCc");
         if ($action == "NotifyCc") $cc = GetTicketWatchers($ticket_id, "Cc");
         if ($action == "NotifyTicketWatchers") $ticketwatchers = GetTicketWatchers($ticket_id);
         if ($action == "NotifyQueueWatchers") $queuewatchers = GetStaffEmailArray(GetWatchers($queue));
         if (($queuewatchers == "") && ($action == "NotifyAllWatchers")) {
            $allwatchers = array_merge(GetTicketWatchers($ticket_id), GetStaffEmailArray(GetWatchers($queue)));
         }
      }
      $to_array = array_merge($requesters, $owner, $admincc, $cc, $ticketwatchers, $queuewatchers, $allwatchers);
      $to_array = array_unique($to_array);
      $to_string = BuildToEmailString($to_array);
      $to_array = BuildToEmailArray($to_array);
      $user_email = GetStaffEmail($username);
      if ($user_email == "") {
         $user_email = $username. GetStaffEmailDomain();
      }

      $footer = "From: $user_email\n";
      $body .= "The following people have received this email: $to_string";
      $newline_character = "$nl\n";
      $body = str_replace("\n", "$nl", $body);
      $body = str_replace("$nl", $newline_character, $body);
      $subject = str_replace("\n", "", $subject);
      $footer .= "From: $user_email\n";
      $return = $to_string;
      if ($to_array) {
         $return = "";

         foreach($to_array as $to) {

            if (function_exists("html_entity_decode")) {
               html_entity_decode($body);
               html_entity_decode($subject);
            }
            else {
               $body = UnHtmlEntities($body);
               $subject = UnHtmlEntities($subject);
            }
            $body = str_replace("<br>", $newline_character, $body);
            $body = str_replace("&#039;", "'", $body);
            $body = str_replace("&#39;", "'", $body);

            $sent = @mail($to, $subject, $body, "From:Help Desk Ticket System<". GetCommentEmail(). ">");

            if ($sent) {
               if ($return != "") $return .= ", ";
               $return .= $to;
            }
         }
      }
   }
   return $return;
}
    
//-----------------------------------------------------------------------------
//
// Function: UnHtmlEntities
// Description:
//    Remoes html entities from.
// Parameters:
//    string $string	String to remove entities from.
// Return Values:
//    UnHtml'ed string.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function UnHtmlEntities ($string) {
   $trans_tbl = get_html_translation_table (HTML_ENTITIES);
   $trans_tbl = array_flip ($trans_tbl);
   return strtr ($string, $trans_tbl);
}
    
//-----------------------------------------------------------------------------
//
// Function: BuildToEmailArray
// Description:
//    Fixes an array of emails.
// Parameters:
//    array $to_array	Array of emails.
// Return Values:
//    Fixed array of emails.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function BuildToEmailArray($to_array) {
   $to_email_array = "";
   $i = 0;

   if ($to_array) {

      foreach($to_array as $to_email) {
         $to_email = FixEmail($to_email);

         if ($to_email != "") {
            $to_email_array[$i] = $to_email;
            $i++;
         }
      }
   }
   return $to_email_array;
}
    
    
//-----------------------------------------------------------------------------
//
// Function: BuildToEmailString
// Description:
//    Creates a string of comma seperated emails.
// Parameters:
//    array $to_array	Array of emails.
// Return Values:
//    String of emails.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function BuildToEmailString($to_array) {
   $to_string = "";
   $addcomma = 0;

   if ($to_array) {

      foreach($to_array as $to_email) {
         $to_email = FixEmail($to_email);
         if ($to_email != "") {
            if ($addcomma) $to_string .= ", ".$to_email;

            else {
                  $to_string = $to_email;
                  $addcomma = 1;
            }
         }
      }
   }
   return $to_string;
}
    
//-----------------------------------------------------------------------------
//
// Function: FixEmail
// Description:
//    Makes sure an email does not have commas or spaces and has @.
// Parameters:
//    string $email	Email to fix.
// Return Values:
//    Fixed email if no errors, nothing if error.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function FixEmail($email) {
   if ($email == "") return $email;
   $email = str_replace(",", "", str_replace(" ", "", $email));
   if (strstr($email, "@"))  return $email;
   return "";
}

//-----------------------------------------------------------------------------
//
// Function: EmailQueueMessageToUser
// Description:
//    Sends the message for the queue to the user who is referenced by the
//    ticket.
// Parameters:
//    object $ticket_row	Ticket info.
//    object $first_comment_row	First comment of ticket info.
//    string $type		Type of email to send.
// Return Values:
//    1 if mail sent successfully. 0 if error.
// Remarks:
//    None.
//-----------------------------------------------------------------------------
function EmailQueueMessageToUser($ticket_row, $first_comment_row, $type) {
   global $db;
   $ticket_id = $ticket_row->id;
   $requester_email = $ticket_row->requester;
   $queue = $ticket_row->queue;
   $owner = $ticket_row->owner;
   $user_id = $ticket_row->user_id;
   $user_row = GetUser($user_id);
   $user_email = $user_row->email;
   $to = $user_email;
   $user_full_name = $user_row->name;

   $survey_url  = GetSurveyURL();
   $survey_url .= "?ticket=" . $ticket_row->id;
   $survey_url .= "&date_created=" . $ticket_row->date_created;
   $ticket_subject = $first_comment_row->subject;
   $queue_row = GetQueueRow($queue);

   if ($type == "greeting") {
      $message = $queue_row->greeting;
      $from = $queue_row->replyto;
      $queue_replyto = $from;

      if ($message == "") {
         print ErrorReport("<br>Greeting not set. No message was sent to the user<br>");
         return 0;
      }
      if ($queue_replyto == "") $from = GetHelpEmail();
      if ($queue_replyto == "requester_email") $from = $requester_email;
      $subject = $queue_row->greeting_subject;
   }

   elseif ($type == "resolution") {
      $message = $queue_row->resolution;
      $from = $queue_row->res_replyto;
      $queue_replyto = $from;

      if ($message == "") {
         print ErrorReport("<br>Resolution not set. No message was sent to the user<br>");
         return 0;
      }

      if ($ticket_row->emailed_resolution == 1) {
         print NoticeReport("A resolution email was previously sent to the user. No additional email is being sent to the user.");
         return 0;
      }
          
      $queue_replyto = $queue_row->replyto;
      $from = $queue_replyto;
      if ($queue_replyto == "") $from = GetHelpEmail();
      if ($queue_replyto == "") $from = GetHelpEmail();
      if ($queue_replyto == "requester_email") $from = $requester_email;
      $subject = $queue_row->res_subject;
   }
   elseif ($type == "") {
      print ErrorReport("In -send email to user- in situation handlers -- no type set");
      return 0;
   }

   $search = array ("\n", "requester_email", "ticket_id", "user_full_name", "requester_email", "ticket_subject", "survey_url", "allowabledays");
   $replace = array ($nl, $requester_email, $ticket_id, $user_full_name, $requester_email, $ticket_subject, $survey_url, $allowabledays);
   $subject = str_replace($search, $replace, $subject);
   $message = str_replace ($search, $replace, $message);
   $subject = str_replace("\n", "", $subject);
   $message = str_replace("\r", "\r\n\r\n", $message);

   if (function_exists("html_entity_decode")) {
     html_entity_decode($message);
     html_entity_decode($subject);
   }
   else {
     $message = unhtmlentities($message);
     $subject = unhtmlentities($subject);
   }
   $message = str_replace("&#039;", "'", $message);
   $message = str_replace("&#39;", "'", $message);


   if (@mail("$to", "$subject", "$message", "From: $from")) {
      $message = str_replace("\r\n\r\n", "\r\n\r\n<p>", $message);
      $notice = "The following email was sent to the"
              ." <b>user</b> $user_full_name:<br>"
              ."<b>To:</b> $to <br><b>From:</b> $from<br>"
              ."<b>Subject:</b> $subject<br><b>Body:</b><br>"
              ."$message<br>";
      print NoticeReport($notice);
      if ($type == "resolution") {
         $query = "update ticket ";
         $query .= "set emailed_resolution='1', date_created=date_created ";
         $query .= "where id='$ticket_row->id'";
         mysql_query($query, $db);
      }
      return 1;
   }
   print ErrorReport("failed to send email to user in -situation handlers-");
   return 0;
}
?>