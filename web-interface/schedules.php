<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


//
// Filename: schedules.php
// Description: creates menus for various scheduling
//              display options
// Supported Language(s):   PHP 4.0
//

// Initiate Security
require_once("header.php");

// Required Files
require_once("scheduling/functions/functions-display.php");
require_once("functions/functions-forms.php");
require_once("functions/functions-widgets.php");

global $username;
print head("Schedules", $username);
$action = $_SERVER['schedules.php']."?";
print StartForm($action, array("method" => "get"));
$functions = array("Display Schedules", "Edit Schedules", "Display All Schedules", "Show Reports", "Clear All Schedules");
$function = (isset($_GET['function'])) ? $_GET['function'] : $_POST['function'];
print SelectField("function", $functions, $functions[$function]);
print SubmitField("", "Submit");
print EndForm();
print "<p>";
    
switch ($functions[$function]) {
   case "Edit Schedules":
   print OpenColorTable("green", "Edit Schedules", "100%");
   require_once("scheduling/edit_schedule.php");
   break;

   case "Display Schedules":
   print OpenColorTable("green", "Display Schedules", "100%");
   require_once("scheduling/display_schedule.php");
   break;

   case "Display All Schedules":
   print OpenColorTable("green", "Display All Schedules", "100%");
   DisplayAllSchedule();
   break;

   case "Show Reports":
   require_once("scheduling/scheduling_reports.php");
   break;

   case "Clear All Schedules":
   print OpenColorTable("green", "Clear All Schedules", "100%");
   require_once("scheduling/clear_all.php");
   break;
}
    
print CloseColorTable();
print Foot();
?>
