<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: lookup_name.php
// Description: Looks up a user by name.
//              Proceeds via LDAP, then database.
// Supprted Language(s):   PHP 4.0
//

// Required Files
require_once("functions/functions-getters.php");
require_once("functions/functions-ldap.php");
require_once("functions/functions-setters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-header.php");
require_once("functions/ru_ldap_auth.php");
require_once("header.php");
$output = Head("Lookup the user by Name", "$username");
SetNoBack("ldap_insert", "reset");
global $self;
$self = $PHP_SELF;
if ($delete) DeleteUser($delete);
$output .= "<table border=0 width=\"100%\"><tr valign=\"top\"><td width=\"100%\">";
$output .= OpenColorTable("green", "Lookup User", "100%");
if (!isset($_GET['user'])) $output .= LdapShowTable("", "", "", $username);

else {
   if (empty($_GET['user'])) {
      $output .=  "<p>Please fill in user's last name.\n";
      $output .= LdapShowTable($iid, $passwd, $_GET['user'], $username);
   } elseif ($LOOKUP_TYPE != "local") {
      $ldap_query = LdapCreateQuery($_GET['user']);
      $oSearch = new RutgersLDAPAuth;
      $oSearch->Connect();
      $result_array = array();
      $attributes = array("cn", "dn", "givenname", "sn", "mail", "rutgerseduiid");
      $oSearch->Search(&$result_array, $LDAP_ROOT_DN[$SERVER_ID],$ldap_query, $attributes);
      $output .= UserPrintResultList(UserGetEntries($_GET['user'], $id), $queue)
              .LdapPrintResultListLdap1($result_array, $_GET['user'], $queue);
   } else {
     $output .= FontBold("To add a new user click <a class=\"main\" href=\"add_user.php\">here</a>.");
     $output .= UserPrintResultList(UserGetEntries($_GET['user'], $id), $queue);
   }
}
$output .= CloseColorTable()."</td></tr></table><br>";
$output .= Foot();
print $output;
?>

