<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//
// Filename: edit_appointment.php
// Description: Edit the values of an appointment
//              Schedule an appointment
// Supprted Language(s):   PHP 4.0
//

// Required Files    
require_once("scheduling/functions/functions-getters.php");
require_once("scheduling/functions/functions-setters.php");
require_once("scheduling/functions/functions-new-appointment.php");
require_once("scheduling/functions/functions-appointment.php");
require_once("functions/functions-forms.php");
require_once("functions/functions-setters.php");
require_once("functions/functions-widgets.php");

$output = "";
if (isset($_GET['delete_appointment_id'])) {
   $appointment_result = GetAppointmentInfo($_GET['delete_appointment_id']);
    
   if (!empty($appointment_result)) {
      $appointment = mysql_fetch_array($appointment_result);
          
      if ($appointment['status'] == "Not Yet Scheduled") {
         DeleteAppointment($_GET['delete_appointment_id']);
         $output .= 'Appointment has been deleted.<br>Click '
                 .'<a class="main" href="ticket.php?id='
                 .$appointment['ticket_id'].'">here</a> to return to the ticket.';
      }
          
      else{
         $output .= ErrorReport("Appointment cannot be deleted.", "50%")
                 .'Click <a class="main" href="ticket.php?id='
                 .$appointment['ticket_id'].'">here</a> to return to the ticket.';
      }
   }
}
elseif (isset($_GET['completed_appointment_id']) && $_GET['form_submitted'] == 'complete_appointment') {
   $appointment_result = GetAppointmentInfo($_GET['completed_appointment_id']);
       
   if (!empty($appointment_result)) {
      $appointment = mysql_fetch_array($appointment_result);
      $statuses = array('New', 'Open', 'Stalled', 'Resolved');
      global $username;
          
      if (GetIsDatetimePast($appointment['appointment_id'])) {
         $comment_body = "<br>Appointment ID: ".$appointment['appointment_id'];
         $comment_body .= "<br>Appointment Status: Completed";
         $comment_body .= "<br>Consultant NetID: ";
         $comment_body .= $appointment['consultant_netid'];
         $comment_body .= "<br>Date/Time: ".$appointment['datetime'];
         $comment_body .= "<p>".$_GET['comment_body'];
         AddComment($appointment['ticket_id'],$statuses[$_GET['ticket_status']], "",$username, $_GET['comment_subject'], "",$comment_body, "");
         LinkNewComment($appointment['appointment_id'],$appointment['ticket_id']);
         UpdateAppointment($_GET['completed_appointment_id'],$appointment['building_number'],$appointment['room_number'], "Completed", $appointment['consultant_netid']);
         $output .= 'Appointment has been completed.<br>Click '
                 .'<a class="main" href="ticket.php?id='
                 .$appointment['ticket_id'].'">here</a> to return to the ticket.';
      }
          
      else{
         $output .= 'Appointment cannot be completed until the scheduled time has '
                 .'passed.<br>Click <a class="main" href="ticket.php?id='
                 .$appointment['ticket_id'].'">here</a> to return to the ticket.';
      }
   }
}
    
elseif (isset($_GET['completed_appointment_id'])) {
   $appointment_result = GetAppointmentInfo($_GET['completed_appointment_id']);
       
   if ($appointment_result) {
      $appointment = mysql_fetch_array($appointment_result);
       
      if (GetIsDatetimePast($appointment['appointment_id'])) {
         $building_numbers = GetBuildingNumbers();
         $building_names = GetBuildings();
         $ticket_statuses = array('New', 'Open', 'Stalled', 'Resolved');
         $ticket = mysql_fetch_array(GetTicketInfo($appointment['ticket_id']));
         for ($i = 0; $i < sizeof($building_numbers); $i++) {
            $buildings[$building_numbers[$i]] = $building_names[$i];
         }
         $output .= StartForm($action, array("method" => "get"))
                 .HiddenField("form_submitted", "complete_appointment")
                 .HiddenField("completed_appointment_id", $appointment['appointment_id'])
                 .StartFormTable()
                 .FormRow("Appointment ID", $appointment['appointment_id'])
                 .FormRow("Building", $buildings[$appointment['building_number']])
                 .FormRow("Room Number", $appointment['room_number'])
                 .FormRow("Date", $appointment['datetime'])
                 .FormRow("Consultant", $appointment['consultant_netid'])
                 .FormRow("Ticket", '<a class="main" href="ticket.php?id='. $appointment['ticket_id'].'">'. $appointment['ticket_id'].'</a>')
                 .FormRow("Ticket Status", SelectField("ticket_status", $ticket_statuses, $ticket['current_status']))
                 .HiddenField("comment_subject", $appointment['consultant_netid'] ." has completed an appointment")
                 .FormRow("Add a Comment", TextareaField("comment_body"))
                 .FormRow("", SubmitField("Complete Appointment", ""));
      }
          
      else {
         $output .= ErrorReport ('Appointment cannot be completed until the scheduled time has passed.', "50%")
                 .'Click <a class="main" href="ticket.php?id='
                 .$appointment['ticket_id'].'">here</a> to return to the ticket.';
      }
   }
}
    
elseif (isset($_GET['appointment_id'])) {
   global $username;
   $input = $_GET;
   $appointment_result = GetAppointmentInfo($input['appointment_id']);
       
   if (!empty($appointment_result)) {
      $appointment = mysql_fetch_array($appointment_result);
      $schedulable_groups = GetSchedulableGroupsForUser($username);
      if ($appointment['status'] != "Scheduled") {
         $building_numbers = GetBuildingNumbers();
         $building_names = GetBuildings();
      } 

      else {
         $building_numbers = GetBuildingNumbersForUser($appointment['consultant_netid']);
         $building_names = GetBuildingsForUser($appointment['consultant_netid']);
      }
          
      $real_hours = GetHours();
      $complete_hours = array(
         '12:00AM', '12:30AM', '1:00AM', '1:30AM', '2:00AM', '2:30AM',
         '3:00AM', '3:30AM', '4:00AM', '4:30AM', '5:00AM', '5:30AM', '6:00AM',
         '6:30AM', '7:00AM', '7:30AM', '8:00AM', '8:30AM', '9:00AM', '9:30AM',
         '10:00AM', '10:30AM', '11:00AM', '11:30AM', '12:00PM', '12:30PM',
         '1:00PM', '1:30PM', '2:00PM', '2:30PM', '3:00PM', '3:30PM', '4:00PM',
         '4:30PM', '5:00PM', '5:30PM', '6:00PM', '6:30PM', '7:00PM', '7:30PM',
         '8:00PM', '8:30PM', '9:00PM', '9:30PM', '10:00PM', '10:30PM',
         '11:00PM', '11:30PM');
          
      for ($i = 0; $i < sizeof($building_numbers); $i++) {
         $buildings[$building_numbers[$i]] = $building_names[$i];
      }
          
      $ticket_statuses = array('New', 'Open', 'Stalled', 'Resolved');
      $statuses = array('Not Yet Scheduled', 'Scheduled', 'Missed - User', 'Missed - Consultant', 'Cancelled');
      $netids = GetEditableGroupsForUser($username);
          
      if ($input['form_submitted'] == "edit_appointment") {
         if (!empty($input['netid'])) $netid = $netids[$input['netid']];
         else  $netid = $appointment['consultant_netid'];

         if (($statuses[$input['status']] == "Missed - User" || $statuses[$input['status']] == "Missed - Consultant") && !GetIsDatetimePast($input['appointment_id'])) {
	   $input['status'] = $appointment['status'];
	   $output .= ErrorReport ("Appointment cannot be missed until the date/time has passed", "50%");
         }
             
         else {
            UpdateAppointment($input['appointment_id'], $input['building'], $input['room_number'], $statuses[$input['status']], $netid);
         }
             
         if ($statuses[$input['status']] == "Not Yet Scheduled" || $statuses[$input['status']] == "Cancelled") {
            UnscheduleAppointment($appointment_id, $statuses[$input['status']]);
         }
             
         $appointment_result = GetAppointmentInfo($input['appointment_id']);
         $appointment = mysql_fetch_array($appointment_result);
         $comment_body = "<br>Appointment ID: ".$appointment['appointment_id'];
         $comment_body .= "<br>Appointment Status: ".$appointment['status'];
         $comment_body .= "<br>Consultant NetID: ";
         $comment_body .= $appointment['consultant_netid'];
         $comment_body .= "<br>Date/Time: ".$appointment['datetime'];
         $comment_body .= "<p>".$_GET['comment_body'];
             
         AddComment($appointment['ticket_id'], $ticket_statuses[$_GET['ticket_status']], "", $username, $_GET['comment_subject'], "", $comment_body, "");
         LinkNewComment($appointment['appointment_id'], $appointment['ticket_id']);
            $output .= 'Appointment has been updated.<br>Click ';
            $output .= '<a class="main" href="ticket.php?id=';
            $output .= $appointment['ticket_id'].'">here</a> to return to the ticket.';
         }
          
      }
       
      if (!empty($appointment_result) && $input['form_submitted'] == "schedule_appointment") {
          
         $schedulable_groups = GetSchedulableGroupsForUser($username);
          
         if (!empty($_GET['hour']) && $input['selected_day'] == $input['day']) {
            $datetime = date("Ymd", $_GET['day']). (str_replace(':', '', $real_hours[$input['hour']])."00");
             
            $appointment_person = GetPersonToSchedule(date("l", $input['day']),
               $input['hour'], $datetime,
               $schedulable_groups[$input['group']],
               $buildings[$input['building']]);
            if (isset($appointment_person)){
               $appointment_scheduled = SetAppointment($input['appointment_id'],
                  $appointment_person, $datetime, $appointment['ticket_id']);
             
               if ($appointment_scheduled) {
                  $output .= "<tr><td colspan=3> Appointment has been scheduled for " .$appointment_person.' on '.date("l, F j, Y", $input['day'])
                     .' at '.$complete_hours[$input['hour']].'
                     .<br>Click <a class="main" href="ticket.php?id=' .$appointment['ticket_id'] .'">here</a> to return to the ticket.</td></tr>';
               }
             
               else{
                  $output .= "A database error has occured.";
               }
             
            }
            else {
               $input['form_submitted'] = "error";
            }
         }
          
         if (!(!empty($_GET['hour']) && $input['selected_day'] == $input['day']) || $input['form_submitted'] == "error"){
            if ($input['form_submitted'] == "error"){
               error_report("Selected time has already been scheduled, please choose another.","50%");
            }
            $output .= StartForm($action, array("method" => "get"));
            $output .= StartFormTable();
            $output .= FormRow("Appointment ID", HiddenField("appointment_id",
               $input['appointment_id']).$input['appointment_id']);
             
            $output .= FormRow("Ticket", '<a class="main" href="ticket.php?id=' .$appointment['ticket_id'].'">'.$appointment['ticket_id'] .'</a>');
             
            if (!empty($appointment['building_number']) && $appointment['building_number'] > 0 && empty($input['building'])){
            $output .= FormRow("Building", SelectField("building", $buildings,
               $buildings[$appointment['building_number']]));
            }
            else {
            $output .= FormRow("Building", SelectField("building", $buildings,
               $buildings[$input['building']]));
            }
            $output .= FormRow("Room Number", TextField("room_number",
               $appointment['room_number']));
             
            $output .= FormRow("Visitor Type", SelectField("group",
               GetSchedulableGroupsForUser($username)));
             
            $output .= HiddenField("appointment_id", $appointment_id);
            $output .= HiddenField("form_submitted", "schedule_appointment");
            $output .= FormRow("Date", GenerateAppointmentDateSelection($input['day']));
             
            if (!empty($input['day'])) {
               $output .= HiddenField("selected_day", $input['day']);
                
               if ($input['day'] < time()) {
                  global $HOURS_BEFORE;
                  $FIRST_APPOINTMENT = ((date("H")+1)+$HOURS_BEFORE) * 2;
               }
                
               else global $FIRST_APPOINTMENT;
               global $LAST_APPOINTMENT;
                
               for ($i = $FIRST_APPOINTMENT; $i <= $LAST_APPOINTMENT; $i++) {
                  $testdatetime = date("Ymd", $input['day']). (str_replace(':', '', $real_hours[$i])."00");
                   
                  $testperson = GetPersonToSchedule(date("l", $input['day']), $i,
                     $testdatetime, $schedulable_groups[$input['group']],
                     $buildings[$input['building']]);
                   
                  if (!empty($testperson)) {
                     $hours[$i] = $complete_hours[$i];
                  }
                   
               }
                
               if (!empty($hours)) {
                  $output .= "<tr><td><b>Time</b></td><td>&nbsp;</td><td>";
                  $output .= SelectField("hour", $hours, $hours[$input['hour']]);
                  $output .= "</td></tr>";
               }
                
            }
             
            $output .= FormRow("", SubmitField());
            $output .= EndFormTable();
         }
          
      }
       
      if (!empty($appointment_result) && empty($input['form_submitted'])) {
         $output .= StartForm($action, array("method" => "get"));
         $output .= StartFormTable();
         $output .= FormRow("Appointment ID", HiddenField("appointment_id",
            $input['appointment_id']).$input['appointment_id']);
          
         $output .= FormRow("Ticket", '<a class="main" href="ticket.php?id=' .$appointment['ticket_id'].'">'.$appointment['ticket_id'] .'</a>');
          
         if (!empty($buildings)) {
            $output .= FormRow("Building", SelectField("building", $buildings,
               $buildings[$appointment['building_number']]));
         }
          
         $output .= FormRow("Room Number", TextField("room_number",
            $appointment['room_number']));
          
         if ($appointment['status'] != "Not Yet Scheduled") {
            $output .= FormRow("Status", SelectField("status", $statuses,
               $appointment['status']));
             
            $output .= HiddenField("form_submitted", "edit_appointment");
             
            if (!empty($netids) && in_array($appointment['consultant_netid'], $netids)) {
                
               $output .= FormRow("Consultant", SelectField("netid", $netids,
                  $appointment['consultant_netid']));
            }
             
            elseif(!empty($appointment['consultant_netid'])) {
               $output .= FormRow("Consultant", $appointment['consultant_netid']);
            }
             
            $ticket_statuses = array('New', 'Open', 'Stalled', 'Resolved');
            $ticket = mysql_fetch_array(GetTicketInfo($appointment['ticket_id']));
            $output .= FormRow("Ticket Status", SelectField("ticket_status",
               $ticket_statuses, $ticket['current_status']));
             
            $output .= HiddenField("comment_subject", "Appointment has been edited.");
            $output .= FormRow("Add a Comment", TextareaField("comment_body"));
         }
          
         else
            {
            $output .= HiddenField("form_submitted", "schedule_appointment");
            $output .= FormRow("Visitor Type", SelectField("group",
               GetSchedulableGroupsForUser($username)));
             
            $output .= FormRow("Date", GenerateAppointmentDateSelection($input['day']));
         }
          
         $output .= FormRow("", SubmitField());
         $output .= EndFormTable();
      }
       
   }
    
   else{
      $output .= ErrorReport("Invalid Appointment ID", "50%");
   }
    
print $output;
?>
