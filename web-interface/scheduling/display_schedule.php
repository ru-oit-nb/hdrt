<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("scheduling/functions/functions-calendar.php");
require_once("scheduling/functions/functions-display.php");
require_once("scheduling/functions/functions-getters.php");
 
require_once("functions/functions-forms.php");
//
// Filename: display_schedule.php
// Description: Displays Potential and Actual Schedules of users
// Supprted Language(s):   PHP 4.0
//
global $username;
$allowed_users = GetViewableUsersForUser($username);
$user_default = (empty($_GET['id'])) ? $username :
$allowed_users[$_GET['id']];
$output = StartForm($action, array("method" => "get"))
        .HiddenField("function", $_GET['function'])
        .SelectField("id", $allowed_users, $user_default)
        .SubmitField("", "Select User")
        .EndForm()."<br>";
print $output;
    
if (!empty($allowed_users[$_GET['id']])) {
   global $START_HOUR;
   global $END_HOUR;
   $output = "<table cellspacing=20 cellpadding=0 border=0 width=100%><tr valign=top>"
           ."<tr><td align=center>".DisplayGenericWeek($START_HOUR, $END_HOUR);
       
   if (empty($_GET['first_day'])) {
      $today = date("j n Y", time());
      $date_array = explode(" ", $today);
   }
       
   else{
      $date_array[0] = $_GET['first_day'];
      $date_array[1] = $_GET['month'];
      $date_array[2] = $_GET['year'];
   }
       
   $output .= "</td></tr><tr><td>";
   $output .= DisplayClickableWeek($date_array[1], $date_array[2], $date_array[0],
      $START_HOUR, $END_HOUR, "Busch",
      $allowed_users[$_GET['id']], 1, 0,
      $allowed_users[$_GET['id']]);
       
   $output .= "</td></tr></table>";
   print $output;
}
?>
