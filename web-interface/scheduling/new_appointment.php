<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//
// Filename: new_appointment.php
// Description: Allows new appointments to be created and scheduled.
// Supprted Language(s):   PHP 4.0
//

// Required Files
require_once("scheduling/functions/functions-getters.php");
require_once("scheduling/functions/functions-new-appointment.php");
require_once("scheduling/functions/functions-setters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-forms.php");    
$buildings = GetBuildings();
global $username;
$input = $_GET;
$action = $_SERVER['buildings.php']."?";
$schedulable_groups = GetSchedulableGroupsForUser($username);
$output = ""; 
$complete_hours = array('12:00AM', '12:30AM', '1:00AM', '1:30AM', '2:00AM', '2:30AM',
   '3:00AM', '3:30AM', '4:00AM', '4:30AM', '5:00AM', '5:30AM', '6:00AM',
   '6:30AM', '7:00AM', '7:30AM', '8:00AM', '8:30AM', '9:00AM', '9:30AM',
   '10:00AM', '10:30AM', '11:00AM', '11:30AM', '12:00PM', '12:30PM',
   '1:00PM', '1:30PM', '2:00PM', '2:30PM', '3:00PM', '3:30PM', '4:00PM',
   '4:30PM', '5:00PM', '5:30PM', '6:00PM', '6:30PM', '7:00PM', '7:30PM',
   '8:00PM', '8:30PM', '9:00PM', '9:30PM', '10:00PM', '10:30PM',
   '11:00PM', '11:30PM');
$real_hours = GetHours();
foreach ($_GET as $k => $v) {
   $action .= "$k=" . urlencode($v) . "&";
}
if (isset($input['building_name'])) {
   $name = $buildings[$input['building_name']];
   $building_number = GetValueFromTable("building_number", "buildings",
      "where building_name='$name'");
}
if (!empty($input['id']) && empty($input['ticket_id'])) {
   $ticket_id = $input['id'];
}
elseif (!empty($input['ticket_id'])) {
   $ticket_id = $input['ticket_id'];
}
    
if (isset($ticket_id) && GetSchedulingPermissionsForTicket($ticket_id)) {
   $output .= StartForm($action, array("method" => "get"));
   $output .= StartFormTable();
       
      if ($input['form_submitted'] == "schedule_appointment") {
         if (!empty($_GET['hour']) && $input['selected_day'] == $input['day']) {
            $datetime = date("Ymd", $_GET['day']).(str_replace(':', '',
               $real_hours[$input['hour']])."00");
            $appointment_person = GetPersonToSchedule(date("l", $input['day']),
               $input['hour'], $datetime,
               $schedulable_groups[$input['group']],
               $buildings[$input['building_name']]);
            if (!empty($appointment_person)){
               $appointment_scheduled = SetAppointment($input['appointment_id'],
                  $appointment_person, $datetime, $ticket_id);
               if ($appointment_scheduled) {
                  global $username;
                  $comment_body = "<br>Appointment ID: ".$input['appointment_id'];
                  $comment_body .= "<br>Appointment Status: Scheduled";
                  $comment_body .= "<br>Consultant NetID: ";
                  $comment_body .= $appointment_person;
                  $comment_body .= "<br>Date/Time: ".date("Y-m-d ", $input['day']).$real_hours[$input['hour']].":00";
                
                  AddComment($input['ticket_id'],
                     "Open", "",
                     $username, "Appointment has been scheduled.", "",
                     $comment_body, "");
                
                  LinkNewComment($input['appointment_id'],
                     $input['ticket_id']);
                
                  $output .= "<tr><td colspan=3> Appointment has been scheduled for " .$appointment_person.' on '.date("l, F j, Y",
                     $input['day']).' at '.$complete_hours[$input['hour']].'
                     .<br>Click <a class="main" href="ticket.php?id=' .$ticket_id.'">here</a> to return to the ticket.</td></tr>';
               }
             
               else
                  {
                  $output .= "A database error has occured.";
               }
            }
            else {
               $input['form_submitted']="error";
            } 
         }
          
            
         if (!(!empty($_GET['hour']) && $input['selected_day'] == $input['day']) || $input['form_submitted']=="error") {
            if ($input['form_submitted']=="error"){
               $output .= "<tr><td colspan=3>";
               $output .= ErrorReport("Selected time has already been scheduled, please choose another.", "50%");
            }
            $output .= HiddenField("form_submitted", "schedule_appointment");
            $output .= FormRow("Ticket", HiddenField("ticket_id", $ticket_id). '<a class="main" href="ticket.php?id='. $ticket_id.'">'.$ticket_id);
            $output .= FormRow("Building", HiddenField("building_name",
               $input['building_name']).$buildings[$input['building_name']]);
            $output .= FormRow("Room Number", HiddenField("room_number",
               $input['room_number']).$input['room_number']);
            $output .= FormRow("Visitor Type", HiddenField("group", $input['group'])
            .$schedulable_groups[$input['group']]);
            $output .= HiddenField("appointment_id", $appointment_id);
            $output .= HiddenField("selected_day", $input['day']);
            $output .= FormRow("Date", GenerateAppointmentDateSelection($input['day']));
            if ($input['day'] < time()) {
               global $HOURS_BEFORE;
               $FIRST_APPOINTMENT = ((date("H")+1)+$HOURS_BEFORE) * 2;
            } else {
               global $FIRST_APPOINTMENT;
            }
            global $LAST_APPOINTMENT;
             
            for ($i = $FIRST_APPOINTMENT; $i <= $LAST_APPOINTMENT; $i++) {
               $testdatetime = date("Ymd", $input['day']).(str_replace(':', '',
                  $real_hours[$i])."00");
               $testperson = GetPersonToSchedule(date("l", $input['day']), $i,
                  $testdatetime, $schedulable_groups[$input['group']],
                  $buildings[$input['building_name']]);
               if (!empty($testperson)) {
                  $hours[$i] = $complete_hours[$i];
               }
                
            }
             
            if (!empty($hours)) {
               $output .= "<tr><td><b>Time</b></td><td>&nbsp;</td><td>";
               $output .= SelectField("hour", $hours, $hours[$input['hour']]);
               $output .= "</td></tr>";
            }
             
         }
          
      }
       
       
      elseif ($input['form_submitted'] == "appointment_basics") {
         $output .= HiddenField("form_submitted", "schedule_appointment");
         $output .= FormRow("Ticket", HiddenField("ticket_id", $ticket_id). '<a class="main" href="ticket.php?id='.$ticket_id. '">'.$ticket_id);
         $output .= FormRow("Building", HiddenField("building_name",
            $input['building_name']).$buildings[$input['building_name']]);
         $output .= FormRow("Room Number", HiddenField("room_number", $input['room_number'])
         .$input['room_number']);
         $output .= FormRow("Visitor Type", HiddenField("group", $input['group'])
         .$schedulable_groups[$input['group']]);
          
         if (empty($input['appointment_id'])) {
            $appointment_id = CreateAppointment($ticket_id, $building_number,
               $input['room_number']);
         }
          
         else {
            $appointment_id = $input['appointment_id'];
         }
          
         $output .= HiddenField("appointment_id", $appointment_id);
         $output .= FormRow("Date", GenerateAppointmentDateSelection($input['day']));
      }
       
      else {
         $output .= HiddenField("form_submitted", "appointment_basics");
         $output .= FormRow("Ticket", HiddenField("ticket_id", $ticket_id). '<a class="main" href="ticket.php?id='.$ticket_id.'">' .$ticket_id);
         $output .= FormRow("Building", SelectField("building_name", $buildings,
            $input['building_name']));
         $output .= FormRow("Room Number", TextField("room_number", $input['room_number']));
         $output .= FormRow("Visitor Type", SelectField("group", $schedulable_groups,
            $input['group']));
      }
       
      if (!isset($appointment_scheduled) || $appointment_scheduled != 1) {
         $output .= FormRow("", SubmitField("", "Submit"));
      }
       
      $output .= EndFormTable();
      $output .= EndForm();
   }
    
   elseif (isset($ticket_id)) {
      $output .= 'The queue of the selected <a href="../ticket.php?id='.$ticket_id .'">ticket</a> does not allow scheduling.';
   }
    
   else {
      $output .= "Invalid Ticket ID";
   }

print $output;
?>
