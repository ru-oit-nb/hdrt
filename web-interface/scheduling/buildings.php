<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("scheduling/functions/functions-buildings.php");
require_once("scheduling/functions/functions-display.php");
require_once("functions/functions-forms.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
//
// Filename:               buildings.php
// Description:            Maintain Building Database
//                         for ruQueue Scheduling System
// Supprted Language(s):   PHP 4.0
//
$output = "";
$buildings = GetBuildings();
$building_groups = GetBuildingGroups();
$campuses = GetCampuses();
$actions = array('Display Buildings', 'Create New Building',
		 'Edit Building Details', 'Delete Buildings',
		 'Edit Building Groups');
$input = $_GET;
$action = $_SERVER['buildings.php']."?";
    
if (isset($input['building_name'])) {
  $name = addslashes($buildings[$input['building_name']]);
  $building_number = GetValueFromTable("building_number", "buildings", "where building_name='$name'");
}
    
if ($input['building_action'] == 'new_building' && $input['action'] == 1) {
  if ($input['choose_campus'] == "db") $campus = $input['campus_db'];
  else {
    CreateCampus($input['campus_text']);
    $campuses = GetCampuses();
    $campus = array_search($input['campus_text'], $campuses)+1;
  }
  if ($input['choose_building_group'] == "db") {
    $output .= CreateNewBuilding($input['building_name'], $campus, $building_groups[$input['building_group_db']]);
  }
       
  else {
    $output .= CreateNewBuilding($input['building_name'], $campus, $input['building_group_text']);
  }
  $updated = 1;
}
    
if ($input['building_action'] == 'edit_building' && $input['action'] == 2) {
  if ($input['choose_campus'] == "db") $campus = $input['campus_db'];
  else {
    CreateCampus($input['campus_text']);
    $campuses = GetCampuses();
    $campus = array_search($input['campus_text'], $campuses);
  }
  if ($input['choose_building_group'] == "db") {
    $output .= EditBuilding($building_number, $input['new_building_name'],
			    $campuses[$campus],
			    $building_groups[$input['new_building_group_db']]);
  }
       
  else {
    $output .= EditBuilding($building_number, $input['new_building_name'],
			    $campuses[$campus],
			    $input['new_building_group_text']);
  }
  $updated = 1;
}
    
if ($input['building_action'] == 'delete_building' && $input['action'] == 3) {
  $building_numbers = GetBuildingNumbers();
  foreach($checked as $del) {
    $building_number = $building_numbers[$del];
    $output .= DeleteBuilding($building_number);
  }
  $buildings = GetBuildings();
}
    
if ($input['building_action'] == 'edit_building_group' && $input['action'] == 4) {
  $new = $input['new_building_group'];
  $old = $building_groups[$input['building_group']];
  $output .= EditBuildingGroup($old, $new);
  $building_groups = GetBuildingGroups();
}
    
$output .= StartForm($action, array("method" => "get"))
     .HiddenField("global", "Buildings")
     .HiddenField("item", "Global")
     .HiddenField("hide", $_GET['hide'])
     .StartFormTable()
     .FormRow("Action", SelectField("action", $actions, $input['action']));
    
     if ($input['action'] == 1 && $updated != 1) {
       $building_groups = GetBuildingGroups();
       $output .= FormRow("Building Name", TextField("building_name", $input['building_name'], 30));

       $num_campus = count($campuses);
       if ($num_campus == 1 && !empty($campuses[0])) {
	 $campus_row = RadioField("choose_campus", "text", "", "")
	   .TextField("campus_text", $input['campus_text'], 30)."<br>"
	   .RadioField("choose_campus", "db", "", "")
	   .HiddenField("campus_db", 0)
	   .$campuses[0]."<br>";
       }
       elseif ($num_campus > 1) {
	 $campus_row = RadioField("choose_campus", "text", "", "")
	   .TextField("campus_text", $input['campus_text'], 30)."<br>"
	   .RadioField("choose_campus", "db", "", "")
	   .SelectField("campus_db", $campuses);
       }
       else {
	 $campus_row = TextField("campus_text", $input['campus_text']);
       }
       $output .= FormRow("Campus", $campus_row);

       if (sizeof($building_groups) == 1 && $building_groups[0] != "") {
	 $groups_row = RadioField("choose_building_group", "text", "", "");
	 $groups_row .= TextField("building_group_text", $input['building_group_text'], 30) . "<br>";
	 $groups_row .= RadioField("choose_building_group", "db", "", "");
	 $groups_row .= HiddenField("building_group_db", "0");
	 $groups_row .= $building_groups[0] . "<br>";
       }
       
       elseif (sizeof($building_groups) > 1) {
	 $groups_row = RadioField("choose_building_group", "text", "", "");
	 $groups_row .= TextField("building_group_text", $input['building_group_text'], 30) . "<br>";
	 $groups_row .= RadioField("choose_building_group", "db", "", "");
	 $groups_row .= SelectField("building_group_db", $building_groups);
       }
       
       else{
	 $groups_row = TextField("building_group_text", $input['building_group_text'], 30);
       }
       $output .= FormRow("Building Group", $groups_row)
	 .HiddenField ("building_action", "new_building");
     }
    
if ($input['action'] == 2) {
  $buildings = GetBuildings();
       
  if (!isset($_GET['building_name']) || $_GET['building_name'] == "") {
    $output .= FormRow("Building Name", SelectField("building_name", $buildings, $input['building_name']));
  }
       
  if (isset($_GET['building_name']) && $updated != 1 && $_GET['building_action'] != 'new_building' && $_GET['building_name'] != "") {
    $building_details = GetBuildingArray($building_number);
          
    $output .= FormRow("Building Name", TextField("new_building_name", $building_details[1], 30));

       $num_campus = count($campuses);
       if ($num_campus == 1 && !empty($campuses[0])) {
	 $campus_row = RadioField("choose_campus", "text", "", "")
	   .TextField("campus_text", $building_details[2], 30)."<br>"
	   .RadioField("choose_campus", "db", "", "")
	   .HiddenField("campus_db", 0)
	   .$campuses[0]."<br>";
       }
       elseif ($num_campus > 1) {
	 $campus_row = RadioField("choose_campus", "text", "", "")
	   .TextField("campus_text", $building_details[2], 30)."<br>"
	   .RadioField("choose_campus", "db", "", "_")
	   .SelectField("campus_db", $campuses);
       }
       else {
	 $campus_row = TextField("campus_text", $input['campus_text']);
       }
       $output .= FormRow("Campus", $campus_row);

    if (sizeof($building_groups) == 1 && $building_groups[0] != "") {
      $groups_row = RadioField("choose_building_group", "text", "", "");
      $groups_row .= TextField("new_building_group_text", $building_details[3], 30) . "<br>";
      $groups_row .= RadioField("choose_building_group", "db", "", "");
      $groups_row .= TextField("new_building_group_db", $building_groups[0], 30) . "<br>";
    }
          
    if (sizeof($building_groups) > 1) {
      $groups_row = RadioField("choose_building_group", "text", "", "");
      $groups_row .= TextField("new_building_group_text", $building_details[3], 30) . "<br>";
      $groups_row .= RadioField("choose_building_group", "db", "", "_");
      $groups_row .= SelectField("new_building_group_db", $building_groups);
    }
          
    else {
      $groups_row = TextField("new_building_group_text", $building_details[3], 30);
    }
          
    $output .= FormRow("Building Group", $groups_row)
      .HiddenField ("building_number", $building_number)
      .HiddenField ("building_action", "edit_building");
  }
       
  if (isset($_GET['building_name']) && $updated == 1) {
    $building_details = GetBuildingArray($building_number);
    $output .= FormRow("Building Name", $building_details[1])
      .FormRow("Campus", $building_details[2])
      .FormRow("Building Group", $building_details[3]);
  }
}
    
if ($input['action'] == 3) {
  $output .= DisplayDeleteBuildings()
    .HiddenField ("building_action", "delete_building")
    .EndFormTable();
  if (DoesUserHaveRight($username, "global")) {
    $output .= "<div align=right><br><br>" . SubmitField("", "Submit") . "</div>";
  }
  $output .= EndForm();
}
    
if ($input['action'] == 4 && sizeof($building_groups > 0)) {
  $output .= FormRow("Building Group", SelectField("building_group", $building_groups), $input['building_group']);
  if (isset($input['building_group'])) {
    $output .= FormRow("Rename To ", TextField("new_building_group", $input['new_building_group'], 30))
      .HiddenField ("building_action", "edit_building_group");
  }
}
    
if ($input['action'] != 3) {
  if (DoesUserHaveRight($username, "global")) {
    $output .= FormRow("", SubmitField("", "Submit"));
  }
  $output .= EndFormTable().EndForm().DisplayBuildings();
}

print $output;
?>
