<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

require_once("scheduling/functions/functions-display.php");
require_once("scheduling/functions/functions-setters.php");
 
require_once("functions/functions-forms.php");
   //
   // Filename: edit_schedule.php
   // Description: Functions for the editing of potential schedules
   // Supprted Language(s):   PHP 4.0
   //
    
   global $username;
   $people = GetEditableGroupsforUser($username);
   if ($_POST['clear'] == "Clear User Schedule" && !empty($people[$_POST['id']])) {
      global $db;
      $query = "delete from potential_schedule where netid='".$people[$_POST['id']]."'";
      mysql_query($query, $db);
   }
    
   if ($_POST['form_submitted'] == "Update_Schedule") {
      $sun = $mon = $tue = $wed = $thu = $fri = $sat = 0;
       
      for ($i = 0; $i < 47; $i++) {
         if (!empty($Sun[$i])) {
            $sun = $sun + pow(2, $Sun[$i]);
         }
          
         if (!empty($Mon[$i])) {
            $mon = $mon + pow(2, $Mon[$i]);
         }
          
         if (!empty($Tue[$i])) {
            $tue = $tue + pow(2, $Tue[$i]);
         }
          
         if (!empty($Wed[$i])) {
            $wed = $wed + pow(2, $Wed[$i]);
         }
          
         if (!empty($Thu[$i])) {
            $thu = $thu + pow(2, $Thu[$i]);
         }
          
         if (!empty($Fri[$i])) {
            $fri = $fri + pow(2, $Fri[$i]);
         }
          
         if (!empty($Sat[$i])) {
            $sat = $sat + pow(2, $Sat[$i]);
         }
          
      }
       
      $person = $people[$_POST['id']];
       
      if (!empty($person)) {
         $availability = array($sun, $mon, $tue, $wed, $thu, $fri, $sat);
         SetHours($person, $availability);
      }
       
   }
    
   $user_default = (empty($_POST['id'])) ? $username :
   $people[$_POST['id']];
   $function = (isset($_GET['function'])) ? $_GET['function'] :
   $_POST['function'];
   $output .= StartForm($action, array("method" => "post"));
   $output .= SelectField("id", $people, $user_default);
   $output .= HiddenField("function", $function);
   $output .= SubmitField("", "Select User")."&nbsp;";
   if (!empty($people[$_POST['id']])) {
      $output .= SubmitField("clear", "Clear User Schedule");
   }
   $output .= EndForm();
    
   if (!empty($people[$_POST['id']])) {
      global $START_HOUR;
      global $END_HOUR;
      $output .= DisplayCheckboxWeek($START_HOUR, $END_HOUR);
   }

print $output;    
?>

