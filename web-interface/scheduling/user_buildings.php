<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("scheduling/functions/functions-display.php");
require_once("scheduling/functions/functions-setters.php");
 
require_once("functions/functions-forms.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
//
// Filename:               user_buildings.php
// Description:            Maintain User/Building Permissions Database
//                         for ruQueue Scheduling System
// Supprted Language(s):   PHP 4.0
//
$netids = GetStaff();
$building_groups = GetBuildingGroups();
$campuses = GetCampuses();
    
$action = $_SERVER['buildings.php']."?";
$input = $_GET;
    
$print .= StartForm($action, array("method" => "get"));
    
$print .= StartFormTable();
$print .= HiddenField("global", "User_Building_Permissions");
$print .= HiddenField("item", "Global");
$print .= HiddenField("hide", $_GET['hide']);
    
if (isset($checked)) {
   $del_netids = GetArrayFromTable("netid", "user_buildings", "order by netid");
   $del_group_types = GetArrayFromTable("group_type", "user_buildings", "order by netid");
   $del_group_values = GetArrayFromTable("group_value", "user_buildings", "order by netid");
   $i = 0;
   foreach($checked as $del) {
      $query = "delete from user_buildings where ";
      $query .= "netid='$del_netids[$del]' and ";
      $query .= "group_type='$del_group_types[$del]' and ";
      $query .= "group_value='$del_group_values[$del]'";
      $result = mysql_query($query, $db);
      if (mysql_errno()) {
         $errors[$i] = mysql_error();
         $i++;
      }
   }
       
   if ($i == 0) {
      $print .= "Permissions Deleted Successfully";
   } else {
      for ($j = 0; $j < $i; $j++) {
         $print .= $errors[$j];
      }
   }
   $print .= "<p>";
}
    
if ($input['user_buildings_action'] == "edit_user_buildings" && $input['netid'] >= 0) {
   $group_type = $input['group_type'];
      
   if ($group_type == "campus") {
      $netid = $netids[$input['netid']];
      $campus = $campuses[$input['campus']];
      $print .= SetUserBuildings($netid, $group_type, $campus);
   }
       
   if ($group_type == "building_group") {
      $netid = $netids[$input['netid']];
      $building_group = $building_groups[$input['building_group']];
      $print .= SetUserBuildings($netid, $group_type, $building_group);
   }
}
    
$print .= "<p>";
$print .= FormRow("NetID", SelectField("netid", $netids, $input['netid']));
    
if (sizeof($building_groups) > 0) {
   $print .= "<tr><td>";
   $print .= RadioField("group_type", "campus", "", $input['group_type']);
   $print .= "<b>Campus</b></td><td>&nbsp;</td><td>";
   $print .= SelectField("campus", $campuses, $input['campus']);
   $print .= "</td></tr><tr><td colspan='3'>&nbsp;</td></tr>";
   $print .= "<tr><td>";
   $print .= RadioField("group_type", "building_group", "", $input['group_type']);
   $print .= "<b>Building Group</b></td><td>&nbsp;</td><td>";
   $print .= SelectField("building_group", $building_groups, $input['building_groups']);
   $print .= "</td></tr><tr><td colspan='3'>&nbsp;</td></tr>";
}
    
else {
   $print .= FormRow("Campus", SelectField("campus", $campuses, $input['campus']));
}
    
$print .= EndFormTable();
$print .= DisplayDeleteUserBuildings();
$print .= HiddenField("user_buildings_action", "edit_user_buildings");

if (DoesUserHaveRight($username, "global")) {
   $print .= "<br><br><div align=right>" . SubmitField("", "Submit") . "</div>";
}

$print .= EndForm();
print $print;
?>
