<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("scheduling/functions/functions-getters.php");
 
require_once("functions/functions-forms.php");
require_once("functions/functions-widgets.php");
//
// Filename: clear_all.php
// Description: allows the clearing of all schedules
// Supprted Language(s):   PHP 4.0
//
global $username, $db;
$user_in_group = GetDistinctArrayFromTable("group_name", "ingroup", "where username='$username'");
$where = "WHERE privgroup=";
if (is_array($user_in_group)) {
   foreach ($user_in_group as $or) {
      $or = str_replace("_", " ", $or);
      $where .= "'$or' OR privgroup=";
   }
   $where .= "'' AND (rule='Edit')";
   $groups = GetDistinctArrayFromTable("schedgroup", "sched_rules", $where);
} else {
   $groups = array();
}
    
if (empty($_POST['clear_group']) && empty($_POST['clear'])) {
   $output .= StartForm($_SERVER['PHP_SELF'], array("method" => "post"));
   $output .= HiddenField("function", 4);
   $output .= "<select name='clear_group'>";
   foreach ($groups as $group) {
      $output .= "<option value='$group'>$group";
   }
   $output .= "</select>";
   $output .= SubmitField("clear", "Clear Group");
   $output .= EndForm();
}
if (!empty($_POST['clear_group']) && empty($_POST['confirm'])) {
   $output .= "This will clear all users in <b>".$_POST['clear_group']."</b><br>\n";
   $output .= StartForm($_SERVER['PHP_SELF'], array("method" => "post"));
   $output .= HiddenField("clear_group", $_POST['clear_group']);
   $output .= HiddenField("function", 4);
   $output .= SubmitField("confirm", "Confirm");
   $output .= EndForm();
}
if (!empty($_POST['clear_group']) && !empty($_POST['confirm'])) {
   $group_name = $_POST['clear_group'];
   $query = "SELECT distinct username FROM ingroup WHERE group_name='$group_name'";
   $result = mysql_query($query, $db);
   $query = "DELETE from potential_schedule WHERE ";
   while ($users = mysql_fetch_array($result)) {
      $user = $users['username'];
      $query .= " netid = '$user' OR";
   }
   $query = substr($query, 0, -2);
   mysql_query($query, $db);
   $output .= "All users in ".$_POST['clear_group']." have been cleared from the potential schedule.";
   $output .= "<p>".$query;
}
print $output;
?>
