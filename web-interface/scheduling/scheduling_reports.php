<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

require_once("functions/functions-forms.php");
require_once("functions/functions-widgets.php");
require_once("functions/ldap.php");

// Filename: scheduling_reports.php
// Description: Provides function for generating reports on appointments.
// Supprted Language(s):   PHP 4.0
//
 
// --------------------------------------------------------
// Function:                Class::ShowTable
 
// Description:             creates table cell for report
//                          output
 
// Type:                    public
 
// Parameters:
//    $result               mysql resource id
//    array $fields         contains field information for
//                          output headers
 
// Return Values:
//    string $table         html of the table cell
 
// Remarks:
//    None
// --------------------------------------------------------
function ShowTable($result, $fields) {
   $table = "<table cellspacing=0 cellpadding=2 border=0 width=100%>";
   $table .= "<tr>";
   foreach ($fields as $field) {
      $link = str_replace("&order=".$_GET['order'], "", $_SERVER['QUERY_STRING']);
      $link = $_SERVER['PHP_SELF']."?".$link."&order=$field";
      if ($field == $_GET['order']) {
         $link .= "+desc";
      }
      $table .= "<td align=left><b><a href='$link' class='main'>".str_replace("_", " ", $field)."</b></a></td>";
   }
   $table .= ReportExtraLabel();
   $table .= "</tr>";
   while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
      $table .= "<tr>";
      $bgcolor = ($bgcolor == "#dcdcdc") ? "#ffffff" :
      "#dcdcdc";
      foreach ($row as $label => $field) {
         if ($label == "Id") {
            $id = $field;
         }
         $field = AdditionalFormat($label, $field);
         $table .= "<td bgcolor=$bgcolor>$field</td>";
      }
      $table .= ReportExtra($bgcolor, $id);
      $table .= "</tr>";
   }
   $table .= "</table>";
   return $table;
}
 
// --------------------------------------------------------
// Function:                Class::AdditionalFormat
 
// Description:             creates special case outputs
//                          for certain fields
 
// Type:                    public
 
// Parameters:
//    string $label         the display label of a field
//    string $field         value contained in a field
 
// Return Values:
//    string $field         new output if conditions are
//                          met, remain unchanged otherwise
 
// Remarks:
//    None
// --------------------------------------------------------
function AdditionalFormat($label, $field) {
   switch ($label) {
      case "Id":
      $field = "<a href='ticket.php?id=$field' class='main'>$field</a>";
      break;
      case "Appointment_Id":
      $field = "<a href='appointment.php?id=$field' class='main'>$field</a>";
      break;
   }
   return $field;
}
 
// --------------------------------------------------------
// Function:                Class::ReportExtra
 
// Description:             creates additional columns not
//                          present in mysql result
 
// Type:                    public
 
// Parameters:
//    string $bgcolor       current background color
//    string $id            key value if needed, empty by
//                          default
 
// Return Values:
//    string $output        html output string
 
// Remarks:
//    None
// --------------------------------------------------------
function ReportExtra($bgcolor, $id = "") {
   switch ($_GET['report_type']) {
      case "ShowNotScheduled":
      $output .= "<td bgcolor=$bgcolor>";
      $output .= "<a href='new_appointment.php?ticket_id=$id' class='main'>Schedule</a>";
      $output .= "</td>";
      break;
      default:
      $output = "";
   }
   return $output;
}
 
// --------------------------------------------------------
// Function:                Class::ReportExtraLabel
 
// Description:             creates additional header not
//                          present in mysql result
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    string $output        html output string
 
// Remarks:
//    None
// --------------------------------------------------------
function ReportExtraLabel() {
   switch ($_GET['report_type']) {
      case "ShowNotScheduled":
      $output = "<td align=left><b>Schedule</b></td>";
      break;
      default:
      $output = "";
   }
   return $output;
}
 
// --------------------------------------------------------
// Function:                Class::ReportQuery
 
// Description:             build mysql query to database
 
// Type:                    public
 
// Parameters:
//    array $fields         list of fields => display names
//    array $tables         list of queried tabled
//    string $where         where constraints
//    string $order         order characteristics
//    bool $print           flag for outputting query to screen
//    int $offset           the result offset for limit x, y
 
// Return Values:
//    $result               mysql resource id
 
// Remarks:
//    None
// --------------------------------------------------------
function ReportQuery($fields, $tables, $where = "", $order = "", $print = 0, $offset = 0) {
   global $db;
   if (is_array($fields)) {
      foreach ($fields as $name => $label) {
         $field .= "$name $label, ";
      }
      $field = substr($field, 0, -2);
   }
   else $field = $fields;
   if (is_array($tables)) {
      foreach ($tables as $name) {
         $table .= "$name, ";
      }
      $table = substr($table, 0, -2);
   }
   else $table = $tables;
   if (isset($where)) $where = "WHERE ".$where;
   if (isset($order)) $order = "ORDER BY ".$order;
   $q = "SELECT $field from $table $where $order";
   if ($print) {
      print OpenColorTable("red", "Query", "100%");
      echo "$q";
      print CloseColorTable();
   }
   $result = mysql_query($q, $db);
   return $result;
}
 
// --------------------------------------------------------
// Function:                Class::ShowNotScheduled
 
// Description:             creates report for all tickets
//                          in schedule-enabled queues with
//                          no appointment and not resolved
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    string                table cell html from function
 
// Remarks:
//    None
// --------------------------------------------------------
function ShowNotScheduled() {
   global $db;
   $result = mysql_query("select ticket.id from ticket, queue where ticket.queue = queue.q_name AND queue.allow_scheduling <> 'None' AND ticket.current_status <> 'Resolved' AND ticket.current_status <> 'Dead'", $db);
   while ($row = mysql_fetch_array($result)) {
      $set .= $row['id'].", ";
   }
   $set = substr($set, 0, -2);
   $fields = GetFields();
   $tables = GetTables();
   $where .= "ticket.first_comment_id = comment.id ";
   $where .= "AND ticket.user_id = user.id ";
   $where .= "AND ticket.id IN ($set) ";
   $where .= BuildSearch("ShowNotScheduled");
   $order = (isset($_GET['order'])) ? $_GET['order'] :
   "Created";
   $result = ReportQuery($fields["ShowNotScheduled"], $tables["ShowNotScheduled"], $where, $order);
   $table_label = CreateTableLabel(mysql_num_rows($result));
   $return = OpenColorTable("green", $table_label, "100%");
   $return .= ShowTable($result, $fields["ShowNotScheduled"]);
   return $return;
}
 
// --------------------------------------------------------
// Function:                Class::ShowAllAppointments
 
// Description:             create report for all
//                          appointments in database
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    string                table cell html from function
 
// Remarks:
//    None
// --------------------------------------------------------
function ShowAllAppointments() {
   $fields = GetFields();
   $tables = GetTables();
   $where .= "appointments.ticket_id = ticket.id ";
   $where .= "AND ticket.first_comment_id = comment.id ";
   $where .= "AND ticket.user_id = user.id";
   $where .= BuildSearch("ShowAllAppointments");
   $order = (isset($_GET['order'])) ? $_GET['order'] :
   "Created";
   $result = ReportQuery($fields["ShowAllAppointments"], $tables["ShowAllAppointments"], $where, $order);
   $table_label = CreateTableLabel(mysql_num_rows($result));
   $return = OpenColorTable("green", $table_label, "100%");
   $return .= ShowTable($result, $fields["ShowAllAppointments"]);
   return $return;
}
 
// --------------------------------------------------------
// Function:                Class::GetFields
 
// Description:             get fields needed for report
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    array $fields         list of field names
 
// Remarks:
//    None
// --------------------------------------------------------
function GetFields() {
   $no_appointment = array(
   "distinct ticket.id" => "Id",
      "CONCAT(user.name,' (',user.uid,')')" => "User",
      "comment.subject" => "Subject",
      "ticket.current_status" => "Ticket_Status",
      "ticket.queue" => "Queue",
      "ticket.owner" => "Owner",
      "date_format(comment.date_created,'%Y-%m-%d')" => "Created" );
   $all_appointment = array(
   "appointments.ticket_id" => "Id",
      "appointments.appointment_id" => "Appointment_Id",
      "CONCAT(user.name,' (',user.uid,')')" => "User",
      "comment.subject" => "Subject",
      "ticket.current_status" => "Ticket_Status",
      "appointments.status" => "Status",
      "ticket.queue" => "Queue",
      "consultant_netid" => "Consultant",
      "date_format(comment.date_created,'%Y-%m-%d')" => "Created" );
   $fields = array(
   "ShowNotScheduled" => $no_appointment,
      "ShowAllAppointments" => $all_appointment);
   return $fields;
}
// --------------------------------------------------------
// Function:                Class::GetTables
 
// Description:             get tables needed for report
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    array $tables         list of table names
 
// Remarks:
//    None
// --------------------------------------------------------
function GetTables() {
   $no_appointment = array(
   "ticket",
      "queue",
      "comment",
      "user" );
   $all_appointment = array(
   "appointments",
      "ticket",
      "comment",
      "user" );
   $tables = array(
   "ShowNotScheduled" => $no_appointment,
      "ShowAllAppointments" => $all_appointment);
   return $tables;
}
 
// --------------------------------------------------------
// Function:                Class::CreateTableLabel
 
// Description:             create header for color_table
 
// Type:                    public
 
// Parameters:
//    int $num              total number of results
 
// Return Values:
//    string                header for color_table
 
// Remarks:
//    None
// --------------------------------------------------------
function CreateTableLabel($num) {
   $offset = $_GET['offset'];
   $section_low = 1+$offset;
   $section_high = 50+$offset;
   if ($num == 0) $section_low = 0;
   if ($section_high > $num) $section_high = $num;
   return "Results $section_low - $section_high of $num results";
}
 
// --------------------------------------------------------
// Function:                Class::SearchOption
 
// Description:             create general search options
 
// Type:                    public
 
// Parameters:
//    string $name          name of field
//    bool $limit           flag to limit only to == and !=
 
// Return Values:
//    string $output        select input html
 
// Remarks:
//    None
// --------------------------------------------------------
function SearchOptions($name, $limit = 0) {
   $output = "<select name='$name'>";
   $output .= "<option value=' = '>is";
   $output .= "<option value=' <> '>is not";
   if (!$limit) {
      $output .= "<option value=' > '>is greater than";
      $output .= "<option value=' < '>is less than";
      $output .= "<option value=' like '>contains";
   }
   $output .= "</select>";
   return $output;
}
// --------------------------------------------------------
// Function:                Class::DateOptions
 
// Description:             create date search options
 
// Type:                    public
 
// Parameters:
//    string $name          name of field
 
// Return Values:
//    string $output        select input html
 
// Remarks:
//    None
// --------------------------------------------------------
function DateOptions($name) {
   $output = "<select name='$name' selected>";
   $output .= "<option value='' selected>&nbsp;";
   $output .= "<option value=' < '>Before";
   $output .= "<option value=' > '>After";
   $output .= "<option value=' = '>On";
   $output .= "</select>";
   return $output;
}
// --------------------------------------------------------
// Function:                Class::StatusOption
 
// Description:             create appointment status
//                          search options
 
// Type:                    public
 
// Parameters:
//    string $name          name of field
 
// Return Values:
//    string $output        select input html
 
// Remarks:
//    None
// --------------------------------------------------------
function StatusOption($name) {
   $output = "<select name='$name'>";
   $output .= "<option value='' selected>&nbsp;";
   $output .= "<option value=1>Not Yet Scheduled";
   $output .= "<option value=2>Scheduled";
   $output .= "<option value=3>Missed - User";
   $output .= "<option value=4>Missed - Consultant";
   $output .= "<option value=5>Cancelled";
   $output .= "<option value=6>Completed";
   $output .= "</select>";
   return $output;
}
// --------------------------------------------------------
// Function:                Class::TicketStatusOption
 
// Description:             create ticket status
//                          search options
 
// Type:                    public
 
// Parameters:
//    string $name          name of field
 
// Return Values:
//    string $output        select input html
 
// Remarks:
//    None
// --------------------------------------------------------
function TicketStatusOption($name) {
   $output = "<select name='$name'>";
   $output .= "<option value='' selected>&nbsp;";
   $output .= "<option value='New'>New";
   $output .= "<option value='Open'>Open";
   $output .= "<option value='Stalled'>Stalled";
   $output .= "<option value='Resolved'>Resolved";
   $output .= "<option value='Dead'>Dead";
   $output .= "</select>";
   return $output;
}
 
// --------------------------------------------------------
// Function:                Class::BuildSearchTable
 
// Description:             create html for search table
 
// Type:                    public
 
// Parameters:
//    none
 
// Return Values:
//    string $table         form html
 
// Remarks:
//    None
// --------------------------------------------------------
function BuildSearchTable() {
   $field_array = GetFields();
   if (isset($_GET['report_type'])) {
      $fields = $field_array[$_GET['report_type']];
      if (is_array($fields)) {
         foreach ($fields as $field) {
            $table .= "<tr>";
            switch ($field) {
               case "Created":
               $table .= "<td align=right>".$field."</td><td>".DateOptions($field."_option1", 1)."</td><td>".TextField($field."_1", "", 20)."&nbsp;YYYY-MM-DD</td>";
               $table .= "</tr><tr><td align=right></td><td>".DateOptions($field."_option2", 1)."</td><td>".TextField($field."_2", "", 20)."&nbsp;YYYY-MM-DD</td>";
               break;
               case "Ticket_Status":
               $table .= "<td align=right>".$field."</td><td>".SearchOptions($field."_option", 1)."</td><td>".TicketStatusOption($field)."</td>";
               break;
               case "Status":
               $table .= "<td align=right>".$field."</td><td>".SearchOptions($field."_option", 1)."</td><td>".StatusOption($field)."</td>";
               break;
               default:
               $table .= "<td align=right>".$field."</td><td>".SearchOptions($field."_option")."</td><td>".TextField($field, "", 20)."</td>";
            }
            $table .= "</tr>";
         }
      }
   }
   return $table;
}
// --------------------------------------------------------
// Function:                Class::BuildSearch
 
// Description:             alter mysql where to account
//                          for search parameters
 
// Type:                    public
 
// Parameters:
//    string $name          name of report
 
// Return Values:
//    string $search        additional search string
 
// Remarks:
//    None
// --------------------------------------------------------
function BuildSearch($name) {
   $field_array = GetFields();
   $fields = $field_array[$name];
   foreach ($fields as $query => $label) {
      if ($label == "Created") {
         if (!empty($_GET[$label."_option1"])) {
            $string = $_GET[$label."_1"];
            $search .= " AND $query".$_GET[$label."_option1"]."'$string', ";
         }
         if (!empty($_GET[$label."_option2"])) {
            $string = $_GET[$label."_2"];
            $search .= " AND $query".$_GET[$label."_option2"]."'$string', ";
         }
      }
      if (!empty($_GET[$label])) {
         switch ($_GET[$label."_option"]) {
            case ' like ':
            $string = $_GET[$label];
            $string = "%".$string."%";
            break;
            default:
            $string = $_GET[$label];
            if ($label != "Status") $string = "'".$string."'";
         }
         $search .= " AND $query".$_GET[$label."_option"]."$string ";
      }
   }
   return $search;
}
 
//-------------------------------------------------------------------
 
$report_types = array(
"" => "&nbsp;",
   "ShowNotScheduled" => "Not Scheduled",
   "ShowAllAppointments" => "All Appointments" );
$form = StartForm($action, array("method" => "get"));
$form .= "<table cellspacing=0 cellpadding=2 border=0>";
$form .= "<tr><td align=left colspan=3>Report&nbsp;";
$form .= "<select name='report_type'>";
foreach ($report_types as $report_type => $label) {
   $select = ($_GET['report_type'] == $report_type) ? "selected" :
   "";
   $form .= "<option value='$report_type' $select>$label";
}
$form .= "</select>";
$form .= HiddenField("function", 3);
$form .= SubmitField("", "Submit");
$form .= "</td></tr>";
$form .= BuildSearchTable();
$form .= "</table>";
$form .= EndForm();
 
if (isset($_GET['report_type']) && $_GET['report_type'] != "") {
   $table = call_user_func($_GET['report_type']);
}
 
echo $form.$table;
?>