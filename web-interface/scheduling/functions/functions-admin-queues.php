<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  
 
require_once("functions/functions-forms.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
//
// Filename: functions-admin-queues.php
// Description: Adds scheduling forms to ruQueue administration
// Supprted Language(s):   PHP 4.0
//
    
//-----------------------------------------------------------------------------
//
// Function: SchedulingOptionsQueueForm
// Description: Displays the Scheduling Options forms for Global Configuration
//
// Parameters:
//    string $item    Global
//    string $queue   Queue Name
//
// Return Values:
//    none
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function SchedulingOptionsQueueForm($item = "", $queue = "") {
   if ($item == "Global") {
      global $db;
      global $username;
      $query = "select * from sched_options order by display_order";
      $result = mysql_query($query, $db);
      $half_hours = array(
         '12:00AM', '12:30AM', '1:00AM', '1:30AM', '2:00AM', '2:30AM',
         '3:00AM', '3:30AM', '4:00AM', '4:30AM', '5:00AM', '5:30AM', '6:00AM',
         '6:30AM', '7:00AM', '7:30AM', '8:00AM', '8:30AM', '9:00AM', '9:30AM',
         '10:00AM', '10:30AM', '11:00AM', '11:30AM', '12:00PM', '12:30PM',
         '1:00PM', '1:30PM', '2:00PM', '2:30PM', '3:00PM', '3:30PM', '4:00PM',
         '4:30PM', '5:00PM', '5:30PM', '6:00PM', '6:30PM', '7:00PM', '7:30PM',
         '8:00PM', '8:30PM', '9:00PM', '9:30PM', '10:00PM', '10:30PM',
         '11:00PM', '11:30PM');
          
      $hours = array(
         '12:00AM', '1:00AM', '2:00AM', '3:00AM', '4:00AM', '5:00AM', '6:00AM',
         '7:00AM', '8:00AM', '9:00AM', '10:00AM', '11:00AM', '12:00PM', '1:00PM',
         '2:00PM', '3:00PM', '4:00PM', '5:00PM', '6:00PM', '7:00PM', '8:00PM',
         '9:00PM', '10:00PM', '11:00PM');
          
      $output .= '<form method="post">
         <input type="hidden" name="item" value = "Global">
         <input type="hidden" name="global" value = "'.$queue.'">
         <input type="hidden" name="sub_group" value = "Scheduling_Options">
         <input type="hidden" name="form_submitted" value="Edit_Global_Scheduling_Options">
         <table border=0>';
          
      while ($option = mysql_fetch_array($result)) {
         if ($option['option_name'] == "Earliest Available Time" || $option['option_name'] == "Latest Available Time") {
            $output .= FormRow($option['option_name'], SelectField(
            $option['option_name'], $hours, $option['option_value']));
         }
             
         elseif ($option['option_name'] == "First Appointment Start Time" || $option['option_name'] == "Last Appointment Start Time") {
            $output .= FormRow($option['option_name'], SelectField($option['option_name'], $half_hours, $option['option_value']));
            }
             
            elseif ($option['option_name'] == "Consultant Initial Email" || $option['option_name'] == "Consultant Reminder Email" || $option['option_name'] == "User Initial Email" || $option['option_name'] == "User Reminder Email" || $option['option_name'] == "Consultant Missed(Consultant) Email" || $option['option_name'] == "User Missed(Consultant) Email" || $option['option_name'] == "Consultant Missed(User) Email" || $option['option_name'] == "User Missed(User) Email" || $option['option_name'] == "Consultant Cancelled Email" || $option['option_name'] == "User Completed Email" || $option['option_name'] == "Consultant Completed Email" || $option['option_name'] == "User Cancelled Email") {
               $output .= FormRow($option['option_name'], TextareaField($option['option_name'], $option['option_value']));
            }
             
            else
               {
               $output .= FormRow($option['option_name'], TextField(
               $option['option_name'], $option['option_value']));
            }
             
         }
          
         if (DoesUserHaveRight($username, "AdminGlobal")) {
            $output .= FormRow ("", SubmitField());
         }
          
         $output .= EndFormTable();
         $output .= EndForm();
      }
      return $output; 
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: SchedulingPermissionsQueueForm
   // Description: Displays form to allow scheduling to be enable or disabled
   //              on a per queue basis, allows Global Scheduling permissions
   //              to be set.
   //
   // Parameters:
   //    string $item    Global or Queue
   //    string $queue   Queue Name
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function SchedulingPermissionsQueueForm($item = "", $queue = "") {
      if ($_GET['item'] == "Queues" || $_POST['item'] == "Queues") {
         $q_name = str_replace("_", " ", $queue);
         $query = "select allow_scheduling from queue where q_name='$q_name'";
         global $db;
         $result = mysql_query($query, $db);
         $object = mysql_fetch_object($result);
         if ($object->allow_scheduling == "Require") {
            $ch1 = "checked";
         } elseif ($object->allow_scheduling == "Allow") {
            $ch2 = "checked";
         } else {
            $ch3 = "checked";
         }
         $output .= '<form><input type="hidden" name="item" value = "Queues">
            <input type="hidden" name="queue" value = "'.$queue.'">
            <input type="hidden" name="sub_group" value = "Scheduling_Permissions">
            <input type="hidden" name="form_submitted" value="Edit_Scheduling_Permissions">
            <p>Use Scheduling?<br>
            <input type="radio" name="allow_scheduling" value="Require" '.$ch1.'>
            Require scheduling for this queue.<br>
            <input type="radio" name="allow_scheduling" value = "Allow" '.$ch2.'>
            Allow scheduling for this queue.<br>
            <input type="radio" name="allow_scheduling" value = "None" '.$ch3.'>
            Do not use scheduling for this queue.<br>
            <div align="right">';
          
         global $username;
          
         if (DoesUserHaveRight($username, "AdminQueue", $queue)) {
            $output .= '<input type="submit" value="Submit">';
         }
          
         $output .= "</div></form>";
      }
       
       
      if ($_GET['item'] == "Global" || $_GET['item'] == "Global") {
         $output .= '<form><input type="hidden" name="item" value = "Global">
            <input type="hidden" name="global" value = "'.$queue.'">
            <input type="hidden" name="sub_group" value = "Scheduling_Permissions">
            <input type="hidden" name="form_submitted" value="Edit_Global_Scheduling_Permissions">
            <table border=0><tr><td>';
          
         $groups = GetDistinctArrayFromTable("group_name", "groups",
            "where type is null order by group_name");
         $allowed = GetArrayFromTable("schedgroup", "sched_rules",
            "where rule='Allow' order by schedgroup");
          
         $output .= '<tr><td>';
         $output .= FontBold("Scheduling Permissions:");
         $output .= '</td></tr><tr><td valign=top>Add group to schedule:<br>';
         $output .= '<select multiple name=addschedgroups[] size=5>';
         $output .= CreateSelectOptions($groups, "open");
         $output .= '</select></td>';
         $output .= '<td valign=top>Scheduled Groups(Check box and submit to delete.)';
         $output .= PrintCheckboxArray("allowed", $allowed);
         $output .= '</td></tr><tr><td colspan=2><br>Add New Scheduling Rule:<br>';
         $output .= RadioField("rule", "Schedule");
         $output .= PrintSelectArray("privgroup", $groups, "", "-");
         $output .= ' can schedule ';
         $output .= PrintSelectArray("schedgroup", $allowed, "", "-");
         $output .= "<br><br>".RadioField("rule", "Edit", "", "_");
         $output .= PrintSelectArray("privgroup1", $groups, "", "-");
         $output .= ' can edit schedules of ';
         $output .= PrintSelectArray("schedgroup1", $allowed, "", "-");
         $output .= '</select></td></tr></table>';
         $result = mysql_query("select privgroup, rule,
            schedgroup from sched_rules order by rule_id");
          
         if (mysql_num_rows($result) > sizeof($allowed)) {
            $output .= "<br>Delete Scheduling Rule:<br>";
         }
          
         $i = 0;
         while ($row = mysql_fetch_array($result)) {
            if ($row[1] != 'Allow') {
               $output .= CheckboxField("checked[$i]", $i, "", "-1");
               $output .= $row[0]." can ".$row[1]." ".$row[2]."<br>";
            }
             
            $i++;
         }
          
         $output .= '<div align="right">';
         global $username;
         if (DoesUserHaveRight($username, "AdminGlobal")) {
            $output .= '<br><input type="submit" value="Submit">';
         }
          
         $output .= '</div>';
      }
   return $output;       
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: DeleteSchedulingRule
   // Description: deletes a global scheduling rule
   //
   // Parameters:
   //    int $rule_id  identifies rule to delete
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function DeleteSchedulingRule($rule_id) {
      global $db;
      $rule = GetValueFromTable("rule", "sched_rules", "where rule_id=".$rule_id);
      if ($rule == "Allow") {
         $schedgroup = GetValueFromTable("schedgroup", "sched_rules",
            "where rule_id=".$rule_id);
         mysql_query("delete from sched_rules where schedgroup='" .$schedgroup."'");
      }
       
      mysql_query("delete from sched_rules where rule_id=$rule_id");
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: AddSchedulingRule
   // Description: adds a new global scheduling rule
   //
   // Parameters:
   //    string $privgroup     Name of priveleged group
   //    string $schedgroup    Name of group that rule applies to
   //    string $rule          Type of rule
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function AddSchedulingRule($privgroup, $schedgroup, $rule) {
      global $db;
      $privgroup = str_replace("_", " ", $privgroup);
      $schedgroup = str_replace("_", " ", $schedgroup);
      if (mysql_num_rows(mysql_query("select * from sched_rules where privgroup='" .$privgroup."' and schedgroup='".$schedgroup."' and rule='".$rule."'"))
         > 0) {
         return;
      }
       
      mysql_query("insert into sched_rules values ('', '".$privgroup."', '" .$schedgroup."', '".$rule."')", $db);
   }
    
?>
