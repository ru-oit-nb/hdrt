<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

//
// Filename:               functions-display.php
// Description:            Display functions for ruQueue Scheduling System
// Supprted Language(s):   PHP 4.0
//
    
require_once("functions-calendar.php");
//-----------------------------------------------------------------------------
//
// Function: DisplayAppointments
// Description: displays appointments as they appear in each ticket
//
// Parameters:
//    mysql_resource_id $result   query containing appointments to display
//
// Return Values:
//    output to display
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DisplayAppointments($result) {
  if (mysql_num_rows($result) <= 0) {
    return;
  }
       
  $num_fields = mysql_num_fields($result);
  $output = "<p><table width=100% cellspacing=0 align=left border=0 cellspacing=1 cellpadding=3><tr>";
       
  for($i = 1; $i < $num_fields; $i++) {
    $field_name = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
    if ($field_name == "Status") $loop_id = $i;
    $output .= "<th align=center>".Font($field_name)."&nbsp;</th>";
  }
  $output .= "<th align=left><a href='new_appointment.php?ticket_id=".$_GET['id']."' class='main'>[Add Appointment]</a></th>";
  $output .= "</tr>\n";
       
  while ($row = mysql_fetch_array($result)) {
    $output .= "<tr>";
          
    for($j = 1; $j < $num_fields; $j++) {
      $output .= "<td align=center>";
      $field_value = $row[$j];
      if ($j == $loop_id && $field_value == "Scheduled") $field_value = "<b>".$field_value."</b>";
      $output .= $field_value;
      $output .= "&nbsp;</td>";
    }
          
    if ($row['status'] != "Not Yet Scheduled") {
      if ($row['status'] == "Cancelled") {
      } elseif ($row['status'] != "Completed") {
	$output .= "<td align=left><a href='edit_appointment.php?appointment_id=".$row[0]."' class='main'>[Edit]</a>&nbsp";
	if (GetIsDatetimePast($row[0])) {
	  $output .="<a href='edit_appointment.php?completed_appointment_id=".$row[0]."' class='main'>[Complete]</a>&nbsp;</td>";
	}
      }
             
      $output .= "</tr>\n";
    } else {
      $output .= "<td align=left><a href='edit_appointment.php?appointment_id=".$row[0]."' class='main'>[Schedule]</a>&nbsp;";
      $output .= "<a href='edit_appointment.php?delete_appointment_id=".$row[0]."' class='main'>[Delete]</a>";
      $output .= "</tr>\n";
    }
  }
  $output .= "</table>";
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function:  DisplayTable
// Description: displays a table for all values in $result
//
// Parameters:
//    mysql_resource_id  $result  results to display
//
// Return Values:
//    none
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DisplayTable($result) {
  if (mysql_num_rows($result) <= 0) {
    return;
  }
       
  $num_fields = mysql_num_fields($result);
  $output = "<p><table width=100% cellspacing=0 align=center border=0 cellspacing=1 cellpadding=3><tr>";
       
  for($i = 0; $i < $num_fields; $i++) {
    $field_name = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
    $output .= "<th align=center>".Font($field_name)."&nbsp;</th>";
  }
  $output .= "<th></th>";
  $output .= "</tr>\n";
       
  while ($row = mysql_fetch_array($result)) {
    $output .= "<tr>";
          
    for($j = 0; $j < $num_fields; $j++) {
      $output .= "<td align=center>";
      $field_value = $row[$j];
      if ($j == $loop_id) $field_value = "<b>".$field_value."</b>";
      $output .= $field_value;
      $output .= "&nbsp;</td>";
    }
          
    $output .= "</tr>\n";
  }
       
  $output .= "</table>";
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function:  DisplayAppointmentsTable
// Description:  displays appointments for view on front page
//
// Parameters:
//    mysql_resource_id $result  appointments to display
//
// Return Values:
//    none
//
// Remarks:
//    none
//
//-----------------------------------------------------------------------------
function DisplayAppointmentsTable($result) {
  if (mysql_num_rows($result) <= 0) {
    return;
  }
       
  $num_fields = mysql_num_fields($result);
  $output = "<table width=100% cellspacing=0 align=left border=0 cellspacing=1 cellpadding=3><tr>";
       
  for($i = 0; $i < $num_fields; $i++) {
    $field_name = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
    $output .= "<th align=left>".Font($field_name)."&nbsp;</th>";
  }
  $output .= "<th></th></tr>\n";
       
  while ($row = mysql_fetch_array($result)) {
    $output .= "<tr>";
          
    for($j = 0; $j < $num_fields; $j++) {
      $output .= "<td align=left>";
      $field_value = $row[$j];
      if ($j == $loop_id) $field_value = '<a class="main" href="old_ticket.php?id='.$field_value.'">'.$field_value.'</a>';
      $output .=  $field_value."&nbsp;</td>";
    }
          
    $output .= "</tr>\n";
  }
       
  $output .= "</table>";
  return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function:         DisplayCheckboxTable
// Description:      Outputs a formatted table from query results
// Type:
//
// Parameters:
//    MySQL Result Object $result  Pointer to data to be displayed
//
// Return Values:
//    N/A
//
// Remarks:
//    Similiar to DiplayTable.  This function also outputs a checkbox
//    for each row in the table.  Useful for multiple delete functions.
//-----------------------------------------------------------------------------
function DisplayCheckboxTable($result) {
  if (mysql_num_rows($result) <= 0) {
    return;
  }
       
  $num_fields = mysql_num_fields($result);
  $output .= "<p><table style=\"border: 1px solid black\" border=0 cellspacing=0 width=75% cellpadding=3 align=center>";
  $output .= "<tr><th>".Font("Delete?")."</td>";
       
  for($i = 0; $i < $num_fields; $i++) {
    $field_name = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
    $output .= "<th align=left>".Font($field_name)."&nbsp;</th>";
  }
       
  $output .= "</tr>\n";
  $i = 0;
       
  while ($row = mysql_fetch_array($result)) {
    $output .= "<tr><td style=\"border-top: 1px solid black\" align=\"center\" width=\"10%\">";
    $output .= CheckboxField("checked[$i]", $i, "", "-1");
    $i++;
          
    for($j = 0; $j < $num_fields; $j++) {
      $field_value = $row[$j];
      $output .= "<td style=\"border-top: 1px solid black\" align=left>" . $field_value."&nbsp;</td>";
    }
          
    $output .= "</tr>\n";
  }
       
  $output .= "</table>";
  return $output;
}
    
    
//-----------------------------------------------------------------------------
//
// Function:         DisplayUserBuildings
// Description:      Outputs the table user_buildings
//
//-----------------------------------------------------------------------------
function DisplayUserBuildings() {
  global $db;
  $result = mysql_query("select * from user_buildings order by
         netid, group_type, group_value", $db);
  return DisplayTable($result);
}
    
    
//-----------------------------------------------------------------------------
//
// Function:         DisplayDeleteUserBuildings
// Description:      Outputs the table user_buildings with checkboxes
//
//-----------------------------------------------------------------------------
function DisplayDeleteUserBuildings() {
  global $db;
  $result = mysql_query("select * from user_buildings order by
         netid, group_type, group_value", $db);
  return DisplayCheckboxTable($result);
}
    
    
//-----------------------------------------------------------------------------
//
// Function:         DisplayBuildings
// Description:      Outputs building information for all buildings
//
//-----------------------------------------------------------------------------
function DisplayBuildings () {
  global $db;
  return DisplayTable(mysql_query("select building_name, campus, building_group
         from buildings order by building_name", $db));
}
    
    
//-----------------------------------------------------------------------------
//
// Function:         DisplayDeleteBuildings
// Description:      Outputs building information with checkboxes
//
//-----------------------------------------------------------------------------
function DisplayDeleteBuildings () {
  global $db;
  return DisplayCheckboxTable(
		       mysql_query("select building_name,campus,building_group
         from buildings order by building_name", $db));
}
    
    
    
//-----------------------------------------------------------------------------
//
// Function: DisplayCheckboxWeek
// Description: displays a week with checkboxes in each cell (day, hour)
//
// Parameters:
//    int $start_hour  first hour to display
//    int $end_hour    last hour to display
//
// Return Values:
//    none
//
// Remarks:
//    Useful for setting a schedule for an entire week.
//
//-----------------------------------------------------------------------------
function DisplayCheckboxWeek($start_hour, $end_hour) {
  global $username;
  $people = GetEditableGroupsforUser($username);
  $person = $people[$_POST['id']];
  if ($end_hour > 24) {
    $end_hour = 24;
  }
       
  if ($start_hour < 0) {
    $start_hour = 0;
  }
       
  $day_labels = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
  $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
       
  $output .= StartForm($action, array("method" => "post"));
  $output .= HiddenField("id", $_POST['id']);
  $output .= "<table width=480 border=1>";
  for ($i = -1; $i < 7; $i++) {
    $output .= "<th width=12.5%>" . $day_labels[$i] . "&nbsp;</th>";
  }
       
  $suffix = " AM";
  $suffix2 = ":30 AM";
       
  for ($i = $start_hour; $i < $end_hour; $i++) {
    if ($i == 0) {
      $hour = 12;
    }
          
    else
      {
	$hour = ($i-1)%12+1;
      }
          
    $output .= "</tr><tr><td align=center nowrap>".$hour.$suffix;
          
    if ($hour == 11) {
      $suffix = " PM";
      $suffix2 = ":30 PM";
    }
          
    if ($i == 23) {
      $suffix = " AM";
      $suffix2 = ":30 AM";
    }
    $checked_hours = GetDaysForHour(2 * $i, $person);
          
    $output .= " - " . (($i-1)%12+1) . $suffix2."</td>
            <td align=center>".CheckboxField($day_labels[0]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[0]])."</td>
            <td align=center>".CheckboxField($day_labels[1]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[1]])."</td>
            <td align=center>".CheckboxField($day_labels[2]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[2]])."</td>
            <td align=center>".CheckboxField($day_labels[3]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[3]])."</td>
            <td align=center>".CheckboxField($day_labels[4]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[4]])."</td>
            <td align=center>".CheckboxField($day_labels[5]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[5]])."</td>
            <td align=center>".CheckboxField($day_labels[6]."[".(2 * $i)."]", (2 * $i), " ", $checked_hours[$days[6]])."</td>";
          
    $output .= "</tr><tr><td align=center nowrap>".(($hour-1)%12+1).$suffix2;
          
    if ($hour == 11) {
      $suffix2 = ":30 PM";
    }
          
    if ($i == 23) {
      $suffix2 = ":30 AM";
    }
          
    $checked_hours = GetDaysForHour((2 * $i)+1, $person);
    $output .= " - " . (($i)%12+1) . $suffix."</td>
            <td align=center>".CheckboxField($day_labels[0]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[0]])."</td>
            <td align=center>".CheckboxField($day_labels[1]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[1]])."</td>
            <td align=center>".CheckboxField($day_labels[2]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[2]])."</td>
            <td align=center>".CheckboxField($day_labels[3]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[3]])."</td>
            <td align=center>".CheckboxField($day_labels[4]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[4]])."</td>
            <td align=center>".CheckboxField($day_labels[5]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[5]])."</td>
            <td align=center>".CheckboxField($day_labels[6]."[".((2 * $i)+1)."]", ((2 * $i)+1), " ", $checked_hours[$days[6]])."</td>";
  }
       
       
  $output .= HiddenField("function", $_POST['function']);
  $output .= HiddenField("form_submitted", "Update_Schedule");
  $output .= "<tr><td colspan=8 align=right>".SubmitField("", "Submit")."</td></tr></table>";
  $output .= EndForm();
  return $output;
}
    
//-----------------------------------------------------------------------------
//
// Function: DisplaySchedule
// Description:  handles function calls from DisplayAllSchedule
//
// Parameters:
//   string $campus  name of campus
//   string $building_group  name of the building group
//   int $function  passes flag for display to parent function
//
// Return Values:
//   none
//
// Remarks:
//   none
//
//-----------------------------------------------------------------------------
function DisplaySchedule($campus, $building_group, $function) {
  global $START_HOUR, $END_HOUR;
  if ($building_group == 'overview') {
    $overview = 1;
  }
  if ($building_group == 'all' || $building_group == 'overview') {
    $building_group = $campus;
  } else {
    $overview = ($_GET['overview']) ? 1 :
      0;
  }
  $actual = ($_GET['actual']) ? 1 :
    0;
  if (isset($_GET['month'])) {
    if (isset($_GET['day'])) {
      //display day view
      DisplayFullDay($_GET['month'], $_GET['year'], $_GET['day'], $START_HOUR, $END_HOUR, $campus, $building_group, $actual, $overview);
    } else {
      if (isset($_GET['first_day'])) {
	//display week view
	//edit to make use of global start/end times
	DisplayClickableWeek($_GET['month'], $_GET['year'], $_GET['first_day'], $START_HOUR, $END_HOUR, $campus, $building_group, $actual, $overview);
      } else {
	//display month view
	DisplayClickableMonth($_GET['month'], $_GET['year'], 0, $_GET['year'], $campus, $building_group, 1, $overview, $function);
      }
    }
  } else {
    //diplay this month
    $today = date("n Y", time());
    $date_array = explode(" ", $today);
    DisplayClickableMonth($date_array[0], $date_array[1], 0, $date_array[1], $campus, $building_group, 0, $overview, $function);
  }
}
    
//-----------------------------------------------------------------------------
//
// Function: DisplayAllSchedule
// Description:  generate menu for DisplaySchedule
//
// Parameters:
//   none
//
//
// Return Values:
//   none
//
//
// Remarks:
//   none
//
//-----------------------------------------------------------------------------
function DisplayAllSchedule() {
  global $username;
  if(count(GetViewableGroupsForUser($username)) <= 0) {
    print "You are not authorized to view schedules.";
    return;
  }
  $function = (isset($_GET['function'])) ? $_GET['function'] :
    $_POST['function'];
  $campus = (isset($_GET['campus']))?$_GET['campus']:
    $_POST['campus'];
  $building_group = (isset($_GET['building_group']))?$_GET['building_group']:
    $_POST['building_group'];
  global $db;
  if (isset($campus) && isset($building_group)) {
    DisplaySchedule($campus, $building_group, $function);
  } else {
    
    $campuses = GetDistinctArrayFromTable("campus", "buildings");
    foreach ($campuses as $value) $campus_temp[$value] = $value;
    $campuses = $campus_temp;
    
    $action = $_SERVER['schedules.php'];
    $forms = StartForm($action, array("method" => "post"));
    $forms .= HiddenField("function", $function);
    $forms .= SelectField("campus", $campuses, $campus);
    $forms .= SubmitField("", "Submit");
    $forms .= EndForm();
    echo $forms;
    if (isset($_POST['campus'])) {
      $campus = $_POST['campus'];
      $forms = StartForm($action, array("method" => "post"));
      $forms .= HiddenField("function", $function);
      $forms .= "<input type='hidden' name='campus' value='$campus'>\n";
      $building_groups = mysql_query("select distinct building_group from buildings where campus='$campus'", $db);
      $forms .= "<select name='building_group'>\n";
      $forms .= "  <option value='overview'>Campus Overview\n";
      $forms .= "  <option value='all'>Entire Campus\n";
      while ($building_group = mysql_fetch_array($building_groups)) {
	$bg = $building_group['building_group'];
	$forms .= "  <option value='$bg'>$bg";
      }
      $forms .= "</select>\n";
      $forms .= SubmitField("", "Submit");
      $forms .= EndForm();
      echo $forms;
    }
  }
}
    
//-----------------------------------------------------------------------------
//
// Function: DisplayGenericWeek
// Description: Displays a 'generic' week - M-F, start_hour - end_hour, shows
//              when user is scheduled. User is picked from dropdown.
//
// Parameters:
//    int $start_hour  first hour to display
//    int $end_hour    last hour to display
//
// Return Values:
//    none
//
// Remarks:
//    noen
//
//-----------------------------------------------------------------------------
function DisplayGenericWeek($start_hour, $end_hour) {
  global $username;
  $people = GetViewableUsersForUser($username);
  $person = $people[$_GET['id']];
  if ($end_hour > 24) {
    $end_hour = 24;
  }
       
  if ($start_hour < 0) {
    $start_hour = 0;
  }
       
  $day_labels = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
  $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
       
  $output = StartForm($action, array("method" => "get"));
  $output .= HiddenField("id", $_GET['id']);
  $output .= "<table width=480 border=0 cellpadding=2 cellspacing=0'>";
  for ($i = -1; $i < 7; $i++) {
    $class = ($i == -1) ? "schedule_week_no_top" :
      "schedule_week";
    $output .= "<th width=12.5% class='$class'>" . $day_labels[$i] . "&nbsp;</th>";
  }
       
  $suffix = " AM";
  $suffix2 = ":30 AM";
       
  for ($i = $start_hour; $i < $end_hour; $i++) {
    if ($i == 0) {
      $hour = 12;
             
    }
          
    else
      {
	$hour = ($i-1)%12+1;
      }
          
    $output .= "</tr><tr><td align=center nowrap class='schedule_week_time'>".$hour.$suffix;
          
    $checked_hours = GetDaysForHour(2 * $i, $person);
    $output .= " - " . (($hour-1)%12+1) . $suffix2."</td>";
          
    for ($j = 0; $j < 7; $j++) {
      if ($checked_hours[$days[$j]] == ((2 * $i))) {
	$output .= '<td class="schedule_week" bgcolor="#00BC55" align=center>&nbsp;</td>';
      } else {
	$output .= '<td class="schedule_week" bgcolor="#AAAAAA" align=center>&nbsp;</td>';
      }
             
    }
    $output .= "</tr><tr><td align=center nowrap class='schedule_week_time'>".(($hour-1)%12+1).$suffix2;
          
    if ($hour == 11) {
      $suffix = " PM";
      $suffix2 = ":30 PM";
    }
          
    if ($i == 23) {
      $suffix = " AM";
      $suffix2 = ":30 AM";
    }
          
    $checked_hours = GetDaysForHour((2 * $i)+1, $person);
    $output .= " - " . (($i)%12+1) . $suffix."</td>";
          
    for ($j = 0; $j < 7; $j++) {
      if ($checked_hours[$days[$j]] == ((2 * $i)+1)) {
	$output .= '<td class="schedule_week" bgcolor="#00BC55" align=center>&nbsp;</td>';
      } else {
	$output .= '<td class="schedule_week" bgcolor="#AAAAAA" align=center>&nbsp;</td>';
      }
             
    }
  }
  $output .= HiddenField("function", $_GET['function']);
  $output .= "</table>";
  $output .= EndForm();
  return $output;
}
    
?>
