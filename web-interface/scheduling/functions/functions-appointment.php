<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

require_once("functions/functions-widgets.php");
//
// Filename: functions-appointment.php
// Description: Provide display for appointment histories
// Supprted Language(s):PHP 4.0
//
// --------------------------------------------------------
// Function:                AppointmentInfo
// Description:             output table with general
//                          appointment information
// Type:                    public
// Parameters:
//    none
// Return Values:
//    none
// Remarks:
//    None
// --------------------------------------------------------
function AppointmentInfo() {
   global $db, $id;
   $output = OpenColorTable("orange", "Appointment", "100%");
   $location = "CONCAT('Room ',room_number,' in ',building_name) Location";
   $query = "SELECT appointment_id Appointment_Id, ticket_id Id, ";
   $query .= "status Status, $location, campus Campus, ";
   $query .= "date_format(last_updated, '%a %M %d, $Y %r') Updated, ";
   $query .= "date_format(datetime, '%a %M %d, $Y %r') Appointment_Time, ";
   $query .= "consultant_netid Consultant FROM appointments, buildings";
   $query .= " WHERE appointment_id=$id AND ";
   $query .= "appointments.building_number=buildings.building_number";
   $result = mysql_query($query, $db);
   $appointment_info = mysql_fetch_array($result, MYSQL_ASSOC);
   $output .= "<table cellspacing=0 cellpadding=2 border=0 width=100%>";
   foreach ($appointment_info as $label => $info) {
      $output .= "<tr><th align=left width=50%><font face='arial,helvetica' size=2>$label</font></th>";
      if ($label == "Id") {
         $info = "<a href='old_ticket.php?id=$info' class='main'>$info</a>";
      }
      $output .= "<td width=50%>$info</td></tr>";
   }
   $output .= "</table>".CloseColorTable();
   return $output;
}
    
// --------------------------------------------------------
// Function:                UserInfo
// Description:             output table with general
//                          user information
// Type:                    public
// Parameters:
//    none
// Return Values:
//    none
// Remarks:
//    None
// --------------------------------------------------------
function UserInfo() {
   global $db, $id;
   $output = OpenColorTable("green", "User", "100%");
   $query = "SELECT user.id Id, user.name Name, user.uid UId, ";
   $query .= "user.email Email, user.phone Phone FROM appointments, ";
   $query .= "ticket, user WHERE appointments.ticket_id=ticket.id AND ";
   $query .= "ticket.user_id=user.id AND appointments.appointment_id=$id";
   $result = mysql_query($query, $db);
   $user_info = mysql_fetch_array($result, MYSQL_ASSOC);
   $output .= "<table cellspacing=0 cellpadding=2 border=0 width=100%>";
   foreach ($user_info as $label => $info) {
      $output .= "<tr><th align=left width=50%><font face='arial,helvetica' size=2>$label</font></th>";
      $output .= "<td width=50%>$info</td></tr>";
   }
   $output .= "</table>".CloseColorTable();
   return $output;
}
    
// --------------------------------------------------------
// Function:                CommentHistory
// Description:             output table with comments
//                          related to the appointment
// Type:                    public
// Parameters:
//    none
// Return Values:
//    none
// Remarks:
//    None
// --------------------------------------------------------
function CommentHistory() {
   global $db, $id;
   $output = OpenColorTable("blue", "Appointment History", "100%");
   $q = "SELECT date_format(date_created,'%a %M %d, $Y %r') date, staff, subject, status, body from comment, sched_comments where appointment_id=$id AND comment_id=id order by date_created";
   $result = mysql_query($q, $db);
   $table = "<table cellpadding=1 cellspacing=1 border=0 width=100%>";
   while ($history = mysql_fetch_array($result)) {
      $bgcolor = ($bgcolor == "#ffffff") ? "#dcdcdc" : "#ffffff";
      $output .= "<tr>";
      $output .= "<td bgcolor='$bgcolor'>".$history['date']."</td>";
      $output .= "<th align=left bgcolor='$bgcolor'><font face='arial,helvetica' size=2>".$history['staff']." - ".$history['subject']."</font></th>";
      $output .= "<td bgcolor='$bgcolor'>Status: ".$history['status']."</td>";
      $output .= "</tr>";
      $output .= "<tr><td colspan=3 bgcolor='$bgcolor'>".$history['body']."</td></tr>";
   }
   $output .= CloseColorTable();
   return $output;
}
?>
