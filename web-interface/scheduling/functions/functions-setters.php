<?php 
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  
 
require_once("scheduling/functions/functions-getters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-ticket-submenu.php");
 
    
   //
   // Filename:               functions-setters.php
   // Description:            Set of functions to set values
   // Supprted Language(s):   PHP 4.0
   //
    
    
   //-----------------------------------------------------------------------------
   //
   // Function:         SetUserBuildings
   // Description:      Adds a new permission for user.
   // Type:
   //
   // Parameters:
   //    string $netid         User NetID
   //    string $group_type    Either campus or building_group (type of group)
   //    string $group_value   Value of group for new permission
   //
   // Return Values:
   //    Returns specific error message if error.
   //    Returns successful if successful.
   //
   // Remarks:
   //    None
   //-----------------------------------------------------------------------------
   function SetUserBuildings($netid, $group_type, $group_value) {
      global $db;
      if ($netid == "") {
         return "Please select a NetID.";
      }
       
      if ($group_type == "") {
         return "Please select either Campus or Building Group.";
      }
       
      if ($group_type == "campus" && $group_value == "") {
         return "Please select a Campus.";
      }
       
      if ($group_type == "building_group" && $group_value == "") {
         return "Please select a Building Group.";
      }
       
      if (mysql_num_rows(mysql_query("select * from user_buildings
         where netid = '$netid' and group_type = '$group_type'
         and group_value = '$group_value'", $db)) > 0) {
         return "Permission already exists.";
      }
       
      $query = "insert into user_buildings values ('$netid', '$group_type',
         '$group_value')";
      $result = mysql_query($query, $db);
       
      if (mysql_errno()) {
         return mysql_error();
      }
       
      else
         {
         return "Permissions Updated Successfully";
      }
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  SetHours
   // Description: sets schedule for user
   //
   // Parameters:
   //    int $netid    user to set hours for
   //    array $hours  indexed by 0-6 related to days, sums of powers of two
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function SetHours($netid, $hours) {
      global $db;
      $days = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
         'Friday', 'Saturday');
      for ($i = 0; $i < 7; $i++) {
         if (mysql_num_rows(mysql_query("select netid from potential_schedule
            where netid='".$netid."' and day='".$days[$i]."'")) > 0) {
            mysql_query("update potential_schedule set hours=".$hours[$i]. " where netid='".$netid."' and day='".$days[$i]."'");
         } else {
            mysql_query("insert into potential_schedule values ('".$netid."','". $days[$i]."',".$hours[$i].")");
         }
      }
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  SetAppointment
   // Description: set appointment schedule with datetime for consultant
   //
   // Parameters:
   //   int $appointment_id        appointment id
   //   string $consultant_netid   netid to schedule appointemnt for
   //   int $datetime              date/time of appointment
   //   int $ticket_id             ticket for appointment
   // Return Values:
   //    0 if unsuccessful
   //    1 if successful
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function SetAppointment($appointment_id, $consultant_netid, $datetime, $ticket_id) {
      global $db;
      $query = "update appointments set status='Scheduled',consultant_netid='" .$consultant_netid."',datetime=".$datetime." where appointment_id=" .$appointment_id;
      mysql_query($query, $db);
      if (mysql_errno()) {
         return 0;
      }
      TicketSteal($ticket_id, $consultant_netid, "supress result");
      return 1;
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: UpdateAppointment
   // Description: updates appointemnt information
   //
   // Parameters:
   //    int $appointment_id        appointment id
   //    int $building_number       building number
   //    string $room_number        room number in building
   //    string $status             status of appointment
   //    string $consultant_netid   consultant to schedule
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function UpdateAppointment($appointment_id, $building_number, $room_number, $status, $consultant_netid) {
      global $db;
       
      $query = "update appointments set ";
      if (!empty($building_number)) {
         $query .= "building_number=".$building_number.",";
      }
      if (!empty($room_number)) {
         $query .= "room_number='".$room_number."',";
      }
      $query .= " status='".$status."',consultant_netid='".$consultant_netid."'
         where appointment_id=$appointment_id";
      mysql_query($query, $db);
      if (mysql_errno()) {
         error_report("There has been a database error here.", "50%");
      }
      return;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  DeleteAppointment
   // Description:  deletes appointment from database
   //
   // Parameters:
   //    int $appointment_id  appointment to delete
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function DeleteAppointment($appointment_id) {
      global $db;
      $query = "delete from appointments where appointment_id=$appointment_id";
      mysql_query($query, $db);
      if (mysql_errno()) {
         error_report("There has been a database error.", "50%");
      }
      return;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  UnscheduleAppointment
   // Description:  makes timeslot available again
   //
   // Parameters:
   //    int $appointment_id  appointment to unschedule
   //    string $status       status of appointment
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function UnscheduleAppointment($appointment_id, $status) {
      global $db;
      if ($status == "Cancelled") {
         $query = 'update appointments set consultant_netid=""
            where appointment_id='.$appointment_id;
      }
      if ($status == "Not Yet Scheduled") {
         $query = 'update appointments set consultant_netid="",datetime=""
            where appointment_id='.$appointment_id;
      }
      mysql_query($query, $db);
      if (mysql_errno()) {
         error_report("There has been a database error.", "50%");
      }
      return;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  LinkNewComment
   // Description: associates a comment with an appointemnt
   //
   // Parameters:
   //    int $appointment_id  appointment to link comment to
   //    int $ticket_id       ticket to link comment from
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    links appointment to most recent comment in ticket
   //
   //-----------------------------------------------------------------------------
   function LinkNewComment($appointment_id, $ticket_id) {
      global $db;
      $comment_id = GetValueFromTable('id', 'comment', 'where ticket_id=' .$ticket_id.' order by id desc');
      mysql_query("insert into sched_comments values($appointment_id,
         $comment_id)", $db);
      if (mysql_errno()) {
         error_report("There has been a database error.", "50%");
      }
   }
    
?>
