<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

   //
   // Filename: function-calendar.php
   // Description: Provides functions for displaying calendars.
   //              Displays year, month, week, day, also schedules.
   // Supprted Language(s):   PHP 4.0
   //
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayYear
   // Description: displays a year calendar in the same format as cal
   //
   // Parameters:
   //    int $year   year to display
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function DisplayYear($year) {
      print "<table border=1><tr><td align=center colspan=3><h1>$year</h1></td></tr><tr>";
       
      for ($i = 1; $i < 4; $i++) {
         print "<td valign=top>";
         DisplayMonth($i, $year, $size, "");
         print "</td>";
      }
       
      print "</tr><tr>";
       
      for ($i; $i < 7; $i++) {
         print "<td valign=top>";
         DisplayMonth($i, $year, $size, "");
         print "</td>";
      }
       
      print "</tr><tr>";
       
      for ($i; $i < 10; $i++) {
         print "<td valign=top>";
         DisplayMonth($i, $year, $size, "");
         print "</td>";
      }
       
      print "</tr><tr>";
       
      for ($i; $i <= 12; $i++) {
         print "<td valign=top>";
         DisplayMonth($i, $year, $size, "");
         print "</td>";
      }
       
      print "</tr></table>";
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayMonth
   // Description:
   //
   // Parameters:
   //    int $month         month to display
   //    int $year          year of month to display
   //    int $size          0-3 selects size of headings
   //    string $year_label prints after month in heading
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function DisplayMonth($month, $year, $size, $year_label) {
       
      if ($month > 12 || $month < 1) {
         return "Invalid Month";
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $timestamp_month = strtotime($months[$month].' 01, '.$year);
      $first_day = date("w", $timestamp_month);
       
      print "<table border=0><tr><th colspan=7>";
      print $months[$month]."&nbsp;".$year_label;
      print "</th></tr><tr>";
       
      if ($size != 1 && $size != 2 && $size != 3) {
         $size = 0;
      }
      $day_sizes = array(
      array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'),
         array('Sunday', 'Monday', 'Tuesday', 'Wednesday',
         'Thursday', 'Friday', 'Saturday'),
         array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'),
         array('S', 'M', 'T', 'W', 'T', 'F', 'S')
      );
       
      print '<tr>';
      for ($i = 0; $i < 7; $i++) {
         print '<th>'.$day_sizes[$size][$i].'</th>';
      }
      print '</tr>';
       
      for ($i = 1; $i <= $first_day; $i++) {
         print "<td>&nbsp;</td>";
      }
       
      for ($i; $i <= ($first_day + $num_days[$month]); $i++) {
         $day = $i - $first_day;
         print "<td align=center>". $day ."</td>";
          
         if ($i%7 == 0) {
            print "</tr><tr>";
         }
          
      }
       
      print "</table>";
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayWeek
   // Description: displays a table of a week, starting from $first_day, showing
   //              hours from $start_hour to $end_hour
   //
   // Parameters:
   //    int $month        month of first_day
   //    int $year         year of first_day
   //    int $first_day    first day to display
   //    int $start_hour   first hour to display
   //    int $end_hour     last hour to display
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    noen
   //
   //-----------------------------------------------------------------------------
   function DisplayWeek($month, $year, $first_day, $start_hour, $end_hour) {
      if ($end_hour > 24) {
         $end_hour = 24;
      }
       
      if ($start_hour < 0) {
         $start_hour = 0;
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $day_labels = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
         'Friday', 'Saturday');
       
       
      if ($month > 12 || $month < 1 || $first_day > $num_days[$month] || $first_day < 1) {
         return "Invalid Request";
      }
       
      $timestamp_first_day = strtotime($months[$month].' '.$first_day.', '.$year);
      $day_of_week = date("w", $timestamp_first_day);

      print "<table border=1><tr><th colspan=8><h2>";
      print date("F d, Y", $timestamp_first_day);
      // 522000 = 518400 (6 days) + 3600 (1 hours) 
      // since date is "off by an hour for {oct,nov,dec}" why? 
      print " - " . date("F d, Y", $timestamp_first_day+522000);
      print "</h2></th></tr><tr><th>&nbsp;</th>";
       
      for ($i = $day_of_week; $i < ($day_of_week+7); $i++) {
         print "<th width=12.5%>" . $day_labels[$i%7] . "</th>";
      }
       
      $suffix = " AM";
       
      for ($i = $start_hour; $i < $end_hour; $i++) {
         if ($i == 0) {
            $hour = 12;
         }
          
         else
            {
            $hour = ($i-1)%12+1;
         }
          
         print "</tr><tr><td align=center>".$hour.$suffix;
          
         if ($hour == 11) {
            $suffix = " PM";
         }
          
         if ($i == 23) {
            $suffix = " AM";
         }
          
         print " - " . (($i)%12+1) . $suffix."</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>";
      }
      print "</tr></table>";
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayDay
   // Description: displays a day from $start_hour to $end_hour
   //
   // Parameters:
   //    int $month        month of first_day
   //    int $year         year of first_day
   //    int $day          day to display
   //    int $start_hour   first hour to display
   //    int $end_hour     last hour to display
   //
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function DisplayDay($month, $year, $day, $start_hour, $end_hour) {
      if ($end_hour > 24) {
         $end_hour = 24;
      }
       
      if ($start_hour < 0) {
         $start_hour = 0;
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $day_labels = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
         'Friday', 'Saturday');
       
       
      if ($month > 12 || $month < 1 || $day > $num_days[$month] || $day < 1) {
         return "Invalid Request";
      }
       
      $timestamp_day = strtotime($months[$month] . ' ' . $day . ', ' . $year);
      $day_of_week = date("w", $timestamp_day);
       
      print "<table border=1><tr><th colspan=2 align=center><h2>";
      print date("l F d, Y", $timestamp_day);
      print "</h2></th></tr><tr><th>&nbsp</th><th>Username</th>";
       
       
      $suffix = " AM";
       
      for ($i = $start_hour; $i < $end_hour; $i++) {
         if ($i == 0) {
            $hour = 12;
         }
          
         else
            {
            $hour = ($i-1)%12+1;
         }
          
         print "</tr><tr><td align=center>".$hour.$suffix;
          
         if ($hour == 11) {
            $suffix = " PM";
         }
          
         if ($i == 23) {
            $suffix = " AM";
         }
          
         print " - " . (($i)%12+1) . $suffix."</td>
            <td>&nbsp;</td>";
      }
      print "</tr></table>";
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: CheckLeapYear
   // Description: checks if year is a leap year
   //
   // Parameters:
   //    int $year  year to check
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function CheckLeapYear($year) {
      if (($year % 4) != 0) {
         return 0;
      }
       
      $test400 = (int)($year/400);
      $test100 = (int)($year/100);
       
      if ($year/100 == $test100 && $year/400 == $test400) {
         return 0;
      }
       
      return 1;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayClickableMonth
   // Description: displays a month with days as links
   //
   // Parameters:
   //    int $month          month to display
   //    int $year           year of month
   //    int $size           determines size of day heading
   //    string $year_label  displays after the month name
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function DisplayClickableMonth($month, $year, $size, $year_label,
      $campus = "", $building_group = "", $set = 0,
      $overview = 0, $function) {
       
      if ($month > 12 || $month < 1) {
         return "Invalid Month";
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $timestamp_month = strtotime($months[$month].' 01, '.$year);
      $first_day = date("w", $timestamp_month);
       
      print "<center><table border=0><tr><th colspan=7>";
      print $months[$month]."&nbsp;".$year_label;
      print "</th></tr><tr>";
       
      if ($size != 1 && $size != 2 && $size != 3) {
         $size = 0;
      }
      $day_sizes = array(
      array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'),
         array('Sunday', 'Monday', 'Tuesday', 'Wednesday',
         'Thursday', 'Friday', 'Saturday'),
         array('Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'),
         array('S', 'M', 'T', 'W', 'T', 'F', 'S')
      );
       
      print '<tr>';
      for ($i = 0; $i < 7; $i++) {
         print '<th>'.$day_sizes[$size][$i].'</th>';
      }
      print '</tr>';
       
      for ($i = 1; $i <= $first_day; $i++) {
         print "<td>&nbsp;</td>";
      }
       
      $location = (!$set && isset($campus) && isset($building_group))? "&campus=$campus&building_group=$building_group":
      "";
       
      if ($overview) $location .= "&overview=1";
       
      if (isset($function)) $location .= "&function=".$function;
       
      for ($i; $i <= ($first_day + $num_days[$month]); $i++) {
         $day = $i - $first_day;
         $query_string = $_SERVER['QUERY_STRING'];
         $query_string = str_replace("&month=".$month, "", $query_string);
         $query_string = str_replace("&year=".$year, "", $query_string);
         print '<td align=center><a class="main" href="'.$_SERVER['PHP_SELF']. '?' .$query_string.$location.'&first_day='.$day.'&month='.$month. '&year=' . $year . '">' . $day . '</a></td>';
          
         if ($i%7 == 0) {
            print "</tr><tr>";
         }
          
      }
       
      $prev = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
      $prevtest = $prev;
       
      if ($month == 1) {
         $prev = str_replace("month=1", "month=12", $prev);
         $prev = str_replace("year=".$year, "year=".($year-1), $prev);
          
         if ($prev == $prevtest) {
            $prev .= "&month=12&year=" . ($year-1);
         }
      }
       
      else
         {
         $prev = str_replace("month=".$month, "month=".($month-1), $prev);
         if ($prev == $prevtest) {
            $prev .= "&month=".($month-1)."&year=".($year);
         }
      }
       
       
      $next = $_SERVER['PHP_SELF'].'?'.$_SERVER['QUERY_STRING'];
      $nexttest = $next;
       
      if ($month == 12) {
         $next = str_replace("month=12", "month=1", $next);
         $next = str_replace("year=".$year, "year=".($year+1), $next);
          
         if ($next == $nexttest) {
            $next .= "&month=1&year=" . ($year+1);
         }
      }
       
      else
         {
         $next = str_replace("month=".$month, "month=".($month+1), $next);
         if ($next == $nexttest) {
            $next .= "&month=".($month+1)."&year=".($year);
         }
      }
       
       
      print '<tr><td align=center colspan=3><a class="main" href="' .$prev.$location.'">&lt;-Prev</a></td><td>&nbsp;</td>';
      print '<td align=center colspan=3><a class="main" href="' .$next.$location.'">Next-&gt;</a></td></tr>';
      print "</table></center>";
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayClickableWeek
   // Description:  display a 7-day view
   //
   // Parameters:
   //   int $month  numeric month
   //   int $year  4-digit numeric year
   //   int $first_day  numeric day of month
   //   int $start_hour  array position of starting hour
   //   int $end_hour  array position of ending hour
   //   string $campus  name of campus
   //   string building_group  name of building group
   //   bool $actual  flag to display actual schedule
   //   bool $overview  flag for overview mode
   //   string $netid  special flag for single netid results
   //
   // Return Values:
   //   none
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function DisplayClickableWeek($month, $year, $first_day, $start_hour,
      $end_hour, $campus = "", $building_group = "",
      $actual = 0, $overview = 0, $netid = "") {
     global $db;
     $hours = GetHours();
      $display_day = $first_day;
      $display_month = $month;
      $display_year = $year;
      $group_value = (isset($building_group)) ? $building_group :
      $campus;
       
      $classes = array(
      "appointment_top",
         "appointment_mid",
         "appointment_bottom" );
       
      if ($end_hour > 24) {
         $end_hour = 24;
      }
       
      if ($start_hour < 0) {
         $start_hour = 0;
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $day_labels = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
       
       
      if ($month > 12 || $month < 1 || $first_day > $num_days[$month] || $first_day < 1) {
         return "Invalid Request";
      }
       
      $timestamp_first_day = strtotime($months[$month].' '.$first_day.', '.$year);
      $day_of_week = date("w", $timestamp_first_day);
       
      print "<center><table border=0 cellspacing=0 class='schedule_week'>
         <tr><th colspan=8 class='schedule_header'><h2>";
      print date("F d, Y", $timestamp_first_day);
      // 522000 = 518400 (6 days) + 3600 (1 hours) 
      // since date is "off by an hour for {oct,nov,dec}" why? 
      print " - " . date("F d, Y", $timestamp_first_day+522000);
       
      if ($overview) $extra = "Overview";
       
      print "</h2>$group_value $extra</th></tr><tr>
         <th class='schedule_week_no_top'>&nbsp;</th>";
       
      for ($i = $day_of_week; $i < ($day_of_week+7); $i++) {
         $query_string = $_SERVER['QUERY_STRING'];
          
         if ($display_day > $num_days[$display_month]) {
            $display_month++;
             
            if ($display_month == 13) {
               $display_month = 1;
               $display_year++;
            }
             
            $display_day = 1;
         }
          
         $query_string = str_replace("&first_day=$first_day", "&day=$display_day",
            $query_string);
         $query_string = str_replace("&month=$month", "&month=$display_month",
            $query_string);
         $query_string = str_replace("&year=$year", "&year=$display_year",
            $query_string);
         $day_link = $_SERVER['PHP_SELF'] . "?" . $query_string;
         $day_label = (empty($netid)) ? "<a href='$day_link' class='main'>". $day_labels[$i%7]."</a>" :
         $day_labels[$i%7];
         print "<th width=12.5% class='schedule_week'>$day_label</th>";
         $display_day++;
      }
       
      $suffix = " AM";
      $suffix2 = ":30 AM";
      for ($i = $start_hour; $i < $end_hour; $i++) {
         if ($i == 0) {
            $hour = 12;
         }
          
         else
            {
            $hour = ($i-1)%12+1;
         }
          
         print "</tr><tr><td align=center nowrap class='schedule_week'>". $hour.$suffix;
          
         if ($hour == 11) {
            $suffix = " PM";
         }
          
         if ($i == 23) {
            $suffix = " AM";
         }
         print " - " . ($hour) . $suffix2."</td>";
          
         for ($k = 0; $k < 7; $k++) {
	   if (empty($netid)) {
	     $query = "SELECT num FROM potential_schedule_cache WHERE hour = '".str_pad($hours[$i*2], 2, '0', STR_PAD_LEFT).":00' AND day = ".(($day_of_week+$k)%7+1)." AND group_value = '$group_value' LIMIT 1";
	     $result = mysql_query($query, $db);
	     $num_users = (mysql_num_rows($result) > 0) ? mysql_result($result, 0) : 0;
	   }
	   else {
	     $users = GetIsScheduled($i * 2, $day_of_week+$k, $group_value, $overview, $netid);
	     $num_users = mysql_num_rows($users);
	   }
            $bgcolor = ($num_users > 0) ? "#00BC55" : "#aaaaaa";
            $display = "&nbsp;";
            $class = "schedule_week";
            if ($actual) {
	      if (empty($netid)) {
		$query = "SELECT SUM(num) FROM appointments_cache WHERE datetime >= '$year-".str_pad($month, 2, '0', STR_PAD_LEFT)."-".($first_day+$k)." ".$hours[$i*2-2]."' AND datetime <= '$year-".str_pad($month, 2, '0', STR_PAD_LEFT)."-".($first_day+$k)." ".$hours[$i*2]."' AND campus = '$group_value' GROUP BY campus";
		$result = mysql_query($query, $db);
		$num_appointment = (mysql_num_rows($result) > 0) ? mysql_result($result, 0) : 0;
	      }
	      else {
		$appointment = GetIsAppointment($i * 2, $first_day+$k, $month, $year, $netid);
		$num_appointment = mysql_num_rows($appointment);
	      }
               $available = $num_users-$num_appointment;

                  if ($available < 1 && $num_appointment > 0) {
                     if (!empty($netid)) {
                        $appointment_data = mysql_fetch_array($appointment);
                        $position = $cell_tracker[$k]++%3;
                        $display = $appointment_data[$position];
                        $display = ModAppointmentDisplay($display, $position);
                        if ($num_appointment <= 0) {
			  $class = 'schedule_week';
			  $display = '&nbsp;';
			}
			else $class = $classes[$position];
                     }
                     $bgcolor = "'red'";
                  }
                  else if ($available < $num_users) $bgcolor = "'yellow'";

            }
            print "<td bgcolor=$bgcolor class='$class' align=center>$display</td>";
         }
          
         print "</tr><tr><td align=center nowrap class='schedule_week'>" .$hour.$suffix2;
          
         if ($hour == 11) {
            $suffix2 = ":30 PM";
         }
          
         if ($i == 23) {
            $suffix2 = ":30 AM";
         }
          
         print " - " . (($i)%12+1) . $suffix."</td>";
          
         for ($k = 0; $k < 7; $k++) {
	   if (empty($netid)) {
	     $query = "SELECT num FROM potential_schedule_cache WHERE hour = '".str_pad($hours[$i*2+1], 2, '0', STR_PAD_LEFT).":00' AND day = ".(($day_of_week+$k)%7+1)." AND group_value = '$group_value' ORDER BY updated DESC LIMIT 1";
	     $result = mysql_query($query, $db);
	     $num_users = (mysql_num_rows($result) > 0) ? mysql_result($result, 0) : 0;
	   }
	   else {
	     $users = GetIsScheduled($i * 2+1, $day_of_week+$k, $group_value, $overview, $netid);
	     $num_users = mysql_num_rows($users);
	   }
            $bgcolor = ($num_users > 0) ? "#00BC55" :
            "#aaaaaa";
            $display = "&nbsp;";
            $class = "schedule_week";
            if ($actual) {
	      if (empty($netid)) {
		$query = "SELECT SUM(num) FROM appointments_cache WHERE datetime >= '$year-".str_pad($month, 2, '0', STR_PAD_LEFT)."-".($first_day+$k)." ".$hours[$i*2-1]."' AND datetime <= '$year-".str_pad($month, 2, '0', STR_PAD_LEFT)."-".($first_day+$k)." ".$hours[$i*2+1]."' AND campus = '$group_value' GROUP BY campus";
		$result = mysql_query($query, $db);
		$num_appointment = (mysql_num_rows($result) > 0) ? mysql_result($result, 0) : 0;
	      }
	      else {
		$appointment = GetIsAppointment($i * 2+1, $first_day+$k, $month, $year, $netid);
		$num_appointment = mysql_num_rows($appointment);
	      }
               $available = $num_users-$num_appointment;

                  if ($available < 1 && $num_appointment > 0) {
                     if (!empty($netid)) {
                        $appointment_data = mysql_fetch_array($appointment);
                        $position = $cell_tracker[$k]++%3;
                        $display = $appointment_data[$position];
                        $display = ModAppointmentDisplay($display, $position);
			if ($num_appointment <= 0) {
			  $class = 'schedule_week';
			  $display = '&nbsp;';
			}
                        else $class = $classes[$position];
                     }
                     $bgcolor = "'red'";
                  }
                  else if ($available < $num_users) $bgcolor = "'yellow'";

            }
            print "<td bgcolor=$bgcolor class='$class' align=center>$display</td>";
         }
          
      }
      print "</tr><tr>";
      $server_query = $_SERVER['QUERY_STRING'];
      if (empty($netid)) {
         if ($actual) {
            $server_query = str_replace("&actual=1", "", $server_query);
            $link_name = "Potential";
         } else {
            $server_query .= "&actual=1";
            $link_name = "Actual";
         }
         $link = $_SERVER['PHP_SELF']."?".$server_query;
         $link = "<a href='$link' class='main'>View $link_name Schedule</a>";
      }
      else $link = "&nbsp;";
       
      $server_query = str_replace("&first_day=$first_day", "", $server_query);
      $server_query = str_replace("&month=$month", "", $server_query);
      $server_query = str_replace("&year=$year", "", $server_query);
      $next_day = $first_day+7;
      $next_month = $month;
      $next_year = $year;
      if ($next_day > $num_days[$month]) {
         $next_day -= $num_days[$month];
         $next_month++;
         if ($next_month == 13) {
            $next_month = 1;
            $next_year++;
         }
      }
      $next = $server_query . "&first_day=$next_day&month=$next_month&year=$next_year";
      $next = "<a class='main' href='".$_SERVER['PHP_SELF']."?".$next. "'>Next-&gt;</a>";
      $prev_day = $first_day-7;
      $prev_month = $month;
      $prev_year = $year;
      if ($prev_day < 1) {
         $prev_month--;
         if ($prev_month < 1) {
            $prev_month = 12;
            $prev_year--;
         }
         $prev_day += $num_days[$prev_month];
      }
      $prev .= $server_query . "&first_day=$prev_day&month=$prev_month&year=$prev_year";
      $prev = "<a class='main' href='".$_SERVER['PHP_SELF']."?".$prev. "'>&lt;-Previous</a>";
       
      print "<td>&nbsp;</td><td align=right colspan=2>$prev</td><td colspan=3 align=center>";
      print "$link</td><td colspan=2 align=left style='border-right: 1px solid #000000'>$next</td>";
      print "</tr></table><p>";
      if ($actual && empty($netid)) {
         print ShowLegend();
      }
      print "</center>";
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: DisplayFullDay
   // Description:  displays the day view
   //
   // Parameters:
   //   int $month  numeric month
   //   int $year  4-digit numeric year
   //   int $day  numeric day of month
   //   int $start_hour  array position of starting hour
   //   int $end_hour  array position of ending hour
   //   string $campus  name of campus
   //   string building_group  name of building group
   //   bool $actual  flag to display actual schedule
   //   bool $overview  flag for overview mode
   //
   // Return Values:
   //   none
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function DisplayFullDay($month, $year, $day, $start_hour, $end_hour, $campus, $building_group, $actual, $overview = 0) {
       
      $group_value = (isset($building_group)) ? $building_group :
      $campus;
       
      if ($end_hour > 48) {
         $end_hour = 48;
      }
      if ($start_hour < 0) {
         $start_hour = 0;
      }
       
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
       
      $months = array(1 => 'January', 2 => 'February', 3 => 'March', 4 => 'April',
         5 => 'May', 6 => 'June', 7 => 'July', 8 => 'August',
         9 => 'September', 10 => 'October', 11 => 'November',
         12 => 'December');
       
      $day_labels = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
         'Friday', 'Saturday');
       
      if ($month > 12 || $month < 1 || $day > $num_days[$month] || $day < 1) {
         return "Invalid Request";
      }
       
      $timestamp_day = strtotime($months[$month] . ' ' . $day . ', ' . $year);
      $day_of_week = date("w", $timestamp_day);
       
      if ($users = GetUsersScheduledToday($day_of_week, $group_value, $overview)) {
      $num_cols = mysql_num_rows($users)+1;
      if ($num_cols < 4) $num_cols = 4;
      $width = 100/($num_cols+1)."%";
      //get table widths
      while ($user = mysql_fetch_array($users)) {
         $user_array[] = $user['netid'];
      }
       
      $colors = array(
      "#ff0000",
         "#ff9900",
         "#00cc00",
         "#0000ff",
         "#0099ff",
         "#ff0099",
         "#9900ff" );
      $colors_off = array(
      "#aa0000",
         "#aa6600",
         "#006600",
         "#0000aa",
         "#0066aa",
         "#aa0099",
         "#9900aa" );
      $classes = array(
      "appointment_top",
         "appointment_mid",
         "appointment_bottom" );
       
      print "<center><table class='schedule' border=0 cellpadding=3 cellspacing=0>";
      print "<tr><th colspan=$num_cols align=center><h2>";
      print date("l F d, Y", $timestamp_day);
      if ($overview) $extra = "Overview";
      print "</h2>$building_group $extra</th></tr><tr>";
      print "<th width=$width class='schedule'>&nbsp</th>";
       
      if (is_array($user_array)) {
	foreach ($user_array as $username) {
	  $color[$username] = $colors[$i%sizeof($colors)];
	  $color_off[$username] = $colors_off[$i++%sizeof($colors_off)];
	  $bgcolor = $color[$username];
	  $bgcolor_off = $color_off[$username];
	  print "<th class='schedule' width=$width bgcolor='$bgcolor'><font color=#ffffff face='arial, helvetica' size=2>$username</font></th>";
	}
      }
      else {
	print "<br />No more users in array\n";
      }
      
      $suffix = " AM";
      $suffix2 = ":30 AM";
      for ($i = $start_hour; $i < $end_hour; $i++) {
         $cells1 = $cells2 = "";
         $display_string = "&nbsp;";
	 if (is_array($user_array)) {
	   foreach ($user_array as $username) {
	     $bgcolor = $color[$username];
	     $bgcolor_off = $color_off[$username];
	     if ($actual) {
	       $appointment = GetIsAppointmentUser($i * 2, $day, $month, $year,
						   $username);
	       if (mysql_num_rows($appointment) > 0) {
		 $bgcolor = "#ffffff";
		 $appointment_data = mysql_fetch_array($appointment);
		 $position = $cell_tracker[$username]++%3;
		 $class = $classes[$position];
		 $display_string = $appointment_data[$position];
		 $display_string = ModAppointmentDisplay($display_string, $position);
	       } else {
		 $class = "schedule_time";
	       }
	     } else {
	       $class = "schedule_time";
	     }
	     if ($display_string == "&nbsp;") {
	       $bgcolor = (GetIsUserScheduled($i*2, $day_of_week, $group_value, $username)) ? $bgcolor: $bgcolor_off;
	       $cells1 .= "<td bgcolor='$bgcolor' class='schedule_time'>&nbsp;</td>";
	     }
	     else $cells1 .= "<td align=center bgcolor='$bgcolor' class='$class'>$display_string</td>";
	     $bgcolor = $color[$username];
	     $bgcolor_off = $color_off[$username];
	     $display_string = "&nbsp;";
	     if ($actual) {
	       $appointment = GetIsAppointmentUser($i * 2+1, $day, $month, $year,
						   $username);
	       if (mysql_num_rows($appointment) > 0) {
		 $bgcolor = "#ffffff";
		 $appointment_data = mysql_fetch_array($appointment);
		 $position = $cell_tracker[$username]++%3;
		 $class = $classes[$position];
		 $display_string = $appointment_data[$position];
		 $display_string = ModAppointmentDisplay($display_string, $position);
	       } else {
		 $class = "schedule_time";
	       }
	     } else {
	       $class = "schedule_time";
	     }
	     if ($display_string == "&nbsp;") {
	       $bgcolor = (GetIsUserScheduled($i*2, $day_of_week, $group_value, $username)) ? $bgcolor: $bgcolor_off;
	       $cells2 .= "<td bgcolor='$bgcolor' class='schedule_time'>&nbsp;</td>";
	     }
	     else $cells2 .= "<td align=center bgcolor='$bgcolor' class='$class'>$display_string</td>";
	     $display_string = "&nbsp;";
	   }  // close foreach
         }  // close is_array
          
         if ($i == 0) {
            $hour = 12;
         } else {
            $hour = ($i-1)%12+1;
         }
         print "</tr><tr><td align=center class='schedule_time'>".$hour.$suffix;
         if ($i == 47) {
            $suffix = " AM";
         }
          
         print " - " . ($hour) . $suffix2."</td>".$cells1;
         print "</tr><tr><td align=center class='schedule_time'>".$hour.$suffix2;
          
         if ($hour == 11) {
            $suffix = " PM";
            $suffix2 = ":30 PM";
         }
         if ($i == 23) {
            $suffix = " AM";
         }
          
         print " - " . (($i)%12+1) . $suffix."</td>".$cells2;
      }
      print "</tr><tr>";
       
      $server_query = $_SERVER['QUERY_STRING'];
       
      $prev_day = $day-1;
      if ($prev_day < 1) {
         $prev_month = $month-1;
         if ($prev_month < 1) {
            $prev_month = 12;
            $prev_year = $year-1;
         } else {
            $prev_year = $year;
         }
         $prev_day = $num_days[$prev_month];
      } else {
         $prev_month = $month;
         $prev_year = $year;
      }
      $next_day = $day+1;
      if ($next_day > $num_days[$month]) {
         $next_day = 1;
         $next_month = $month+1;
         if ($next_month == 13) {
            $next_month = 1;
            $next_year = $year+1;
         } else {
            $next_year = $year;
         }
      } else {
         $next_month = $month;
         $next_year = $year;
      }
      $next_date = $_SERVER['PHP_SELF']."?".str_replace("&day=$day", "&day=$next_day", $server_query);
      $next_date = str_replace("&month=$month", "&month=$next_month", $next_date);
      $next_date = str_replace("&year=$year", "&year=$next_year", $next_date);
      $next = "<a href='$next_date' class='main'>Next-&gt;</a>";
      $prev_date = $_SERVER['PHP_SELF']."?".str_replace("&day=$day", "&day=$prev_day", $server_query);
      $prev_date = str_replace("&month=$month", "&month=$prev_month", $prev_date);
      $prev_date = str_replace("&year=$year", "&year=$prev_year", $prev_date);
      $prev = "<a href='$prev_date' class='main'>&lt;-Previous</a>";
       
      if ($actual) {
         $server_query = str_replace("&actual=1", "", $server_query);
         $link_name = "Potential";
      } else {
         $server_query .= "&actual=1";
         $link_name = "Actual";
      }
      $link = $_SERVER['PHP_SELF']."?".$server_query;
      $link = "<a href='$link' class='main'>View $link_name Schedule</a>";
      print "<td>&nbsp;</td><td align=right>$prev</td><td colspan=".($num_cols-3)." align=center>$link</td><td align=left>$next</td>";
      print "</tr></table></center>";
      }
      else print "No users found.";
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: ModAppointmentDisplay
   // Description:  handle linking exceptions for display
   //
   // Parameters:
   //   string $string  data to be checked
   //   int $position  position of the string that determines the link
   //   string $color  special color (if any)
   //
   // Return Values:
   //   string $output  the transformed string with or without link
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function ModAppointmentDisplay($string, $position, $color = "") {
      if (empty($color)) $class = "class='main'";
      else $class = "style='color=\'$color\''";
      switch ($position) {
         case 0:
         $output = "<a $class href='old_ticket.php?id=" . substr($string, 1) . "'>" . $string . "</a>";
         break;
         default:
         $output = $string;
      }
      return $output;
   }
    
   function ShowLegend() {
      $legend = array(
      "#ffffff" => "<b>Legend</b>",
         "#00bc55" => "All Consultants Available",
         "yellow" => "At Least 1 Consultant Scheduled",
         "red" => "All Consultants Scheduled",
         "#aaaaaa" => "No Consultants Available" );
      $table = "<table cellspacing=3 cellpadding=2 border=0 style='border: 1px solid #000000'>";
      foreach ($legend as $color => $key) {
         $table .= "<tr>";
         $table .= "<td bgcolor='$color' width=20>&nbsp;</td>";
         $table .= "<td>&nbsp;$key</td>";
         $table .= "</tr>";
      }
      $table .= "</table>";
      return $table;
   }
?>
