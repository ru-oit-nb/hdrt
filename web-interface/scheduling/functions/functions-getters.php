<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

   //
   // Filename:               functions-getters.php
   // Description:            Set of functions to retrieve and return information
   // Supprted Language(s):   PHP 4.0
   //
    
   //-----------------------------------------------------------------------------
   //
   // Function:         GetDistinctArrayFromTable
   // Description:      Retrieves information from a table.
   //
   // Parameters:
   //    string $field    database field to be returned
   //    string $table    table to select information from
   //    string $where    optional extensions to query
   //
   // Return Values:
   //    Returns array of values for the field with no duplicates.
   //
   // Remarks:
   //    None
   //-----------------------------------------------------------------------------
   function GetDistinctArrayFromTable ($field, $table, $where = '') {
      global $db;
      $result = mysql_query("select distinct $field from $table $where", $db);
      if ($result && mysql_num_rows($result) > 0) {
         $i = 0;
         while ($row = mysql_fetch_array($result)) {
            $field_array[$i] = $row[0];
            $i++;
         }
         return $field_array;
      }
      else return array();
   }

    
   //-----------------------------------------------------------------------------
   //
   // Function: GetBuildings
   // Description: returns a list of buildings
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    Returns an array of all building names
   //
   // Remarks:
   //
   //
   //-----------------------------------------------------------------------------
   function GetBuildings() {
      return GetArrayFromTable("building_name", "buildings",
         "order by building_name");
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetBuildingsForUser
   // Description: returns a list of all buildings a user can be scheduled in
   //
   // Parameters:
   //    string $netid  netid of user to search for
   //
   // Return Values:
   //    Returns an array of building names
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetBuildingsForUser($netid) {
      return GetArrayFromTable("building_name", "buildings,user_buildings",
         "where (building_group=group_value or
         campus=group_value) and
         netid='".$netid."' order by building_name");
   }
   //-----------------------------------------------------------------------------
   //
   // Function:  GetBuildingGroups
   // Description:  returns a list of all building groups
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    returns an array of building groups
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetBuildingGroups() {
      return GetDistinctArrayFromTable("building_group", "buildings",
         "order by building_group");
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetCampuses
   // Description: returns a list of the campuses
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    returns an array of campuses
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetCampuses() {
     global $db;
     $query = "DESCRIBE buildings campus";
     $result = mysql_fetch_object(mysql_query($query, $db));
     $result = substr($result->Type, 0, -1);
     $result = substr($result, 5);
     $result = str_replace("'", "", $result);
     return explode(",", $result);
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetBuildingNumbers
   // Description:  returns a list of all building numbers
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    returns an array of all building nubmers
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetBuildingNumbers() {
      return GetArrayFromTable("building_number", "buildings",
         "order by building_name");
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetBuildingNumbersForUser
   // Description:  returns a list of building numbers that the user can be
   //               scheduled in
   // Parameters:
   //    string $netid  netid of user to search for
   //
   // Return Values:
   //    returns an array of building numbers
   //
   // Remarks:
   //    noen
   //
   //-----------------------------------------------------------------------------
   function GetBuildingNumbersForUser($netid) {
      return GetArrayFromTable("building_number", "buildings,user_buildings",
         "where (building_group=group_value or
         campus=group_value) and
         netid='".$netid."' order by building_name");
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetSchedulingAllowed
   // Description: determines if scheduling is allowed for a queue
   //
   // Parameters:
   //    string $queue   name of queue to check
   //
   //
   // Return Values:
   //    returns 1 if scheduling is allowed
   //    returns 0 if scheduling disallowed
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetSchedulingAllowed($queue) {
      $rule = GetValueFromTable('allow_scheduling', 'queue',
         'where q_name="'.$queue.'"');
      if ($rule == "Allow" || $rule == "Require") {
         return 1;
      }
       
      return 0;
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetSchedulingPermissionsForTicket
   // Description: determines if the queue the ticket is in allows scheduling
   //
   // Parameters:
   //    int $ticket_id  id of ticket to check
   //
   // Return Values:
   //    returns 1 if scheduling is allowed
   //    returns 0 if scheduling disallowed
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetSchedulingPermissionsForTicket($ticket_id) {
      $queue = GetValueFromTable("queue", "ticket", "where id = '" .$ticket_id."'");
      $scheduling = GetValueFromTable("allow_scheduling", "queue",
         "where q_name = '".$queue."'");
      if ($scheduling == 'Allow' || $scheduling == 'Require') {
         return 1;
      }
       
      else
         {
         return 0;
      }
       
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetSchedulableGroupsForUser
   // Description:  returns a list of groups that the user is allowed to schedule
   //
   // Parameters:
   //    string $netid  username to check
   //
   //
   // Return Values:
   //    returns an array of groups user is allowed to schedule
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetSchedulableGroupsForUser($netid) {
      $user_in_groups = GetDistinctArrayFromTable("group_name", "ingroup",
         "where username='".$netid."'");
      $where = "where privgroup=";
      if (is_array($user_in_groups)) {
         foreach ($user_in_groups as $or) {
            //$or = str_replace("_", " ", $or);
            $where .= "'" . $or ."'" . " or privgroup=";
         }
         $where .= "'' and (rule='Edit' or rule='Schedule')";
         return GetDistinctArrayFromTable("schedgroup", "sched_rules", $where);
      }
       
      else return array();
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetSchedulableGroupsForUser
   // Description:  returns a list of groups that the user is allowed to view
   //               schedules of
   // Parameters:
   //    string $netid  username to check
   //
   // Return Values:
   //    returns an array of groups user is allowed to view
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetViewableGroupsForUser($netid) {
      $user_in_groups = GetDistinctArrayFromTable("group_name", "ingroup",
         "where username='".$netid."'");
       
      $where = "where privgroup=";
      if (is_array($user_in_groups)) {
         foreach ($user_in_groups as $or) {
            //$or = str_replace("_", " ", $or);
            $where .= "'" . $or ."'" . " or privgroup=";
         }
         $where .= "'' and (rule='View' or rule='Edit')";
         return GetDistinctArrayFromTable("schedgroup", "sched_rules", $where);
      }
       
      else return array();
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetViewableUsersForUser
   // Description:  returns a list of users for whom the user is allowed to
   //               see schedules
   //
   // Parameters:
   //    string $username  user to check
   //
   // Return Values:
   //    returns an array of usernames
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetViewableUsersForUser($username) {
      global $db;
      $groups = GetViewableGroupsForUser($username);
      $q = "SELECT distinct username FROM ingroup WHERE ";
       
      foreach ($groups as $group) {
         $q .= " group_name='$group' OR";
      }
       
      $q = substr($q, 0, -2) . " ORDER BY username";
      $people = mysql_query($q, $db);
       
      while ($r = mysql_fetch_array($people)) {
         $allowed_users[] = $r['username'];
      }
       
      return $allowed_users;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetIsScheduled
   // Description:  Find if the time slot has a consultant scheduled to work
   //
   // Parameters:
   //   int $hour  location is hour array
   //   int $day  day of the week starting from Sunday=0
   //   string $group_value  name of the building group or campus
   //   bool $overview  flag for overview mode
   //   string $user  special case for searching by user
   //
   // Return Values:
   //   return mysql resource id
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsScheduled($hour, $day, $group_value, $overview = 0, $user = "") {
      global $db;
      $day = ($day%7)+1;
      $hours = GetHours();
      $q = "select netid from potential_schedule where day=$day and hours like '%" .$hours[$hour]."%' and (  ";
       
      if (empty($user)) {
         $view_users = GetIsOnSchedule($day, $group_value, $overview);
         $num_users = mysql_num_rows($view_users);
         while ($users = mysql_fetch_array($view_users)) {
            $user = $users['username'];
            $q .= " netid='$user' or";
         }
         $q = substr($q, 0, -2).")";
         //strip last or
          
         if ($num_users == 0) {
            $q = str_replace(" and ()", "", $q);
            $result = $view_users;
         }
          
         else
            {
            $result = mysql_query($q, $db);
         }
          
      }
       
      else
         {
         $q .= " netid='$user' )";
         //echo $q;
         $result = mysql_query($q, $db);
      }
       
      return $result;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetUsersScheduledToday
   // Description:  Find the users scheduled today
   //
   // Parameters:
   //   int $day  day of the week starting with Sun=0
   //   string $group_value  name building group or campus
   //   bool $overview  flag for overview mode
   //
   // Return Values:
   //   returns mysql resource id
   //
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetUsersScheduledToday($day, $group_value, $overview = 0) {
      global $db;
      $day = ($day%7)+1;
      $view_users = GetIsOnSchedule($day, $group_value, $overview);
      $q = "select netid from potential_schedule where day=$day and
         (hours like '%00%' or hours like '%30%') and (";
       
      while ($users = mysql_fetch_array($view_users)) {
         $user = $users['username'];
         $q .= " netid='$user' or";
      }
       
      $q = substr($q, 0, -2).")";
      //strip last or
      $q .= " ORDER BY netid";
      $result = mysql_query($q, $db);
      return $result;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetIsOnSchedule
   // Description:  determines set of usernames allowed to work
   //
   // Parameters:
   //   int $day  day of the week starting with Sun=0
   //   string $group_value  name building group or campus
   //   bool $overview  flag for overview mode
   //
   // Return Values:
   //   returns mysql resource id
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsOnSchedule($day, $group_value, $overview = 0) {
      global $db, $username;
      $day = ($day%7)+1;
      $groups = GetViewableGroupsForUser($username);
      if ($overview) {
         $q = "SELECT distinct username FROM ingroup,user_buildings,buildings
            WHERE ingroup.username=user_buildings.netid AND
            (user_buildings.group_value='$group_value' OR
            (user_buildings.group_value=buildings.building_group AND
            buildings.campus='$group_value')) AND (";
      }
       
      else
         {
         $q = "SELECT distinct username FROM ingroup,user_buildings,buildings
            WHERE ingroup.username=user_buildings.netid
            AND (user_buildings.group_value='$group_value'
            OR (user_buildings.group_value=buildings.campus
            AND buildings.building_group='$group_value'))
            AND (";
      }
       
      foreach ($groups as $group) {
         $q .= " ingroup.group_name='$group' OR";
      }
       
      $q = substr($q, 0, -2);
      //strip last or
      $q .= ") ORDER BY username";
      $users = mysql_query($q, $db);
      return $users;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetIsUserScheduled
   // Description:  determines if username is allowed to work
   //
   // Parameters:
   //   int $hour  array position of hour
   //   int $day day of the week starting with Sun=0
   //   string $group_value  name of building group or campus
   //   string $username  name of the user to be searched
   //
   // Return Values:
   //   returns mysql resource id
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsUserScheduled($hour, $day, $group_value, $username) {
      global $db;
      $day = ($day%7)+1;
      $hours = GetHours();
      $q = "select netid from potential_schedule where day=$day
         and hours like '%".$hours[$hour]."%' and netid='$username'";
      $result = mysql_query($q, $db);
      $scheduled = (mysql_num_rows($result) > 0) ? 1 :
      0;
      return $scheduled;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetIsAppointment
   // Description:  determine if the time slot is taken by an appointment
   //
   // Parameters:
   //   int $hour  array position of hour
   //   int $day  day of the week starting with Sun=0
   //   int $month  numeric month
   //   int $year  4-digit numeric year
   //   mysql resource id $users  contains list of users
   //
   // Return Values:
   //   returns mysql resource id
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsAppointment($hour, $day, $month, $year, $netid) {
      global $db;
      $hours = GetHours();
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
      if ($day > $num_days[$month]) {
         $day -= $num_days[$month];
         $month++;
         if ($month == 13) {
            $month = 1;
            $year++;
         }
      }
      if ($day < 10) $day = "0".$day;
      if ($month < 10) $month = "0".$month;
      $max_hour = $hour+1;
      for ($i = $hour-2; $i < $max_hour; $i++) {
         $appointments .= "datetime='" . $year . "-" . $month . "-" . $day . " " . $hours[$i] . "' or ";
      }
       
      $appointments = substr($appointments, 0, -3);
      $location = "concat(buildings.building_name,' ',appointments.room_number)";
      $q = "select concat('#',ticket_id),$location,status from
         appointments,buildings where ($appointments) and
         consultant_netid = '$netid' and status<>1 and
         status<>5 and appointments.building_number=buildings.building_number";
      return mysql_query($q, $db);
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetIsAppointmentUser
   // Description:  determine if a user has an appointment in a time slot
   //
   // Parameters:
   //   int $hour  array position of hour
   //   int $day  day of the week starting with Sun=0
   //   int $month  numeric month
   //   int $year  4-digit numeric year
   //   string $user  user to be searched
   //
   // Return Values:
   //   returns mysql resource id
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsAppointmentUser($hour, $day, $month, $year, $user) {
      global $db;
      $hours = GetHours();
      $num_days = array(1 => 31, 2 => 28 + CheckLeapYear($year), 3 => 31, 4 => 30,
         5 => 31, 6 => 30, 7 => 31, 8 => 31, 9 => 30, 10 => 31,
         11 => 30, 12 => 31);
      if ($day > $num_days[$month]) {
         $day = 1;
         $month++;
         if ($month == 13) {
            $month = 1;
            $year++;
         }
      }
       
      if ($day < 10) $day = "0".$day;
       
      if ($month < 10) $month = "0".$month;
      $max_hour = $hour+1;
       
      for ($i = $hour-2; $i < $max_hour; $i++) {
         $appointments .= "datetime='" . $year . "-" . $month . "-" . $day . " " . $hours[$i] . "' or ";
      }
       
      $appointments = substr($appointments, 0, -3);
      $location = "concat(buildings.building_name,' ',appointments.room_number)";
      $q = "select concat('#',appointments.ticket_id),$location,status from
         appointments,buildings where ($appointments) and
         consultant_netid='$user' and status<>1 and status<>5 and
         appointments.building_number=buildings.building_number";
       
      $result = mysql_query($q, $db);
      return $result;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetHours
   // Description: returns a list of the possible hours
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    returns an array of hours
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetHours() {
      $hours = array(
      '12:00', '12:30', '01:00', '01:30', '02:00', '02:30', '03:00',
         '03:30', '04:00', '04:30', '05:00', '05:30', '06:00', '06:30',
         '07:00', '07:30', '08:00', '08:30', '09:00', '09:30', '10:00',
         '10:30', '11:00', '11:30', '12:00', '12:30', '13:00', '13:30',
         '14:00', '14:30', '15:00', '15:30', '16:00', '16:30', '17:00',
         '17:30', '18:00', '18:30', '19:00', '19:30', '20:00', '20:30',
         '21:00', '21:30', '22:00', '22:30', '23:00', '23:30');
       
      return $hours;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetDaysForHour
   // Description:  returns the days a user is scheduled for a particular hour
   //
   // Parameters:
   //    string $hour   hour to check schedule
   //    string $netid  netid of user
   //
   // Return Values:
   //    returns an array of days
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetDaysForHour($hour, $netid) {
      $hours = GetHours();
      $buffer_days = GetArrayFromTable("day", "potential_schedule",
         'where hours like "%'.$hours[$hour].'%"
         and netid="'.$netid.'"');
       
      for ($i = 0; $i < sizeof($buffer_days); $i++) {
         $days[$buffer_days[$i]] = $hour;
      }
       
      return $days;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetPersonToSchedule
   // Description:  returns the netid of a person to schedule for the selected
   //               timeslot
   //
   // Parameters:
   //    string $day            day of week to schedule for
   //    int $hour              hour to check for availability
   //    int $datetime          day and time of appointment
   //    string $group          group to get users from
   //    string $building_name  building name to schedule in
   //
   // Return Values:
   //    return a netid
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetPersonToSchedule($day, $hour, $datetime, $group, $building_name) {
      $real_hours = GetHours();
      $possibilities = GetDistinctArrayFromTable("potential_schedule.netid",
         "potential_schedule, ingroup, user_buildings, buildings",
         "where building_name='".$building_name."' and
         (building_group=group_value or campus=group_value)
         and user_buildings.netid=potential_schedule.netid and
         group_name='".$group."' and
         username=potential_schedule.netid and day='".$day."' and
         hours like '%".$real_hours[$hour].','.$real_hours[$hour+1] .','.$real_hours[$hour+2]."%' order by rand()");
       
      if (sizeof($possibilities) <= 0) {
         return 0;
      }
       
      $consultants = "";
       
      for ($i = 0; $i < sizeof($possibilities); $i++) {
         $consultants .= "'".$possibilities[$i]."' or ";
      }
       
      $consultants .= "''";
      $not_possibilities = GetDistinctArrayFromTable('consultant_netid',
         'appointments', 'where consultant_netid=(' .$consultants.') and datetime between ' .($datetime-10000).' and '.($datetime+10000));
       
      if (sizeof($not_possibilities) > 0) {
         foreach($not_possibilities as $remove) {
            ;
             
            if (in_array($remove, $possibilities)) {
               $key = array_search($remove, $possibilities);
               unset ($possibilities[$key]);
            }
             
         }
          
      }
       
      $possibilities = array_values($possibilities);
      return $possibilities[0];
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetAvailablePeople
   // Description:  returns the netids of available people for the selected
   //               timeslot
   //
   // Parameters:
   //    string $day            day of week to schedule for
   //    int $hour              hour to check for availability
   //    int $datetime          day and time of appointment
   //    string $group          group to get users from
   //    string $building_name  building name to schedule in
   //
   // Return Values:
   //    return an array of netids
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetAvailablePeople($day, $hour, $datetime, $group, $building_name) {
      $real_hours = GetHours();
      $possibilities = GetDistinctArrayFromTable("potential_schedule.netid",
         "potential_schedule, ingroup, user_buildings, buildings",
         "where building_name='".$building_name."' and
         (building_group=group_value or campus=group_value) and
         user_buildings.netid=potential_schedule.netid and
         group_name='".$group."' and
         username=potential_schedule.netid and day='".$day."' and
         hours like '%".$real_hours[$hour].','.$real_hours[$hour+1] .','.$real_hours[$hour+2]."%' order by
         potential_schedule.netid");
       
      $consultants = "";
       
      for ($i = 0; $i < sizeof($possibilities); $i++) {
         $consultants .= "'".$possibilities[$i]."' or ";
      }
       
      $consultants .= "''";
       
      $not_possibilities = GetDistinctArrayFromTable('consultant_netid',
         'appointments', 'where consultant_netid=(' .$consultants.') and datetime between ' .($datetime-10000).' and '.($datetime+10000));
       
      foreach($not_possibilities as $remove) {
         ;
         if (in_array($remove, $possibilities)) {
            $key = array_search($remove, $possibilities);
            unset ($possibilities[$key]);
         }
          
      }
       
      $possibilities = array_values($possibilities);
      return $possibilities;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetEditableGroupsForUser
   // Description: returns the groups that a user can edit schedules of
   //
   // Parameters:
   //    string $netid   user to check for
   //
   // Return Values:
   //    returns an array of groups
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetEditableGroupsForUser($netid) {
      global $username;
      $ingroups = GetDistinctArrayFromTable("group_name", "ingroup",
         "where username='".$username."'");
       
      $where = "where rule='Edit' and (";
      foreach ($ingroups as $ingroup) {
         $where .= "privgroup='$ingroup' or ";
      }
      $where .= "privgroup='')";
       
      $schedgroups = GetDistinctArrayFromTable("schedgroup", "sched_rules", $where);
       
      $where = "where (";
       
      for ($i = 0; $i < sizeof($schedgroups); $i++) {
         $where .= "group_name='".$schedgroups[$i]."' or ";
      }
       
      $where .= "group_name='') order by username";
       
      return GetDistinctArrayFromTable("username", "ingroup", $where);
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetSchedulingHourRanges
   // Description:  sets values for items that will be global
   //
   // Parameters:
   //    none
   //
   // Return Values:
   //    none
   //
   // Remarks:
   //    global values defined in header, assigned values in configuration
   //
   //-----------------------------------------------------------------------------
   function GetSchedulingHourRanges() {
      $START_HOUR = GetValueFromTable("option_value", "sched_options", "where option_name='Earliest Available Time'");
      $END_HOUR = GetValueFromTable("option_value", "sched_options", "where option_name='Latest Available Time'");
      $FIRST_APPOINTMENT = GetValueFromTable("option_value", "sched_options", "where option_name='First Appointment Start Time'");
      $LAST_APPOINTMENT = GetValueFromTable("option_value", "sched_options", "where option_name='Last Appointment Start Time'");
      $HOURS_BEFORE = GetValueFromTable("option_value", "sched_options", "where option_name='Minimum Hours To Schedule Before Appointment'");
      return array($START_HOUR, $END_HOUR, $FIRST_APPOINTMENT, $LAST_APPOINTMENT, $HOURS_BEFORE);
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetAppointmentInfo
   // Description: returns information about an appointment
   //
   // Parameters:
   //    int $id  appointment_id
   //
   //
   // Return Values:
   //    returns a mysql_resource_id of information about the appointment
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetAppointmentInfo($id) {
      global $db;
      $query = "select * from appointments where appointment_id=".$id." limit 1";
      $result = mysql_query($query, $db);
      if (mysql_errno()) {
         print ErrorReport("There has been a database error.", "50%");
      }
      if (mysql_num_rows($result) <= 0) {
         print ErrorReport("Invalid Appointment", "50%");
      }
      return $result;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetTicketInfo
   // Description:  returns information about a ticket
   //
   // Parameters:
   //   int $id  ticket_id
   //
   // Return Values:
   //   returns a mysql_resource_id of information about the ticket
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GetTicketInfo($id) {
      global $db;
      $query = "select * from ticket where id=".$id." limit 1";
      $result = mysql_query($query, $db);
      if (mysql_errno()) {
         print ErrorReport("There has been a database error.", "50%");
      }
      if (mysql_num_rows($result) <= 0) {
         print ErrorReport("Invalid Ticket", "50%");
      }
      return $result;
   }
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GetIsDatetimePast
   // Description:  checks if appointment's scheduled time has past
   //
   // Parameters:
   //   int $appointment_id  appointment_id
   //
   // Return Values:
   //   returns 1 if time is past, 0 if not
   //
   // Remarks:
   //   none
   //
   //-----------------------------------------------------------------------------
   function GetIsDatetimePast($appointment_id) {
      global $db;
      $query = 'select now() > datetime test from appointments where appointment_id='.$appointment_id;
      $test = mysql_fetch_array(mysql_query($query, $db));
      return $test['test'];
   }
    
?>
