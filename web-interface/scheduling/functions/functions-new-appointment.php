<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

   //
   // Filename: functions-new-appointment.php
   // Description: Provides functions for creating appointments.
   // Supprted Language(s):   PHP 4.0
   //
    
   //-----------------------------------------------------------------------------
   //
   // Function: CreateAppointment
   // Description: creates a new appointemnt
   //
   // Parameters:
   //    int $ticket_id       ticket appointment will be associated with
   //    int $building_number where the appointment will be scheduled
   //    string $room_number     room number in building
   // Return Values:
   //    returns the mysql_insert_id of the appointment if successful,
   //    returns the error if unsuccessful
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function CreateAppointment($ticket_id, $building_number, $room_number) {
      global $db;
      $query = "insert into appointments set ticket_id='$ticket_id',
         building_number='$building_number', room_number='$room_number'";
      $result = mysql_query($query);
       
      if (mysql_errno()) {
         return mysql_error();
      }
       
      else
         {
         return mysql_insert_id();
      }
   }
    
    
    
   //-----------------------------------------------------------------------------
   //
   // Function:  GenerateAppointmentDateSelection
   // Description:  generates a select box for date selection
   //
   // Parameters:
   //    int $day_selected   day to select from drop down
   //
   // Return Values:
   //    returns html for a form select box indexed by a timestamp for each day
   //
   // Remarks:
   //    none
   //
   //-----------------------------------------------------------------------------
   function GenerateAppointmentDateSelection($day_selected = "") {
      $now = mktime("0", "0", "0", date("n"), date("j"), date("Y"));
      for ($i = $now; $i <= ($now+14 * 86400); $i = $i+86400) {
         $month_dates[$i] = date("l, F d, Y", $i);
      }
      $form = SelectField("day", $month_dates, $month_dates[$day_selected]);
      return $form;
   }
    
?>
