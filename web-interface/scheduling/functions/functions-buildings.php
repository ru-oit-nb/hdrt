<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey  
//    This file is part of ruQueue.  
//  
//    ruQueue is free software; you can redistribute it and/or modify  
//    it under the terms of the GNU General Public License as published by  
//    the Free Software Foundation; either version 2 of the License, or  
//    (at your option) any later version.  
//  
//    ruQueue is distributed in the hope that it will be useful,  
//    but WITHOUT ANY WARRANTY; without even the implied warranty of  
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
//    GNU General Public License for more details.  
//  
//    You should have received a copy of the GNU General Public License  
//    along with ruQueue; if not, write to the Free Software  
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA  

require_once("functions-getters.php");

   //
   // Filename:               functions-buildings.php
   // Description:            Functions to Maintain Building Database for ruQueue Scheduling System
   // Supprted Language(s):   PHP 4.0
   //
    
   //-----------------------------------------------------------------------------
   //
   // Function: EditBuilding
   // Description: Create building in database
   //
   // Parameters:
   //    string $building_name    Name of building
   //    string $campus           Campus of building
   //    string $building_group   Group of building
   //
   // Return Values:
   //    Returns specific errors if error.
   //    Returns successful message if successful.
   //
   // Remarks:
   //    None
   //
   //-----------------------------------------------------------------------------
   function CreateNewBuilding ($building_name, $campus, $building_group) {
      if ($building_name == "") {
         return "Please enter a building name.";
      }
      if ($campus < 0) {
         return "Please select a campus";
      }
      if ($building_group == "") {
         return "Please enter a building group.";
      }
      global $db;
      $result = mysql_query("insert into buildings values
         ('','$building_name','$campus','$building_group')", $db);
      if (mysql_errno()) {
         return mysql_error();
      } else {
         return "New Building Created Successfully";
      }
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: EditBuilding
   // Description: Update previously entered building information
   //
   // Parameters:
   //    int $building_number     Unique id for building information in db
   //    string $building_name    New name of building
   //    string $campus           New campus
   //    string $building_group   New building group
   //
   // Return Values:
   //    Returns specific errors if error.
   //    Returns successful message if successful.
   //
   // Remarks:
   //    None
   //
   //-----------------------------------------------------------------------------
   function EditBuilding($building_number, $building_name, $campus, $building_group) {
      if ($building_name == "") {
         return "Please enter a building name.";
      }
      if ($campus == "") {
         return "Please select a campus";
      }
      if ($building_group == "") {
         return "Please enter a building group.";
      }
      global $db;
      $result = mysql_query("update buildings set building_name='$building_name',
         campus='$campus', building_group='$building_group' where
         building_number=$building_number", $db);
      if (mysql_errno()) {
         return mysql_error();
      } else {
         return "Building Information Updated Successfully";
      }
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: DeleteBuilding
   // Description: Delete building information from database
   //
   // Parameters:
   //    int $building_number     Unique id for building information in db
   //
   // Return Values:
   //    Returns specific errors if error.
   //    Returns successful message if successful.
   //
   // Remarks:
   //    None
   //
   //-----------------------------------------------------------------------------
   function DeleteBuilding($building_number) {
      global $db;
      $result = mysql_query("delete from buildings where
         building_number=$building_number", $db);
      if (mysql_errno()) {
         return mysql_error();
      } else {
         return "Building Deleted Successfully";
      }
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: GetBuildingArray
   // Description: Get details of an individual building
   //
   // Parameters:
   //    int $building_number     Unique id for building information in db
   //
   // Return Values:
   //    Returns specific errors if error.
   //    Returns array of building values.
   //
   // Remarks:
   //    None
   //
   //-----------------------------------------------------------------------------
   function GetBuildingArray($building_number) {
      global $db;
      $result = mysql_query("select * from buildings
         where building_number=$building_number", $db);
      return mysql_fetch_array($result);
   }
    
    
   //-----------------------------------------------------------------------------
   //
   // Function: EditBuildingGroup
   // Description: Change Group Name.
   //
   // Parameters:
   //    string $old   Current name of building group
   //    string $new   New name of building group
   //
   // Return Values:
   //    Returns specific errors if error.
   //    Returns successful message if successful.
   //
   // Remarks:
   //    None
   //
   //-----------------------------------------------------------------------------
   function EditBuildingGroup($old, $new) {
      global $db;
      mysql_query("update user_buildings set group_value='$new' where
         group_value='$old'", $db);
      $query = "update buildings set building_group='$new' where
         building_group='$old'";
      mysql_query($query, $db);
      $building_groups = GetDistinctArrayFromTable("building_group", "buildings", "order by building_group");
      if (mysql_errno()) {
         return mysql_error();
      } else {
         return "Group Name Changed Successfully";
      }
       
   }

// -------------------------------------------------------- 
// Function:                Class::CreateCampus

// Description:             add a new campus through alter tables

// Type:                    public

// Parameters:
//    string $campus        campus name

// Return Values:           
//    interger              -1 if an error occured
//                          0 if authentication failed
//                          1 on sucess

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function CreateCampus($campus) {
  global $db;
  $campuses = GetCampuses();
  if (!in_array($campus, $campuses)) {
    $campuses[] = $campus;
    $enum = "enum(";
    foreach ($campuses as $value) {
      $enum .= "'$value',";
    }
    $enum = substr($enum, 0, -1).")";
    $query = "ALTER TABLE buildings CHANGE campus campus $enum";
    mysql_query($query, $db);
    $query = "ALTER TABLE appointments_cache CHANGE campus campus $enum NOT NULL";
    mysql_query($query, $db);
  }
}


?>
