<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


// Filename:                index.php
// Description:             Front Page
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-01-25 11:58:37 jfulton> 

// Initiate Security
require_once("header.php");

// Required Files
require_once("scheduling/functions/functions-display.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-index.php");
require_once("functions/functions-ldap.php");

global $SCHEDULING,$db;
unset($_SESSION['search_fields']);
$output = head("Start Page", "$username", $rate);
if (!is_array($_SESSION['search_fields'])) $_SESSION['search_fields'] = array();
$output .= '<table border="0" width="100%"><tr valign=top><td width="66%">';

if (!(IsEmailSet($username))) {
   $url = "admin.php?item=Users&user=$username";
   $output .= OpenColorTable("red", "Warning", "100%")
           ."<tr valign=top><td width=\"66%\">"
           ."You have not set your email address."
           ."<a class=\"main\" href = \"$url\">Click here</a>"
           ." to set your info.</td></tr>"
           .CloseColorTable();
}

$output .= "<p>"
        .OpenColorTable("green", "Tickets", "100%")
        .ShowCreateTicketForm()
        .CloseColorTable()
        ."</td><td><p>"
        .OpenColorTable("green", "Refresh", "100%")
        .Refresh($_GET['rate'])
        .CloseColorTable();
$display_overview = GetArrayFromTable("display_overview", "staffprefs where username='".$username."'");
if ($display_overview[0] != "0") $output .= '</td></tr><tr><td valign="top">';
else $output .= '</td></tr></tr><td colspan=2>';
$tr = GetFirstRowFromTable("staff where username = '$username'");
$email = $tr->email;

// Displays a user's Appointments
if ($SCHEDULING==1){
   $query = "select ticket_id '#', ";
   $query .= "date_format(datetime,'%a %M %e, %Y at %h:%i %p') date, ";
   $query .= "concat('Room ',room_number,' in ',building_name) ";
   $query .= "location, campus from appointments, buildings ";
   $query .= "where appointments.building_number=buildings.building_number ";
   $query .= "and consultant_netid='$username' and status='Scheduled' ";
   $query .= "order by datetime";
   $result=mysql_query($query, $db);
   if (mysql_num_rows($result)>0){
      $output .= OpenColorTable("orange", "Appointments", "100%");
      $output .= DisplayAppointmentsTable($result);
      $output .= CloseColorTable();
      $output .= "<p>";
   }
}

// Displays a user's owned tickets.
$query = "select owner from ticket where owner='$username' and current_status!='Resolved'";
$result = mysql_query($query, $db);
if (mysql_num_rows($result) != 0){
   $output .= OpenColorTable("red", "25 highest priority tickets I own...","100%");
   $output .= GetTicketTable(25, $username);
   $output .= CloseColorTable();
   $output .= '<p>';
}

// Displays a user's requested tickets.
$query = "select email from ticket,ticketwatcher where email='$email' "; 
$query .= "and ticketwatcher.ticket_id=ticket.id "; 
$query .= "and ticket.current_status!='Resolved' "; 
$query .= "and ticketwatcher.type='Requester'";
$result = mysql_query($query, $db);
if (mysql_num_rows($result)!=0){
   $output .= OpenColorTable("yellow", "25 highest priority tickets I requested...", "100%");
   $output .= GetTicketTable(25, $username, "requested");
   $output .= CloseColorTable();
   $output .= '<p>';
}

// Displays a user's admincc'ed tickets.
$query = "select email from ticket,ticketwatcher where email='$email' ";
$query .= "and ticketwatcher.ticket_id=ticket.id ";
$query .= "and ticket.current_status!='Resolved' ";
$query .= "and ticketwatcher.type='AdminCC'";
$result = mysql_query($query, $db);
if (mysql_num_rows($result)!=0){
   $output .= OpenColorTable("orange","5 highest priority tickets I AdminCCed...","100%");
   $output .= GetTicketTable(5, $username, "admincc");
   $output .= CloseColorTable();
   $output .= '<p>';
}

// Displays a user's cc'ed tickets.
$query = "select email from ticket,ticketwatcher where email='$email' ";
$query .= "and ticket_id=id and current_status!='Resolved' and type='CC'";
$result=mysql_query($query, $db);
if (mysql_num_rows($result)!=0){
   $output .= OpenColorTable("purple","5 highest priority tickets I CCed...","100%");	
   $output .= GetTicketTable(5, $username, "cc");
   $output .= CloseColorTable();
}
$output .= "</td><td>";

// Display Queue and Ticket Overview
if ($display_overview[0] != "0"){
   $output .= OpenColorTable("blue", "Find new/open tickets", "100%");
   $output .= OverviewList();
   $output .= CloseColorTable();
}    
$output .= "</table>";
$output .= Foot();
print $output;//Display the Page
?>
