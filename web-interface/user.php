<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
// Filename: user.php
// Description: Allows a user to edit their preferences.
// Supprted Language(s):   PHP 4.0

// Initiate security
require_once("header.php");

// Required Files
require_once("functions/functions-widgets.php");
require_once("functions/functions-user.php");
require_once("functions/functions-setters.php");

$output = head("Preferences", "$username");

if (IsSupervisor($username)) {
   $output .= OpenColorTable("yellow", "E-mail Preferences", "100%");
   $output .= EmailPrefsForm($username);
   $output .= CloseColorTable();
}

$output .= "<p>";	
$output .= OpenColorTable("green", "Change Overview Display", "100%");
$output .= ChangeDisplayOverview($username, $_POST['display_overview']);
$output .= CloseColorTable();

$output .= "<p>";	
$output .= OpenColorTable("green", "Change Email Signature", "100%");
$output .= ChangeSignatureForm($username, $_POST['new_signature']);
$output .= CloseColorTable();

$output .= "<p>";	
$output .= OpenColorTable("green", "Change HTML Preferences", "100%");
$output .= ChangeHTMLComment($username, $_POST['html_in_comments']);
$output .= CloseColorTable();

$output .= "<p>";	
$output .= OpenColorTable("blue", "Change Default Refresh Time", "100%");
$output .= RefreshForm($_POST['rate']);
$output .= CloseColorTable();

global $AUTH_TYPE;
if ($AUTH_TYPE == "local"){
   $output .= "<p>";	
   $output .= OpenColorTable("red", "Change Local Password", "100%");
   $output .= ChangePasswordForm($username, $_POST['passwd1'], $_POST['passwd2']);
   $output .= CloseColorTable();
}

$output .= foot();
print $output; //display the page
?>
