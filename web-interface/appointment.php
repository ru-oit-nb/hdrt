<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: appointment.php
// Description: Provide display for appointment histories
// Supprted Language(s): PHP 4.0
//

require_once("scheduling/functions/functions-appointment.php");
require_once("functions/functions-widgets.php");
 
require_once("header.php");
$id = $_GET['id'];
$output = Head("Appointment #$id", $username, $rate)
        ."<table cellspacing=3 cellpadding=0 border=0 width=\"100%\">"
        ."<tr valign=top><td width=50%>"
        .AppointmentInfo()."<p>".UserInfo()
        ."</td><td width=\"50%\">"
        .CommentHistory()."</td></tr></table>"
        .foot();
print $output;
?>
