<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

//
// Filename: about.php
// Description: About page.
// Supprted Language(s):   PHP 4.0
//
include("header.php");
print head("About this system", $username);
?>
<table width="100%">
 <tr>
  <td>

<p>
ruQueue was created by the
<a class = "main" href="http://www.nbcs.rutgers.edu/">Office of Information Technology: New Brunswick Computing Services</a>
of 
<a class = "main" href="http://www.rutgers.edu/">Rutgers</a>, the 
State University of New Jersey and is available as 
<a class = "main" href="http://www.fsf.org/licensing/essays/free-sw.html">Free Software</a>.


<p>
For more information please see the 
<a class = "main" href="http://ruqueue.rutgers.edu/">ruQueue</a> 
project page.   

  </td>
  <td align="right">
<a class = "main" href="http://www.mysql.com"><img border=1 alt="Powered by mysql" src="images/mysql_logo.png"></a><br>
<a class = "main" href="http://www.php.net"><img border=1 alt="Powered by PHP" src="images/php_logo.png"></a>

  </td>
 </tr>
</table>

<br />
<?php
   $date = date("m/d/y", getlastmod());
print foot("", $date);
?>
