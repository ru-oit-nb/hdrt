<?php
// Filename:      buildings.php
// 
// Description:   Light "web service" for Javascript pull-downs
//                Populates campus/building special field helper
//                via buildings.js.  
// 
//                Returns XML list of buildings if ?campus not 
//                empty, otherwise returns XML of buidlings.   
// 
//                Javascript and ID tags embeded in HTML and 
//                then passed by an AJAX call won't work directly 
//                in IE.  I.e you can't just set innerHTML to new 
//                ID tags or new javascript.  You have to pass DOM 
//                objects.  This amounts to passing XML (which is 
//                the HTML you wanted in the first place) and then 
//                parsing it with DOM to convert it into... HTML!
//  
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2006-06-29 10:12:59 jfulton> 
// -------------------------------------------------------- 
// Copyright (c) 2006, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

// -------------------------------------------------------- 
require_once('ruqueue_mysql.php');
// -------------------------------------------------------- 
$campus = mysql_escape_string($_GET['campus']);
$sql = "SELECT campus, building_name FROM buildings ";
if (!empty($campus)) {
  $sql .= "WHERE campus='$campus' ORDER BY building_name";
  $id = 'trig_building';
  $onChange= "setField('building');";  // sending jscript to jscript
  $field = 'building_name';
  $div_val = 'trig_building_div';
}
else {
  $sql .= "GROUP BY campus ORDER BY campus";
  $id = 'trig_campus';
  $onChange= "setField('campus'); sndReq('building')";  // IE doesn't like this (and caches it) 
  $field = 'campus';
  $div_val = 'trig_campus_div';
}
$div = "<div id=\"$div_val\">\n";
$select  = "  <select id=\"$id\" onchange=\"$onChange\">\n";
$options = "    <option value=\"\"></option>\n";
$result = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_object($result)) {
  $options .= "    <option value=\"" . $row->{$field} . "\">" . $row->{$field}  . "</option>\n";
}
$xml = $div . $select . $options . "  </select>\n</div>\n";
header('Content-type: text/xml');  // or Firefox will say XMLHttpRequest().responseXML is null
print $xml;
mysql_close();
?>