<?php
//xml source constants
define("STRING", 0, true);
define("FILE", 1, true);

class XMLDoc {
  var $xml;
  var $root;
  var $tags;
  var $parse_error;
  //XMLDoc constructor
  function XMLDoc($source = STRING, $value = "") {
    $dom = ($source == FILE) ? 
      @domxml_open_file($value, DOMXML_LOAD_DONT_KEEP_BLANKS):
      @domxml_open_mem($value, DOMXML_LOAD_DONT_KEEP_BLANKS);
    if ($dom) {
      $this->xml = $dom;
      $this->root = $this->xml->document_element();
      $this->tags = $this->GetAllTags();
      $this->parse_error = false;
    }
    else $this->parse_error = true;
  }
  //return array of all tags
  function GetAllTags() {
    return $this->xml->get_elements_by_tagname("*");
  }
  //see if is a tag
  function IsTag($needle) {
    foreach ($this->tags as $tag) {
      if ($tag->tagname == $needle) return true;
    }
    return false;
  }
  //find a tag name
  function FindTag($tag) {
    $result = $this->root->get_elements_by_tagname($tag);
    return $result;
  }
  //get attributes for tag
  function GetAttributesForTag($tag) {
    $nodes = $this->FindTag($tag);
    $attrs = array();
    foreach ($nodes as $node) array_push($attrs, $node->attributes());
    return $attrs;
  }
  //get usable attribute array for tag
  function GetUsableAttributeArray($tag, $node_id = 0) {
    $nodes = $this->GetAttributesForTag($tag);
    $attributes = array();
    $attribute_list = $nodes[$node_id];
    if (empty($attribute_list)) return $attributes;
    foreach ($attribute_list as $attribute) {
      $attributes[$attribute->name] = $attribute->value;
    }
    return $attributes;
  }
}
?>