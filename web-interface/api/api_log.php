<?php
// Filename:                api_log.php
// Description:             Logs each API transaction
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2006-05-25 16:39:30 jfulton> 
// -------------------------------------------------------- 
// Store XML in and XML out in the following table: 
/*
CREATE TABLE api_log (
  api_log_id int(11) NOT NULL auto_increment,
  xml_in text,
  xml_out text,
  host varchar(80) default NULL,
  date_created timestamp(14) NOT NULL,
  PRIMARY KEY (api_log_id)
);
*/
// Create the above table if you do not have it
// -------------------------------------------------------- 
// turn API logging on (true) or off (false)
$logging = false;
global $logging;
// -------------------------------------------------------- 
// Function:                ApiLog

// Description:             Stores transaction in DB

// Type:                    public

// Parameters:
//    [in] string $xml_in   The XML that was sent
//    [in] string $xml_out  The XML that was returned

// Return Values:           
//    boolean               true if transaction was logged
//                          false otherwise

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function ApiLog($xml_in, $xml_out="") {
  global $logging;
  if (!$logging) return false;
  $xml_in = addslashes($xml_in);
  $xml_out = addslashes($xml_out);
  $sql  = "INSERT INTO api_log SET ";
  $sql .= "xml_in = '$xml_in', ";
  $sql .= "xml_out = '$xml_out', ";
  $sql .= "host='" . $_SERVER['REMOTE_ADDR'] . "'";
  return mysql_query($sql);
}

?>