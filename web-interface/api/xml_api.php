<?php
// difference from CVS
require_once("../../mysql.php");
// require_once("../ruqueue_mysql.php");

require_once("../functions/functions-header.php");
require_once("../functions/functions-getters.php");
require_once("class-xml_api.php");
require_once("class-ruqueue_xml.php");
require_once("api_log.php");

// Get XML:
//  From POST or GET via local file (debugging), via $query in 
//  POST/GET, or via raw HTTP POST (most likely).  
$filename = (empty($_POST["file"])) ? $_GET["file"] : $_POST["file"];
if (!empty($filename)) {
  $file = fopen($filename, "r");
  $xml = fread($file, filesize($filename));
  fclose($file);
}
else {
  if (empty($_GET["query"]) and empty($_POST["query"])) {
    $xml = $GLOBALS['HTTP_RAW_POST_DATA'];
    if (empty($xml)) {
      $xml = file_get_contents('php://input');
    }
  }
  else {
    $xml = (empty($_POST["query"])) ? $_GET["query"] : $_POST["query"];
    $xml = stripslashes($xml);
  }
}

// Process XML
$ruqueue_api = new RUQUEUE_XML(STRING, $xml);
if ($ruqueue_api->xml_doc->parse_error) {
  $ruqueue_api->xml_reply->root->set_attribute("status", 12);
  $ruqueue_api->xml_reply->root->set_attribute("error_string", "Error parsing XML");
}
elseif ($ruqueue_api->Login()) {
  if ($ruqueue_api->xml_doc->IsTag("TICKET")) $ruqueue_api->ProcessTicket();
  if ($ruqueue_api->xml_doc->IsTag("QUERY")) $ruqueue_api->ProcessQuery();
}
print $xml_out = $ruqueue_api->xml_reply->XMLDump();
ApiLog($xml, $xml_out);
?>
