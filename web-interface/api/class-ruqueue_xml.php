<?php
define("SEC_TO_EXPIRE", GetSessionExpireTime(), true);

class RUQUEUE_XML {
  var $xml_doc;
  var $xml_reply;
  var $username;
  var $session_code;
  //RUQUEUE_XML constructor
  function RUQUEUE_XML($source = STRING, $value = "") {
    $this->xml_doc = new XMLDoc($source, $value);
    $this->xml_reply = new RUQUEUE_Reply_XML();
  }
  //decode a coded string
  function Decode($string) {
    return base64_decode($string);
  }
  //encode a string
  function Encode($string) {
    return base64_encode($string);
  }
  //login into system
  function Login() {
    global $db;
    $auth = $this->xml_doc->GetUsableAttributeArray("AUTH");
    $auth_node = $this->xml_reply->AddChild($this->xml_reply->root, "AUTH", $auth);
    if ($this->xml_doc->IsTag("AUTH_LOGIN")) { //if new login
      $auth_login = $this->xml_doc->GetUsableAttributeArray("AUTH_LOGIN");
      $auth_login_node = $this->xml_reply->AddChild($auth_node, "AUTH_LOGIN", $auth_login);
      $auth_login_node->set_attribute("status", 0);
      switch ($auth_login["method"]) {
      case "LDAP":
	require_once("../functions/functions-auth-ru-ldap.php");
	if ($this->IsStaff($auth_login["username"]) && Auth($auth_login["username"], $this->Decode($auth_login["password"]))) {
	  if ($exp_time = $this->RegisterSession($auth_login["username"], $auth["keep_session"])) {
	    $exp_time_text = date("m/d/Y H:i:s", $exp_time);
	    $auth_session = array(
				  "session_key" => $this->session_code,
				  "username" => $this->username,
				  "status" => 0,
				  "idle_exp_time" => $exp_time_text,
				  "sess_exp_time" => $exp_time_text
				  );
	    $login_node = $this->xml_reply->AddChild($auth_node, "AUTH_SESSION", $auth_session);
	    $auth_node->set_attribute("status", 0);
	    return true;
	  }
	  else {
	    $auth_login_node->set_attribute("status", 11);
	    $auth_login_node->set_attribute("error_string", "Database Connection Error");
	  }
	}
	else { //bad username/password (login)
	  $auth_login_node->set_attribute("status", 102);
	  $auth_login_node->set_attribute("error_string", "Bad username or password");
	}
	break;
      case "Shared":
	$shared_key = $auth_login["password"];
	$host_ip = $_SERVER['REMOTE_ADDR'];
	$host_name = gethostbyaddr($host_ip);
	$query = "SELECT username FROM api_users WHERE (host = '%' OR host = '$host_ip' OR host = '$host_name') AND shared_key = '$shared_key' LIMIT 1";
	$result = mysql_query($query, $db);
	if (mysql_num_rows($result) > 0) {
	  $username = mysql_result($result, 0);
	  $exp_time = $this->RegisterSession($username, $auth["keep_session"]);
	  $exp_time_text = date("m/d/Y H:i:s", $exp_time);
	  $auth_session = array(
				"session_key" => $this->session_code,
				"username" => $this->username,
				"status" => 0,
				"idle_exp_time" => $exp_time_text,
				"sess_exp_time" => $exp_time_text
				);
	  $login_node = $this->xml_reply->AddChild($auth_node, "AUTH_SESSION", $auth_session);
	  $auth_node->set_attribute("status", 0);
	}
	else {
	  $auth_login_node->set_attribute("status", 102);
	  $auth_login_node->set_attribute("error_string", "Bad username or password");
	}
	break;
      default:
	$auth_login_node->set_attribure("status", 101);
	$auth_login_node->set_attribute("error_string", "Unknown Method");
      }
    }
    else { //if using existing login
      $auth_session = $this->xml_doc->GetUsableAttributeArray("AUTH_SESSION");
      $auth_session_node = $this->xml_reply->AddChild($auth_node, "AUTH_SESSION", $auth_session);
      //check for valid username/session
      $query = "SELECT COUNT(*) FROM session WHERE username = '".$auth_session["username"]."' AND session_code = '".$auth_session["session_key"]."'";
      if (mysql_result(mysql_query($query, $db), 0) > 0) {
	//check if session is expired
	$query = "SELECT COUNT(*) FROM session WHERE username = '".$auth_session["username"]."' AND session_code = '".$auth_session["session_key"]."' AND NOW() <= DATE_ADD(session_start, INTERVAL ".SEC_TO_EXPIRE." SECOND)";
	if (mysql_result(mysql_query($query, $db), 0) > 0) { //if login found
	  if ($exp_time = $this->RefreshSession($auth_session["username"], $auth_session["session_key"], $auth["keep_session"])) {
	    $exp_time_text = date("m/d/Y H:i:s", $exp_time);
	    $auth_session = array(
				  "status" => 0,
				  "idle_exp_time" => $exp_time_text,
				  "sess_exp_time" => $exp_time_text
				  );
	    foreach ($auth_session as $attribute => $value) {
	      $auth_session_node->set_attribute($attribute, $value);
	    }
	    $auth_node->set_attribute("status", 0);
	    return true;
	  }
	  else {
	    $auth_session_node->set_attribute("status", 11);
	    $auth_session_node->set_attribute("error_string", "Database Connection Error");
	  }
	}
	else { //session expired (session login)
	  $auth_session_node->set_attribute("status", 101);
	  $auth_session_node->set_attribute("error_string", "Session Expired");
	}
      }
      else { //bad username/session (session login)
	$auth_session_node->set_attribute("status", 102);
	$auth_session_node->set_attribute("error_string", "Bad username or session");
      }
    }
    return false;
  }

  // lookup user by netid in local DB, add from LDAP if necessary, return user_id or 0
  function GetUserID($netid) {
    global $db;
    $user_id = 0;
    if (!empty($netid)) {
      $sql = "SELECT id FROM user WHERE uid = '$netid'";
      $result = mysql_query($sql, $db);
      if (mysql_num_rows($result) > 0) {
	$user_id = mysql_result($result, 0);
      }
      else {  // do ldap if system is configured for that

	$sql  = "SELECT COUNT(val) FROM system_variables WHERE ";
	$sql .= "var='user_lookup_type' AND val='extensible'";
	$result = mysql_query($sql, $db);
	if (mysql_num_rows($result) > 0) {
	  if (mysql_result($result, 0)) {
	    require_once("api_extensible_lookup.php");
	    $user_id = SaveExternalData($netid);
	  }
	}
      }
    }
    return $user_id;
  }
  //check to see if a valid staff member
  function IsStaff($username) {
    global $db;
    $query = "SELECT COUNT(*) FROM staff WHERE username = '$username'";
    if (mysql_result(mysql_query($query, $db), 0) > 0) return true;
    else return false;
  }
  //register session
  function RegisterSession($username, $keep_session) {
    global $db;
    $code = crypt(time(), SessionSalt());
    $this->username = $username;
    $this->session_code = $code;
    $session_time = ($keep_session) ? "NOW()" : "DATE_SUB(NOW(), INTERVAL ".SEC_TO_EXP." SECOND";
    $query = "REPLACE INTO session (session_start, session_code, username) VALUES ($session_time, '$code', '$username')";
    if (mysql_query($query, $db)) {
      if ($keep_session) return time()+SEC_TO_EXPIRE;
      else return time();
    }
    else return false;
  }
  //refresh session
  function RefreshSession($username, $session, $keep_session) {
    global $db;
    $this->username = $username;
    $this->session_code = $session;
    $session_time = ($auth["keep_session"]) ?
      "NOW()":
      "DATE_SUB(NOW(), INTERVAL ".SEC_TO_EXPIRE." SECOND)";
    $query = "UPDATE session SET session_start = $session_time WHERE username = 'username' AND session_code = '$session'";
    if (mysql_query($query, $db)) {
      if ($keep_session) return time()+SEC_TO_EXPIRE;
      else return time();
    }
    else return false;
  }
  //check to see if valid ticket number
  function ValidTicket($ticket_id) {
    global $db;
    $query = "SELECT COUNT(*) FROM ticket where id = $ticket_id";
    if (mysql_result(mysql_query($query, $db), 0) > 0) return true;
    else return false;
  }
  //check to see if valid queue
  function ValidQueue($queue) {
    global $db;
    $query = "SELECT COUNT(*) FROM queue WHERE q_name = '$queue'";
    if (mysql_result(mysql_query($query, $db), 0) > 0) return true;
    else return false;
  }
  //process ticket
  function ProcessTicket() {
    require_once("../functions/functions-situation-handlers.php");  // want to send email

    $ticket = $this->xml_doc->GetUsableAttributeArray("TICKET");
    $ticket_node = $this->xml_reply->AddChild($this->xml_reply->root, "TICKET", $ticket);
    if ($this->ValidQueue($ticket["queue"])) {
      switch ($ticket["mode"]) {
      case "create": //create a new ticket
	if (DoesUserHaveRight($this->username, "CreateTicket", $ticket["queue"]) && ((empty($ticket["owner"]) || DoesUserHaveRight($ticket["owner"], "OwnTicket", $ticket["queue"])))) {
	  if ($this->HasNewComments()) {
	    $user_id = $this->GetUserID($ticket["user_netid"]);
	    if (!$ticket["force_lookup"] or ($user_id != 0)) {  
	      if ($ticket_id = $this->CreateTicket($ticket)) {
		$ticket_node->set_attribute("id", $ticket_id);
		$ticket_node->set_attribute("user_id", $user_id);  
		if ($this->CreateNewComments($ticket_id)) {
		  $ticket_node->set_attribute("status", 0);
		  SituationHandler('OnCreate', $ticket_id, $this->username);  
		}
		else {
		  $ticket_node->set_attribute("status", 105);
		  $ticket_node->set_attribute("error_string", "Could not append comments");
		}
	      }
	      else {
		$ticket_node->set_attribute("status", 104);
		$ticket_node->set_attribute("error_string", "Could not create ticket");
	      }
	    }
	    else { 
	      $ticket_node->set_attribute("status", 106);
	      $ticket_node->set_attribute("error_string", "Unknown user");
	    }
	  }
	  else {
	    $ticket_node->set_attribute("status", 103);
	    $ticket_node->set_attribute("error_string", "No comment appended");
	  }
	}
	else {
	  $ticket_node->set_attribute("status", 10);
	  $ticket_node->set_attribute("error_string", "Permission denied");
	}
	break;
      case "modify": //modify the base attributes of an existing ticket
	if (DoesUserHaveRight($this->username, "ModifyTicket", $ticket["queue"])) {
	  if ($this->ValidTicket($ticket["id"])) {
	    $ticket_id = $ticket["id"];
	    $output = $this->ModifyTicket($ticket_id, $ticket["subject"], $ticket["ticket_status"], $ticket["priority"], $ticket["queue"], $ticket["owner"]);
	    if ($this->CreateComment($ticket_id, "Ticket has been modified", "", $output)) {
	      $ticket_node->set_attribute("status", 0);
	      SituationHandler('OnTransaction', $ticket_id, $this->username);  
	    }
	    else {
	      $ticket_node->set_attribute("status", 11);
	      $ticket_node->set_attribute("error_string", "Database Connection Error");
	    }
	  }
	  else {
	    $ticket_node->set_attribute("status", 101);
	    $ticket_node->set_attribute("error_string", "Invalid ticket number");
	  }
	}
	else {
	  $ticket_node->set_attribute("status", 10);
	  $ticket_node->set_attribute("error_string", "Permission denied");
	}
	break;
      case "comment":
	if (DoesUserHaveRight($this->username, "CommentTicket", $ticket["queue"])) {
	  if ($this->ValidTicket($ticket["id"])) {
	    $ticket_id = $ticket["id"];
	    if ($this->HasNewComments()) {
	      if ($this->CreateNewComments($ticket_id)) {
		$ticket_node->set_attribute("status", 0);
		SituationHandler('OnTransaction', $ticket_id, $this->username);  
	      }
	      else {
		$ticket_node->set_attribute("status", 105);
		$ticket_node->set_attribute("error_string", "Could not append comments");
	      }
	    }
	    else {
	      $ticket_node->set_attribute("status", 103);
	      $ticket_node->set_attribute("error_string", "No comment appended");
	    }
	  }
	  else {
	    $ticket_node->set_attribute("status", 101);
	    $ticket_node->set_attribute("error_string", "Invalid ticket number");
	  }
	}
	else {
	  $ticket_node->set_attribute("status", 10);
	  $ticket_node->set_attribute("error_string", "Permission denied");
	}
	break;
      }
      $this->CreateTicketHistory($ticket_id);
    }
    else {
      $ticket_node->set_attribute("status", 102);
      $ticket_node->set_attribute("error_string", "Invalid queue");
    }
  }

  //check to see if there are new comments present
  function HasNewComments() {
    return $this->xml_doc->IsTag("HISTORY") && $this->xml_doc->IsTag("NEW_COMMENT");
  }
  //add a new ticket to the system
  function CreateTicket($data) {
    global $db;
    $user_id = $this->GetUserID($data["user_netid"]);
    $info = array(
		  "date_created" => (empty($data["create_date"])) ? "NOW()" : "'".$data["create_date"]."'",
		  "queue" => "'".$data["queue"]."'",
		  "requester" => "'".$data["requester"]."'",
		  "owner" => "'".$data["owner"]."'",
		  "user_id" => "'".$user_id."'",
		  "current_status" => (empty($data["ticket_status"])) ? "'New'" : "'".$data["ticket_status"]."'",
		  "old_user_id" => "'".$user_id."'"
		  );
    $fields = implode(", ", array_keys($info));
    $values = implode(", ", $info);
    $query = "INSERT INTO ticket ($fields) VALUES ($values)";
    if (mysql_query($query, $db)) {
      $ticket_id = mysql_insert_id($db);
      $query = "INSERT INTO ticketwatcher (ticket_id, type, email) VALUES ($ticket_id, 'Requester', '".$data["requester"]."')";
      mysql_query($query, $db);
      return $ticket_id;
    }
    else return false;
  }
  //create comments
  function CreateNewComments($ticket_id) {
    $new_nodes = $this->xml_doc->FindTag("NEW_COMMENT");
    foreach ($new_nodes as $new_node) {
      $attributes = $new_node->attributes();
      foreach ($attributes as $attribute) {
	switch ($attribute->name) {
	case "subject":
	  $subject = $attribute->value;
	  break;
	case "ticket_status":
	  $status = $attribute->value;
	  break;
	}
      }
      $content = $new_node->get_content();
      if (!$this->CreateComment($ticket_id, $subject, $status, $content)) {
	$error = true;
      }
    }
    if ($error) return false;
    else return true;
  }
  //create comment in database
  function CreateComment($ticket_id, $subject, $status, $content) {
    global $db;
    $query = "SELECT current_status FROM ticket WHERE id = $ticket_id";
    $old_ticket = mysql_fetch_object(mysql_query($query, $db));
    if (empty($status)) $status = $old_ticket->current_status;
    elseif ($status != $old_ticket->current_status) {
      $content .= $this->ModifyTicket($ticket_id, "", $status, 0 ,"", "");
    }
    $info = array(
		  "ticket_id" => $ticket_id,
		  "status" => "'$status'",
		  "staff" => "'$this->username'",
		  "subject" => "'".addslashes("$subject")."'",
		  "body" => "'".addslashes($content)."'",
		  "ip_address" => "'" . $_SERVER['REMOTE_ADDR'] . "'", 
		  );
    $fields = implode(", ", array_keys($info));
    $values = implode(", ", $info);
    $query = "INSERT INTO comment ($fields) VALUES ($values)";
    if (mysql_query($query, $db)) {
      if ($this->UpdateTicket($ticket_id, mysql_insert_id($db))) {
	return true;
      }
    }
    return false;
  }
  //update ticket after inserting comment
  function UpdateTicket($ticket_id, $new_comment_id) {
    global $db;
    $query = "SELECT first_comment_id, last_comment_id FROM ticket WHERE id = $ticket_id";
    $old_ticket = mysql_fetch_object(mysql_query($query, $db));
    $update_query = "UPDATE ticket SET ";
    if (empty($old_ticket->first_comment)) {
      $update_query .= "first_comment_id = $new_comment_id, ";
    }
    $update_query .= "last_comment_id = $new_comment_id WHERE id = $ticket_id";
    return mysql_query($update_query, $db);
  }
  //modify ticket properties
  function ModifyTicket($ticket_id, $subject = "", $status = "", $pri = 0, $queue = "", $owner = "") {
    global $db;
    $query = "SELECT comment.id as first_comment_id, ticket.queue, comment.subject, ticket.owner, ticket.current_status, comment.priority FROM ticket, comment WHERE ticket.id = $ticket_id AND ticket.first_comment_id = comment.id";
    $old_ticket = mysql_fetch_object(mysql_query($query, $db));
    if (empty($subject)) $subject = $old_ticket->subject;
    if ($subject != $old_ticket->subject || $priority != $old_ticket->priority) {
      $query = "UPDATE comment SET subject = '".addslashes($subject)."', priority = $priority WHERE id = $old_ticket->first_comment_id";
      mysql_query($query, $db);
      $change[] = "Subject=$subject, Status=$status, Priority=0";
    }
    if (empty($status)) $status = $old_ticket->current_status;
    elseif ($status != $old_ticket->current_status) {
      $status_change_str = "status_change_date = NOW(), ";
      $change[] = "Status has been changed from $old_ticket->current_status to $status";
    }
    if (empty($queue)) $queue = $old_ticket->queue;
    elseif ($queue != $old_ticket->queue) {
      $change[] = "ExitQueue=$old_ticket->queue";
      $change[] = "EnterQueue=$queue";
    }
    if (empty($owner)) $owner = $old_ticket->owner;
    elseif ($owner != $old_ticket->owner) {
      $change[] = "Owner has been changed from $old_ticket->owner to $owner";
    }
    if ($status != $old_ticket->status
	|| $queue != $old_ticket->queue
	|| $owner != $old_ticket->owner) {
      $query = "UPDATE ticket SET $status_change_str current_status = '$status', queue = '$queue', owner = '$owner' WHERE id = $ticket_id";
      mysql_query($query, $db);
    }
    foreach ($change as $str) $output .= "<br>$str";
    if (!empty($output)) $output = "<br>$output";
    return $output;
  }
  //generate ticket history to XML
  function CreateTicketHistory($ticket_id) {
    global $db;
    $query = "SELECT DATE_FORMAT(date_created, '%Y-%m-%d %T') as date_created, subject, body FROM comment WHERE ticket_id = $ticket_id ORDER BY date_created";
    $result = mysql_query($query, $db);
    if ($result && mysql_num_rows($result) > 0) {
      $history_node = $this->xml_reply->AddChild($this->xml_reply->root, "HISTORY");
      while ($comment = mysql_fetch_object($result)) {
	$attributes = array(
			    "timestamp" => $comment->date_created,
			    "subject" => $comment->subject
			    );
	$comment_node = $this->xml_reply->AddChild($history_node, "COMMENT", $attributes);
	$comment_node->set_content($comment->body);
      }
    }
  }
  //process query
  function ProcessQuery() {
    $query = $this->xml_doc->GetUsableAttributeArray("QUERY");
    $query_node = $this->xml_reply->AddChild($this->xml_reply->root, "QUERY", $query);
    $nodes = $this->xml_doc->FindTag("QUERY_TICKET");
    foreach ($nodes as $node) {
      $query_ticket_attributes = $node->attributes();
      foreach ($query_ticket_attributes as $attribute) {
	$query_ticket[$attribute->name] = $attribute->value;
      }
      $this->xml_reply->AddChild($query_node, "QUERY_STRING", $query_ticket);
      $results = $this->RunQuery($query_ticket);
      foreach ($results as $result) {
	$ticket_node = $this->xml_reply->AddChild($this->xml_reply->root, "TICKET", $result);
	if ($query_ticket["mode"] == "Details") {
	  $this->CreateTicketHistory($result["id"]);
	}
      }
    }
  }
  //build result array
  function RunQuery($search) {
    global $db;
    $query = "SELECT ticket.id, ticket.current_status, ticket.queue, ticket.owner, ticket.requester, DATE_FORMAT(ticket.date_created, '%Y-%m-%d %T') as date_created, DATE_FORMAT(comment.date_created, '%Y-%m-%d %T') as last_updated, ticket.first_comment_id FROM ticket, comment WHERE ";
    foreach ($search as $field => $value) {
      switch ($field) {
      case "id":
	$where["ticket.id"] = $value;
	break;
      case "ticket_status":
	$where["ticket.current_status"] = "'$value'";
	break;
      case "queue":
	$where["ticket.queue"] = "'$value'";
	break;
      case "requester":
	$where["ticket.requester"] = "'$value'";
	break;
      case "last_update_date":
	$where["comment.date_created"] = "'".str_replace(array("-", ":", " "), "", $value)."'";
	break;
      case "create_date":
	$where["ticket.date_created"] = "'".str_replace(array("-", ":", " "), "", $value)."'";
	break;
      case "owner":
	$where["ticket.owner"] = "'$value'";
	break;
      }
    }
    $where["comment.ticket_id"] = "ticket.id";
    $where["comment.id"] = "ticket.last_comment_id";
    foreach ($where as $val1 => $val2) {
      switch (true) {
      case ($val1 == "ticket.date_created" || $val1 == "comment.date_created") && (($lt = strstr($val2, "<") !== false) || ($gt = strstr($val2, ">") !== false)):
	$op = ($lt !== false) ? "<" : ">";
	$val2 = str_replace($op, "", $val2);
	break;
      default:
	$op = "=";
      }
      if (($val1 == "ticket.date_created" || $val1 == "comment.date_created") && (strstr($val2, "|") !== false)) {
	$dates = explode("|", str_replace("'", "", $val2));
	$where_string .= "$val1 > '".$dates[0]."' AND $val1 < '".$dates[1]."' AND ";
      }
      elseif ($val1 == "ticket.current_status" && (strstr($val2, ",") !== false || strstr($val2, "|") != false)) {
	$val2 = str_replace("'", "", $val2);
	$val2 = str_replace("|", ",", $val2);
	$val2 = explode(",", $val2);
	foreach ($val2 as $value) {
	  $list .= "'$value',";
	}
	$list = substr($list, 0, -1);
	$where_string .= "$val1 IN ($list) AND ";
      }
      else $where_string .= "$val1 $op $val2 AND ";
    }
    $query .= substr($where_string, 0, -4);
    $result = mysql_query($query, $db);
    $result_array = array();
    while ($row = mysql_fetch_object($result)) {
      $first_comment_query = "SELECT subject FROM comment WHERE id = $row->first_comment_id";
      if (!empty($search["subject"])) {
	if (strstr($search["subject"], "%") !== false) $first_comment_query .= " AND subject LIKE '".$search["subject"]."'";
	else $first_comment_query = " AND subject = '".$search["subject"]."'";
      }
      $first_comment_result = mysql_query($first_comment_query, $db);
      if ($first_comment_result && mysql_num_rows($first_comment_result) > 0) {
	$result_array[] = array(
				"id" => $row->id,
				"ticket_status" => $row->current_status,
				"queue" => $row->queue,
				"subject" => mysql_result($first_comment_result, 0),
				"owner" => $row->owner,
				"requester" => $row->requester,
				"create_date" => $row->date_created,
				"last_update_date" => $row->last_updated,
				);
      }
    }
    return $result_array;
  }
}


class RUQUEUE_Reply_XML {
  var $xml;
  var $root;
  //RUQUEUE_Replay_XML constructor
  function RUQUEUE_Reply_XML() {
    $this->xml = domxml_new_doc("1.0");
    $this->root = $this->xml->add_root("RUQUEUE");
  }
  //add child
  function AddChild($parent, $tag, $attributes = array()) {
    $node = $this->xml->create_element($tag);
    foreach ($attributes as $attribute => $value) {
      $node->set_attribute($attribute, $value);
    }
    $node = $parent->append_child($node);
    return $node;
  }
  //output XML document
  function XMLDump() {
    return $this->xml->dump_mem(true);
  }
}

?>