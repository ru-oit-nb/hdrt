<?php
// Filename:                api_extensible_lookup.php
// Description:             Does LDAP lookup for API
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2006-05-30 17:01:46 jfulton> 
// -------------------------------------------------------- 
// Modify this file to use your LDAP lookup functions as 
// opposed to those of Rutgers.  
// -------------------------------------------------------- 

require_once("../functions/functions-ldap.php");
require_once("../functions/ru_ldap_auth.php");

// -------------------------------------------------------- 
// Function:                SaveExternalData($uid)

// Description:             Looks user up in LDAP by $uid
//                          Saves LDAP data returns local
//                          tracking number

// Type:                    public

// Parameters:
//    [in] string $uid      The user's NetID

// Return Values:           
//    interger              
//                          The id of the user table for 
//                          the new user

// Remarks:                 
//    None                  
// -------------------------------------------------------- 

function SaveExternalData($uid) {
  $user_id = 0;
  $ldap_query = "uid=$uid";
  $root_dn = "ou=People,dc=rutgers,dc=edu";
  $oSearch = new RutgersLDAPAuth;
  $oSearch->Connect();
  $result_array = array();
  $oSearch->Search(&$result_array, $root_dn, $ldap_query);
  // if more than one result, use *first entry* only
  if (count($result_array[0]) > 0) {  
    $ldap_to_local_map = array(
	 'name' =>  "cn",
	 'email' =>  "mail",
	 'iid' =>  "rutgerseduiid",
	 'uid' =>  "uid",
	 'phone' =>  "telephonenumber",
	 'fax' =>  "facsimiletelephonenumber",
	 'address' =>  "postaladdress",
	 'type' =>  "employeetype",
	 'rcpid' =>  "rcpid",
	 'ruservice' =>  "ruservice",
	 'rudisable' =>  "rudisable",
	 'location' =>  "location",
    );
    $sql = "INSERT INTO user SET ";
    foreach ($ldap_to_local_map as $localv => $ldapv) {
      $sql .= "$localv='" 
	. str_replace('$', "\n", $result_array[0]["$ldapv"][0]) 
	. "', ";  // note last zero of result_array (first only) 
    }
    $sql = substr($sql, 0, -2);  // trim last ", "
    global $db;
    $result = mysql_query($sql, $db);
    $user_id = mysql_insert_id($db);
  }
  return $user_id;
}

?>