<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: new_ticket.php
// Description: Creates a new ticket.
// Supprted Language(s):   PHP 4.0
//

/*----------------------------------------------------------------------------
 Ticket creation can take up to three steps.
 First, the user must be found or added.
 Second, queue is selected, basics are filled out.
 Third, if the selected queue has special fields, these are presented.
 After this, the ticket is created.
----------------------------------------------------------------------------*/

// Required Files
require_once("scheduling/functions/functions-getters.php");
require_once("functions/functions-form-objects.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-new-ticket.php");
require_once("functions/functions-setters.php");
require_once("functions/functions-situation-handlers.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-header.php");
require_once("header.php");

global $self, $detail_state;
$self = $_SERVER['PHP_SELF'];
print Head("Create a New Ticket", "$username");
$output = "";  

// Keeps content consistent.
if ($email_user != 1) $email_user = 0;
$special_fields_count = SpecialFieldsCount($queue);

$contentdisplay = htmlentities(stripslashes($content));

// Form submitted, no subject! Must have a subject.
if (!isset($subject) || !empty($allqueues) || !empty($limitqueues)) {
   global $detail_state;
   $detail_state = "Hide-Details";
   $output .= NewTicketShowForm($username, $id, $button, $queue, $requester, $email_user);
}

// Subject is set, check where we are in ticket creation.
else {
  //fix quote rendering for forms
  $subjectdisplay = htmlentities(stripslashes($subject));

   if ($button == "Show-Details") {
      global $detail_state;
      $detail_state = "Show-Details";
      $output .= NewTicketShowForm($username, $id, $button, $queue, $requester,
                                   $status, $owner, $cc, $admin_cc, $subjectdisplay, 
                                   $attach, $contentdisplay, $priority, 
                                   $final_priority, $time_worked,
                                   $time_left, $starts, $due, $depends_on, 
                                   $depended_on_by, $parents, $children, 
                                   $refers_to, $refered_to_by, 
                                   $special_fields_count, $email_user,
                                   $form_type);
   }
       
   if ($button == "Hide-Details") {
      global $detail_state;
      $detail_state = "Hide-Details";
      $output .= NewTicketShowForm($username, $id, $button, $queue, $requester,
                                   $status, $owner, $cc, $admin_cc,
                                   $subjectdisplay, $attach, $contentdisplay, $priority, 
                                   $final_priority, $time_worked, $time_left, 
                                   $starts, $due, $depends_on, $depended_on_by,
                                   $parents, $children, $refers_to,
                                   $refered_to_by, $special_fields_count,
                                   $email_user, $form_type);
   }

   if ($button == "Continue") {

      $Permission = SetNoBack("CreateTicket", "TicketCreated");
      $errmsg = CheckInputNewTicket($username, $status, $subjectdisplay, $contentdisplay,
                                    $starts, $due, $requester, $cc, $admin_cc, 
                                    $user_id, $queue);
          
      if (!empty($errmsg)) $output .= ErrorReport($errmsg, "50%");

      if (!empty($errmsg)) {
         $output .= NewTicketShowForm($username, $id, $button, $queue,
                                      $requester, $status, $owner, $cc,
                                      $admin_cc, $subjectdisplay, $attach, $contentdisplay,
                                      $priority, $final_priority,
                                      $time_worked, $time_left, $starts,
                                      $due, $depends_on, $depended_on_by,
                                      $parents, $children, $refers_to,
                                      $refered_to_by, $special_fields_count,
                                      $email_user, $form_type);
      }

      elseif(!empty($subject) or !empty($content)) {
         if ($special_fields_count > 0) {
            $output .= NewTicketShowForm($username, $id, $button, $queue,
                                         $requester, $status, $owner, $cc, 
                                         $admin_cc, $subjectdisplay, $attach, 
                                         $contentdisplay, $priority, $final_priority,
                                         $time_worked, $time_left, $starts,
                                         $due, $depends_on, $depended_on_by,
                                         $parents, $children, $refers_to,
                                         $refered_to_by, $special_fields_count,
                                         $email_user, $form_type);
         }

         else if($special_fields_count == 0 and $Permission) {
            $requester = $requester;
            $queue = str_replace("_", " ", $queue);
            $str = "queue='$queue', ";
            $str .= "requester='$requester', ";
            $str .= "owner='$owner', ";
            $str .= "user_id='$user_id', ";
            $str .= "cc='$cc', ";
            $str .= "admin_cc='$admin_cc', ";
            $str .= "final_priority='$final_priority', ";
            $str .= "time_worked='$time_worked', ";
            $str .= "time_left='$time_left', ";
            $str .= "starts='$starts', ";
            $str .= "due='$due', ";
            $str .= "depends_on='$depends_on', ";
            $str .= "depended_on_by='$depended_on_by', ";
            $str .= "parents='$parents', ";
            $str .= "children='$children', ";
            $str .= "refers_to='$refers_to', ";
            $str .= "refered_to_by='$refered_to_by',";
            $str .= "status_change_date=now(), ";
            $str .= "current_status='New'";
            $result1 = insert_into_db("ticket", $str);
                
         if (!$result1) {
            $errmsg = "Internal error. (Line 569)";
            $errmsg .= "Unable to insert ";
            $errmsg .= "ticket into queue.";
            $output .= ErrorReport($errmsg, "50%");
         }
         else {
	   $comment_id = AddComment($result1, $status, $priority, $username, addslashes($subject), $attach, $content);
	   if (!is_int($comment_id)) {
	     $output .= ErrorReport($comment_id, "50%");
	   }
	   else {
               $pq = str_replace("_", " ", $queue);
               $ticket_id = $result1;
               $url = LinkToTicket($ticket_id);
               global $SCHEDULING;

               if ($SCHEDULING && GetSchedulingAllowed($pq)) {
                  $output .= "<center>"
                          .OpenColorTable("green", "Scheduling", "50%")
                          ."Click <a class=\"main\" "
                          ."href=\"new_appointment.php?ticket_id=$ticket_id\">"
                          ."here</a> to schedule an appointment."
                          .CloseColorTable()
                          ."</center><p>";
               }
                      
               $all_ticket_watchers = GetMoreEmail($requester, $cc, $admin_cc, 1);
               $errmsg .= "<br>".SetTicketWatchers($ticket_id, $all_ticket_watchers);
               SetCustomizedFieldsValues($ticket_id);
               UpdateFirstCommentId($ticket_id);
               UpdateLastCommentId($ticket_id);
               $sent = SituationHandler("OnCreate", $ticket_id, $username, $email_user);
               if ($sent == "error") {
                  $errmsg = "Internal error.";
                  $errmsg .= "Ticket inserted, ";
                  $errmsg .= "but unable to ";
                  $errmsg .= "send email ";
                  $errmsg .= "to watchers.  ";
                  $errmsg .= "<p>$url";
                  $output .= ErrorReport($errmsg, "50%");
               }
               else {
                  $rcps = ArrayToString($emails, "<br>");
                  $msg = "Ticket has been ";
                  $msg .= "inserted into ";
                  $msg .= "Queue <b>$pq</b>.";
                  if ($sent != "") {
                     $msg .= "<p>Email has been ";
                     $msg .= "sent to the ";
                     $msg .= "following:<p><ul>";
                     $msg .= "$sent</ul>";
                  }
                  $msg .= "<p>$url";
                  $output .= GoodReport($msg, "50%");
               }
	   }
         }
      }
      elseif (empty($Permission)) $output .= "Your ticket has already been created.<br>";
   }
}
       
if ($button == "Create Ticket") {
   $Permission = SetNoBack("CreateTicket", "TicketCreated");
         $errmsg = CheckInputNewTicket($username, $status, $subject, $content,
                                       $starts, $due, $requester, $cc, 
                                       $admin_cc, $user_id, $queue);
          
   if (!empty($errmsg)) {
      $output .= ErrorReport($errmsg, "50%")
              .NewTicketShowForm($username, $id, $button, $queue, $requester,
                                 $status, $owner, $cc, $admin_cc, $subject, 
                                 $attach, $content, $priority, $final_priority,
                                 $time_worked, $time_left, $starts, $due, 
                                 $depends_on, $depended_on_by, $parents, 
                                 $children, $refers_to, $refered_to_by, 
                                 $special_fields_count, $email_user, $form_type);
   }

   elseif ($Permission) {
      $requester = $requester;
      $queue = str_replace("_", " ", $queue);
      $str = "queue='$queue', ";
      $str .= "requester='$requester', ";
      $str .= "owner='$owner', ";
      $str .= "user_id='$user_id', ";
      $str .= "cc='$cc', ";
      $str .= "admin_cc='$admin_cc', ";
      $str .= "final_priority='$final_priority', ";
      $str .= "time_worked='$time_worked', ";
      $str .= "time_left='$time_left', ";
      $str .= "starts='$starts', ";
      $str .= "due='$due', ";
      $str .= "depends_on='$depends_on', ";
      $str .= "depended_on_by='$depended_on_by', ";
      $str .= "parents='$parents', ";
      $str .= "children='$children', ";
      $str .= "refers_to='$refers_to', ";
      $str .= "refered_to_by='$refered_to_by',";
      $str .= "status_change_date=now(),";
      $str .= "current_status='New'";
      $result1 = insert_into_db("ticket", $str);
             
      if (!$result1) {
         $errmsg = "Internal error. (Line 707) ";
         $errmsg .= "Unable to insert ";
         $errmsg .= "ticket into queue.";
         $output .= ErrorReport($errmsg, "50%");
      }

      else {
	$comment_id = AddComment($result1, $status, $priority, $username, addslashes($subject), $attach, $content);
	if (!is_int($comment_id)) {
	  $output .= ErrorReport($comment_id, "50%");
	}
	else {
	  $pq = str_replace("_", " ", $queue);
             $ticket_id = $result1;
             global $SCHEDULING;
             if ($SCHEDULING && GetSchedulingAllowed($pq)) {
                  $output .= "<center>"
                          .OpenColorTable("green", "Scheduling", "50%")
                          ."Click <a class=\"main\" "
                          ."href=\"new_appointment.php?ticket_id=$ticket_id\">"
                          ."here</a> to schedule an appointment."
                          .CloseColorTable()
                          ."</center><p>";
             }
                   
            $url = LinkToTicket($ticket_id);
            $all_ticket_watchers = GetMoreEmail($requester, $cc, $admin_cc, 1);
            $errmsg .= "<br>".SetTicketWatchers($ticket_id, $all_ticket_watchers);
            SetCustomizedFieldsValues($ticket_id);
            UpdateFirstCommentId($ticket_id);
            UpdateLastCommentId($ticket_id);
            $sent = SituationHandler("OnCreate", $ticket_id, $username, $email_user);

            if ($sent == "error") {
               $errmsg = "Internal error. (Line 757)";
               $errmsg .= "Ticket inserted, ";//checked
               $errmsg .= "but unable to ";
               $errmsg .= "send email ";
               $errmsg .= "to watchers.  ";
               $errmsg .= "<p>$url";
               $output .= ErrorReport($errmsg, "50%");
            } 

            else {
               $rcps = ArrayToString(
               $emails, "<br>");
               $msg = "Ticket has been ";
               $msg .= "inserted into ";
               $msg .= "Queue <b>$pq</b>.";
               $msg .= "<p>Email has been ";
               $msg .= "sent to the ";
               $msg .= "following:<p><ul>";
               $msg .= "$sent</ul>";
               $msg .= "<p>$url";
               $output .= GoodReport($msg, "50%");
            }
         }
      }
   }
   elseif (empty($Permission)) $output .= "Your ticket has already been created.<br>";
} 
} 
    
$output .= Foot();
print $output;
?>
