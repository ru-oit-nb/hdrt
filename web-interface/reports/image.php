<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


   require_once("../ruqueue_mysql.php");
   require_once("functions-image.php");
   require_once("functions-reports.php");
   global $db;
    
   if (!empty($_GET['type'])) {
      switch ($_GET['type']) {
         case "activity":
          
         if (!empty($_GET['year'])) {
             
            if ($_GET['year'] < date(Y)) {
                
               for ($i = 0; $i < 12; $i++) {
                  $stroftime = $_GET['year']."-".($i+1)."-01";
                  $timestamp = strtotime($stroftime)-86400;
                  $month = date(Ymd, $timestamp);
                   
                  if ($i == 11) {
                     $stroftime2 = ($_GET['year']+1)."-01-01";
                  }
                   
                  else
                     {
                     $stroftime2 = $_GET['year']."-".($i+2)."-01";
                  }
                   
                  $timestamp2 = strtotime($stroftime2);
                  $nextmonth = date(Ymd, $timestamp2);
                  $query = "select count(*) from ticket where date_created between $month and $nextmonth";
                  $query2 = "select count(*) from ticket where ticket.current_status='Resolved' and status_change_date between $month and $nextmonth";
                  $result = mysql_query($query, $db);
                  $result2 = mysql_query($query2, $db);
                  $row = mysql_fetch_array($result);
                  $row2 = mysql_fetch_array($result2);
                  $new[$i] = $row[0];
                  $resolved[$i] = $row2[0];
                  $months[$i] = date("M Y", $timestamp+86400);
               }
                
            }
             
            if ($_GET['year'] == date(Y)) {
               $currentmonth = date(m);
                
               for ($i = 0; $i < $currentmonth; $i++) {
                  $stroftime = $_GET['year']."-".($i+1)."-01";
                  $timestamp = strtotime($stroftime)-86400;
                  $month = date(Ymd, $timestamp);
                   
                  if ($i == 11) {
                     $stroftime2 = ($_GET['year']+1)."-01-01";
                  }
                   
                  else
                     {
                     $stroftime2 = $_GET['year']."-".($i+2)."-01";
                  }
                   
                  $timestamp2 = strtotime($stroftime2);
                  $nextmonth = date(Ymd, $timestamp2);
                  $query = "select count(*) from ticket where date_created between $month and $nextmonth";
                  $query2 = "select count(*) from ticket where ticket.current_status='Resolved' and status_change_date between $month and $nextmonth";
                  $result = mysql_query($query, $db);
                  $result2 = mysql_query($query2, $db);
                  $row = mysql_fetch_array($result);
                  $row2 = mysql_fetch_array($result2);
                  $new[$i] = $row[0];
                  $resolved[$i] = $row2[0];
                  $months[$i] = date("M Y", $timestamp+86400);
                   
               }
                
            }
             
            if ($_GET['year'] > date(Y)) {
               print "Error: A date in the future has been entered.<br>";
            }
             
            else
            {
               $title = "Activity By Month for ".$_GET['year'];
               $xlabel = "Month";
               $ylabel = "Tickets";
               $image = InitializeGraph($title, $_GET['width'], $_GET['height'], $xlabel, $ylabel);
               GraphDoubleBars($image, $months, $new, $resolved);
               OutputGraph($image);
            }
             
         }
         break;//end case "activity"
          
         case "new_tickets":
         if (isset($past)) {
            if ($_GET['past'] == 2) {
               $query = "select queue from ticket where date_created between date_sub(now(),interval 2 day) and date_sub(now(), interval 1 day) order by queue";
               $title = "New Tickets: Yesterday";
            } else {
               $query = "select queue from ticket where date_created between date_sub(now(),interval $past day) and now() order by queue";
               $title = "New Tickets: Last $past Days";
            }
            if ($_GET['past'] == 1) {
               $title = "New Tickets: Today";
            }
            list($names, $numbers) = TicketsPerQueue($query);
            $xlabel = "Queue";
            $ylabel = "Tickets";
            $image = InitializeGraph($title, $width, $height, $xlabel, $ylabel);
            GraphBars($image, $names, $numbers);
            OutputGraph($image);
         }
          
         break;//end case "new_tickets"
          
         case "tickets_resolved":
         if (isset($past)) {
            if ($_GET['past'] == 2) {
               $query = "select queue from ticket where ticket.current_status='Resolved' and status_change_date between date_sub(now(),interval 2 day) and date_sub(now(),interval 1 day) order by queue";
               $title = "Resolved Tickets: Yesterday";
            } else {
               $query = "select queue from ticket where ticket.current_status='Resolved' and status_change_date between date_sub(now(),interval $past day) and now() order by queue";
               $title = "Resolved Tickets: Last $past Days";
            }
            if ($_GET['past'] == 1) {
               $title = "Resolved Tickets: Today";
            }
            list($names, $numbers) = TicketsPerQueue($query);
            $ylabel = "Tickets";
            $xlabel = "Queue";
            $image = InitializeGraph($title, $width, $height, $xlabel, $ylabel);
            GraphBars($image, $names, $numbers);
            OutputGraph($image);
         }
         break;//end case "tickets_resolved":
          
         case "new_tickets_by_month":
         $number_months = (date(Y) - 2002) * 12+7+date(m);
         $current_month = (date(Ym)."01");
         for ($i = $number_months-2; $i >= 0; $i--) {
            $query = "select count(*),date_format(date_sub($current_month, interval ($i+1) month), \"%b %Y\") from ticket where date_created between";
            $query .= " date_format(date_sub($current_month,interval ($i+1) month),\"%Y%m%d\") ";
            $query .= "and date_format(date_sub($current_month, interval $i month),\"%Y%m%d\")";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array($result);
            $new[$i] = $row[0];
            $months[$i] = $row[1];
         }
         $ylabel = "Tickets";
         $title = "New Tickets by Month";
         $image = InitializeGraph($title, $width, $height, "", $ylabel);
         GraphBars($image, $months, $new, 1);
         OutputGraph($image);
         break;
          
         case "tickets_resolved_by_month":
         $number_months = (date(Y) - 2002) * 12+7+date(m);
         $current_month = (date(Ym)."01");
         for ($i = $number_months-2; $i >= 0; $i--) {
            $query = "select count(*),date_format(date_sub($current_month, interval ($i+1) month), \"%b %Y\") from ticket where ticket.current_status='Resolved' and status_change_date between ";
            $query .= "date_format(date_sub($current_month,interval ($i+1) month),\"%Y%m%d\") and date_format(date_sub($current_month, interval $i month),\"%Y%m%d\")";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array($result);
            $resolved[$i] = $row[0];
            $months[$i] = $row[1];
         }
         $title = "Tickets Resolved by Month";
         $ylabel = "Tickets";
         $image = InitializeGraph($title, $width, $height, "", $ylabel);
         GraphBars($image, $months, $resolved, 1);
         OutputGraph($image);
         break;
          
          
          
      }
      //end switch $type
       
   }
    
?>


