<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


$form = '<table style="border: 0px solid black">';
$form .= '<form method="get"><input type="hidden" name="report" value="surveys">';
$form .= '<tr><td>Start Date (YYYYMMDD)</td><td>';
$form .= '<input type="text" width="9" name="start" value="';
$form .= $_GET['start'].'">';
$form .= '</td></tr>';
$form .= '<tr><td>End Date (YYYYMMDD)</td><td>';
$form .= '<input type="text" width="9" name="end" value="';
$form .= $_GET['end'].'">';
$form .= '</td></tr>';
$form .= '<tr><td>Queue</td><td>';
$sql = "SELECT q_name FROM queue ORDER BY q_name";
$result = mysql_query($sql) or die(mysql_error());
$form .= "<select name=\"queue\">\n";
while ($row = mysql_fetch_object($result)) {
  $selected = "";
  if ($_GET['queue'] == $row->q_name) $selected = "selected";
  $form .= "  <option value=\"" . $row->q_name;
  $form .= "\" $selected>" . $row->q_name . "</option>\n";
}
$form .= "</select>\n";
$form .= '</td></tr>';
$form .= '<tr><td>&nbsp;</td><td><input type="submit" value="Retrieve Surveys"></td></tr>';
$form .= '</form></table>';
print $form;
    
if (!empty($_GET['start']) && !empty($_GET['end'])) {
   global $db;
   $start = $_GET['start'];
   $end = $_GET['end'];
   $select = "select ticket_id ticket, date_format(date,'%W, %M %e, %Y at %h:%i %p') date, how contacted, time_to_speak, response_time, question_addressed, question_addressed_comments, attitude_of_staff, intimidated intimidated_before_call, after_call_intimidated intimidated_after_call, level_of_advice, documentation, hand_offs, call_again, additional_comments ";
   $from = "from survey ";
   $where = "where date between $start and $end";
   if (!empty($_GET['queue'])) {
     $from = "from survey, ticket ";
     $where  = "where ticket.id=survey.ticket_id ";
     $where .= "and date between $start and $end ";
     $where .= "and queue='" . $_GET['queue'] . "'";
   }
   $query = $select.$from.$where;
   $result = mysql_query($query, $db);
   $msg  = "<p />".mysql_num_rows($result)." surveys found ";
   $msg .= "between $start and $end";
   if (!empty($_GET['queue'])) {
     $msg .= " for the queue " . $_GET['queue'] . ".  ";
   }
   else {
     $msg .= " for all queues. ";
   }
   print $msg;
   DisplaySurveys($result);
}
    
    
   function DisplaySurveys($result) {
      if (mysql_num_rows($result) <= 0) {
         return;
      }
       
      $num_fields = mysql_num_fields($result);
       
      for($i = 0; $i < $num_fields; $i++) {
         $field_names[$i] = ucwords(str_replace('_', ' ', mysql_field_name($result, $i)));
      }
       
      while ($row = mysql_fetch_array($result)) {
         print '<p><table width=97% align="center" style="border: 1px solid black; font-size: 10px">';
         print "<tr>";
         print '<th valign="top" width="10%" nowrap align=left>'.$field_names[0]."&nbsp;</th>";
         print '<td><a class="main" href="ticket.php?id='.$row[0].'">'.$row[0].'&nbsp;</td>';
         print "</tr>\n";
         for($j = 1; $j < $num_fields; $j++) {
            if (empty($row[$j])) $row[$j] = "N/A";
            print "<tr>";
            print '<th valign="top" width="10%" nowrap align=left>'.$field_names[$j]."&nbsp;</th>";
            print "<td>".$row[$j]."&nbsp;</td>";
            print "</tr>\n";
         }
         print "</table>";
      }
   }
?>
