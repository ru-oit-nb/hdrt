<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


   require_once("image_string_box.php");
    
   /*
    image(Filled)Rectangle($image, 0, 0, $width, $height, $background);
    imageline($image, $startpointx, $startpointy, $endpointx, $endpointy, $color);
    imageString($image, 3, 10, $height - 42,  "Text Goes Here", $color);
    imagestring ($image, $font, x, y, $string, $color)
    font: An integer value specifying the number of the font to draw the text in. If font is 1, 2, 3, 4 or 5, then a built-in font is used
    x: The horizontal displacement from the left of the image where the text will be drawn from, measured in pixels.
    y: The vertical displacement from the top of the image where the text will be drawn from, measured in pixels.
    */
    
   function OutputGraph ($image) {
      header("Content-type:  image/png");
      imageInterlace($image, 1);
      imagePNG($image);
   }
    
   function InitializeGraph($title = "", $width, $height, $xlabel = "", $ylabel = "") {
      if (empty($width)) $width = 640;
      if (empty($height)) $height = 480;

      $image = imageCreate($width, $height);
       
      $bg = imageColorAllocate($image, 255, 255, 255);
      $fg = imageColorAllocate($image, 100, 100, 100);
      $grid = imageColorAllocate($image, 0, 0, 0);
      $text = imageColorAllocate($image, 0, 0, 0);
       
      imagefill($image, 0, 0, $bg);
       
      imageRectangle($image, 0, 0, $width-1, $height - 1, $grid);
      imagefilledrectangle($image, 60, 40, $width-20, $height-60, $fg);
      imageRectangle($image, 60, 40, $width-20, $height - 60, $grid);
       
      imagestring($image, 5, 60, 10, $title, $text);
      imagestringup($image, 3, 10, 3 * $height/5, $ylabel, $text);
      imagestring($image, 3, 2 * $width/5, $height-20, $xlabel, $text);
       
      return $image;
   }
    
   function GraphBars($image, $barnames, $barvalues, $labels = 0) {
      $black = imageColorAllocate($image, 0, 0, 0);
      $width = imagesx($image);
      $height = imagesy($image);
      $size = sizeof($barvalues);
      $max = 0;
      for ($i = 0; $i < $size; $i++) {
         if ($barvalues[$i] > $max) {
            $max = $barvalues[$i];
         }
      }
      if ($max == 0) {
         imagestring($image, 5, (($width/2)-50), ($height/2-20), "NO DATA" , $black);
      }
       
      else
      {
         $yscale = ($height-100)/(11 * $max/10);
         $xscale = ($width-80)/(($size+1)/2+$size);
          
         $startx = 60+$xscale/2;
         $starty = $height-60;
         $endx = $startx+$xscale;
          
         for ($i = 0; $i < $size; $i++) {
            $color = imageColorAllocate($image, (43 * ($i+1))%128+127, (66 * ($i+1))%128+127, (51 * ($i+1))%128+127);
            $endy = $height-60-$barvalues[$i] * $yscale;
            $x1 = (int)($startx);
            $x2 = (int)($endx);
            $y1 = (int)$starty;
            $y2 = (int)$endy;
            imagefilledrectangle($image, $x1, $y2, $x2, $y1, $color);
            imagerectangle($image, $x1, $y2, $x2, $y1, $black);
            imagestringup($image, 1, $x1, $y2-5, $barvalues[$i], $color);
            $startx = $endx+.5 * $xscale;
            $endx = $startx+$xscale;
            if ($labels == 1) {
               imagestringup($image, 1, $x1, ($height-15), $barnames[$i], $black);
            }
         }
      }
   }
    
   function GraphDoubleBars($image, $barnames, $bar1values, $bar2values) {
      $black = imageColorAllocate($image, 0, 0, 0);
      $bar1color = imageColorAllocate($image, 102, 153, 220);
      $bar2color = imageColorAllocate($image, 102, 220, 153);
      $width = imagesx($image);
      $height = imagesy($image);
      $size = sizeof($barnames);
      $max = 0;
      for ($i = 0; $i < $size; $i++) {
         if ($bar1values[$i] > $max) {
            $max = $bar1values[$i];
         }
         if ($bar2values[$i] > $max) {
            $max = $bar2values[$i];
         }
      }
      if ($max == 0) {
         imagestring($image, 5, (($width/2)-50), ($height/2-20), "NO DATA" , $black);
      } else {
         $yscale = ($height-100)/(11 * $max/10);
         $xscale = ($width-80)/(($size+1)/2+(2 * $size));
          
         $startx = 60+$xscale/2;
         $starty = $height-60;
         $midx = $startx+$xscale;
         $endx = $midx+$xscale;
          
         for ($i = 0; $i < $size; $i++) {
            $endy1 = $height-60-$bar1values[$i] * $yscale;
            $endy2 = $height-60-$bar2values[$i] * $yscale;
             
            $x1 = (int)($startx);
            $x2 = (int)($midx);
            $x3 = (int)($endx);
            $y1 = (int)$starty;
            $y2a = (int)$endy1;
            $y2b = (int)$endy2;
             
            imagefilledrectangle($image, $x1, $y2a, $x2, $y1, $bar1color);
            imagerectangle($image, $x1, $y2a, $x2, $y1, $black);
            imagestringup($image, 1, $x1, $y2a-5, $bar1values[$i], $bar1color);
             
            imagefilledrectangle($image, $x2, $y2b, $x3, $y1, $bar2color);
            imagerectangle($image, $x2, $y2b, $x3, $y1, $black);
            imagestringup($image, 1, $x2, $y2b-5, $bar2values[$i], $bar2color);
             
            imagestringbox($image, 2, $x1, ($height-55), $x3, ($height-25), ALIGN_CENTER, VALIGN_BOTTOM, 0, $barnames[$i], $black);
             
             
            $startx = $endx+.5 * $xscale;
            $midx = $startx+$xscale;
            $endx = $midx+$xscale;
         }
      }
   }
    
    
    
?>
