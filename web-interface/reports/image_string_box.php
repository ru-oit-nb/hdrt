<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


    
   // ### CODE ###
    
   define("ALIGN_LEFT", 0);
   define("ALIGN_CENTER", 1);
   define("ALIGN_RIGHT", 2);
   define("VALIGN_TOP", 0);
   define("VALIGN_MIDDLE", 1);
   define("VALIGN_BOTTOM", 2);
    
   function imagestringbox(&$image, $font, $left, $top, $right, $bottom, $align, $valign, $leading, $text, $color) {
      // Get size of box
      $height = $bottom - $top;
      $width = $right - $left;
       
      // Break the text into lines, and into an array
      $lines = wordwrap($text, floor($width / imagefontwidth($font)), "\n", true);
      $lines = explode("\n", $lines);
       
      // Other important numbers
      $line_height = imagefontheight($font) + $leading;
      $line_count = floor($height / $line_height);
      $line_count = ($line_count > count($lines)) ? (count($lines)) :
       ($line_count);
       
      // Loop through lines
      for ($i = 0; $i < $line_count; $i++) {
         // Vertical Align
         switch ($valign) {
            case VALIGN_TOP: // Top
            $y = $top + ($i * $line_height);
            break;
            case VALIGN_MIDDLE: // Middle
            $y = $top + (($height - ($line_count * $line_height)) / 2) + ($i * $line_height);
            break;
            case VALIGN_BOTTOM: // Bottom
            $y = ($top + $height) - ($line_count * $line_height) + ($i * $line_height);
            break;
            default:
            return false;
         }
          
         // Horizontal Align
         $line_width = strlen($lines[$i]) * imagefontwidth($font);
         switch ($align) {
            case ALIGN_LEFT: // Left
            $x = $left;
            break;
            case ALIGN_CENTER: // Center
            $x = $left + (($width - $line_width) / 2);
            break;
            case ALIGN_RIGHT: // Right
            $x = $left + ($width - $line_width);
            break;
            default:
            return false;
         }
          
         // Draw
         imagestring($image, $font, $x, $y, $lines[$i], $color);
      }
       
      return $image;
   }
    
    
?>
