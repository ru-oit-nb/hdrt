<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
@include_once("functions/functions-widgets.php");
@include_once("../../functions/functions-widgets.php");
   function TicketsPerQueue($query) {
      global $db;
      $result = mysql_query($query, $db);
      $i = 0;
      if (mysql_num_rows($result) != 0) {
         $queue_results = GetFieldArray($result, "queue");
         $size = sizeof($queue_results);
         $i = 0;
         $names[$i] = $queue_results[0];
         $numbers[$i] = 1;
         for ($j = 0; $j < $size-1; $j++) {
            if (!strcmp($queue_results[$j], $queue_results[$j+1])) {
               $numbers[$i]++;
            } else {
               $i++;
               $names[$i] = $queue_results[$j+1];
               $numbers[$i] = 1;
            }
         }
         return array($names, $numbers);
      }
      return;
   }
    
   function GetFieldArray($result, $field) {
      $i = 0;
      while ($table_row = mysql_fetch_object($result)) {
         $field_array[$i] = $table_row->$field;
         $i++;
      }
      return $field_array;
   }
    
    
   function rgb2hex($rgb) {
      if (!is_array($rgb) || count($rgb) != 3) {
         echo "Argument must be an array with 3 integer elements";
         return false;
      }
      for($i = 0; $i < count($rgb); $i++) {
         if (strlen($hex[$i] = dechex($rgb[$i])) == 1) {
            $hex[$i] = "0".$hex[$i];
         }
      }
      return $hex;
   }
   function DisplayImage($image_options = "") {
      //      print '<img src="image.php?'.$image_options.'" align=left hspace=10>';
      print '<img src="reports/image.php?'.$image_options.'" align=left hspace=10>';
   }
    
   function DisplayDataTable($names, $numbers) {
      $size = sizeof($numbers);
      print '<table border=0 style="border:1px solid black;font-size:10px" cellspacing=0 cellpadding=3><tr><th align="left">Queue</th><th align="left">Tickets</th></tr>';
      $total = 0;
      for ($i = 0; $i < $size; $i++) {
         $icolor = array((43 * ($i+1))%128+127, (66 * ($i+1))%128+127, (51 * ($i+1))%128+127);
         $color = rgb2hex($icolor);
         echo "<tr><td bgcolor=#".$color[0].$color[1].$color[2].">&nbsp;$names[$i]</td><td bgcolor=#".$color[0].$color[1].$color[2]." align=right>";
         echo $numbers[$i]."</td></tr>";
         $total = $total+$numbers[$i];
      }
      print '<tr><td>&nbsp;Total</td><td align="right">'.$total.'</td></tr></table>';
   }
    
   function DisplayDataTableByMonth($months, $new, $resolved) {
      $size = sizeof($months);
      print '<table border=0 style="border:1px solid black;font-size:10px" cellspacing=0 cellpadding=3>';
      print '<tr><th align="left">Month</th><th align="left">New</th><th align="left">Resolved</th></tr>';
      $total = 0;
      for ($i = 0; $i < $size; $i++) {
         $icolor = array((43 * ($i+1))%128+127, (66 * ($i+1))%128+127, (51 * ($i+1))%128+127);
         $color = rgb2hex($icolor);
         echo "<tr><td style=\"border-top: 1px solid black\">&nbsp;$months[$i]</td>";
         echo "<td bgcolor=#6699DC align=right style=\"border-top: 1px solid black\">".$new[$i]."</td>";
         echo "<td bgcolor=#66DC99 align=right style=\"border-top: 1px solid black\">".$resolved[$i]."</td></tr>";
         $total_new = $total_new+$new[$i];
         $total_resolved = $total_resolved+$resolved[$i];
      }
      print '<tr><td style="border-top: 1px solid black">&nbsp;Total</td><td style="border-top: 1px solid black" align="right">';
      print $total_new.'</td><td style="border-top: 1px solid black" align="right">'.$total_resolved.'</td></tr></table>';
   }
    
?>
