<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
   require_once("functions/functions-widgets.php");
   require_once("functions-reports.php");
   global $db;
    
   if (empty($_GET['width'])) {
      $width = 640;
   } else {
      $width = $_GET['width'];
   }
   if (empty($_GET['height'])) {
      $height = 480;
   } else {
      $height = $_GET['height'];
   }
    
   if (!empty($_GET['type'])) {
       
      switch ($_GET['type']) {
          
         case "activity":
          
         if (!empty($_GET['year'])) {
             
            if ($_GET['year'] < date(Y)) {
                
               for ($i = 0; $i < 12; $i++) {
                  $stroftime = $_GET['year']."-".($i+1)."-01";
                  $timestamp = strtotime($stroftime)-86400;
                  $month = date(Ymd, $timestamp);
                  if ($i == 11) {
                     $stroftime2 = ($_GET['year']+1)."-01-01";
                  } else {
                     $stroftime2 = $_GET['year']."-".($i+2)."-01";
                  }
                  $timestamp2 = strtotime($stroftime2);
                  $nextmonth = date(Ymd, $timestamp2);
                  $query = "select count(*) from ticket where date_created between $month and $nextmonth";
                  $query2 = "select count(*) from ticket where ticket.current_status='Resolved' and status_change_date between $month and $nextmonth";
                  $result = mysql_query($query, $db);
                  $result2 = mysql_query($query2, $db);
                  $row = mysql_fetch_array($result);
                  $row2 = mysql_fetch_array($result2);
                  $new[$i] = $row[0];
                  $resolved[$i] = $row2[0];
                  $months[$i] = date("M Y", $timestamp+86400);
               }
                
            }
             
            if ($_GET['year'] == date(Y)) {
               $currentmonth = date(m);
                
               for ($i = 0; $i < $currentmonth; $i++) {
                  $stroftime = $year."-".($i+1)."-01";
                  $timestamp = strtotime($stroftime)-86400;
                  $month = date(Ymd, $timestamp);
                  if ($i == 11) {
                     $stroftime2 = ($_GET['year']+1)."-01-01";
                  } else {
                     $stroftime2 = $_GET['year']."-".($i+2)."-01";
                  }
                  $timestamp2 = strtotime($stroftime2);
                  $nextmonth = date(Ymd, $timestamp2);
                  $query = "select count(*) from ticket where date_created between $month and $nextmonth";
                  $query2 = "select count(*) from ticket where ticket.current_status='Resolved' and status_change_date between $month and $nextmonth";
                  $result = mysql_query($query, $db);
                  $result2 = mysql_query($query2, $db);
                  $row = mysql_fetch_array($result);
                  $row2 = mysql_fetch_array($result2);
                  $new[$i] = $row[0];
                  $resolved[$i] = $row2[0];
                  $months[$i] = date("M Y", $timestamp+86400);
               }
                
            }
             
            if ($_GET['year'] > date(Y)) {
               print "Error: A date in the future has been entered.<br>";
            }
             
            else
            {
               $imageoptions = "type=activity&year=".$_GET['year']."&width=".$width."&height=".$height;
               DisplayImage($imageoptions);
               DisplayDataTableByMonth($months, $new, $resolved);
            }
             
         }
          
         break;
          
         case "new_tickets":
          
         if (!empty($_GET['past'])) {
             
            if ($_GET['past'] == 2) {
               $query = "select queue from ticket where date_created between date_sub(now(),interval 2 day) and date_sub(now(), interval 1 day) order by queue";
            }
             
            else
            {
               $query = "select queue from ticket where date_created between date_sub(now(),interval $past day) and now() order by queue";
            }
             
            list($names, $numbers) = TicketsPerQueue($query);
            $imageoptions = "type=new_tickets&past=".$past."&width=".$width."&height=".$height;
            DisplayImage($imageoptions);
            DisplayDataTable($names, $numbers);
         }
          
         break;
          
         case "tickets_resolved":
          
         if (!empty($_GET['past'])) {
             
            if ($_GET['past'] == 2) {
               $query = "select queue from ticket where ticket.current_status='Resolved' and status_change_date between date_sub(now(),interval 2 day) and date_sub(now(),interval 1 day) order by queue";
            }
             
            else
            {
               $query = "select queue from ticket where ticket.current_status='Resolved' and status_change_date between date_sub(now(),interval $past day) and now() order by queue";
            }
             
            list($names, $numbers) = TicketsPerQueue($query);
            $imageoptions = "type=tickets_resolved&past=".$past."&width=".$width."&height=".$height;
            DisplayImage($imageoptions);
            DisplayDataTable($names, $numbers);
         }
          
         break;
          
         case "new_tickets_by_month":
         $number_months = (date(Y) - 2002) * 12+7+date(m);
         $current_month = (date(Ym)."01");
         for ($i = $number_months-2; $i >= 0; $i--) {
            $query = "select count(*),date_format(date_sub($current_month, interval ($i+1) month), \"%b %Y\") from ticket where date_created between";
            $query .= " date_format(date_sub($current_month,interval ($i+1) month),\"%Y%m%d\") ";
            $query .= "and date_format(date_sub($current_month, interval $i month),\"%Y%m%d\")";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array($result);
            $new[$i] = $row[0];
            $months[$i] = $row[1];
         }
         $imageoptions = "type=new_tickets_by_month&width=".$width."&height=".$height;
         DisplayImage($imageoptions);
         DisplayDataTable($months, $new);
         break;
          
         case "tickets_resolved_by_month":
         $number_months = (date(Y) - 2002) * 12+7+date(m);
         $current_month = (date(Ym)."01");
         for ($i = $number_months-2; $i >= 0; $i--) {
            $query = "select count(*),date_format(date_sub($current_month, interval ($i+1) month), \"%b %Y\") from ticket where ticket.current_status='Resolved' and status_change_date between ";
            $query .= "date_format(date_sub($current_month,interval ($i+1) month),\"%Y%m%d\") and date_format(date_sub($current_month, interval $i month),\"%Y%m%d\")";
            $result = mysql_query($query, $db);
            $row = mysql_fetch_array($result);
            $resolved[$i] = $row[0];
            $months[$i] = $row[1];
         }
         $imageoptions = "type=tickets_resolved_by_month&width=".$width."&height=".$height;
         DisplayImage($imageoptions);
         DisplayDataTable($months, $resolved);
         break;
      }
       
   }
    
    
    
?>

