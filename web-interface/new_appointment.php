<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: new_appointment.php
// Description: wrapper for the correct function in
//              scheduling directory
// Supprted Language(s):   PHP 4.0
//
require_once("functions/functions-widgets.php");
require_once("header.php");
global $username;
print Head("Schedule a New Appointment", $username)
     ."<center>".OpenColorTable("green", "New Appointment", "50%")
     ."<tr><td align=center>";
include "scheduling/new_appointment.php";
print "</td></tr>".CloseColorTable()."</center>";
print Foot();
?>
