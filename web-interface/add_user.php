<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: add_user.php
// Description: Add a user to the system.
// Supprted Language(s):   PHP 4.0
//
// Required Files
require_once("functions/functions-getters.php");
require_once("functions/functions-ldap.php");
require_once("functions/functions-widgets.php");
require_once("functions/ru_ldap_auth.php");
require_once("header.php");

// Start of the Page
$output = Head("Add the user to the system", $username)
        ."<table border=0 width=\"100%\"><tr valign=\"top\">"
        ."<td width=\"100%\">"
        .OpenColorTable("green", "Results of LDAP Search", "100%");

// Submitted Form
if ($edit) {
  if (empty($_GET['name'])) {
    $output .= ErrorReport('Please enter a name');  
  }
  else {
    $set_string = "";
    foreach ($set as $item) {
      if (${$item} == "") ${$item} = " ";
      $set_string .= "$item='${$item}', ";
    }
    if ($set_string) $set_string = "set ".substr($set_string, 0, -2);

    if (($id > 0) && ($do != "") && ($edit == 1)) {
      // Generate and run the update query.
      $query = "Update user $set_string where id='$id'";
      $output .=  "User info has been updated";
      $qtype = "update";
      mysql_query($query, $db);
    }
    else { // New user.
      $SetResult = SetNoBack("ldap_insert", "new");
      $query = "Insert into user $set_string";
      if ((empty($SetResult)) && ($do != "Save changes")) $output .= "User already added!  &nbsp;No action taken.";
      else $msg = "User successfully entered into the system";
      $qtype = "insert";
    }
    if ($SetResult && $qtype == "insert") {
      if (mysql_query($query, $db)) {
	if ($qtype == "insert") $id = mysql_insert_id();
	$output .=  $msg." for user #$id";
      } 
      else $output .= "Error in database entry<br>Query = $query";
   }
  }
  
}
$cn = $name;
$addr = $address;
    
if ($id) { // Lookup User
   $user_row = GetFirstRowFromTable("user where id = '$id'");
   $cn = $user_row->name;
   $iid = $user_row->iid;
   $email = $user_row->email;
   $uid = $user_row->uid;
   $type = $user_row->type;
   $phone = $user_row->phone;
   $fax = $user_row->fax;
   $location = $user_row->location;
   $addr = $user_row->address;
   $rcpid = $user_row->rcpid;
   $objclass = $user_row->objclass;
   $ruservice = $user_row->ruservice;
   $ruenable = $user_row->ruenable;
   $rudisable = $user_row->rudisable;
}

if ((empty($iid)) || (!(empty($id)))) {
  $output .= LdapShowForm($cn, $iid, $email, $uid, $type, $phone, $fax,
			  $location, $addr, $rcpid, $objclass, $ruservice,
			  $ruenable, $rudisable, $id, $edit, $queue);
}
else { // Lookup the user in LDAP
   $ldap_query = "iid=$iid";
   $oSearch = new RutgersLDAPAuth;
   $oSearch->Connect();
   $result_array = array();
   $oSearch->Search(&$result_array, $LDAP_ROOT_DN[$SERVER_ID],$ldap_query);
   $output .= LdapPrintResultList($result_array, $id, $edit, $queue);
}
    
$output .= CloseColorTable()."</td></table>";
$output .= foot();
print $output;

?>
