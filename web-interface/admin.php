<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: admin.php
// Description: Admin page.
// Supprted Language(s):   PHP 4.0
//
/*----------------------------------------------------------------------------
This file manages a large set of menus, and displays arrows on the current 
menu item.  This is done by listening for the appropriate variables which 
setup links with the correct hierarchy.  The menus are similar to a tree.

The arrows bellow show the hierarchy in desending order.  There are 4 boxes, 
and so there are 3 variables inbetween the boxes to keep track of what should 
be displayed per box.  If the variables are empty, the first item is always 
assumed.

|---------------|       |---------------|       |---------------|
|       -->     |       |       -->     |       |       ||      |
|               |       |               |       |       ||      |
|       I       | $i    |       M       | $m($i)|       \/      |
|               |       |               |       |       SM      |
|               |       |               |       |               |
|---------------|       |---------------|       |---------------|
$f($m($i))
|---------------------------------------------------------------|
|                                                               |
| F                                                             |
|                                                               |
|---------------------------------------------------------------|

    I=item  M=menu  SM=submenu      F=form

The variables in the middle that map to different submenus, are funtions of the 
previous variables.  While the expected progression is I -> M -> SM -> F, some 
configuration will proceed along I -> M -> F, especially for global options.

The code from the Groups submenu, is a little messy, since there was an extra 
complication with "Pseudogroups" and "Groups".  To get a better feel of what is 
going on in this file look at the code for Queues & Global, which call 
'GeneralMenu()' and 'GeneralSubMenu()'. Users & Keywords vary in their own way.

The following function is used to generate the menus:
CreateLinksFromArray($arr, "item=$item&submenu", "<br>", $hl);

It takes an array as its first argument and for every item in the array creates 
a link such that the the link has as its tail the 2nd argument.  The second 
argument is crucial to preserving the hierarchy as the page is reloaded.  The 
3rd argument specifies how each link is separated, and the 4th arguemnt 
specifies which link gets higlighed, i.e. gets an arrow next to it (which shows 
the next submenu).  The value of the 4th arguemnt ($hl) is usually 'y' such 
that "<a href=x>y</a>".  If "first" is passed in place of the link name 
($hl="first") only the first item in the list gets the arrow.  This is the 
default when each new menu loads.

The related files in the functions directory are included below.
-------------------------------------------------------------------------*/

// Required Files
require_once("header.php");
require_once("functions/functions-admin-groups.php");
require_once("functions/functions-admin-queues.php");
require_once("functions/functions-admin-users.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-admin.php");

global $self;
$self = $PHP_SELF;
$output = Head("Configuration", $username);

// Form Submitted, Delete User.
if ($DeleteUser == "Delete This User") {
   global $db;
   $query = "Delete from staff where username = '$user'";
   $deleted = mysql_query($query, $db);
   mysql_query($statusQuery, $db);
   $commit == "no";
}

// Form Submitted, Edit User Profile
if ($commit == "yes") {
   preg_match("/^(.*)(\s)(\w*)$/", $name, $realName);
   $firstname = $realName[1];
   $lastname = $realName[3];
   if ($access == "on") $access = 1;
   if ($rights == "on") $rights = 1;
   $staffInfo = array("username" => $user, "firstname" => $firstname, "lastname" => $lastname, "email" => $email, "nickname" => $nickname, "extra_info" => $extra_info, "organization" => $organization, "address1" => $address1, "address2" => $address2, "city" => $city, "state" => $state, "zip" => $zip, "country" => $country, "home_phone" => $home_phone, "work_phone" => $work_phone, "mobile_phone" => $mobile_phone, "pager" => $pager, "access" => $access, "rights" => $rights, "comments" => $comments);
   $commit_error1 = CommitUserProfile($staffInfo, $updateType);
   if (isset($signature)) $commit_error2 = CommitUserSignature($user, $signature, $updateType);
   if (empty($commit_error1) && empty($commit_error2)) $message = "Record for $user successfully saved.";
   elseif ($commit_error1 != $commit_error2)
   $message = $commit_error1.$commit_error2;
   else $message = $commit_error2;
   if ($DeleteUser == "Delete This User") $message = "$user has been deleted";
   $item = "Users";
   $UserField = "username";
   $UserOp = "LIKE";
   $UserString = "$user";
   unset($user);
   $output .= $message."<br>";
}

// This section allows the menus at the top to be hidden and shown at will.
// This is useful for demonstration purposes, or when you must make many 
// of the same configurations at once.
if (isset($_GET['hide']) && $_GET['hide'] == 1) {
   $old_query_string = $_SERVER['QUERY_STRING'];
   $new_query_string = str_replace("hide=1", "hide=0", $old_query_string);
   if ($new_query_string == $old_query_string) $new_query_string .= "&hide=0";
   $output .= '<a class="main" href="' . $_SERVER['PHP_SELF'] . '?' . $new_query_string . '">Show Menus</a>';
}

// The menus are not being hidden, so show them.
if ($_GET['hide'] != 1 || !isset($hide)) {
   $new_query_string = str_replace("hide=0", "hide=1", $_SERVER['QUERY_STRING']);
   if ($new_query_string == $_SERVER['QUERY_STRING']) $new_query_string .= "&hide=1";
   if (empty($item)) $item = "Groups";
   $output .= "<a class=\"main\" href=\"$self?$new_query_string\">Hide Menus</a>"
           ."<table border=0 width=\"100%\"><tr valign=\"top\"><td width=\"33%\">"
           .OpenColorTable("blue", "Menu", "100%").MainMenu($item).CloseColorTable();
   if ($item == "Users") $output .= "</td ><td width=\"33%\">";
   else $output .= "</td ><td width=\"33%\">";
   $output .= OpenColorTable("red", $item, "100%");
       
   if ($item == "Groups") {
      if (isset($name) && isset($DeleteGroup)) $output .= DeleteGroup($name);
      if (isset($name) && isset($description)) $output .= ChangeGroupDescription($name, $description, $NewGroup);
      $down = "first";
      if (!empty($sub_group)) $down = $sub_group;
      if (!empty($pseudo) and empty($group)) {
         $output .= GroupsMenu($pseudo, $item);
         $sub_menu = $pseudo;
         $content = GroupsSubMenu($pseudo, $item, $down);
      }

      if (empty($pseudo) and !empty($group)) {
         $output .= GroupsMenu($group, $item);
         $sub_menu = $group;
         $content = GroupsSubMenu($group, $item, $down);
      }

      if (empty($pseudo) and empty($group)) {
         $output .= GroupsMenu("first", $item);
         $content = GroupsSubMenu($pseudo, $item, $down);
      }
   }
       
   elseif ($item == "Queues") {
      $down = "first";
      if (!empty($sub_group)) $down = $sub_group;
      if (empty($queue)) $output .= QueuesMenu("first", $item);

      else {
         $output .= QueuesMenu($queue, $item);
         $sub_menu = $queue;
      }
      $content = QueuesSubMenu($queue, $item, $down);
   }
       
   elseif ($item == "Global") {
      $down = "first";
      if (!empty($global)) $down = $global;
      $output .= GlobalMenu($down, $item);
   }
       
   elseif ($item == "Users") {
/**
		echo "item=".$_GET['item']."subitem".$_GET['subitem'];
		if (empty($subitem)) $output .= UsersMenu("first", $item);
		else{
			$output .= UsersMenu($_GET['subitem'], $item);
		}
**/
      $down = "Find_Users";
      if (!empty($subitem)) $down = $subitem;
      $output .= UsersMenu($down, $item);
      $sub_menu = $down;

      if (empty($_GET['bulk'])) $users_output .= UsersSubMenu("Add_New_Users", $subitem, $item);
		else { $users_output .= UsersSubMenu($_GET['bulk'], $_GET['subitem'], $_GET['item']); }

      if (strlen($users_output)<= 15 && $down == "Find_Users") {
         $user = $users_output;
         $content = UsersSubMenu("", $item);
      }
      else $content = $users_output;
//      if (isset($sub_menu)) $user = $sub_menu;
   }
   $output .= CloseColorTable()."</td><td>";

   if (($item != "Global")) { //and ($item != "Users")) {
         $output .= OpenColorTable("yellow", str_replace("_", " ", $sub_menu), "100%")
                 .$content.CloseColorTable();
   }
   $output .= "</td></tr></table>";
}

// Display the Form at the bottom of the page - for editing specifics.
$output .= "<p>";

if ($item == "Groups") {

   if (empty($group) and empty($pseudo)) {
      $arr = GetPseudoGroups();
      $pseudo = $arr[0];
   }

   if (empty($sub_group)) {
      $sub_group = "Basics";
      $arr = GetPseudogroupPropertiesArray();
      $update = "Modify <u>$arr[0]</u> of <u>$group$pseudo</u>";
   }
   else $update = "Modify <u>$sub_group</u> of <u>$group$pseudo</u>";
   if ($sub_group != "Basics") $update = "Editing membership for Group <u>$group</u>";

   if ($group == "Create_a_new_group") {
      $update = "Create a new group";
      $group = $pseudo = "";
   }
}
       
elseif ($item == "Queues") {
   if (empty($queue)) {
      $q_arr1 = GetQueue();
      $queue = str_replace("_", " ", $q_arr1[0]);
   }

   if (empty($sub_group)) {
      $q_arr2 = GetQueuePropertiesArray();
      $sub_group = str_replace("_", " ", $q_arr2[0]);
   }
   $update = "Edit <u>$sub_group</u> for Queue <u>$queue</u>";
   if ($queue == "Create_a_new_queue") $update = "Create a new queue";
}

elseif ($item == "Global") {
   if (empty($global)) {
      $g_arr = GetGlobalPropertiesArray();
      $global = str_replace("_", " ", $g_arr[0]);
   }
   $update = "Modify Global <u>$global</u>";
}
       
if ($item != "Users") $output .= OpenColorTable("green", str_replace("_", " ", $update), "100%");
       
if ($item == "Groups") {
   if (isset($AddMembers)) {
      $errors = AddGroupMember($group, $AddMembers);
      if (count($errors) > 0) foreach ($errors as $error) $output .= "Error: &nbsp;could not add user $error.<br \>";
   }
   if (isset($DelMembers)) {
      $errors = DelGroupMember($group, $DelMembers);
      if (count($errors) > 0) foreach ($errors as $error) $output .= "Error: &nbsp;could not delete user $error<br \>";
   }
   if ($sub_group == "Basics") {
      $name = "$group$pseudo";
      if ($group != "" or $pseudo != "") $Description = GetGroupDescription($name);
      $output .= ModifyGroupForm($name, $Description);
   }
   if ($sub_group == "Members") {
      $name = "$group$pseudo";
      $output .= ModifyGroupMemberForm($group);
   }
}
       
elseif ($item == "Queues") {
   if (empty($queue)) $output .= FontBold("Please select a Queue.");
   else {
      if (($update == "Create a new queue") and (empty($sub_group))) BasicQueueForm($item, $queue);
      elseif ($sub_group == "Basics") $output .= BasicQueueForm($item, $queue);
      elseif ($sub_group == "Greetings") $output .= GreetingQueueForm($item, $queue);
      elseif ($sub_group == "Watchers") $output .= WatchersQueueForm($item, $queue);
      elseif ($sub_group == "Supervisors") $output .= SupervisorsQueueForm($item, $queue);
      elseif ($sub_group == "Special_Fields") $output .= SpecialFieldsQueueForm($item, $queue, $field_edit);
      elseif ($sub_group == "Scripts") $output .= ScriptsQueueForm($item, $queue);
      elseif ($sub_group == "Templates") $output .= TemplatesQueueForm($item, $queue);
      elseif ($sub_group == "Group_Rights") $output .= GroupRightsQueueForm($item, $queue);
      elseif ($sub_group == "User_Rights") $output .= UserRightsQueueForm($item, $queue);
      elseif ($sub_group == "Allow_Mail_Comments") $output .= MailCommentsQueueForm($item, $queue);
      elseif ($sub_group == "Scheduling_Permissions") $output .= SchedulingPermissionsQueueForm($item, $queue);
   }
}
       
elseif ($item == "Global") {
   if (empty($global)) $global = "Options";
   if ($global == "Options") $output .= OptionsGlobalForm();
   elseif ($global == "Scripts") $output .= ScriptsQueueForm($item, $global);
   elseif ($global == "Templates") $output .= TemplatesQueueForm($item, $global);
   elseif ($global == "Group_Rights") $output .= GroupRightsQueueForm($item, $global);
   elseif ($global == "User_Rights") $output .= UserRightsQueueForm($item, $global);
   elseif ($global == "Modules") $output .= ModulesGlobalForm();
   elseif ($global == "Scheduling_Options") $output .= SchedulingOptionsQueueForm($item, $global);
   elseif ($global == "Scheduling_Permissions") $output .= SchedulingPermissionsQueueForm($item, $global);
   elseif ($global == "Buildings") {
      print $output;
      include("scheduling/buildings.php");
      $output = "";
   }
   elseif ($global == "User_Building_Permissions") {
      print $output;
      include("scheduling/user_buildings.php");
      $output = "";
   }
}
       
elseif ($item == "Users") {

   if (!empty($user)) $output .= UpdateUserProfileForm($item, $user);
	switch ($_GET['subitem']){

		case 'Find_Users':
			break;

		case 'Bulk_Management':	
			if(!isset($_GET['bulk'])) $bulk_title = "Add New Users";
			else $bulk_title = str_replace("_", " ", $_GET['bulk']);

			switch ($bulk_title){

				case 'Add New Users':
						if(isset($_FILES) && !empty($_FILES['csv_file']['name'])) { 
							$filename = $_FILES['csv_file']['tmp_name'];
						  if($data = file($filename)){
						    $output .= BulkAddCheck($AUTH_TYPE, $data, $_POST['group_listing']);
						  }
						}else{
              //convert post to a csv format array 
              if(isset($_POST['uid']) && isset($_POST['email']) && isset($_POST['BulkAdd'])){
              //build data array
                for($i = 0; $i < 20; $i++){
                  if($AUTH_TYPE == "local") {
                    if(!empty($_POST['uid'][$i]) || !empty($_POST['email'][$i]) || !empty($_POST['password'][$i])){
                      $data[$i] = $_POST['uid'][$i].",".$_POST['email'][$i].",".$_POST['password'][$i];
                    }
                  }else if($AUTH_TYPE == "extensible"){
                    if(!empty($_POST['uid'][$i]) || !empty($_POST['email'][$i])){
                      $data[$i] = $_POST['uid'][$i].",".$_POST['email'][$i];
                    }
                  }
                }
              $output .= BulkAddCheck($AUTH_TYPE, $data, $_POST['group_listing']);
						  }
            }
						$output .= "<br \>";
						$output .= OpenColorTable("green", $bulk_title, "100%");
						$output .= BulkAddUsersForm($AUTH_TYPE, $username);
					break;
		
				case 'User Removal':
					$output .= OpenColorTable("green", $bulk_title, "100%");
          if(!empty($staff_removal)){ RemoveStaff($staff_removal); }
          $staff_listing = GetStaff();
          $output .= BulkStaffRemovalForm($staff_listing);
					break;

			}
			break;

		default:
		  // groups
		  if (!empty($user)) {
		    $output .= OpenColorTable("blue", "Groups", "100%")
		      . UserGroups($user);
		    $output .= CloseColorTable()."<p>";
		  }
		  if (empty($message)) $message = "No user";
		  $output .= OpenColorTable("green", "Message", "100%")
		    . Font("$message");
		  break;

	}
	
}

$output .= CloseColorTable()."<p>";
$output .= Foot();
print $output;
?>
