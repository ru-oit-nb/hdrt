<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA


// Filename:                header.php
// Description:             Manages security for entire site
// Supported Lanauge(s):    PHP 4.2.x
// Time-stamp:              <2005-01-25 16:25:26 jfulton> 
// -------------------------------------------------------- 
// Extended Description:  
// 
// All you have to do is include this file in any PHP page
// and its content will be password protected.  
// 
// This included file works by exiting (quiting PHP) at the 
// right time, so that no further info can be read.  Otherwise
// it lets the user in.  
// -------------------------------------------------------- 
//require_once("functions/ssl_only.php");  
session_start();  // Start Session (sends headers) 
// -------------------------------------------------------- 

// Other dependencies that send headers (thus, after session_start)
require_once("ruqueue_mysql.php");
require_once("functions/functions-header.php");
require_once("functions/functions-widgets.php");
// -------------------------------------------------------- 

$AUTH_TYPE = CheckAuthType();
$LOOKUP_TYPE = CheckLookupType();

// Are they logging out?
if ($_GET['logout']) {
  if (empty($_SESSION['username'])) $who = "Your";
  else $who = $_SESSION['username'] . "&#039;s";
  session_unset();
  ShowCredentials(NoticeReport($who . " session has ended. \n"));
}
elseif (!SessionValid()){
  ShowCredentials();  // make sure they can stay
}
elseif (SessionExpired()) {
  ShowCredentials(NoticeReport("Your session has expired. \n"));  
}

// -------------------------------------------------------- 
// User has passed authentication, bring in other header items
// -------------------------------------------------------- 
global $self,$input,$SCHEDULING, $AUTH_TYPE;

require_once("scheduling/functions/functions-getters.php");
require_once("functions/functions-form-objects.php");

$SCHEDULING = CheckScheduling();
// -------------------------------------------------------- 

// check module 
$SCHEDULING = CheckScheduling();
if ($SCHEDULING) {
  list($START_HOUR, $END_HOUR, $FIRST_APPOINTMENT, $LAST_APPOINTMENT, $HOURS_BEFORE) = GetSchedulingHourRanges();
}

// This could be done a better way...
// Order recommended by PHP developers.
extract($_SERVER,EXTR_SKIP);
extract($_SESSION,EXTR_SKIP);
extract($_COOKIE,EXTR_SKIP);
extract($_POST,EXTR_SKIP);
extract($_GET,EXTR_SKIP);

$DEFAULT_EMAIL = GetHelpEmail();
$DEFAULT_DOMAIN = "domain.tld";
$DEFAULT_EMAIL_DOMAIN = "domain.tld";
global $DEFAULT_EMAIL,$DEFAULT_DOMAIN,$DEFAULT_EMAIL_DOMAIN;

$LDAP_NAME[0] = "Rutgers Directory Service";
$LDAP_SERVER[0] = "ldap.domain.tld";
$LDAP_ROOT_DN[0] = "ou=People,dc=domain,dc=tld";
if (!$SERVER_ID) $SERVER_ID = 0;
global $LDAP_NAME,$LDAP_SERVER,$LDAP_ROOT_DN
?>
