<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: reports.php
// Description: Selects and generates reports.
// Supprted Language(s):   PHP 4.0
//

// Required Files
require_once("functions/functions-widgets.php");
require_once("header.php");
global $username;
$selected = array();
$selected[$_GET['report']]="selected";
print head("Reports", $username);
$form = '<form method="get">';
$form .= '<select name="report">';
$form .= '<option value="statistics" '.$selected['statistics'].'>Statistics</option>';
if (mysql_num_rows(mysql_query("SHOW TABLES LIKE 'survey'"))==1) {
  $form .= '<option value="surveys" '.$selected['surveys'].'>Surveys</option>';
}
$form .= '</select>';
$form .= '<input type="submit" value="Select Report">'; 
$form .= '</form><p>';

print $form;
$selected[$_GET['type']]="selected";
$selected[$_GET['year']]="selected";
$selected[$_GET['past']]="selected";
switch($_GET['report']){ 
 case "statistics":
   OpenColorTable("green", "Statistics", "100%");
   $form = '<form method="get">';
   $form .= '<select name="type">';
   $form .= '<option value="activity" '.$selected['activity'].'>Activity</option>';
   $form .= '<option value="new_tickets" '.$selected['new_tickets'].'>New Tickets</option>';
   $form .= '<option value="new_tickets_by_month" '.$selected['new_tickets_by_month'].'>New Tickets By Month</option>';
   $form .= '<option value="tickets_resolved" '.$selected['tickets_resolved'].'>Tickets Resolved</option>';
   $form .= '<option value="tickets_resolved_by_month" '.$selected['tickets_resolved_by_month'].'>Tickets Resolved By Month</option>';
   $form .= '</select>';
   if ($_GET['type']=="activity"){
     $form .= '<select name="year">';
     $this_year = date('Y');
     for ($i = $this_year; $i >= 2002; $i--) {
       $form .= "<option value=\"$i\" ". $selected["$i"] .">$i By Month</option>\n";
     }
     $form .= '</select>';
   }
   if ($_GET['type']=="new_tickets" || $_GET['type']=="tickets_resolved"){
     $form .= '<select name="past">';
     $form .= '<option value="1" '.$selected['1'].'>Today</option>';
     $form .= '<option value="2" '.$selected['2'].'>Yesterday</option>';
     $form .= '<option value="7" '.$selected['7'].'>Last 7 Days</option>';
     $form .= '<option value="30" '.$selected['30'].'>Last 30 Days</option>';
     $form .= '</select>';
   }
   $form .= '<input type="hidden" name="report" value="statistics">';
   $form .= '<input type="submit" value="Display">'; 
   $form .= '</form><p>';

   print $form;
   include("reports/reports.php");
   CloseColorTable();
   break;

 case "surveys":
   OpenColorTable("green", "Surveys", "100%");
   include("reports/surveys.php");
   CloseColorTable();
   break;
}

print Foot();
?>
