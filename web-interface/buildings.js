// Filename:                buildings.js
// 
// Description:             Javascript to make helper pull-down
//                          for campus/building see buildings.php
// 
// Supported Lanauge(s):    Javascript
// Time-stamp:              <2006-09-01 11:11:44 jfulton> 
// -------------------------------------------------------- 
// This work is available under the terms of the Modified 
// BSD license:  
// 
// Copyright (c) 2006, Rutgers, The State University of New Jersey 
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions 
// are met:
// 
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. The name of the author may not be used to endorse or promote
//       products derived from this software without specific prior
//       written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
// INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
// SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
// HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
// -------------------------------------------------------- 

function jScriptInit() {
  // only want to do this if there are special fields
  if (findSpecial()) {
    sndReq('campus');
    pullDownToText('campus');  
    disable();
  }
}

function findSpecial() {
  // return true iff campus and building are in document
  var camp  = document.getElementById('campus');
  if (camp != null) {
    var build = document.getElementById('building');
    if (build != null) {
      return true;
    }
  }
  return false;
}


function pullDownToText(name) {
  // IE pulldowns don't set nicely, but text does
  // So, convert form type (default pull-down if no js) 
  var old_elm = document.getElementById(name);
  var parent = old_elm.offsetParent;
  var new_elm = document.createElement('input');  
  new_elm.setAttribute('type', 'text');
  new_elm.setAttribute('id', name);
  new_elm.setAttribute('name', name);
  parent.replaceChild(new_elm, old_elm);  
}

function disable() {
  document.getElementById("campus").disabled = true;
  document.getElementById("building").disabled = true;
}

function enable() {
  document.getElementById("campus").disabled = false;
  document.getElementById("building").disabled = false;
}

function setField(field) {
  var trig_value = 'trig_' + field;
  document.getElementById(field).value = document.getElementById(trig_value).value;
}

function createRequestObject() {
  var ro;
  var browser = navigator.appName;
  if (browser == "Microsoft Internet Explorer") {
    ro = new ActiveXObject("Microsoft.XMLHTTP");
  }
  else {
    ro = new XMLHttpRequest();
  }
  return ro;
}

var http = createRequestObject();

function sndReq(action) {
  if (action == 'building') {
    // want buiding, so use campus to get group
    var campus = document.getElementById('trig_campus').value;
    // clear building in case of campus change
    document.getElementById(action).value = '';
  }
  if (action == 'campus') {
    var campus = '';  // empty for default campus listing behavior
  }
  http.open('GET', 'buildings.php?campus=' + campus);
  http.onreadystatechange = handleResponse;  // see function below
  http.send(null);
}

function handleResponse() {
  if (http.readyState == 4 && http.status == 200) {
    var domObj = http.responseXML;
    if (domObj == null) {
      alert('domObj is null');
    }
    else {
      // parse domObj to get desired vars
      var root = domObj.getElementsByTagName('div').item(0);
      var div = root.getAttribute('id');
      var select = root.getElementsByTagName('select').item(0);
      var id = select.getAttribute('id');
      var onChange = select.getAttribute('onchange');
      var options = select.getElementsByTagName('option');

      // create form
      var myForm = document.createElement('form');
      var myFormSelect = document.createElement('select');
      myFormSelect.id = id;

      // setup passed javascript to be called
      function lambda() {
	eval(onChange);
      }
      var evType = 'change';
      if (myFormSelect.addEventListener) { 
	 myFormSelect.addEventListener(evType, lambda, false); 
      }
      else if (myFormSelect.attachEvent) { 
	 myFormSelect.attachEvent('on' + evType, lambda);
      }
      else {
	 myFormSelect['on' + evType] = lambda; 
      }
      myForm.appendChild(myFormSelect);  // add select to form

      // add options to select
      for (i=0; i < options.length; i++) {
	var option = options.item(i).getAttribute('value');
	var myFormOpt = document.createElement('option');
	myFormOpt.setAttribute('value', option);
	myFormOpt.innerHTML = option;  
	myFormSelect.appendChild(myFormOpt);  
      }

      // add form to document
      document.getElementById(div).innerHTML = '';  // rm 'no jscript'
      document.getElementById(div).appendChild(myForm);  
    }
  }
}
