<?php
// Copyright (c) 2005, Rutgers, The State University of New Jersey
//    This file is part of ruQueue.
//
//    ruQueue is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    ruQueue is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with ruQueue; if not, write to the Free Software
//    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

 
//
// Filename: ticket.php
// Description: Displays and adds to tickets.
// Supprted Language(s):   PHP 4.0
//

// Initial Security
require_once("header.php");

// Required files
require_once("functions/functions-form-objects.php");
require_once("functions/functions-general-utils.php");
require_once("functions/functions-getters.php");
require_once("functions/functions-setters.php");
require_once("functions/functions-situation-handlers.php");
require_once("functions/functions-ticket-submenu.php");
require_once("functions/functions-widgets.php");
require_once("functions/functions-ticket.php");

$menu = str_replace("_", " ", $menu);
global $self, $db;
$self = $PHP_SELF;
if (empty($menu)) $menu = "Display";
$ticket = GetFirstRowFromTable("ticket where id='$id'");
$queue = str_replace("_", " ", $queue);
$output = $notice = "";
if ($email_user != 1) $email_user = "0";
if ($menu == "Resolve") $email_user = 1;
if ($form_submitted){

switch ($form_submitted){
   case "jumbo": // Edit nearly all information on a ticket.
   case "basics": // Edit only the basics of a ticket.
   if ($delete == "on") {
      if (DoesUserHaveRight($username, "DeleteTicket", $queue)) {
         $sent = SituationHandler("OnDelete", $id, $username, $email_user);
         DeleteTicket($id);
         AddComment($id, "", "", "", "Ticket Deleted");
         mysql_query("Delete from ticketwatcher where ticket_id = '$id'", $db);
         $notice = "Ticket Deleted";
         $provision = "";
      }
   }
   else{
      $first_comment_id = GetFirstCommentOfTicket($id);
      $subject = html_entity_decode($subject);
      UpdateComment($first_comment_id, "subject = '$subject'");
      $notice .= "Subject=$subject, Status=$status, Priority=$priority";
      $subject = "Ticket has been modified";
          
      if (($ticket->queue) != $queue) {
         $notice .= "\r\nExitQueue=".$ticket->queue;
         $notice .= "\r\nEnterQueue=$queue";
         $queuesave = $queue;
         $queue = $ticket->queue;
         $provision = "OnQueueExit";
         SituationHandler($provision, $id, $username, $email_user);
         $queue = $queuesave;
         $provision = "OnQueueEnter";
         $spfields = GetArrayFromTable("field_name", "q_specific_fields where q_name='$queue'");
         $size = sizeof($spfields);

         if ($spfields > 0) {
            $antispfields = GetArrayFromTable("field_name", "q_specific_ticket_value where ticket_id='$id'");
            $size2 = sizeof($antispfields);
            $k = 0;

            for ($i = 0; $i <= $size; $i++) {
               $alreadyset = 0;
               for ($j = 0; $j <= $size2; $j++) {
                  if (strcmp($antispfields[j], $spfields[i] != 0)) $alreadyset = 1;
               }

               if ($alreadyset == 0) {
                  $newspfields[$k] = $spfields[$i];
                  $k++;
               }
            }

            for ($i = 0; $i <= $k; $i++) {
               $query = "insert into q_specific_ticket_value (ticket_id,field_name,value) values ($id,'$newspfields[$i]','')";
               mysql_query($query, $db);
               $newspecialfields = 1;
            }
         }
      }
   }
   if($form_submitted!="jumbo") break;

   case "dates": // Edit the dates of a ticket.
   if ($notice != "Ticket Deleted"){
      UpdateTicketDates($id, $starts, $started, $last_contact, $due);
      $notice .= "<br>Dates have been updated- starts=$starts, started=$started, last contact=$last_contact, due=$due";
      $provision = "OnTransaction";
   }
   if($form_submitted!="jumbo") break;

   case "people": // Edit who is associated with a ticket.
   if ($notice != "Ticket Deleted"){
      for($i = 0; $i < sizeof($WatcherTypeEmail); $i++) {
         if ($WatcherTypeEmail[$i] != "none") {
            if ($WatcherAddressEmail[$i] != "") {
               $email = $WatcherAddressEmail[$i];
            }
            else {
               $email = GetStaffEmail($WatcherUsername[$i]);
            }
         }

         if ($email != "") {
            $query = "Insert into ticketwatcher set email='$email', ";
            $query .= "ticket_id='$id', type='$WatcherTypeEmail[$i]'";
            mysql_query($query, $db);
            $notice .= "<br>$email has been added as a $WatcherTypeEmail[$i].";
            $email = "";
         }
      }

      if (GetFirstRowFromTable("staff where username = '$Owner'")) {
         $old_owner = $ticket->owner;
         if ($old_owner != $Owner) {
            TicketSteal($id, $Owner, "suppress_result");
            $provision = "OnOwnerChange";
            $notice .= "<br>Owner has been changed from $old_owner to $Owner";
         }
      }

      if (isset($DelTicketWatchersQueryArray1)) {
         $k = 0;

         foreach($DelTicketWatchersQueryArray1 as $DelMe) {
            if ($DelMe != "") {
               $query1 = "Delete from ticketwatcher where ticket_id = '$id' and email = '$DelMe' and type='requester'";
               $query2 = "update ticket set requester='' where id = '$id' and requester = '$DelMe'";
               $k++;
               mysql_query($query1, $db);
               mysql_query($query2, $db);
            }
         }

         if ($k == 1) {
            $notice .= "<br>A Watcher has been removed from this ticket.";
         }

         if ($k > 1) {
            $notice .= "<br>$k Watchers have been removed from this ticket.";
         }
      }
      $k = 0;

      if (isset($DelTicketWatchersQueryArray2)) {
         foreach($DelTicketWatchersQueryArray2 as $DelMe) {

            if ($DelMe != "") {
               $query1 = "Delete from ticketwatcher where ticket_id = '$id' and email = '$DelMe' and type='cc'";
               $query2 = "update ticket set cc='' where id = '$id' cc='$DelMe'";
               $k++;
               mysql_query($query1, $db);
               mysql_query($query2, $db);
            }
         }

         if ($k == 1) {
            $notice .= "<br>A CC has been removed from this ticket.";
         }

         if ($k > 1) {
            $notice .= "<br>$k CCs have been removed from this ticket.";
         }
      }

      if (isset($DelTicketWatchersQueryArray3)) {
         $k = 0;

         foreach($DelTicketWatchersQueryArray3 as $DelMe) {

            if ($DelMe != "") {
               $query1 = "Delete from ticketwatcher where ticket_id = '$id' and email = '$DelMe' and type='admincc'";
               $query2 = "update ticket set admincc='' where id = '$id' and admincc='$DelMe'";
               $k++;
               mysql_query($query1, $db);
               mysql_query($query2, $db);
            }
         }
         if ($k == 1) {
            $notice .= "<br>An AdminCC has been removed from this ticket.";
         }
         if ($k > 1) {
            $notice .= "<br>$k AdminCCs have been removed from this ticket.";
         }
      }
   }
   if($form_submitted!="jumbo") break;
   break; //endjumbo

   case "comment": // Add a comment to the ticket.
      $last_comment = GetLastCommentRowOfTicket($id);
      $priority = $last_comment->priority;
       
      if ($status == "Resolved") $provision = "OnResolve";

      if (GetFirstRowFromTable("staff where username = '$owner'")) {
         $old_owner = $ticket->owner;
         if ($old_owner != $owner) {
            UpdateTicket($id, "owner = '$owner'");
            $provision = "OnOwnerChange";
            $notice .= "<br>Owner has been changed from $old_owner to $owner";
         }
      }
   break;

   case "Special_Fields": // Edit special fields.
      $sp_fields_changes = SetCustomizedFieldsValues($id);
      if ($req_err) {
         $form_submitted = "";
         $menu = "Special Fields";
      }
   break;    
   }
}

if ($notice != "Ticket Deleted"  && $form_submitted != ""){    
   if ($queue != "") UpdateTicket($id, "queue = '$queue'");
   if ($time_left != "") UpdateTicket($id, "time_left = '$time_left'");
   if ($final_priority != "") UpdateTicket($id, "final_priority = '$final_priority'");
   if ($provision == "") $provision = "OnTransaction";
   if ($status == "") $status = "Open";
       
   $body = str_replace("<br>", "\r\n", $body);
   $body = $body.$notice.$sp_fields_changes;
     
   $adjust = AdjustTimeWorked($id, $time_worked);

   if ($jumbo_subject) $subject = html_entity_decode($jumbo_subject);

   AddComment($id, $status, $priority, $username, $subject, $attach, $body, $adjust);
   UpdateTimeWorked($id);
   SituationHandler($provision, $id, $username, $email_user);
}
if ($notice == "Ticket Deleted") $notice .= "<br> The following people have been emailed: <br>$sent";


// Begin to generate output.
$output .= Head("Ticket #$id ".$title_comment, "$username");

// Print any changes or errors.
if (!(empty($notice))) {
   $output .= "<center>"
           .OpenColorTable("green", "Notifications", "50%")
           .Font(htmlentities(stripslashes($notice.$sp_fields_changes)))
           .CloseColorTable()
           ."</center>";

   if ($notice == "Ticket Deleted") {
      $output .= Foot();
      print $output;
      exit;
   }
}

if (!(empty($sp_fields_changes))) {
   $output .= "<center>"
           .OpenColorTable("green", "Notifications", "50%")
           .Font($sp_fields_changes)
           .CloseColorTable()
           ."</center>";
}
    
// Check for an empty ticket ID.    
if (empty($id)) {
   $err_msg = "Please enter a <i>ticket number</i>. ";
   $err_msg .= "<a class=\"main\" href=\"index.php\">";
   $err_msg .= "Click here</a> ";
   $err_msg .= "to continue.  ";
   $output .= ErrorReport($err_msg, "50%");
   $output .=Foot();
   print $output;
   exit;
} 

// We have a ticket ID, get the ticket information and then display the ticket.
else {
   $query = "select * from ticket where id='$id'";
   $result = mysql_query($query, $db);
   if ($result){
      if (mysql_num_rows($result) <= 0) {
         $err_msg = "The ticket your are trying to lookup ";
         $err_msg .= "(#$id), does not seem to exist.  ";
         $err_msg .= "<a class=\"main\" href=\"index.php\">";
         $err_msg .= "Click here</a> ";
         $err_msg .= "to continue.  ";
         $output .= ErrorReport($err_msg, "50%");
         $output .= Foot();
         print $output;
         exit;
      }
   }
}

$main_menu = array("Display", "History", "Basics", "Special Fields", "Dates",
                   "People", "Jumbo", "User", "Comment", "Open", "Resolve", 
                   "Resolve-No User Email", "Resolve With Comment", "Steal",
                   "Recent Search");
$q_sp = GetCountFromTable("queue, q_specific_fields where queue.q_name='$ticket->queue' and queue.q_name=q_specific_fields.q_name");
$q_row = GetFirstRowFromTable("queue where q_name='$ticket->queue'");

if ($q_row->require_resolve && $q_sp > 0) {
   $main_menu = array("Display", "History", "Basics", "Special Fields", "Dates",
                     "People", "Jumbo", "User", "Comment" ,"Open", 
                     "Resolve With Comment", "Steal", "Recent Search");
}
    
if ($q_row->require_resolve && $q_sp <= 0) {
   $main_menu = array("Display", "History", "Basics", "Dates",
                      "People", "Jumbo", "User", "Comment",  "Open", 
                      "Resolve With Comment", "Steal", "Recent Search");
}
$output .= OldTicketMenu($main_menu, $sub_menu, $id, $menu);
if ($newspecialfields == 1) $output .= TicketSpecialFields($id);
    
if (!empty($new_ticket)) {
      $msg = "Ticket <b>$id</b> created in queue <b>$queue</b>";
      $output .= '<p><table border="0" width="100%"><tr valign="top"><td width="100%">'
              .OpenColorTable("red", "Results", "100%")
              .Font($msg).CloseColorTable()."</td></tr></table><p>";
}

// What are we displaying?    
if ($menu == "Display") $output .= OldTicketDisplay($id);
elseif ($menu == "History") $output .= TicketHistory($id);
elseif ($menu == "Basics") $output .= TicketBasics($id);
elseif ($menu == "Dates") $output .= TicketDates($id);
elseif ($menu == "People") $output .= TicketPeople($id);
elseif ($menu == "Special Fields") $output .= TicketSpecialFields($id);
elseif ($menu == "Jumbo") $output .= TicketJumbo($id);
elseif ($menu == "User") $output .= ShowTicketUser($id);
elseif ($menu == "Comment") $output .= TicketComment($id, $username);
elseif ($menu == "Reply") $output .= TicketReply($id, $username);
elseif ($menu == "Open") $output .= TicketOpen($id, $username);
elseif ($menu == "Resolve") $output .= TicketResolve($id, $username);
elseif ($menu == "Steal") {
   $steal = TicketSteal($id, $username);
   if ($steal) $output .= GoodReport("Owner has been changed to <b>$username</b>");
}
elseif ($menu == "Resolve-No User Email") $output .= TicketResolve($id, $username);

elseif ($menu == "Resolve With Comment") {
   $force_resolve = "1";
   $output .= TicketComment($id, $username);
}

if (($menu == "Open") || ($menu == "Resolve") || ($menu == "Resolve-No User Email") || ($menu == "Steal")) {
   if ($menu == "Open") {
      $provision = "OnStatus";
      $notice .= "This ticket's status has been set to <b>open</b>.<br>";
   }

   if ($menu == "Resolve") {
      $provision = "OnResolve";
      $notice .= "This ticket's status has been set to <b>resolved</b>. An email has been sent to the user.<br>";
   }

   if ($menu == "Resolve-No User Email") {
      $provision = "OnResolve";
      $email_user = 0;
      $notice .= "This ticket's status has been set to <b>resolved</b>. No email has been sent to the user.<br>";
   }
       
   if ($menu == "Steal")  $provision = "OnOwnerChange";
   SituationHandler($provision, $id, $username, $email_user);
}
    
if ($menu == "Recent Search") {
   $move = "search.php?previous_search=1";
   $output .= "<meta http-equiv=\"Refresh\" content=\"0;URL=$move\">";
}
    
if (($menu == "Display") || ($menu == "History")) {
   $output .= OldTicketMenu($main_menu, $sub_menu, $id, $menu);
}

if ($menu == "Resolve-No User Email" || $menu == "Resolve" || $menu == "Open" || $menu == "Steal")  UpdateLastCommentId($id);

if ($form_submitted != NULL) {
   UpdateFirstCommentId($id);
   UpdateLastCommentId($id);
}
UpdateTicketStatus($id);
$output .= Foot();
print $output;

?>
