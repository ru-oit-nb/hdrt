--
-- Filename:               ruqueue_mysql_5_upgrade.sql
-- Description:            SQL script to fix a MYSQL 5.0 reserved word issue
--                         and/or help users upgrade to ruQueue 1.2.4
-- Supprted Language(s):   MYSQL 5.0
--

--
-- Alters the comment table to store the IP Address of the person making 
-- the comment
--
ALTER TABLE comment ADD ip_address VARCHAR(15) DEFAULT NULL AFTER attach;

--
-- Alters the variable name condition (now a reserved word) to one named
-- provision.
--
ALTER TABLE script CHANGE `condition` provision VARCHAR(80);

--
-- Alters the staffprefs table to let each user to see html or not
--
ALTER TABLE staffprefs ADD show_html_in_comments tinyint(1) default '0';

--
-- Adds new system variables
-- 
INSERT INTO system_variables VALUES ('comment_email_address', 'comment@ruQueue.domain.tld', 20);
INSERT INTO system_variables VALUES ('help_email_address', 'helpdesk@domain.tld', 25);
INSERT INTO system_variables VALUES ('staff_email_domain', '@domain.tld', '30');
INSERT INTO system_variables VALUES ('survey_url', 'http://helpdesksurvey.domain.tld', '35'); 
INSERT INTO system_variables VALUES ('email_information_url', 'http://hotwired.lycos.com/webmonkey/98/08/index3a.html?tw=authoring', '40');
INSERT INTO system_variables VALUES ('notify_perl_problem', '0', NULL);
INSERT INTO system_variables VALUES ('show_html_in_comments', '0', '45');
