-- Copyright (c) 2005, Rutgers, The State University of New Jersey
--    This file is part of ruQueue.
--
--    ruQueue is free software; you can redistribute it and/or modify
--    it under the terms of the GNU General Public License as published by
--    the Free Software Foundation; either version 2 of the License, or
--    (at your option) any later version.
--
--    ruQueue is distributed in the hope that it will be useful,
--    but WITHOUT ANY WARRANTY; without even the implied warranty of
--    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--    GNU General Public License for more details.
--
--    You should have received a copy of the GNU General Public License
--    along with ruQueue; if not, write to the Free Software
--    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  
--    02110-1301  USA

-- CREATE DATABASE /*!32312 IF NOT EXISTS*/ ruqueue;

USE ruqueue;

--
-- Table structure for table `appointments`
--

CREATE TABLE appointments (
  appointment_id int(10) unsigned NOT NULL auto_increment,
  building_number int(10) unsigned NOT NULL default '0',
  room_number varchar(10) default NULL,
  last_updated timestamp(14) NOT NULL,
  datetime datetime default NULL,
  consultant_netid varchar(20) default NULL,
  ticket_id int(11) NOT NULL default '0',
  status enum('Not Yet Scheduled','Scheduled','Missed - User','Missed - Consultant','Cancelled','Completed') default NULL,
  PRIMARY KEY  (appointment_id),
  KEY consultant_netid (consultant_netid),
  KEY status (status)
) TYPE=MyISAM;

--
-- Table structure for table `appointments_cache`
--

CREATE TABLE appointments_cache (
  campus enum('Busch','Camden','College Ave','Cook','Douglass','Livingston','Newark','Off Campus') NOT NULL default 'Busch',
  building_group varchar(80) NOT NULL default '',
  building_name varchar(80) NOT NULL default '',
  datetime datetime NOT NULL default '0000-00-00 00:00:00',
  num tinyint(3) unsigned NOT NULL default '0',
  updated timestamp(14) NOT NULL,
  PRIMARY KEY  (campus,building_group,building_name,datetime)
) TYPE=MyISAM;

--
-- Table structure for table `buildings`
--

CREATE TABLE buildings (
  building_number int(10) unsigned NOT NULL auto_increment,
  building_name varchar(80) default NULL,
  campus enum('Busch','Camden','College Ave','Cook','Douglass','Livingston','Newark','Off Campus') default NULL,
  building_group varchar(80) default NULL,
  PRIMARY KEY  (building_number),
  UNIQUE KEY building_name (building_name)
) TYPE=MyISAM;

--
-- Table structure for table `comment`
--

CREATE TABLE comment (
  id int(11) NOT NULL auto_increment,
  ticket_id int(11) NOT NULL default '0',
  date_created timestamp(14) NOT NULL,
  status varchar(15) default NULL,
  priority int(2) default NULL,
  staff varchar(15) default NULL,
  time_worked decimal(10,0) default '0',
  subject varchar(80) default NULL,
  attach varchar(80) default NULL,
  ip_address varchar(15) default NULL,
  body text,
  PRIMARY KEY  (id),
  KEY ticket_id (ticket_id),
  KEY status (status),
  KEY ticket_status (ticket_id,status)
) TYPE=MyISAM;

--
-- Dumping data for table `comment`
--

INSERT INTO comment VALUES (6,1,now(),'New',0,'root','0','Test Ticket!','','This is a comment!',NULL);

--
-- Table structure for table `groupright`
--

CREATE TABLE groupright (
  group_name varchar(20) NOT NULL default '',
  group_right varchar(15) NOT NULL default '',
  queue varchar(80) NOT NULL default '',
  PRIMARY KEY  (group_name,group_right,queue)
) TYPE=MyISAM;

--
-- Dumping data for table `groupright`
--

INSERT INTO groupright VALUES ('Administrators','AllRights','global');

--
-- Table structure for table `groups`
--

CREATE TABLE groups (
  group_name varchar(20) NOT NULL default '',
  type varchar(10) default NULL,
  description varchar(80) default NULL,
  PRIMARY KEY  (group_name)
) TYPE=MyISAM;

--
-- Dumping data for table `groups`
--

INSERT INTO groups VALUES ('Administrators',NULL,'Administrators of the ruQueue installation.');
INSERT INTO groups VALUES ('Cc','pseudo','');
INSERT INTO groups VALUES ('Everyone','pseudo','');
INSERT INTO groups VALUES ('Owner','pseudo','');
INSERT INTO groups VALUES ('Requestor','pseudo','');
INSERT INTO groups VALUES ('AdminCc','pseudo','');

--
-- Table structure for table `ingroup`
--

CREATE TABLE ingroup (
  group_name varchar(20) NOT NULL default '',
  username varchar(15) NOT NULL default '',
  PRIMARY KEY  (group_name,username)
) TYPE=MyISAM;

--
-- Dumping data for table `ingroup`
--

INSERT INTO ingroup VALUES ('Administrators','root');

--
-- Table structure for table `modules`
--

CREATE TABLE modules (
  module_name varchar(80) NOT NULL default '',
  use_module int(1) NOT NULL default '0',
  PRIMARY KEY  (module_name)
) TYPE=MyISAM;

--
-- Dumping data for table `modules`
--

INSERT INTO modules VALUES ('Scheduling', 0);

--
-- Table structure for table `overview_cache`
--

CREATE TABLE overview_cache (
  queue varchar(80) NOT NULL default '',
  new int(8) default '0',
  open int(8) default '0',
  stalled int(8) default '0',
  resolved int(8) default '0',
  overdue int(8) default '0',
  d_day date default NULL,
  updated timestamp(14) NOT NULL,
  PRIMARY KEY  (queue)
) TYPE=MyISAM;

--
-- Table structure for table `potential_schedule`
--

CREATE TABLE potential_schedule (
  netid varchar(20) NOT NULL default '',
  day enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') default NULL,
  hours set('12:00','12:30','01:00','01:30','02:00','02:30','03:00','03:30','04:00','04:30','05:00','05:30','06:00','06:30','07:00','07:30','08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30') default NULL,
  KEY netid (netid),
  KEY day (day),
  KEY hours (hours),
  KEY sched_time (day,hours)
) TYPE=MyISAM;

--
-- Table structure for table `potential_schedule_cache`
--

CREATE TABLE potential_schedule_cache (
  group_type varchar(20) NOT NULL default '',
  group_value varchar(80) NOT NULL default '',
  day enum('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday') NOT NULL default 'Sunday',
  hour time NOT NULL default '00:00:00',
  num tinyint(3) unsigned NOT NULL default '0',
  updated timestamp(14) NOT NULL,
  PRIMARY KEY  (group_type,group_value,day,hour)
) TYPE=MyISAM;

--
-- Table structure for table `q_specific_fields`
--

CREATE TABLE q_specific_fields (
  field_name varchar(80) NOT NULL default '',
  field_display_name varchar(80) default NULL,
  field_type varchar(20) default 'text',
  q_name varchar(80) NOT NULL default '',
  date_create timestamp(14) NOT NULL,
  required int(1) default '0',
  PRIMARY KEY  (field_name,q_name)
) TYPE=MyISAM;

--
-- Dumping data for table `q_specific_fields`
--

INSERT INTO q_specific_fields VALUES ('special_field','Special Field','text','Test Queue',now(),0);

--
-- Table structure for table `q_specific_menu`
--

CREATE TABLE q_specific_menu (
  field_name varchar(80) NOT NULL default '',
  q_name varchar(80) NOT NULL default '',
  item_value varchar(80) NOT NULL default '',
  PRIMARY KEY  (field_name,q_name,item_value)
) TYPE=MyISAM;

--
-- Table structure for table `q_specific_ticket_value`
--

CREATE TABLE q_specific_ticket_value (
  ticket_id int(11) NOT NULL default '0',
  field_name varchar(80) NOT NULL default '',
  value varchar(80) default NULL,
  PRIMARY KEY  (field_name,ticket_id)
) TYPE=MyISAM;

--
-- Dumping data for table `q_specific_ticket_value`
--

INSERT INTO q_specific_ticket_value VALUES (1,'special_field','This is a special field.');

--
-- Table structure for table `q_supervisor`
--

CREATE TABLE q_supervisor (
  q_name varchar(80) NOT NULL default '',
  username varchar(15) NOT NULL default '',
  PRIMARY KEY  (q_name,username)
) TYPE=MyISAM;

--
-- Table structure for table `queue`
--

CREATE TABLE queue (
  q_name varchar(80) NOT NULL default '',
  description varchar(80) default NULL,
  correspondence_address varchar(80) default NULL,
  comment_address varchar(80) default NULL,
  priority_starts_at varchar(20) default NULL,
  allowabledays int(3) default '0',
  overtime_moves_to varchar(80) default NULL,
  enabled int(1) default NULL,
  greeting text,
  replyto varchar(80) default NULL,
  greeting_subject varchar(80) default NULL,
  resolution text,
  res_replyto varchar(80) default NULL,
  res_subject varchar(80) default NULL,
  color varchar(8) default 'ffffff',
  require_resolve int(1) default '0',
  q_group varchar(80) default '0 - Main',
  mail_comments int(1) NOT NULL default '0',
  allow_scheduling enum('Require','Allow','None') default 'None',
  PRIMARY KEY  (q_name),
  KEY enabled (enabled)
) TYPE=MyISAM;

--
-- Dumping data for table `queue`
--

INSERT INTO queue VALUES ('Test Queue','This is the default test queue for ruQueue.','test@localhost','test@localhost','',0,'',1,'Greeting','test@localhost','Greeting Subject','Ticket resolved.','test@localhost','Ticket resolved.','1e518a',1,'Queue groups are deprecated.',0,'Allow');

--
-- Table structure for table `sched_comments`
--

CREATE TABLE sched_comments (
  appointment_id int(10) unsigned default NULL,
  comment_id int(11) NOT NULL default '0',
  PRIMARY KEY  (comment_id)
) TYPE=MyISAM;

--
-- Table structure for table `sched_mailer`
--

CREATE TABLE sched_mailer (
  appointment_id int(10) unsigned NOT NULL default '0',
  last_status enum('Not Yet Scheduled','Scheduled','Missed - User','Missed - Consultant','Cancelled','Completed') default NULL,
  last_consultant varchar(20) default NULL,
  reminder_sent tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (appointment_id)
) TYPE=MyISAM;

--
-- Table structure for table `sched_options`
--

CREATE TABLE sched_options (
  option_name varchar(80) default NULL,
  option_value text NOT NULL,
  display_order int(10) unsigned NOT NULL auto_increment,
  PRIMARY KEY  (display_order),
  UNIQUE KEY option_name (option_name)
) TYPE=MyISAM;

--
-- Dumping data for table `sched_options`
--

INSERT INTO sched_options VALUES ('First Appointment Start Time','0',1);
INSERT INTO sched_options VALUES ('Last Appointment Start Time','43',2);
INSERT INTO sched_options VALUES ('Latest Available Time','23',4);
INSERT INTO sched_options VALUES ('Earliest Available Time','8',3);
INSERT INTO sched_options VALUES ('Minimum Hours To Schedule Before Appointment','24',5);
INSERT INTO sched_options VALUES ('Consultant Initial Email','[first]:\r\n\r\nYou are scheduled for a service appointment with [user] in [building] [room] on [datetime]. If you cannot make this appointment you should contact your Area Coordinator or the ResNet Office immediately.',50);
INSERT INTO sched_options VALUES ('Consultant Reminder Email','[first]:\r\n\r\nThis is just a reminder that you are scheduled for a service appointment with [user] in [building] [room] on [datetime]. If you cannot make this appointment you should contact your Area Coordinator or the ResNet Office immediately.\r\n\r\n[user]\r\n[building] \r\n[room] \r\n[datetime] ',51);
INSERT INTO sched_options VALUES ('User Initial Email','Dear [user]:\r\n\r\nYou are scheduled for a ResNet service appointment with [first] in your dorm ([building] [room]) on [datetime]. If you need to postpone this appointment:\r\n\r\nIn New Brunswick/Piscataway, please call the Help Desk at 732-445-HELP(4357) as soon as possible. Your ticket number is [ticket]. \r\n\r\nIn Newark, please call the Help Desk at 973-353-5083 as soon as possible. Your ticket number is [ticket].\r\n\r\nThank you for your cooperation.',100);
INSERT INTO sched_options VALUES ('User Reminder Email','Dear [user]:\r\n\r\nThis is just a reminder that you are scheduled for a ResNet service appointment with [first] in your dorm ([building] [room]) on [datetime]. If you need to postpone this appointment:\r\n\r\nIn New Brunswick/Piscataway, please call the Help Desk at 732-445-HELP(4357) as soon as possible. Your ticket number is [ticket]. \r\n\r\nIn Newark, please call the Help Desk at 973-353-5083 as soon as possible. Your ticket number is [ticket].\r\n\r\nThank you for your cooperation.',101);
INSERT INTO sched_options VALUES ('Consultant Cancelled Email','[first]:\r\n\r\nYour appointment on [datetime] has been cancelled.',54);
INSERT INTO sched_options VALUES ('User Cancelled Email','Dear [user]:\r\n\r\nYour appointment on [datetime] has been cancelled.  If you still require service, please call to schedule another appointment:\r\n\r\nIn New Brunswick/Piscataway, please call 732-445-HELP(4357).\r\n\r\nIn Newark, please call 973-353-5083.',104);
INSERT INTO sched_options VALUES ('User Missed(Consultant) Email','Dear [user]:\r\n\r\nWe apologize for the inconvenience - it has come to our attention that the ResNet consultant scheduled to visit you failed to arrive.  If you have not already done so, please call to schedule another appointment:\r\n\r\nIn New Brunswick/Piscataway, please call 732-445-HELP(4357). Your ticket number is [ticket]. \r\n\r\nIn Newark, please call 973-353-5083. Your ticket number is [ticket].\r\n',102);
INSERT INTO sched_options VALUES ('User Missed(User) Email','Dear [user]:\r\n\r\nThe consultant scheduled to visit you on [datetime] reports that you missed this appointment.  As such, your ticket has been resolved pending further action from you.  If you still require service, please call to schedule another appointment:\r\n\r\nIn New Brunswick/Piscataway, please call 732-445-HELP(4357).\r\n\r\nIn Newark, please call 973-353-5083.',103);
INSERT INTO sched_options VALUES ('Consultant Missed(Consultant) Email','[first]:\r\n\r\nYou have missed a scheduled appointment in [building] [room] on [datetime].  Please provide an explanation for this occurance.',52);
INSERT INTO sched_options VALUES ('Consultant Missed(User) Email','[first]:\r\n\r\nThis is to confirm that the end-user missed the appointment scheduled on [datetime].  Please contact your Area Coordinator about this issue.',53);
INSERT INTO sched_options VALUES ('Send Reminder Emails (hours)','12',6);
INSERT INTO sched_options VALUES ('User Completed Email','Dear [user]:\r\n\r\nYour appointment on [datetime] has been successfully completed.  If you have any further problems:\r\n\r\nIn New Brunswick/Piscataway, please call 732-445-HELP(4357).\r\n\r\nIn Newark, please call 973-353-5083.',105);
INSERT INTO sched_options VALUES ('Consultant Completed Email','[first]:\r\n\r\nYour appointment on [datetime] has been completed.',55);

--
-- Table structure for table `sched_rules`
--

CREATE TABLE sched_rules (
  rule_id int(10) unsigned NOT NULL auto_increment,
  privgroup varchar(80) default '',
  schedgroup varchar(80) NOT NULL default '',
  rule varchar(80) NOT NULL default 'Allow',
  PRIMARY KEY  (rule_id)
) TYPE=MyISAM;

--
-- Table structure for table `script`
--

CREATE TABLE script (
  id int(10) NOT NULL auto_increment,
  queue varchar(80) default NULL,
  provision varchar(80) default NULL,
  action varchar(200) default NULL,
  template varchar(200) default NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `search`
--

CREATE TABLE search (
  username varchar(20) NOT NULL default '',
  url text,
  PRIMARY KEY  (username)
) TYPE=MyISAM;

--
-- Table structure for table `session`
--

CREATE TABLE session (
  session_start timestamp(14) NOT NULL,
  session_code varchar(32) NOT NULL default '0',
  username varchar(15) NOT NULL default '',
  PRIMARY KEY  (username)
) TYPE=MyISAM;

--
-- Table structure for table `staff`
--

CREATE TABLE staff (
  username varchar(15) NOT NULL default '',
  firstname varchar(40) default NULL,
  lastname varchar(40) default NULL,
  email varchar(80) default NULL,
  last_login timestamp(14) NOT NULL,
  nickname varchar(80) default NULL,
  extra_info text,
  access int(1) default NULL,
  rights int(1) default NULL,
  organization varchar(80) default NULL,
  address1 varchar(255) default NULL,
  address2 varchar(255) default NULL,
  city varchar(80) default NULL,
  state char(2) default NULL,
  zip varchar(20) default NULL,
  country varchar(80) default NULL,
  home_phone varchar(25) default NULL,
  work_phone varchar(25) default NULL,
  mobile_phone varchar(25) default NULL,
  pager varchar(25) default NULL,
  comments text,
  html_email int(1) default '0',
  password varchar(64) default NULL,
  PRIMARY KEY  (username)
) TYPE=MyISAM;

--
-- Dumping data for table `staff`
--

INSERT INTO staff VALUES ('root','Root','ruQueue','test@localhost',now(),'','',0,0,'','','','','','','','','','','','',0,password('Rutgers1766'));

--
-- Table structure for table `staffprefs`
--

CREATE TABLE staffprefs (
  username varchar(15) NOT NULL default '',
  signature text,
  display_overview tinyint(1) default '1',
  refresh smallint(6) default NULL,
  show_html_in_comments tinyint(1) default '0',
  PRIMARY KEY  (username)
) TYPE=MyISAM;

--
-- Dumping data for table `staffprefs`
--

INSERT INTO staffprefs VALUES ('root','',1,600,0);

--
-- Table structure for table `staffright`
--

CREATE TABLE staffright (
  username varchar(15) NOT NULL default '',
  staff_right varchar(15) NOT NULL default '',
  queue varchar(80) NOT NULL default '',
  PRIMARY KEY  (username,staff_right,queue)
) TYPE=MyISAM;

--
-- Table structure for table `system_variables`
--

CREATE TABLE system_variables (
  var varchar(80) NOT NULL default '',
  val varchar(80) default NULL,
  display_order tinyint(4) default NULL,
  PRIMARY KEY  (var),
  UNIQUE KEY display_order (display_order)
) TYPE=MyISAM;

--
-- Dumping data for table `system_variables`
--

INSERT INTO system_variables VALUES ('overview_cache_date','2005-04-26 16:55:27',NULL);
INSERT INTO system_variables VALUES ('session_expire','1800',0);
INSERT INTO system_variables VALUES ('comment_email_address', 'comment@ruQueue.domain.tld', 20);
INSERT INTO system_variables VALUES ('help_email_address', 'helpdesk@nbcs.domain.tld', 25);
INSERT INTO system_variables VALUES ('authentication_type','local',10);
INSERT INTO system_variables VALUES ('user_lookup_type','local',15);
INSERT INTO system_variables VALUES ('session_salt','Hdrt15C001',NULL);
INSERT INTO system_variables VALUES ('staff_email_domain', '@domain.tld', '30');
INSERT INTO system_variables VALUES ('survey_url', 'http://helpdesksurvey.domain.tld', '35'); 
INSERT INTO system_variables VALUES ('email_information_url', 'http://hotwired.lycos.com/webmonkey/98/08/index3a.html?tw=authoring', '40');
INSERT INTO system_variables VALUES ('notify_perl_problem', '0', NULL); 

--
-- Table structure for table `ticket`
--

CREATE TABLE ticket (
  id int(11) NOT NULL auto_increment,
  date_created timestamp(14) NOT NULL,
  queue varchar(80) default NULL,
  requester varchar(80) default NULL,
  owner varchar(80) default NULL,
  user_id int(10) default NULL,
  cc varchar(80) default NULL,
  admin_cc varchar(80) default NULL,
  final_priority varchar(5) default NULL,
  time_worked decimal(10,0) default '0',
  time_left decimal(10,0) default NULL,
  starts date default NULL,
  due date default NULL,
  depends_on varchar(11) default NULL,
  depended_on_by varchar(11) default NULL,
  parents varchar(11) default NULL,
  children varchar(11) default NULL,
  refers_to varchar(11) default NULL,
  refered_to_by varchar(11) default NULL,
  started date default NULL,
  last_contact date default NULL,
  first_comment_id int(11) default NULL,
  last_comment_id int(11) default NULL,
  status_change_date timestamp(14) NOT NULL default '00000000000000',
  current_status varchar(15) default NULL,
  old_user_id int(10) default NULL,
  emailed_resolution int(1) default '0',
  PRIMARY KEY  (id),
  KEY owner (owner),
  KEY cc (cc),
  KEY admin_cc (admin_cc),
  KEY queue (queue),
  KEY requester (requester),
  KEY current_status (current_status),
  KEY date_created (date_created),
  KEY user_id (user_id),
  KEY old_user_id (old_user_id),
  KEY queue_status (queue,current_status)
) TYPE=MyISAM;

--
-- Dumping data for table `ticket`
--

INSERT INTO ticket VALUES (1,now(),'Test Queue','test@localhost','',1,'','','','0','0','0000-00-00','0000-00-00','','','','','','',NULL,NULL,6,6,now(),'New',NULL,0);

--
-- Table structure for table `ticketwatcher`
--

CREATE TABLE ticketwatcher (
  ticket_id int(11) NOT NULL default '0',
  type varchar(20) NOT NULL default '',
  email varchar(80) NOT NULL default '',
  PRIMARY KEY  (ticket_id,type,email)
) TYPE=MyISAM;

--
-- Dumping data for table `ticketwatcher`
--

INSERT INTO ticketwatcher VALUES (1,'Requester','test@localhost');
INSERT INTO ticketwatcher VALUES (2,'Requester','test@localhost');

--
-- Table structure for table `user`
--

CREATE TABLE user (
  id int(10) NOT NULL auto_increment,
  name varchar(80) default NULL,
  iid varchar(20) default NULL,
  email varchar(80) default NULL,
  uid varchar(20) default NULL,
  type varchar(40) default NULL,
  phone varchar(255) default NULL,
  fax varchar(18) default NULL,
  location varchar(255) default NULL,
  address text,
  rcpid varchar(20) default NULL,
  ruservice varchar(80) default NULL,
  ruenable varchar(200) default NULL,
  rudisable varchar(80) default NULL,
  duplicate_of int(10) default NULL,
  PRIMARY KEY  (id),
  KEY name (name),
  KEY uid (uid)
) TYPE=MyISAM;

--
-- Dumping data for table `user`
--

INSERT INTO user VALUES (1,'Test User','','test@localhost','test','Test user','(123) 555-1234','','',' ',NULL,'','','',NULL);

--
-- Table structure for table `user_buildings`
--

CREATE TABLE user_buildings (
  netid varchar(20) NOT NULL default '',
  group_type varchar(30) NOT NULL default '',
  group_value varchar(80) NOT NULL default '',
  KEY netid (netid),
  KEY group_type (group_type),
  KEY group_value (group_value),
  KEY user_group (netid,group_value)
) TYPE=MyISAM;

--
-- Table structure for table `watcher`
--

CREATE TABLE watcher (
  q_name varchar(80) NOT NULL default '',
  username varchar(15) NOT NULL default '',
  PRIMARY KEY  (q_name,username)
) TYPE=MyISAM;


